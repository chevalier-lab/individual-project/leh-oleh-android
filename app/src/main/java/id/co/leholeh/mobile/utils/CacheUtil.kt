package id.co.leholeh.mobile.utils


import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.gson.Gson

class CacheUtil {
    private var sharePref: SharedPreferences? = null

    fun start(activity: Activity, PREFS: String) {
        sharePref = activity.getSharedPreferences(PREFS, Context.MODE_PRIVATE)
    }

    fun destroy() { this.sharePref = null }

    fun <T> set(PREFS: String, value: T) {
        this.sharePref?.let {
            with(it.edit()) {
                putString(PREFS, Gson().toJson(value))
                Log.d("CACHE UTIL", PREFS)
                apply()
            }
        }
    }

    fun clear() {
        this.sharePref?.edit()?.clear()?.apply()
    }

    fun get(PREFS: String): String? {
        if (sharePref != null) return sharePref!!.getString(PREFS, null)
        return null
    }
}