package id.co.leholeh.mobile.customer.constant

object ConstantBanks {
    const val BASE_URL_GET_BANKS_NAME = "master/banks"
    const val BASE_URL_BANKS_ACCOUNT = "guest/banks"
    const val BASE_URL_UPDATE_BANKS = "guest/banks/{id}"
    const val BASE_URL_DELETE_BANKS = "guest/banks/{id}"
    const val BASE_URL_DETAIL_BANKS = "guest/banks/{id}"
}