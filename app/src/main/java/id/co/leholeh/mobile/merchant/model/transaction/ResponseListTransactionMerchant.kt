package id.co.leholeh.mobile.merchant.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseListTransactionMerchant(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<ListTransaction>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class ListTransaction(

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("id_u_user_location")
	val idUUserLocation: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("area_id")
	val areaId: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("suburb_id")
	val suburbId: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("province")
	val province: String? = null,

	@field:SerializedName("province_id")
	val provinceId: String? = null,

	@field:SerializedName("suburb")
	val suburb: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("city_id")
	val cityId: String? = null
): Serializable
