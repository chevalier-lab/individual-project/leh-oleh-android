package id.co.leholeh.mobile.customer.ui.merchantcustomer

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.model.merchant.ProductsByMerchant
import id.co.leholeh.mobile.customer.model.merchant.ResponseProductByMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MerchantFilterPreviewDataSource(
    private val idMerchant : Int,
    private val search : String,
    private val orderDirection : String,
    private val jsonObject: JsonObject
):PageKeyedDataSource<Int, ProductsByMerchant>() {
    private val apiService = ApiService().serviceGuestMerchants
    val apiState = MutableLiveData<ApiStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ProductsByMerchant>
    ) {
        apiState.postValue(ApiStatus.LOADING)
        val response =
            apiService.getFilterProductMerchant(idMerchant, FIRST_PAGE,search,orderDirection,jsonObject)
            response.enqueue(object:Callback<ResponseProductByMerchant>{
                override fun onResponse(
                    call: Call<ResponseProductByMerchant>,
                    response: Response<ResponseProductByMerchant>
                ) {
                    when {
                        response.isSuccessful -> {
                            val apiResponse = response.body()
                            val responseItem = apiResponse!!.data
                            apiState.postValue(ApiStatus.SUCCESS)
                            responseItem?.let {
                                if (responseItem.isEmpty()) {
                                    apiState.postValue(ApiStatus.EMPTY)
                                } else {
                                    apiState.postValue(ApiStatus.LOADED)
                                }
                                callback.onResult(responseItem, null, FIRST_PAGE + 1)
                            }
                        }
                        else -> {
                            apiState.postValue(ApiStatus.SUCCESS)
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseProductByMerchant>, t: Throwable) {
                    Log.d("failed_initial_filter", "failed_initial_filter")
                    apiState.postValue(ApiStatus.FAILED)
                    Log.d(TAG, t.localizedMessage!!)
                }

            })
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ProductsByMerchant>
    ) {
        apiState.postValue(ApiStatus.LOADING)
        val response =
            apiService.getFilterProductMerchant(idMerchant, params.key,search,orderDirection,jsonObject)
        response.enqueue(object:Callback<ResponseProductByMerchant>{
            override fun onResponse(
                call: Call<ResponseProductByMerchant>,
                response: Response<ResponseProductByMerchant>
            ) {
                val key = if (params.key > 0) params.key - 1 else -1
                when {
                    response.isSuccessful -> {
                        val apiResponse = response.body()
                        val responseItem = apiResponse!!.data
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()) {
                                apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            } else {
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                    else -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseProductByMerchant>, t: Throwable) {
                Log.d("failed_before_filter", "failed_before_filter")
                apiState.postValue(ApiStatus.FAILED)
                Log.d(TAG, t.localizedMessage!!)
            }

        })
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ProductsByMerchant>
    ) {
        apiState.postValue(ApiStatus.LOADING)
        val response =
            apiService.getFilterProductMerchant(idMerchant, params.key,search,orderDirection,jsonObject)
        response.enqueue(object:Callback<ResponseProductByMerchant>{
            override fun onResponse(
                call: Call<ResponseProductByMerchant>,
                response: Response<ResponseProductByMerchant>
            ) {
                when {
                    response.isSuccessful -> {
                        val responseItem = response.body()!!.data
                        val key = params.key + 1
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()) {
                                apiState.postValue(ApiStatus.EMPTY_AFTER)
                            } else {
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                    else -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseProductByMerchant>, t: Throwable) {
                Log.d("failed_after_filter", "failed_after_filter")
                apiState.postValue(ApiStatus.FAILED)
                Log.d(TAG, t.localizedMessage!!)
            }

        })
    }

    companion object {
        const val FIRST_PAGE = 0
        const val TAG = "ERR_REQ_FILTER_PRODUCT"
    }
}