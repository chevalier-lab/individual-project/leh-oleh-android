package id.co.leholeh.mobile.customer.network.shipper

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.shipper.ConstantPickup
import id.co.leholeh.mobile.customer.model.shipper.pickuprequest.ResponseCancelPickup
import id.co.leholeh.mobile.customer.model.shipper.pickuprequest.ResponseGetAgent
import id.co.leholeh.mobile.customer.model.shipper.pickuprequest.ResponsePickupRequest
import retrofit2.Call
import retrofit2.http.*

interface PickupRequestInterface {

    @POST(ConstantPickup.BASE_URL_PICKUP_REQUEST)
    @Headers(
        "User-Agent: Shipper/",
        "Content-Type: application/x-www-form-urlencoded"
    )
    fun pickupRequest(
        @Query("apiKey") apiKey: String,
        @Body jsonObject: JsonObject
    ): Call<ResponsePickupRequest>

    @PUT(ConstantPickup.BASE_URL_PICKUP_CANCEL)
    @Headers(
        "User-Agent: Shipper/",
        "Content-Type: application/x-www-form-urlencoded"
    )
    fun cancelPickup(
        @Query("apiKey") apiKey: String,
        @Body jsonObject: JsonObject
    ): Call<ResponseCancelPickup>

    @GET(ConstantPickup.BASE_URL_PICKUP_GET_AGENT)
    @Headers(
        "User-Agent: Shipper/",
        "Content-Type: application/x-www-form-urlencoded"
    )
    suspend fun getAgentSuburbs(
        @Query("apiKey") apiKey: String,
        @Query("suburbId") suburbId: String
    ): ResponseGetAgent
}