package id.co.leholeh.mobile.merchant.ui.feedback

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantReviewBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.feedback.DataTransactionReview
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_feedback_review.view.*

class MerchantReviewActivity : AppCompatActivity() {
    lateinit var binding :ActivityMerchantReviewBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val viewModel : FeedbackViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(FeedbackViewModel::class.java)
    }
    private lateinit var adapterReview : AdapterUtil<DataTransactionReview>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMerchantReviewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if(getAuth(cacheUtil).token!!.isNotEmpty()){
            auth= getAuth(cacheUtil)
            setupToolbar()
            getDataReview()
        }

    }

    private fun getDataReview(){
        viewModel.getDataListReview(auth.token!!)
        viewModel.review.observe(this, Observer {
            if(it != null){
                binding.emptyView.visibility = View.GONE
                adapterReview.data = it
            }else{
                binding.emptyView.visibility = View.VISIBLE
            }
        })

        adapterReview = AdapterUtil(
            R.layout.item_list_feedback_review,
            arrayListOf(),
            {_, itemView, item->
                itemView.ratingReview.rating = item.rating!!.toFloat()
                itemView.textViewReviewDescription.text = item.review
                itemView.textViewReviewSender.text = item.fullName
                itemView.textViewReviewDate.text = item.createdAt

            },{ _, _ ->

            })
        binding.recyclerReview.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerReview.adapter = adapterReview
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Review"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}