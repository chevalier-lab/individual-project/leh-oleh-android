package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseHistorySaldo(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataHistorySaldo? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataHistorySaldo(

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("wallet_name")
	val walletName: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("id_m_wallets")
	val idMWallets: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("balance")
	val balance: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_user")
	val idUUser: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("logs")
	val logs: List<LogsItem>? = null
)

data class LogsItem(

	@field:SerializedName("balance")
	val balance: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_user_wallet")
	val idUUserWallet: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
