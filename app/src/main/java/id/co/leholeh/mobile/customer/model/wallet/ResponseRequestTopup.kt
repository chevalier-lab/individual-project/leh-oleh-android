package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseRequestTopup(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: RequestTopup? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class RequestTopup(

	@field:SerializedName("balance_transfer")
	val balanceTransfer: String? = null,

	@field:SerializedName("balance_request")
	val balanceRequest: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_user_wallet")
	val idUUserWallet: String? = null,

	@field:SerializedName("proof_id")
	val proofId: String? = null,

	@field:SerializedName("token_payment")
	val tokenPayment: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("token")
	val token: String? = null
)
