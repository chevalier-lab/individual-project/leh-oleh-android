package id.co.leholeh.mobile.merchant.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.dashboard.DataIncome
import id.co.leholeh.mobile.merchant.model.dashboard.MonthsItem
import id.co.leholeh.mobile.merchant.network.DashboardService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class DashboardMerchantViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
    get() = _status

    private val _totalTransaksi = MutableLiveData<List<MonthsItem>>()
    val totalTransaksi : LiveData<List<MonthsItem>>
        get() = _totalTransaksi

    private val _dataIncome = MutableLiveData<DataIncome>()
    val dataIncome : LiveData<DataIncome>
        get() = _dataIncome

    private val dashboardService : DashboardService by lazy {
        ApiService().createService(DashboardService::class.java)
    }

    private suspend fun getIncome(auth : String, date: String){
        try{
            _status.postValue(ApiStatus.LOADING)
            _dataIncome.postValue(dashboardService.getDataDashboardIncome(auth, date).data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("ERR_REQ_INCOME", e.localizedMessage!!)
        }
    }

    fun getDataIncome(auth: String, date: String){
        viewModelScope.launch {
            getIncome(auth, date)
        }
    }

    private suspend fun getTotalTransaksi(auth : String, date : String){
        try{
            _status.postValue(ApiStatus.LOADING)
            _totalTransaksi.postValue(dashboardService.getDataDashboardIncome(auth, date).data!!.incomeDetails!![0].months)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("ERR_REQ_INCOME", e.localizedMessage!!)
        }
    }

    fun getDataTotalTransaksi(auth: String, date : String){
        viewModelScope.launch {
            getTotalTransaksi(auth,date)
        }
    }
}