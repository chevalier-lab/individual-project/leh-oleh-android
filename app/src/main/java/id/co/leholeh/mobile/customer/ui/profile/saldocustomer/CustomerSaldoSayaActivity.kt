package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.co.leholeh.R
import id.co.leholeh.mobile.customer.model.ProfileMenuItem
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.databinding.ActivityCustomerSaldoSayaBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup.CustomerTopUpSaldoActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_menu_akun.view.*

class CustomerSaldoSayaActivity : AppCompatActivity() {
    lateinit var binding: ActivityCustomerSaldoSayaBinding
    private lateinit var adapter: AdapterUtil<ProfileMenuItem>
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth : Login
    private val viewModelBalance : BalanceViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(BalanceViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomerSaldoSayaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Setup Toolbar
        setupToolbar()
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (getAuth(cacheUtil).token!!.isEmpty()){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }else{
            auth = getAuth(cacheUtil)
            getBalance()
        }

        //Setup Profil Menu
        binding.rvIconmenu.layoutManager = LinearLayoutManager(this@CustomerSaldoSayaActivity)
        adapter = AdapterUtil(
            R.layout.item_list_menu_akun,
            listOf(
                ProfileMenuItem(
                    getString(R.string.top_up),
                    R.drawable.ic_topup
                ),
                ProfileMenuItem(
                    getString(R.string.withdraw),
                    R.drawable.ic_withdraw
                ),
                ProfileMenuItem(
                    getString(R.string.riwayat_saldo),
                    R.drawable.ic_history
                ),
                ProfileMenuItem(
                    getString(R.string.akun_bank_saya),
                    R.drawable.ic_bank
                )
            ), { position, itemView, item ->
                itemView.tv_menu!!.text = item.label
                itemView.im_akun_icon!!.setImageResource(item.icon)
                itemView.im_chevron_right.setImageResource(R.drawable.ic_chevron_right)
            }, { position, item ->
                when (position) {
                    0 -> startActivity(Intent(this, CustomerTopUpSaldoActivity::class.java))
                    1 -> startActivity(Intent(this, CustomerWithDrawActivity::class.java))
                    2 -> startActivity(Intent(this, CustomerHistorySaldoActivity::class.java))
                    3 -> startActivity(Intent(this,ListBankUserActivity::class.java))
                }
            })
        binding.rvIconmenu.adapter = adapter

        //init profil picture
        binding.imFotoProfil.setImageResource(R.drawable.ic_home_black_24dp)


    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Saldo Saya"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //onItem Selected
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }else{
            auth = getAuth(cacheUtil)
            viewModelBalance.getDataBalance(auth.token!!)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getBalance(){
        viewModelBalance.getDataBalance(auth.token!!)
        viewModelBalance.balance.observe(this, Observer {
            //set id_wallet and get balance
            cacheUtil.set(ConstantWallet.ID_WALLET, it.id)
            cacheUtil.set(ConstantWallet.BALANCE_WALLET, it.balance)
            binding.tvNamaAkun.text = it.fullName
            binding.tvEmailAkun.text = it.email
            binding.tvSaldoSaya.text = changeToRupiah(it.balance!!.toDouble())
            Glide.with(this).load(it.uri).into(binding.imFotoProfil)
            Log.d("DATA SALDO SAYA", "DATA SALDO SAYA SHOW")
        })
    }
}