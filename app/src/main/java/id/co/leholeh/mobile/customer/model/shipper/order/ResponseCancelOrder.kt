package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseCancelOrder(

	@field:SerializedName("data")
	val data: DataCancelOrder? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataCancelOrder(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
