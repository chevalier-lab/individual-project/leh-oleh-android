package id.co.leholeh.mobile.customer.model.shipper.location

import com.google.gson.annotations.SerializedName

data class ResponseSuburbs(

	@field:SerializedName("data")
	val data: DataSuburbs? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataSuburbs(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItemSuburbs>? = null,

	@field:SerializedName("content")
	val content: String? = null
)

data class RowsItemSuburbs(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("alias")
	val alias: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
