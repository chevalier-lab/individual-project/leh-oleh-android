package id.co.leholeh.mobile.merchant.ui.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.leholeh.databinding.ActivityKeuanganMerchantBinding
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.BalanceViewModel
import id.co.leholeh.mobile.merchant.adapter.SectionsPagerAdapterDashboard
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.merchant.ui.profile.ProfileTokoViewModel
import id.co.leholeh.mobile.utils.changeToRupiah
import java.text.SimpleDateFormat
import java.util.*

class KeuanganMerchantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityKeuanganMerchantBinding
    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private val viewModelDashboard: DashboardMerchantViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            DashboardMerchantViewModel::class.java
        )
    }
    private val viewModelBalance : BalanceViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(BalanceViewModel::class.java)
    }
    private lateinit var auth: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKeuanganMerchantBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val sectionsPagerAdapterDashboard =
            SectionsPagerAdapterDashboard(this, supportFragmentManager)
        binding.viewPager.adapter = sectionsPagerAdapterDashboard
        binding.tabLayout.setupWithViewPager(binding.viewPager)

        //auth
        auth = intent.getSerializableExtra(INTENT_SEARCH) as String
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = "Keuangan"
        //check location null
        locationNullCheck()
        getIncome()
        getBalance()
    }

    @SuppressLint("SimpleDateFormat")
    private fun getIncome(){
        val date = SimpleDateFormat("dd-MM-yyyy").format(Date())
        viewModelDashboard.getDataIncome(auth, date)
        viewModelDashboard.dataIncome.observe(this, Observer {
            if(it.incomeTotal != null){
                binding.textViewIncome.text = changeToRupiah(it.incomeTotal.toDouble())
            }
        })
    }
    //check location null
    private fun locationNullCheck() {
        viewModel.getDataProfileToko(auth)
        viewModel.toko.observe(this, Observer { profileToko ->
            if (profileToko.location == null) {
                binding.tvAlert.visibility = View.VISIBLE
                binding.tvAlert.setOnClickListener {
                    startActivity(
                        Intent(
                            this,
                            MerchantLocationActivity::class.java
                        ).putExtra(INTENT_DETAIL, profileToko.id)
                            .putExtra(INTENT_SEARCH, auth)
                    )
                }
            }
        })
    }
    @SuppressLint("SetTextI18n")
    private fun getBalance(){
        viewModelBalance.getDataBalance(auth)
        viewModelBalance.balance.observe(this, Observer {
            if(it.balance != null){
                binding.textViewSaldo.text = changeToRupiah(it.balance.toDouble())
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.getDataProfileToko(
            auth
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}