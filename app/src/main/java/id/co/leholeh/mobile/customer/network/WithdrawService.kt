package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.model.wallet.ResponseDetailWithdraw
import id.co.leholeh.mobile.customer.model.wallet.ResponseListWithdraw
import id.co.leholeh.mobile.customer.model.wallet.ResponseRequestWithdraw
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface WithdrawService {
    @GET(ConstantWallet.BASE_URL_HISTORY_WITHDRAW)
    suspend fun getListHistoryWithdraw(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): Response<ResponseListWithdraw>

    @GET(ConstantWallet.BASE_URL_PERMINTAAN_WITHDRAW)
    suspend fun getListPermintaanWithdraw(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): Response<ResponseListWithdraw>

    @GET(ConstantWallet.BASE_URL_DETAIL_WITHDRAW)
    suspend fun getDetailWithdraw(
        @Header("Authorization") authorization: String,
        @Path("id") id: Int
    ): ResponseDetailWithdraw

    @POST(ConstantWallet.BASE_URL_REQUEST_WITHDRAW)
    fun requestWithdraw(@Header("Authorization") authorization: String,@Body jsonObject: JsonObject) : Call<ResponseRequestWithdraw>

}