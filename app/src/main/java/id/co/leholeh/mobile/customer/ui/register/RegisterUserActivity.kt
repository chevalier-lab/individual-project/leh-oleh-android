package id.co.leholeh.mobile.customer.ui.register

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantProfile
import id.co.leholeh.mobile.customer.model.auth.DataRegister
import id.co.leholeh.mobile.customer.model.profile.Profile
import id.co.leholeh.mobile.customer.model.profile.ResponseProfile
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.databinding.ActivityRegisterUserBinding
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class RegisterUserActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterUserBinding
    private lateinit var cacheUtil: CacheUtil
    private val itemsGender = arrayListOf("Laki-laki", "Perempuan")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        binding.textViewTanggalLahirRegis.setOnClickListener {
            showDatePickerDialog()
        }
        if(intent.getStringExtra(ConstantProfile.INTENT_DATA_EDIT) != null){
            supportActionBar!!.title = getString(R.string.ubah_profile)
        }
        else{
            supportActionBar!!.title = getString(R.string.registrasi)
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        //Register Fungsi
        registerHandler()

    }

    private fun registerHandler() {

        //getData from intent in edit condition
        if(intent.getSerializableExtra(ConstantProfile.INTENT_DATA_PROFILE) != null){
            binding.button2.text = getString(R.string.ubah)
            val profile = intent.getSerializableExtra(ConstantProfile.INTENT_DATA_PROFILE) as Profile
            binding.textInputLayoutNoHp.visibility = View.VISIBLE
            binding.textInputLayoutEmail.visibility = View.VISIBLE
            binding.textInputLayoutPassword.visibility = View.GONE
            binding.textInputLayoutUsername.visibility = View.GONE

            binding.editTextNamaLengkapRegis.setText(profile.fullName)
            binding.editTextUsernameRegis.setText(profile.username)
            binding.textViewTanggalLahirRegis.text = profile.tglLahir
            if(profile.phoneNumber != null && profile.email != null){
                binding.editTextEmail.setText(profile.email)
                binding.editTextNomorHpUser.setText(profile.phoneNumber)
            }
            if(profile.jk == "l"){
                binding.dropwDownGender.setText(itemsGender[0])
            }
            else if(profile.jk == "p"){
                binding.dropwDownGender.setText(itemsGender[1])
            }
            binding.textViewTanggalLahirRegis
            binding.editTextAlamat.setText(profile.alamat)
        }
        registerValidation()

        binding.editTextPasswordRegis.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.editTextNamaLengkapRegis.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.editTextUsernameRegis.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.textViewTanggalLahirRegis.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsGender)
        binding.dropwDownGender.setAdapter(adapterCategory)
        binding.dropwDownGender.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }



        })

        binding.editTextPasswordRegis.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.editTextNomorHpUser.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.editTextAlamat.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                registerValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.button2.setOnClickListener {
            var gender = ""
            if (binding.dropwDownGender.text.toString() == itemsGender[0]) {
                gender = "l"
            } else if (binding.dropwDownGender.text.toString() == itemsGender[1]) {
                gender = "p"
            }

            if (intent.getStringExtra(ConstantProfile.INTENT_DATA_EDIT) != null &&
                intent.getStringExtra(ConstantProfile.INTENT_DATA_EDIT) == ConstantProfile.INTENT_EDIT_CONDITION
            ) {
                val jsonAdapter = JsonObject()
                jsonAdapter.apply {
                    addProperty("full_name", binding.editTextNamaLengkapRegis.text.toString())
                    addProperty("jk", gender)
                    addProperty("alamat", binding.editTextAlamat.text.toString())
                    addProperty("tgl_lahir", binding.textViewTanggalLahirRegis.text.toString())
                    addProperty("email", binding.editTextEmail.text.toString())
                    addProperty("phone_number", binding.editTextNomorHpUser.text.toString())
                }
                setEditProfile(jsonAdapter,{
                    if (it.code == 200) {
                        Toast.makeText(
                            this@RegisterUserActivity,
                            getString(R.string.berhasil_mengubah_profile),
                            Toast.LENGTH_SHORT
                        ).show()
                        finish()
                    }else Toast.makeText(this@RegisterUserActivity, it.message, Toast.LENGTH_LONG).show()
                },{
                    Toast.makeText(this@RegisterUserActivity, it, Toast.LENGTH_LONG).show()
                })
            }
            else{
                val jsonAdapter = JsonObject()
                jsonAdapter.apply {
                    addProperty("full_name", binding.editTextNamaLengkapRegis.text.toString())
                    addProperty("username", binding.editTextUsernameRegis.text.toString())
                    addProperty("password", binding.editTextPasswordRegis.text.toString())
                    addProperty("birth_date", binding.textViewTanggalLahirRegis.text.toString())
                    addProperty("address", binding.editTextAlamat.text.toString())
                    addProperty("gender", gender)
                }
                setRegister(
                    jsonAdapter, {
                        if (it.code == 200) {
                            Toast.makeText(
                                this@RegisterUserActivity,
                                "Berhasil Mendaftar!",
                                Toast.LENGTH_SHORT
                            ).show()
                            val intent = Intent(this@RegisterUserActivity, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        }else Toast.makeText(this@RegisterUserActivity, it.message, Toast.LENGTH_LONG).show()
                    }, {
                        Toast.makeText(this@RegisterUserActivity, it, Toast.LENGTH_LONG).show()
                    }
                )
            }


        }
    }

    private fun setEditProfile(
        jsonObject: JsonObject,
        onSuccess: (ResponseProfile) -> Unit,
        onFailed: (String) -> Unit
    ) {
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            val auth = getAuth(cacheUtil)
            val call : Call<ResponseProfile> = ApiService().serviceProfileUser.editProfile(auth.token!!, jsonObject)
            call.enqueue(object : Callback<ResponseProfile>{
                override fun onFailure(call: Call<ResponseProfile>, t: Throwable) {
                    onFailed(t.message.toString())
                }

                override fun onResponse(
                    call: Call<ResponseProfile>,
                    response: Response<ResponseProfile>
                ) {
                    if (response.isSuccessful)
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message().toString())
                    else onFailed(response.message().toString())
                }

            })
        }

    }

    // Sign Up
    private fun setRegister(
        jsonObject: JsonObject,
        onSuccess: (DataRegister) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<DataRegister> = ApiService().serviceAuth.getRegister(jsonObject)
        call.enqueue(object : Callback<DataRegister> {
            override fun onFailure(call: Call<DataRegister>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(call: Call<DataRegister>, response: Response<DataRegister>) {
                Log.d("REQUEST_BASIC_AUTH", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }
        })
    }

    private fun registerValidation() {
        if (intent.getStringExtra(ConstantProfile.INTENT_DATA_EDIT) != null &&
            intent.getStringExtra(ConstantProfile.INTENT_DATA_EDIT) == ConstantProfile.INTENT_EDIT_CONDITION
        ){
            if(isFormEditFilled())binding.button2.apply {
                isEnabled = true
                setBackgroundResource(R.drawable.rounded_corners)
                setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
            }else binding.button2.apply {
                isEnabled = false
                setBackgroundResource(R.drawable.rounded_grey_checkout)
                setTextColor(ContextCompat.getColor(applicationContext, R.color.colorBlack))
            }
        }
        else{
            if (isFormRegisterFilled()) binding.button2.apply {
                isEnabled = true
                setBackgroundResource(R.drawable.rounded_corners)
                setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
            }
            else binding.button2.apply {
                isEnabled = false
                setBackgroundResource(R.drawable.rounded_grey_checkout)
                setTextColor(ContextCompat.getColor(applicationContext, R.color.colorBlack))
            }
        }

    }

    private fun isFormRegisterFilled(): Boolean =
        !(TextUtils.isEmpty(binding.editTextNamaLengkapRegis.text.toString()) ||
                TextUtils.isEmpty(binding.editTextAlamat.text.toString()) ||
                TextUtils.isEmpty(binding.editTextPasswordRegis.text.toString()) ||
                TextUtils.isEmpty(binding.editTextUsernameRegis.text.toString()) ||
                TextUtils.isEmpty(binding.textViewTanggalLahirRegis.toString())) ||
                TextUtils.isEmpty(binding.dropwDownGender.toString())

    private fun isFormEditFilled(): Boolean =
        !(TextUtils.isEmpty(binding.editTextNamaLengkapRegis.text.toString()) ||
                TextUtils.isEmpty(binding.editTextAlamat.text.toString()) ||
                TextUtils.isEmpty(binding.editTextNomorHpUser.text.toString()) ||
                TextUtils.isEmpty(binding.textViewTanggalLahirRegis.toString()) ||
                TextUtils.isEmpty(binding.editTextEmail.text.toString())) ||
                TextUtils.isEmpty(binding.dropwDownGender.toString())

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun showDatePickerDialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val datePicker = DatePickerDialog(
            this,
            { _, _year, monthOfYear, dayOfMonth ->
                val strDate = SimpleDateFormat("dd MM yyyy").parse("$dayOfMonth $monthOfYear $_year")
                val formatDate = SimpleDateFormat("dd MMMM yyyy").format(strDate!!)
                binding.textViewTanggalLahirRegis.text = formatDate.toString()
            },
            year,
            month,
            day
        )
        datePicker.show()
    }
}