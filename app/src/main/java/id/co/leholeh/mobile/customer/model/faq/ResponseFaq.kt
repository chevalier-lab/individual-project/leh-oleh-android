package id.co.leholeh.mobile.customer.model.faq

import com.google.gson.annotations.SerializedName

data class ResponseFaq(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataFaq>? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataFaq(

	@field:SerializedName("question")
	val question: String? = null,

	@field:SerializedName("answer")
	val answer: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("updated_by")
	val updatedBy: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("created_by")
	val createdBy: String? = null
)
