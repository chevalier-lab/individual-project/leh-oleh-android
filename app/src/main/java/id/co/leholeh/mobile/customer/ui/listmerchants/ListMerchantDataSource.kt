package id.co.leholeh.mobile.customer.ui.listmerchants

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class ListMerchantDataSource(coroutineContext: CoroutineContext):PageKeyedDataSource<Int,Merchants>() {
    private val serviceMerchant = ApiService().serviceGuestMerchants
    val apiState = MutableLiveData<ApiStatus>()
    private val job= Job()
    private val scope = CoroutineScope(coroutineContext+job)

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Merchants>
    ) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = serviceMerchant.getListMerchant(FIRST_PAGE,"DESC")
                val responseItem = response.body()!!.data
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem,null, FIRST_PAGE +1)
                        }
                    }
                }
            }catch (e:Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_PRMNTAAN_WD", e.localizedMessage!!)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Merchants>) {
        scope.launch {
            try {
               apiState.postValue(ApiStatus.LOADING)
                val response = serviceMerchant.getListMerchant(params.key,"DESC")
                val responseItem = response.body()!!.data
                val key = if (params.key>0) params.key-1 else -1
                when{
                    response.isSuccessful->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem,key)
                        }
                    }
                }
            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_PRMNTAAN_WD", e.localizedMessage!!)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Merchants>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = serviceMerchant.getListMerchant(params.key,"DESC")
                val responseItem = response.body()!!.data
                val key = params.key + 1
                when{
                    response.isSuccessful->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_AFTER)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem,key)
                        }
                    }
                }
            }catch (e:Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_PRMNTAAN_WD", e.localizedMessage!!)
            }
        }
    }
    companion object {
        const val FIRST_PAGE = 0
    }
}