package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.shipper.pickuprequest.DataItemGetAgent
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.shipper.PickupRequestInterface
import id.co.leholeh.mobile.merchant.model.transaction.DetailTransactionMerchant
import id.co.leholeh.mobile.merchant.model.transaction.ListTransaction
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TransactionViewModel : ViewModel() {
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .build()

    private val _statusDetailTransaction = MutableLiveData<ApiStatus>()
    val statusDetailTransaction: LiveData<ApiStatus>
        get() = _statusDetailTransaction

    private val _detailTransaction = MutableLiveData<DetailTransactionMerchant>()
    val detailTransaction: LiveData<DetailTransactionMerchant>
        get() = _detailTransaction

    private val _listDataAgent = MutableLiveData<List<DataItemGetAgent>>()
    val listDataAgent: LiveData<List<DataItemGetAgent>>
        get() = _listDataAgent


    private val _statusListAgent = MutableLiveData<ApiStatus>()
    val statusListAgent: LiveData<ApiStatus>
        get() = _statusListAgent

    private val apiServiceTransaction: TransactionService by lazy {
        ApiService().createService(TransactionService::class.java)
    }

    private val apiServiceShipper : PickupRequestInterface by lazy {
        ApiService().createServiceShipper(PickupRequestInterface::class.java)
    }

    private val _kelolaPesananDataSource = MutableLiveData<KelolaPesananDataSource>()
    private lateinit var listTransaction: LiveData<PagedList<ListTransaction>>
    lateinit var statusListTransaction: LiveData<ApiStatus>

    //LIST TRANSACTION
    fun setUpListTransaction(authorization: String, status: String){
        statusListTransaction = Transformations.switchMap<KelolaPesananDataSource, ApiStatus>(_kelolaPesananDataSource, KelolaPesananDataSource::apiState)
        listTransaction  = initializedListTransactionPagedListBuilder(authorization,config, status).build()
    }

    fun getDataListTransaction() : LiveData<PagedList<ListTransaction>> = listTransaction

    private fun initializedListTransactionPagedListBuilder(authorization: String, config: PagedList.Config, status: String):
            LivePagedListBuilder<Int, ListTransaction> {

        val dataSourceFactory = object : DataSource.Factory<Int, ListTransaction>() {
            override fun create(): DataSource<Int, ListTransaction> {
                val kelolaPesananDataSource = KelolaPesananDataSource(Dispatchers.IO, authorization, status)
                _kelolaPesananDataSource.postValue(kelolaPesananDataSource)
                return kelolaPesananDataSource
            }
        }
        return LivePagedListBuilder<Int, ListTransaction>(dataSourceFactory, config)
    }
    //END OF LIST TRANSACTION

    private suspend fun getResponseDetailTransaction(token: String, id: String) {
        try {
            _statusDetailTransaction.postValue(ApiStatus.LOADING)
            _detailTransaction.postValue(apiServiceTransaction.getDetailTransactionMerchant(token, id).data)
            _statusDetailTransaction.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("GET_Transaction", e.toString())
            _statusDetailTransaction.postValue(ApiStatus.FAILED)
        }
    }

    fun getDetailTransaction(token: String, id: String) {
        viewModelScope.launch {
            getResponseDetailTransaction(token, id)
        }
    }

    private suspend fun getListShipperAgent(apiKey : String, subUrb : String){
        try {
            _statusListAgent.postValue(ApiStatus.LOADING)
            _listDataAgent.postValue(apiServiceShipper.getAgentSuburbs(apiKey, subUrb).data!!.data)
            _statusListAgent.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _statusListAgent.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataListShipperAgent(apiKey: String, subUrb: String){
        viewModelScope.launch {
            getListShipperAgent(apiKey, subUrb)
        }
    }

}