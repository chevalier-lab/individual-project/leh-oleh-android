package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DetailLocation(
    @field:SerializedName("id_m_locations")
    val idMLocations: String? = null,

    @field:SerializedName("id_u_product_location")
    val idUProductLocation: String? = null,

    @field:SerializedName("province_name")
    val provinceName: String? = null
) : Serializable