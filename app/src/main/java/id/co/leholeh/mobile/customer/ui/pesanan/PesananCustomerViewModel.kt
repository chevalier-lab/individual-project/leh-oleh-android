package id.co.leholeh.mobile.customer.ui.pesanan

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.orders.Order
import id.co.leholeh.mobile.customer.model.orders.detail.DataDetailOrder
import id.co.leholeh.mobile.customer.model.orders.midtrans.ResponseGetStatusTransaction
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.OrdersService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PesananCustomerViewModel : ViewModel() {

    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val _detailPesanan = MutableLiveData<DataDetailOrder>()
    val detailPesanan: LiveData<DataDetailOrder>
        get() = _detailPesanan

    private val _transactionStatus = MutableLiveData<ResponseGetStatusTransaction>()
    val transactionStatus: LiveData<ResponseGetStatusTransaction>
        get() = _transactionStatus

    private val apiServiceOrder = ApiService().serviceOrder

    private val statusTransactionService : OrdersService by lazy {
        ApiService().createServiceMidtrans(OrdersService::class.java)
    }

    private lateinit var listPesanan: LiveData<PagedList<Order>>
    lateinit var statusListPesanan: LiveData<ApiStatus>
    private val _pesananDataSource = MutableLiveData<PesananCustomerDataSource>()

    //LIST PESANAN
    fun setUpListPesanan(authorization: String){
        statusListPesanan =
            Transformations.switchMap<PesananCustomerDataSource, ApiStatus>(_pesananDataSource, PesananCustomerDataSource::apiState)
        listPesanan  = initializedPesananPagedListBuilder(authorization,config).build()
    }

    fun getDataListPesanan() : LiveData<PagedList<Order>> = listPesanan

    private fun initializedPesananPagedListBuilder(authorization: String, config: PagedList.Config):
            LivePagedListBuilder<Int, Order> {

        val dataSourceFactory = object : DataSource.Factory<Int, Order>() {
            override fun create(): DataSource<Int, Order> {
                val pesananCustomerDataSource = PesananCustomerDataSource(Dispatchers.IO, authorization)
                _pesananDataSource.postValue(pesananCustomerDataSource)
                return pesananCustomerDataSource
            }
        }
        return LivePagedListBuilder<Int, Order>(dataSourceFactory, config)
    }
    //END OF LIST PESANAN

    private suspend fun requestTransactionStatus(auth: String, token: String){
        try {
            _status.postValue(ApiStatus.LOADING)
            _transactionStatus.postValue(statusTransactionService.getTransactionStatus(auth, token))
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("TRANSACTION_STATUS", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    private suspend fun requestDetailPesanan(token: String,id:String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _detailPesanan.postValue(apiServiceOrder.getDetailOrders(token,id).dataDetailOrder)
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("DETAIL_PESANAN", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getTransactionStatus(auth: String, token: String){
        viewModelScope.launch {
            requestTransactionStatus(auth, token)
        }
    }

    fun getDetailPesanan(token: String,id:String) {
        viewModelScope.launch {
            requestDetailPesanan(token,id)
        }
    }

}