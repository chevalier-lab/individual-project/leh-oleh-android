package id.co.leholeh.mobile.customer.model.auth

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Register(
    @field:SerializedName("full_name")
    val fullName: String,

    @field:SerializedName("level")
    val level: String,

    @field:SerializedName("username")
    val username: String ,

    @field:SerializedName("token")
    val token: String
):Serializable