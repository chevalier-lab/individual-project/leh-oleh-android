package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantOrders
import id.co.leholeh.mobile.customer.model.midtransbody.ResponsePaymentToken
import id.co.leholeh.mobile.customer.model.orders.DataOrder
import id.co.leholeh.mobile.customer.model.orders.detail.ResponseDetailOrder
import id.co.leholeh.mobile.customer.model.orders.komplain.DataKomplain
import id.co.leholeh.mobile.customer.model.orders.midtrans.ResponseGetStatusTransaction
import id.co.leholeh.mobile.customer.model.orders.review.DataReview
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface OrdersService {
    @GET(ConstantOrders.BASE_URL_ORDER_ACTIVE)
    suspend fun getActiveOrders(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int,
        @Query("order-by") orderBy: String?,
        @Query("order-direction") orderDirection: String?
    ): Response<DataOrder>

    @GET(ConstantOrders.BASE_URL_ORDER_HISTORY)
    suspend fun getHistoryOrders(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int?,
        @Query("order-by") orderBy: String?,
        @Query("order-direction") orderDirection: String?
    ): Response<DataOrder>

    @GET(ConstantOrders.BASE_URL_DETAIL_ORDER)
    suspend fun getDetailOrders(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): ResponseDetailOrder

    @POST(ConstantOrders.BASE_URL_REVIEW)
    fun getReviewOrders(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<DataReview>

    @POST(ConstantOrders.BASE_URL_KOMPLAIN)
    fun getKomplainOrders(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<DataKomplain>

    @GET(ConstantOrders.BASE_URL_GET_STATUS_MIDTRANS)
    suspend fun getTransactionStatus(
        @Header("Authorization") auth: String,
        @Path("token") token: String
    ): ResponseGetStatusTransaction

    @PUT(ConstantOrders.BASE_URL_UPDATE_PAYMENT_TOKEN)
    fun updatePaymentToken(
        @Header("Authorization") auth: String,
        @Path("id") token: String,
        @Body jsonObject: JsonObject
    ): Call<ResponsePaymentToken>
}