package id.co.leholeh.mobile.customer.constant

const val PESANAN_DATA = "produk_data"

//DEV
const val BASE_URL = "http://213.190.4.40/leh-oleh/be/index.php/"
const val BASE_URL_INVOICE_MIDTRANS = "https://app.sandbox.midtrans.com/snap/v2/vtweb/"
const val BASE_URL_SHIPPER = "https://api.sandbox.shipper.id/"
const val BASE_URL_MIDTRANS = "https://api.sandbox.midtrans.com/"
const val BASE_URL_MIDTRANS_TRANSACTION = "https://app.sandbox.midtrans.com/"
const val API_KEY_MIDTRANS = "SB-Mid-server-QeNXGt1SBCc9ENoXEhzCRzL_"
const val API_KEY_SHIPPER = "fb5ebbc47c08464ded1a8fd8daf5dab3"

//PROD
//const val BASE_URL = "https://leholeh.co.id/v1/b/index.php/"
//const val BASE_URL_SHIPPER = "https://api.shipper.id/prod/"
//const val BASE_URL_MIDTRANS = "https://api.midtrans.com/"
//const val BASE_URL_MIDTRANS_TRANSACTION = "https://app.midtrans.com/"
//const val BASE_URL_INVOICE_MIDTRANS = "https://app.midtrans.com/snap/v2/vtweb/"
//const val API_KEY_MIDTRANS = "Mid-server-sE5_A-Gog9-owa1eDZiv0meD"
//const val API_KEY_SHIPPER = "463195fe92712c0149f16bb70f81e060"


const val BASE_URL_GUEST_NEW_PRODUCT =
    "guest/products"
const val BASE_URL_GUEST_PRODUCT_BYLOCATION = "guest/products/location"
const val BASE_URL_GUEST_PRODUCT_DETAIL = "guest/products/{id}"
const val BASE_URL_GUEST_PRODUCT = "guest/products/"
const val BASE_URL_GUEST_PRODUCT_FILTER =
    "guest/products/filter"
const val BASE_URL_GUEST_PRODUCT_SEARCH = "guest/products?search"
const val BASE_URL_GUEST_USER_LOCATION = "guest/location"
const val BASE_URL_GUEST_USER_CURRENT_LOCATION = "guest/location/current"
const val BASE_URL_GUEST_UPDATE_USER_LOCATION = "guest/location/{id}"
const val BASE_URL_GUEST_UPDATE_USER_CURRENT_LOCATION = "guest/location/actived/{id}"
const val BASE_URL_LOGIN = "auth/login"
const val BASE_URL_REGISTER = "auth/register"
const val BASE_URL_CATEGORIES = "guest/general/categories"
const val BASE_URL_LOCATION = "guest/general/locations"
const val BASE_URL_BANNER = "guest/general/banner"
const val BASE_URL_NOTIFICATIONS = "notifications"


const val INTENT_DETAIL = "DetailPruduct"
const val INTENT_SEARCH = "CariProduk"

var status = 1
//1 = cutomer
//0= merchant