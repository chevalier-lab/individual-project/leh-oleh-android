package id.co.leholeh.mobile.customer.ui.profile.saldocustomer


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityCustomerWithDrawCustomerBinding
import id.co.leholeh.databinding.ComponentDialogWithdrawFailedBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.wallet.ResponseRequestWithdraw
import id.co.leholeh.mobile.customer.model.wallet.Withdraw
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.WithdrawService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_withdraw.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat

class CustomerWithDrawActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCustomerWithDrawCustomerBinding
    private lateinit var adapterWithDraw: PagedAdapterUtil<Withdraw>
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    private var idBankAccount: Int? = null

    private val viewModel: WithdrawViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(WithdrawViewModel::class.java)
    }

    private val viewModelBanks: BanksViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(BanksViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //init binding
        binding = ActivityCustomerWithDrawCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            getListPermintaanWithdaw()
            getListBankAccountUser()
            sendRequestWithdraw()
        }

        //setup Toolbar
        setupToolbar()
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.withdraw)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.menu_history -> {
            startActivity(Intent(this, HistoryWithdrawActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    //show dialog failed
    private fun showDialogFailed() {
        val dialog = Dialog(this)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_withdraw_failed)
        val layoutInflater = LayoutInflater.from(this)

        //binding here
        val binding = ComponentDialogWithdrawFailedBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun getListPermintaanWithdaw() {
        // Start of Request Withdraw
        with(viewModel) {
            setUpListRequestWithdraw(auth.token.toString())
            statusListRequestWithdraw.observe(this@CustomerWithDrawActivity, Observer {
                stateUtilWithEmptyView(
                    this@CustomerWithDrawActivity,
                    it,
                    binding.progressBar,
                    binding.rvTopUp,
                    binding.emptyView.tvEmptyView,
                    binding.emptyView.imEmptyView,
                    binding.emptyView.layoutEmptyView,
                    "Daftar Permintaan Withdraw Kosong! ",
                    R.drawable.ic_withdraw
                )
            })
        }

        adapterWithDraw = PagedAdapterUtil(
            R.layout.item_withdraw,
            { _, itemView, item ->
                when (item.status) {
                    "0" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_refund)
                        text = getString(R.string.failed)
                    }
                    "1" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_selesai)
                        text = getString(R.string.success)
                    }
                    "2" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_diproses)
                        text = getString(R.string.withdrawmemunggu)
                    }
                }

                with(itemView) {
                    tv_bank.text = item.bankName
                    tv_nominal.text = changeToRupiah(item.balanceRequest!!.toDouble())

                    val tglTrans =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.createdAt!!)
                    textViewTglTransaksi.text =
                        SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTrans!!)
                }
            },
            { _, item ->
//                showDialogDetailWithdraw(item.id!!.toInt())
                startActivity(
                    Intent(
                        this@CustomerWithDrawActivity,
                        DetailWithdrawActivity::class.java
                    ).putExtra(INTENT_DETAIL, item)
                )
            })

        with(binding.rvTopUp) {
            layoutManager = LinearLayoutManager(
                this@CustomerWithDrawActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = adapterWithDraw
        }

        viewModel.getListRequestWithdraw().observe(this, Observer {
            adapterWithDraw.submitList(it)
        })

    }

    private fun setRequestWithdraw(
        jsonObject: JsonObject,
        onSuccess: (ResponseRequestWithdraw) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceWithdraw: WithdrawService by lazy {
            ApiService().createService(WithdrawService::class.java)
        }
        val call: Call<ResponseRequestWithdraw> =
            serviceWithdraw.requestWithdraw(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseRequestWithdraw> {
            override fun onFailure(call: Call<ResponseRequestWithdraw>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRequestWithdraw>,
                response: Response<ResponseRequestWithdraw>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun sendRequestWithdraw() {
        val balance = ConstantWallet.getBalance(cacheUtil)
        val idWallet = ConstantWallet.getIdWallet(cacheUtil)
        binding.btProses.setOnClickListener {
            val nominal = binding.editTextNominalWithdraw.text
            val bankAccont = binding.dropdownBankAccount.text
            val note = binding.editTextNote.text
            //check idWallet and balance are not null or empty
            if (idWallet.isEmpty() || balance.isEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.message_something_wrong),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                //validate form
                if (nominal.isNullOrEmpty() || bankAccont.isNullOrEmpty() || note.isNullOrEmpty()) {
                    Toast.makeText(this, getString(R.string.message_fill_form), Toast.LENGTH_SHORT)
                        .show()
                } else if (nominal.toString().toInt() > balance.toInt()) {
                    showDialogFailed()
                } else {
                    val jsonObject = JsonObject()
                    jsonObject.apply {
                        addProperty("id_u_user_wallet", idWallet.toInt())
                        addProperty("id_u_user_bank_account", idBankAccount!!.toInt())
                        addProperty("balance_request", nominal.toString().toInt())
                        addProperty("note", note.toString())
                    }

                    setRequestWithdraw(jsonObject, {
                        if (it.code == 200) {
                            Toast.makeText(
                                this,
                                "Berhasil Ajukan Tarik Saldo",
                                Toast.LENGTH_SHORT
                            ).show()
                            getListPermintaanWithdaw()
                            viewModel.getListRequestWithdraw()

                            nominal.clear()
                            note.clear()
                            bankAccont.clear()
                        }
                    },
                        {
                            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                        })
                }
            }

        }
    }

    //function to get bank account user to set it with AutoCompleteTextview
    private fun getListBankAccountUser() {
        viewModelBanks.getDataListBankAccount(auth.token!!)
        viewModelBanks.listBanksAccount.observe(this, Observer { dataBankAccount ->
            if (dataBankAccount.isEmpty()){
            binding.dropdownBankAccount.setOnClickListener {
                Toast.makeText(
                    this,
                    getString(R.string.isi_bank),
                    Toast.LENGTH_SHORT
                ).show()
            }

            }else {
                val listItemBanks: ArrayList<String> = arrayListOf()
                for (item in dataBankAccount.indices) {
                    listItemBanks.add(dataBankAccount[item].bankName!!)
                }
                val adapterBank =
                    ArrayAdapter(
                        applicationContext,
                        R.layout.item_list_dropdown_text,
                        listItemBanks
                    )
                binding.dropdownBankAccount.setAdapter(adapterBank)
                binding.dropdownBankAccount.addTextChangedListener(object : TextWatcher {
                    override fun afterTextChanged(s: Editable?) {

                    }

                    override fun beforeTextChanged(
                        s: CharSequence?, start: Int, count: Int, after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence?,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        for (item in dataBankAccount.indices) {
                            if (binding.dropdownBankAccount.text.toString() == dataBankAccount[item].bankName) {
                                idBankAccount = dataBankAccount[item].id!!.toInt()
                            }
                        }
                    }

                })
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.history_menu, menu)
        return true
    }

    override fun onResume() {
        super.onResume()
        getListPermintaanWithdaw()

    }

}