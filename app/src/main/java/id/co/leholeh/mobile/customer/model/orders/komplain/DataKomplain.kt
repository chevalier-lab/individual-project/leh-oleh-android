package id.co.leholeh.mobile.customer.model.orders.komplain

import com.google.gson.annotations.SerializedName

data class DataKomplain(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Komplain? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)