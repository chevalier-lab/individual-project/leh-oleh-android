package id.co.leholeh.mobile.customer.ui.produk

import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityProductListBinding
import id.co.leholeh.databinding.ComponentSeachingDialogBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.cart.DataAddCart
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.model.product.FilterProduct
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.ui.rekomendasi.RekomendasiFragment
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_produk.view.*
import retrofit2.Callback
import retrofit2.Response

class ProductListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProductListBinding
    private lateinit var produkPencarianAdapter: PagedAdapterUtil<FilterProduct>
    private lateinit var itemData: ArrayList<RekomendasiFragment.ModelSearching>

    private val viewModel: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }

    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //cache util
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        //Data
        itemData =
            intent.getSerializableExtra(INTENT_SEARCH)!! as ArrayList<RekomendasiFragment.ModelSearching>

        //setup Toolbar
        setupToolbar()

        //Setup UI
        initUI()

    }

    //setup UI
    private fun initUI() {
        val jsonObject = JsonObject()
        val jsonCategoryArray = JsonArray()
        val jsonLocationArray = JsonArray()
        if (itemData[0].makanan != "") {
            jsonCategoryArray.add(itemData[0].makanan.toInt())
            jsonObject.add("category", jsonCategoryArray)
        }

        if (itemData[0].lokasi != "") {
            jsonLocationArray.add(itemData[0].lokasi.toInt())
            jsonObject.add("location", jsonLocationArray)
            Log.v("IDLOCATION", "cek location if : " + itemData[0].lokasi)

        }

        if (itemData[0].urutan == "") {
            filterProduct("DESC", itemData[0].keyword, jsonObject)
        } else {
            filterProduct(itemData[0].urutan, itemData[0].keyword, jsonObject)
        }
        Log.v("IDLOCATION", "atas id category : " + itemData[0].makanan)
        Log.v("IDLOCATION", "atas id location : " + itemData[0].lokasi)


        Log.v("ITEMDATA", itemData.toString())

    }


    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            if (itemData[0].keyword == "") {
                this.title = "List Produk"
            } else {
                this.title = itemData[0].keyword.capitalize()
            }
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_cart_menu, menu)
        return true
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            startActivity(Intent(this, MainActivity::class.java))
            true
        }
        R.id.menu_search -> {
            //setup dialog searching
            showDialog()
            true
        }
        R.id.menu_cart -> {
            startActivity(Intent(this, KeranjangActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }



    private fun showDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_seaching_dialog)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentSeachingDialogBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        //json Array
        //filter product
        val json = JsonObject()
        val categoriesArray = JsonArray()
        val locationArray = JsonArray()
        val listLocationArray = arrayListOf<String>()
        val listCategoryArray = arrayListOf<String>()
        listCategoryArray.add("")
        listLocationArray.add("")
        //Categori


        val items: ArrayList<Category> = arrayListOf()
        val itemsCategory: ArrayList<String> = arrayListOf()
        viewModel.getCategories()
        viewModel.categories.observe(this, Observer {
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsCategory.add(it.data[item].category!!)
            }
        })

        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsCategory)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.tvMakananFilter.text = binding.dropdownMakanan.text.toString()
                binding.tvMakananFilter.visibility = View.VISIBLE
                for (i in items.indices) {
                    if (binding.tvMakananFilter.text.toString() == items[i].category) {
                        Log.e("DATA_ISI_CATEGORY", items[i].category!!)
                        listCategoryArray.clear()
                        listCategoryArray.add(items[i].id!!.toString())
                        break
                    }
                }
            }
        })
        binding.tvMakananFilter.setOnClickListener {
            listCategoryArray.clear()
            binding.tvMakananFilter.text = ""
            binding.tvMakananFilter.visibility = View.GONE
        }


        //Location
        val itemsLocation: ArrayList<LocationProduct> = arrayListOf()
        val itemsLocationProvince: ArrayList<String> = arrayListOf()
        viewModel.getLocation()
        viewModel.location.observe(this, Observer {
            for (item in it.data?.indices!!) {
                itemsLocation.add(it.data[item])
                itemsLocationProvince.add(it.data[item].provinceName!!)
            }
        })

        val arrayLocation =
            ArrayAdapter(
                applicationContext,
                R.layout.item_list_dropdown_text,
                itemsLocationProvince
            )
        binding.dropdownLokasi.setAdapter(arrayLocation)
        binding.dropdownLokasi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.tvLocationFilter.text = binding.dropdownLokasi.text.toString()
                binding.tvLocationFilter.visibility = View.VISIBLE
                for (i in itemsLocation.indices) {
                    if (binding.tvLocationFilter.text.toString() == itemsLocation[i].provinceName) {
                        Log.e("DATA_ISI_CATEGORY", itemsLocation[i].provinceName!!)
                        listLocationArray.clear()
                        listLocationArray.add(itemsLocation[i].id!!.toString())
                        break
                    }
                }
            }
        })

        binding.tvLocationFilter.setOnClickListener {
            listLocationArray.clear()
            binding.tvLocationFilter.text = ""
            binding.tvLocationFilter.visibility = View.GONE
        }


        //Urutan Product
        val itemsUrutan: ArrayList<String> = arrayListOf("ASC", "DESC")
        val arrayUrutan =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsUrutan)
        binding.dropdownUrutkan.setAdapter(arrayUrutan)





        binding.tvCari.setOnClickListener {
            Log.e(
                "ISIFILTER",
                "$json ${binding.dropdownUrutkan.text} etKeyword ${binding.etKeyword.text.toString()}"
            )

            if (listLocationArray[0] != "") {
                locationArray.add(listLocationArray[0])
                json.add("location", locationArray)
            }
            if (listCategoryArray[0] != "") {
                categoriesArray.add(listCategoryArray[0])
                json.add("category", categoriesArray)
            }
//            Log.v("IDLOCATION", "id category Bawah: " + listLocationArray[0])
//            Log.v("IDLOCATION", "id location Bawah: " + categoriesArray[0])

            if (binding.dropdownUrutkan.text.toString() == "") {
                filterProduct(
                    "DESC",
                    binding.etKeyword.text.toString(),
                    json
                )
            } else {
                filterProduct(
                    binding.dropdownUrutkan.text.toString(),
                    binding.etKeyword.text.toString(),
                    json
                )
            }

            supportActionBar?.apply {
                this.title = binding.etKeyword.text.toString().capitalize()
            }
            dialog.hide()
        }

        binding.tvBatal.setOnClickListener {
            dialog.hide()
        }
    }

    private fun filterProduct(urutan: String, keyword: String, jsonObject: JsonObject) {
        viewModel.setUpListProductSearch(urutan, keyword, jsonObject)
        observeFilterProduct()

        //Adapter
        produkPencarianAdapter =
            PagedAdapterUtil(
                R.layout.item_produk,
                { position, itemView, item ->
                    itemView.textViewNamaProduk.text = item.productName
                    val harga_diskon =
                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                    itemView.textViewHargaDiskon.text =
                        changeToRupiah(
                            harga_diskon
                        )
                    if (item.discount.toString().toDouble() > 0.0) {
                        itemView.textViewHargaAsliProduk.paintFlags =
                            itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        itemView.textViewHargaAsliProduk.text =
                            changeToRupiah(
                                item.priceSelling.toDouble()
                            )

                    } else {
                        itemView.textViewHargaAsliProduk.visibility =
                            View.INVISIBLE
                    }
                    Glide.with(this).load(item.uri)
                        .into(itemView.imageViewProduk)

//                                    //Categories
//                                    var categories = ""
//
//                                    for (i in item.categories!!.indices) {
//                                        categories += if (i < item.categories.size - 1) {
//                                            item.categories[i].category + ", "
//                                        } else {
//                                            item.categories[i].category
//                                        }
//                                    }
//                                    itemView.textViewKategoriProduk.text = categories

                    //Beli Sekarang
                    itemView.btBeliSekarang.setOnClickListener {
                        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
                            auth = getAuth(cacheUtil)

                            val json = JsonObject()
                            json.addProperty("idMProducts", item.id)
                            json.addProperty("qty", "1")

                            addToCart(auth.token!!, json, {
                                if (it.code == 200) {
                                    Toast.makeText(
                                        this,
                                        "Berhasil Menambahkan Keranjang",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    val intent = Intent(
                                        this,
                                        KeranjangActivity::class.java
                                    )
                                    startActivity(intent)
                                } else {
                                    Toast.makeText(
                                        this,
                                        it.message,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }, {
                                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                            })
                        } else {
                            Toast.makeText(
                                this,
                                "Silahkan Login Terlebih Dahulu",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }


                },
                { position, item ->
                    val intent =
                        Intent(
                            this@ProductListActivity,
                            DetailProductActivity::class.java
                        )
                            .putExtra(
                                INTENT_DETAIL,
                                item.id
                            )
                    startActivity(intent)
                })

        this.binding.rvListProduk.layoutManager =
            GridLayoutManager(this@ProductListActivity, 2)
        this.binding.rvListProduk.adapter = produkPencarianAdapter


//                    } else {
//                        this.binding.tvDataKosong.visibility = View.VISIBLE
//                        this.binding.rvListProduk.visibility = View.GONE
//                    }

    }

    private fun observeFilterProduct() {
        viewModel.getDataListProductSearch().observe(this, Observer {
            produkPencarianAdapter.submitList(it)
        })
        viewModel.statusProductSearch.observe(this, Observer {
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBarProductList,
                binding.rvListProduk,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Produk Tidak Ditemukan",
                R.drawable.ic_kelola_produk
            )
        })
    }


    //Add Cart
    private fun addToCart(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataAddCart) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val call: retrofit2.Call<DataAddCart> =
            ApiService().serviceCart.addCart(auth, jsonObject)
        call.enqueue(object : Callback<DataAddCart> {
            override fun onFailure(call: retrofit2.Call<DataAddCart>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: retrofit2.Call<DataAddCart>,
                response: Response<DataAddCart>
            ) {
                Log.d("ADD_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }

}