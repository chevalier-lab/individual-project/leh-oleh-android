package id.co.leholeh.mobile.utils

//status loading
enum class ApiStatus { EMPTY, EMPTY_BEFORE, EMPTY_AFTER, LOADING, LOADED, SUCCESS, FAILED }

