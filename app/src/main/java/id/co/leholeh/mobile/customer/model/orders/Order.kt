package id.co.leholeh.mobile.customer.model.orders

import com.google.gson.annotations.SerializedName

data class Order(
    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("total_price")
    val totalPrice: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("id_m_users")
    val idMUsers: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("transaction_token")
    val transactionToken: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)