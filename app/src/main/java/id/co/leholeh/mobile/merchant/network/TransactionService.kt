package id.co.leholeh.mobile.merchant.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.model.orders.DataOrderResponse
import id.co.leholeh.mobile.merchant.constant.ConstantTransactionMerchant
import id.co.leholeh.mobile.merchant.model.transaction.*
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface TransactionService {
    @GET(ConstantTransactionMerchant.BASE_URL_LIST_TRANSACTION_MERCHANT)
    suspend fun getListTranscationMerchant(
        @Header("Authorization") authorization: String,
        @Query("page") page : Int,
        @Query("status") status :String,
        @Query("order-by") orderBy: String,
        @Query("order-direction") orderDirection: String
    ): Response<ResponseListTransactionMerchant>

    @GET(ConstantTransactionMerchant.BASE_URL_DETAIL_TRANSACTION_MERCHANT)
    suspend fun getDetailTransactionMerchant(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): ResponseDetailTransactionMerchant

    @PUT(ConstantTransactionMerchant.BASE_URL_UPDATE_TRANSACTION_MERCHANT)
    fun updateTransactionMerchant(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject,
        @Path("id")id: String
    ): Call<ResponseUpdateTransaction>

    @POST(ConstantTransactionMerchant.BASE_URL_ORDER_ACTIVATION)
    fun sendOrderActivation(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<ResponseOrderActivation>

    @POST(ConstantTransactionMerchant.BASE_URL_PICKUP_REQUEST)
    fun sendPickupRequest(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<ResponsePickupRequest>

    @POST(ConstantTransactionMerchant.BASE_URL_CREATE_ORDER)
    fun createOrder(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<DataOrderResponse>
}