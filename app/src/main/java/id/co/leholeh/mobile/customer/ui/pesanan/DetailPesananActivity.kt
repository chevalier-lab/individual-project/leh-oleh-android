package id.co.leholeh.mobile.customer.ui.pesanan

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityDetailPesananBinding
import id.co.leholeh.databinding.CustomDialogDetailPesananBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.*
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.orders.detail.ProductsOrder
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.merchant.model.transaction.ResponseUpdateTransaction
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_detail_pesanan.view.*
import okhttp3.Credentials
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailPesananActivity : AppCompatActivity() {

    private lateinit var adapterProdukDetailPesanan: AdapterUtil<ProductsOrder>
    private lateinit var binding: ActivityDetailPesananBinding
    private lateinit var idPesananData: String
    private var tokenInvoice = ""
    private val viewModel: PesananCustomerViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(PesananCustomerViewModel::class.java)
    }
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val listProductId = arrayListOf<String>()
    private val basicAuth = Credentials.basic(API_KEY_MIDTRANS, "")

    private var isOnResume = false
    private var isOnPause = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailPesananBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)
        //set toolbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Detail Pesanan"

        if(!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            auth = getAuth(cacheUtil)
            //getdata Intent
            idPesananData = intent.getSerializableExtra(PESANAN_DATA)!! as String
            isOnResume = false
            isOnPause = false
            viewModel.getDetailPesanan(auth.token!!, idPesananData)
            initUI()
            binding.btnBayarTagihan.setOnClickListener {
                if(tokenInvoice != ""){
                    val tabsIntent = CustomTabsIntent.Builder().build()
                    tabsIntent.launchUrl(this, Uri.parse(BASE_URL_INVOICE_MIDTRANS+tokenInvoice))
                }
            }
        }else{
            Toast.makeText(this, "Anda Belum Login", Toast.LENGTH_SHORT).show()
        }

    }


    private fun initUI() {
        var transactionToken = ""
        var statusStr = ""
        var statusNumber = ""
        binding.layoutBayarTagihan.visibility = View.GONE
        viewModel.detailPesanan.observe(this, Observer {
            Log.v("DetailOrderData", it.toString())
            transactionToken = it.transactionToken!!
            if (it.infoTransaction?.tokenPayment != null) {
                tokenInvoice = it.infoTransaction.tokenPayment
                binding.layoutBayarTagihan.visibility = View.VISIBLE
            }
            binding.textViewIdDetailTransaksi.text = it.transactionToken
            binding.textViewHargaDetailTransaksi.text = changeToRupiah(it.totalPrice!!.toDouble())
            statusNumber = it.status!!
            when (it.status) {
                "0" -> {
                    statusStr = "Belum Dibayar"
                    binding.textViewStatusDetailTransaksi.setBackgroundResource(R.drawable.status_not_payment)
                }
                "1" -> {
                    statusStr = "Terverifikasi"
                    binding.textViewStatusDetailTransaksi.setBackgroundResource(R.drawable.status_payment_verified)
                }
                "2" -> {
                    statusStr = "Diproses"
                    binding.textViewStatusDetailTransaksi.setBackgroundResource(R.drawable.status_diproses)
                }
                "3" -> {
                    statusStr = getString(R.string.selesai)
                    binding.textViewStatusDetailTransaksi.setBackgroundResource(R.drawable.status_selesai)
                }
                "4" -> {
                    statusStr = getString(R.string.dikembalikan)
                    binding.textViewStatusDetailTransaksi.setBackgroundResource(R.drawable.status_refund)
                }
            }

            binding.textViewStatusDetailTransaksi.text = statusStr

            //setup time
            val tanggal = it.updatedAt!!.substring(0, 10)
            val jam = it.updatedAt.substring(10)
            binding.textViewTglDetailTransaksi.text = tanggal
            binding.textViewJamDetailTransaksi.text = jam
            listProductId.clear()
            adapterProdukDetailPesanan =
                AdapterUtil(
                    R.layout.item_detail_pesanan,
                    it.products!!,
                    { _, itemView, item ->

                        listProductId.add(item.id!!)
                        var statusProduct = ""
                        when (item.status) {
                            "0" -> {
                                statusProduct = "Belum Dibayar"
                                itemView.tvStatusDetailPesanan.setBackgroundResource(R.drawable.status_not_payment)
                            }
                            "1" -> {
                                statusProduct = "Terverifikasi"
                                itemView.tvStatusDetailPesanan.setBackgroundResource(R.drawable.status_payment_verified)
                            }
                            "2" -> {
                                statusProduct = "Diproses"
                                itemView.tvStatusDetailPesanan.setBackgroundResource(R.drawable.status_diproses)
                            }
                            "3" -> {
                                statusProduct = "Selesai"
                                itemView.tvStatusDetailPesanan.setBackgroundResource(R.drawable.status_selesai)
                            }
                            "4" -> {
                                statusProduct = "Refund"
                                itemView.tvStatusDetailPesanan.setBackgroundResource(R.drawable.status_refund)
                            }
                        }
                        itemView.tvNamaProdukDetailPesanan.text = item.productName

                        itemView.tvHargaProdukDetailPesanan.text =
                            changeToRupiah(item.subTotalPrice!!.toDouble())
                        itemView.tvJumlahProdukDetailPesanan.text =
                            StringBuilder("Jumlah : ").append(item.qty.toString())
                        itemView.tvStatusDetailPesanan.text = statusProduct
                        Glide.with(this).load(item.uri)
                            .into(itemView.imageViewProdukDetailPesanan)

                        //Categories
                        var categories = ""

                        for (i in item.categories!!.indices) {
                            categories += if (i < item.categories.size - 1) {
                                item.categories[i]!!.category + ", "
                            } else {
                                item.categories[i]!!.category
                            }
                        }
                        itemView.tvKategoriProdukDetailPesanan.text = categories


                    },
                    { _, item ->
                        //dialog intent
                        if (item.status == "3" || item.status == "4")
                            showActionDialog(item.idUProduct.toString())
                        Log.v(
                            "DETAILPESANAN",
                            "Idean U User Transcation" + item.idUUserTransaction.toString()
                        )
                        Log.v("DETAILPESANAN", "IUUSERProduct" + item.idUProduct.toString())
                    })

            viewModel.getTransactionStatus(basicAuth, it.transactionToken)
            binding.recyclerProdukDetailPesanan.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
            binding.recyclerProdukDetailPesanan.adapter = adapterProdukDetailPesanan
        })
        Log.d("TRANSACTION_TKN_KSNG", transactionToken)
        if(transactionToken != ""){
            Log.d("TRANSACTION_TKN", transactionToken)

        }
        viewModel.transactionStatus.observe(this, Observer { responseStat ->
            if (responseStat.statusCode == "200" && responseStat.transactionStatus == "settlement") {
                binding.layoutBayarTagihan.visibility = View.GONE
                if(statusNumber == ConstantMerchant.STATUS_PESANAN_NOT_PAYMENT){
                    for (i in listProductId.indices) {
                        sendRequestUpdateStatusVerified(listProductId[i])
                    }
                }
            }
        })
    }

    private fun showActionDialog(idItem: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_detail_pesanan)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogDetailPesananBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        binding.tvPenilaian.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    PenilaianActivity::class.java
                ).putExtra(INTENT_DETAIL, idPesananData).putExtra(INTENT_SEARCH, idItem)
            )
        }
        binding.tvKomplain.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    KomplainActivity::class.java
                ).putExtra(INTENT_DETAIL, idPesananData).putExtra(INTENT_SEARCH, idItem)
            )
        }
        dialog.show()
    }

    private fun sendRequestUpdateStatusVerified(idProduk : String) {
        val jsonObject = JsonObject()
        var count = 0
        jsonObject.addProperty("status", "1")
        updatePesananProses(jsonObject, idProduk, {
            if (it.code == 200) {
                count++
                if(count == listProductId.size){
                    Toast.makeText(
                        this,
                        "Berhasil mengubah status",
                        Toast.LENGTH_SHORT
                    ).show()
                    viewModel.getDetailPesanan(auth.token!!, idPesananData)
                }
            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun updatePesananProses(
        jsonObject: JsonObject, idProduk: String, onSuccess: (ResponseUpdateTransaction) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: TransactionService by lazy {
            ApiService().createService(TransactionService::class.java)
        }
        val call: Call<ResponseUpdateTransaction> =
            apiService.updateTransactionMerchant(auth.token!!, jsonObject,idProduk)
        call.enqueue(object : Callback<ResponseUpdateTransaction> {
            override fun onResponse(
                call: Call<ResponseUpdateTransaction>,
                response: Response<ResponseUpdateTransaction>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }
            override fun onFailure(call: Call<ResponseUpdateTransaction>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return true
    }

    override fun onResume() {
        Log.d("ON_RESUME_NIH_GBLK", "ON_RESUME_NIH")
        super.onResume()
        isOnResume = true
        val auth = getAuth(cacheUtil)
        viewModel.getDetailPesanan(auth.token!!, idPesananData)
    }

    override fun onPause() {
        super.onPause()
        isOnPause = true
    }
}