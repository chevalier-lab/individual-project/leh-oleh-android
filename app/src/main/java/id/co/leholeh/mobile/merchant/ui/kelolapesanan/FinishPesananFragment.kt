package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentFinishPesananBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.merchant.model.transaction.ListTransaction
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_transaksi_kelola_pesanan.view.*
import java.text.SimpleDateFormat
import java.util.*

class FinishPesananFragment : Fragment() {
    private var _binding: FragmentFinishPesananBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapterPesananVerified: PagedAdapterUtil<ListTransaction>
    private val viewModel: TransactionViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(TransactionViewModel::class.java)
    }
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment return inflater.inflate(R.layout.fragment_verified_pesanan, container, false)
        _binding = FragmentFinishPesananBinding.inflate(inflater, container, false)

        //Cache Util Auth
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)

        //Auth token
        auth = getAuth(cacheUtil)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //Id Prpduct
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
            onDestroy()
        } else {
            loadData()
        }

    }
    @SuppressLint("SetTextI18n")
    private fun loadData() {
        viewModel.setUpListTransaction(auth.token!!, "3")
        observerListTransaction()
        adapterPesananVerified =
            PagedAdapterUtil(
                R.layout.item_transaksi_kelola_pesanan,
                { _, itemView, item ->
                    itemView.textViewNamaPemesan.text = "Nama pemesan : ${item.fullName}"
                    itemView.textViewTotalTransaksi.text =
                        changeToRupiah(item.totalPrice?.toDouble()!!)
                    itemView.textViewJumlahProduk.visibility = View.GONE
                    itemView.textViewNoTransaksi.text = item.transactionToken
                    itemView.textViewStatusPesanan.text = getString(R.string.finish)
                    val orderDate = SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss",
                        Locale("ID")
                    ).parse(item.createdAt!!)
                    val formatedOrderDate =
                        SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale("ID")).format(orderDate!!)
                    itemView.textViewTanggalPesanan.text =
                        "Tanggal pesanan dibuat: $formatedOrderDate"
                },
                { _, item ->
                    val intent = Intent(
                        requireContext(),
                        DetailPesananMerchantActivity::class.java
                    )
                    intent.putExtra(
                        ConstantMerchant.ID_PESANAN,
                        item.id
                    )
                    intent.putExtra(ConstantMerchant.STATUS_PESANAN, item.status)
                    startActivity(intent)
                })

        binding.rvFinishPesanan.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvFinishPesanan.adapter = adapterPesananVerified
    }


    private fun getState() {
        viewModel.statusListTransaction.observe(viewLifecycleOwner, Observer {
            when (it) {
                ApiStatus.SUCCESS -> {

                }
                ApiStatus.LOADING -> {

                }
                ApiStatus.EMPTY -> {
                    binding.emptyView.visibility = View.VISIBLE
                    binding.rvFinishPesanan.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {

                }
                ApiStatus.EMPTY_AFTER -> {

                }
                ApiStatus.LOADED -> {
                    binding.rvFinishPesanan.visibility = View.VISIBLE
                    binding.emptyView.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    Toast.makeText(requireContext(), "Jaringan bermasalah", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    Toast.makeText(requireContext(), "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    private fun observerListTransaction() {
        viewModel.getDataListTransaction().observe(viewLifecycleOwner, Observer {
            adapterPesananVerified.submitList(it)
        })
        getState()
    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            viewModel.setUpListTransaction(auth.token!!, "3")
            observerListTransaction()
        }
    }
}