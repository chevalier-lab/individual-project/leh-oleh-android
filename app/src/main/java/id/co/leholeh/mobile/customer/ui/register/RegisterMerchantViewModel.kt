package id.co.leholeh.mobile.customer.ui.register

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.merchant.DataGetStatusMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class RegisterMerchantViewModel : ViewModel(){

    private val _statusMerchant = MutableLiveData<DataGetStatusMerchant>()
    val statusMerchant : LiveData<DataGetStatusMerchant>
        get() = _statusMerchant

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
        get() = _status


    private val apiService = ApiService().serviceGuestMerchants

    fun getDataStatusMerchant(token: String) {
        viewModelScope.launch {
            getStatusMerchant(token)
        }
    }

    private suspend fun getStatusMerchant(token: String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _statusMerchant.postValue(apiService.getStatusMerchant(token).data)

            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("ERROR_REQ_STATUS", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }

    }


}