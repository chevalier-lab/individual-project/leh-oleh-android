package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityCustomerHistorySaldoBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.wallet.LogsItem
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_history_saldo.view.*

class CustomerHistorySaldoActivity : AppCompatActivity() {
    lateinit var binding: ActivityCustomerHistorySaldoBinding
    lateinit var adapter: PagedAdapterUtil<LogsItem>
    private val viewModel: HistorySaldoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(HistorySaldoViewModel::class.java)
    }

    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomerHistorySaldoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setup toolbar
        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            initUI()
        }
    }

    //data dummy adapter
    @SuppressLint("SetTextI18n")
    private fun initUI() {
        viewModel.setUpListSaldo(auth.token.toString())
        viewModel.statusListHistorySaldo.observe(this, Observer {
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBar,
                binding.rvHistory,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Riwayat Saldo Kosong!",
                R.drawable.dompet
            )
        })

        adapter = PagedAdapterUtil(
            R.layout.item_history_saldo,
            { _, itemView, item ->
                when (item.status) {
                    "1" -> {
                        itemView.tv_nominal.setTextColor(resources.getColor(R.color.colorGreenApp))
                        itemView.tv_nominal.text =
                            "+" + changeToRupiah(item.balance!!.toDouble())
                        itemView.tvRekeningStatus.text = "Rekening Masuk"
                    }
                    "0" -> {
                        itemView.tv_nominal.setTextColor(resources.getColor(R.color.colorRed))
                        itemView.tv_nominal.text =
                            "-" + changeToRupiah(item.balance!!.toDouble())
                        itemView.tvRekeningStatus.text = "Rekening Keluar"
                    }
                }

                val tanggal = item.updatedAt!!.substring(0, 10)
                val jam = item.updatedAt.substring(10)
                itemView.textViewTglTransaksi.text = tanggal
                itemView.textViewJamTransaksi.text = jam

            },
            { _, _ -> })

        binding.rvHistory.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvHistory.adapter = adapter

        viewModel.getListHistorySaldo().observe(this, Observer {
            adapter.submitList(it)
        })

//
//        //data sementara
//
//        val listWithdraw: MutableList<SaldoTopUp> = arrayListOf()
//        val arrStatusWithdraw = arrayOf("Diproses", "Selesai", "Refund")
//        val arrBank = arrayOf("BRI", "BCA", "Mandiri")
//        val arrIdWithdraw = arrayOf(
//            "TRX fd24e671042a1a953825a864f5d19de2",
//            "TRX fd24e671042a1a953825a864f5d19de11",
//            "TRX fd24e671042a1a953825a864f5d19de23"
//        )
//        val arrNominalWithdraw = arrayOf(120000, 240000, 400000)
//        for (i in 0 until 3) {
//            val pesanan = SaldoTopUp(
//                arrIdWithdraw[i],
//                arrNominalWithdraw[i],
//                arrStatusWithdraw[i],
//                "12 September 2020",
//                "14:14:05",
//                arrBank[i]
//            )
//            listWithdraw.add(pesanan)
//        }
//        adapter.data = listWithdraw
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.riwayat_saldo)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}