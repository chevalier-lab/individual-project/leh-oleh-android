package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.model.wallet.ResponseGetBalance
import retrofit2.http.GET
import retrofit2.http.Header

interface BalanceService {
    @GET(ConstantWallet.BASE_URL_GET_BALANCE)
    suspend fun getBalance(@Header("Authorization") authorization : String) : ResponseGetBalance
}