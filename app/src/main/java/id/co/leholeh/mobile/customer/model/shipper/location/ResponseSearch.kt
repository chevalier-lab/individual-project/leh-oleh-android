package id.co.leholeh.mobile.customer.model.shipper.location

import com.google.gson.annotations.SerializedName

data class ResponseSearch(

	@field:SerializedName("data")
	val data: DataSearch? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RowsItemSearch(

	@field:SerializedName("area_name")
	val areaName: String? = null,

	@field:SerializedName("city_name")
	val cityName: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("suburb_name")
	val suburbName: String? = null,

	@field:SerializedName("order_list")
	val orderList: Int? = null
)

data class DataSearch(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItemSearch>? = null,

	@field:SerializedName("content")
	val content: String? = null
)
