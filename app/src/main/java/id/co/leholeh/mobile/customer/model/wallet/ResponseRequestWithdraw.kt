package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseRequestWithdraw(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: RequestWithdraw? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

