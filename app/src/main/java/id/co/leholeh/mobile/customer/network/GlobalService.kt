package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.BASE_URL_CATEGORIES
import id.co.leholeh.mobile.customer.constant.BASE_URL_LOCATION
import id.co.leholeh.mobile.customer.model.category.DataCategory
import id.co.leholeh.mobile.customer.model.location.DataLocation
import retrofit2.http.GET

interface GlobalService {
    @GET(BASE_URL_CATEGORIES)
    suspend fun getCategories() : DataCategory

    @GET(BASE_URL_LOCATION)
    suspend fun getLocation() : DataLocation
}