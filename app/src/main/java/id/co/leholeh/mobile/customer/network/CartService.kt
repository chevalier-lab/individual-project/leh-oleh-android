package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantCart
import id.co.leholeh.mobile.customer.model.cart.DataAddCart
import id.co.leholeh.mobile.customer.model.cart.DataDeleteCart
import id.co.leholeh.mobile.customer.model.cart.OrderCheckout
import id.co.leholeh.mobile.customer.model.cart.ResponseListCart
import retrofit2.Call
import retrofit2.http.*

// Updated
interface CartService {
    @GET(ConstantCart.BASE_URL_LIST_CART)
    suspend fun getListCart(
        @Header("Authorization") authorization: String
    ): ResponseListCart

    @POST(ConstantCart.BASE_URL_ADD_CART)
    fun addCart(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<DataAddCart>

    @PUT(ConstantCart.BASE_URL_QTY_CART)
    fun updateQty(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject,
        @Path("id") id: String
    ): Call<ResponseListCart>

    @DELETE(ConstantCart.BASE_URL_DELETE_CART)
    fun deleteCart(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<DataDeleteCart>

    @POST(ConstantCart.BASE_URL_CHECKOUT_CART)
    fun checkoutCart(
        @Header("Authorization") authorization: String,
        @Body json: JsonObject
    ): Call<OrderCheckout>
}