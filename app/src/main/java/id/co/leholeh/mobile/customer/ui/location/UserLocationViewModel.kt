package id.co.leholeh.mobile.customer.ui.location

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.shipper.location.ResponseAreas
import id.co.leholeh.mobile.customer.model.shipper.location.ResponseCities
import id.co.leholeh.mobile.customer.model.shipper.location.ResponseSuburbs
import id.co.leholeh.mobile.customer.model.shipper.location.RowsItemProvince
import id.co.leholeh.mobile.customer.model.userlocation.DataUserLocation
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.shipper.LocationService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class UserLocationViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()

    val status: LiveData<ApiStatus>
        get() = _status

    private val apiServiceGuestLocation = ApiService().service
    private val serviceShipperLocation: LocationService by lazy {
        ApiService().createServiceShipper(LocationService::class.java)
    }

    // REQUEST USER LOCATION LIST
    private val _listLocation = MutableLiveData<DataUserLocation>()
    val listLocation: LiveData<DataUserLocation>
        get() = _listLocation

    private suspend fun requestListLocation(auth: String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _listLocation.postValue(apiServiceGuestLocation.getUserLocation(auth))
            Log.i(USER_LOCATION_TAG, "${apiServiceGuestLocation.getUserLocation(auth).data}")
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(USER_LOCATION_TAG, "${e.message} --- ${e.printStackTrace()}")
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getUserListLocation(auth: String) {
        viewModelScope.launch { requestListLocation(auth) }
    }

    // REQUEST ALL AVAILABLE PROVINCE FROM SHIPPER
    private val _listShipperProvince = MutableLiveData<List<RowsItemProvince>>()

    val listShipperProvince: LiveData<List<RowsItemProvince>>
        get() = _listShipperProvince

    private suspend fun requestListProvince(apiKey: String) =
        try {
            _status.postValue(ApiStatus.LOADING)
            _listShipperProvince.postValue(serviceShipperLocation.getProvince(apiKey).data!!.rows!!)

            Log.i(
                SHIPPER_PROVINCE_TAG,
                "${serviceShipperLocation.getProvince(apiKey).data}"
            )
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(SHIPPER_PROVINCE_TAG, "${e.message} --- ${e.printStackTrace()}")
            _status.postValue(ApiStatus.FAILED)
        }


    fun getListProvince(apiKey: String) = viewModelScope.launch { requestListProvince(apiKey) }

    // REQUEST ALL AVAILABLE CITIES FROM SHIPPER
    private val _listShipperCities = MutableLiveData<ResponseCities>()
    val listShipperCities: LiveData<ResponseCities>
        get() = _listShipperCities

    private suspend fun requestListCities(apiKey: String, provinceID: Int) =
        try {
            _status.postValue(ApiStatus.LOADING)
            _listShipperCities.postValue(serviceShipperLocation.getCities(apiKey, provinceID))

            Log.i(
                SHIPPER_CITY_TAG,
                "${serviceShipperLocation.getCities(apiKey, provinceID).data}"
            )
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(SHIPPER_CITY_TAG, "${e.message} --- ${e.printStackTrace()}")
            _status.postValue(ApiStatus.FAILED)
        }

    fun getListCities(apiKey: String, provinceID: Int) {
        viewModelScope.launch { requestListCities(apiKey, provinceID) }
    }

    // REQUEST ALL AVAILABLE SUBURBS FROM SHIPPER
    private val _listShipperSuburbs = MutableLiveData<ResponseSuburbs>()
    val listShipperSuburbs: LiveData<ResponseSuburbs>
        get() = _listShipperSuburbs

    private suspend fun requestListSuburbs(apiKey: String, cityID: Int) =
        try {
            _status.postValue(ApiStatus.LOADING)
            _listShipperSuburbs.postValue(serviceShipperLocation.getSuburbs(apiKey, cityID))

            Log.i(
                SHIPPER_CITY_TAG,
                "${serviceShipperLocation.getSuburbs(apiKey, cityID).data}"
            )
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(SHIPPER_SUBURBS_TAG, "${e.message} --- ${e.printStackTrace()}")
            _status.postValue(ApiStatus.FAILED)
        }

    fun getListSuburbs(apiKey: String, cityID: Int) {
        viewModelScope.launch { requestListSuburbs(apiKey, cityID) }
    }

    // REQUEST ALL AVAILABLE AREAS FROM SHIPPER
    private val _listShipperAreas = MutableLiveData<ResponseAreas>()

    val listShipperAreas: LiveData<ResponseAreas>
        get() = _listShipperAreas

    private suspend fun requestListAreas(apiKey: String, suburbID: Int) =
        try {
            _status.postValue(ApiStatus.LOADING)
            _listShipperAreas.postValue(serviceShipperLocation.getAreas(apiKey, suburbID))

            Log.i(
                SHIPPER_AREAS_TAG,
                "${serviceShipperLocation.getProvince(apiKey).data}"
            )
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(SHIPPER_AREAS_TAG, "${e.message} --- ${e.printStackTrace()}")
            _status.postValue(ApiStatus.FAILED)
        }


    fun getListAreas(apiKey: String, suburbID: Int) {
        viewModelScope.launch { requestListAreas(apiKey, suburbID) }
    }

    companion object {
        private const val USER_LOCATION_TAG = "User Location"
        private const val SHIPPER_PROVINCE_TAG = "Shipper Province"
        private const val SHIPPER_CITY_TAG = "Shipper Province"
        private const val SHIPPER_SUBURBS_TAG = "Shipper Province"
        private const val SHIPPER_AREAS_TAG = "Shipper Province"
    }
}