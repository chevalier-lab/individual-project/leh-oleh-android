package id.co.leholeh.mobile.customer.model

data class ProfileMenuItem(
    var label : String,
    var icon : Int
)