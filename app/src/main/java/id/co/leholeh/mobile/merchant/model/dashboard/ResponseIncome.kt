package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class ResponseIncome(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataIncome? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataIncome(

	@field:SerializedName("income_this_year")
	val incomeThisYear: Int? = null,

	@field:SerializedName("income_details")
	val incomeDetails: List<IncomeDetailsItem>? = null,

	@field:SerializedName("income_total")
	val incomeTotal: Int? = null,

	@field:SerializedName("income_today")
	val incomeToday: Int? = null,

	@field:SerializedName("income_this_month")
	val incomeThisMonth: Int? = null
)

data class MonthsDataItem(

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null
)

data class IncomeDetailsItem(

	@field:SerializedName("months")
	val months: List<MonthsItem>? = null,

	@field:SerializedName("year")
	val year: Int? = null
)

data class MonthsItem(

	@field:SerializedName("month")
	val month: Int? = null,

	@field:SerializedName("data")
	val data: List<MonthsDataItem>?
)
