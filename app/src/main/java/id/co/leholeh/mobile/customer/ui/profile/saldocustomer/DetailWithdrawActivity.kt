package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityDetailWithdrawBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.wallet.Withdraw
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import java.text.SimpleDateFormat

class DetailWithdrawActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailWithdrawBinding
    private lateinit var itemDataWithdraw: Withdraw
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val viewModelBanks: BanksViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(BanksViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailWithdrawBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setuptoolbar
        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            itemDataWithdraw = intent.getSerializableExtra(INTENT_DETAIL) as Withdraw
            //InitUI
            initUI()
        }
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Detail Withdraw"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    //initUI
    @SuppressLint("SimpleDateFormat")
    fun initUI() {
        //UI Bank
        when (itemDataWithdraw.status) {
            "0" -> {
                binding.tvStatus.setBackgroundResource(R.drawable.status_refund)
                binding.tvStatus.text = getString(R.string.failed)
                binding.tvKeteranganStatus.visibility= View.GONE
            }
            "1" -> {
                binding.tvStatus.setBackgroundResource(R.drawable.status_selesai)
                binding.tvStatus.text = getString(R.string.success)
                binding.tvKeteranganStatus.visibility= View.GONE
            }
            "2" -> {
                binding.tvStatus.setBackgroundResource(R.drawable.status_diproses)
                binding.tvStatus.text = getString(R.string.withdrawmemunggu)
            }
        }
        binding.tvBankAtas.text = itemDataWithdraw.bankName
        binding.tvNominal.text = changeToRupiah(itemDataWithdraw.balanceRequest!!.toDouble())
        val tglTransaksi =
            SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(itemDataWithdraw.createdAt!!)
        binding.textViewTglTransaksi.text =
            SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTransaksi!!)

        binding.tvBank.text = itemDataWithdraw.bankName
        binding.tvJumlahTransfer.text = changeToRupiah(itemDataWithdraw.balanceRequest!!.toDouble())

        //UI Pembayaran
        viewModelBanks.getDataDetailBank(auth.token!!, itemDataWithdraw.idUUserBankAccount!!)
        viewModelBanks.detailBanksAccount.observe(this, Observer {
            binding.tvVirtualAccount.text = it.accountNumber
            binding.tvAkunPemilikBank.text = it.accountName
        })
    }
}