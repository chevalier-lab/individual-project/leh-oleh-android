package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentMerchantTambahLokasiBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.barang.BarangTemporary
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_category.view.*


class MerchantTambahLokasiFragment : Fragment() {
    private lateinit var binding: FragmentMerchantTambahLokasiBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private lateinit var adapterTextLocation: AdapterUtil<String>
    private var locationsId = arrayListOf<String>()
    private var locationsString: ArrayList<String> = arrayListOf()
    private val items: ArrayList<LocationProduct> = arrayListOf()
    private val itemsLocation: ArrayList<String> = arrayListOf()
    private val viewModelProductList: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }
    private var barangTemporary = BarangTemporary()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMerchantTambahLokasiBinding.inflate(inflater, container, false)
        //Auth Token
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        auth = getAuth(cacheUtil)
        return binding.root
    }

    fun setDataBarang(barangTemp : BarangTemporary){
        barangTemporary = barangTemp
    }

    fun getDataBarang() : BarangTemporary {
        barangTemporary.lokasiId = locationsId
        barangTemporary.lokasiString = locationsString
        return barangTemporary
    }

    override fun onResume() {
        super.onResume()
        setUpDataLocation()
    }

    fun isLocationNotNull(): Boolean {
        return if(locationsId.size > 0){
            true
        }else{
            Toast.makeText(requireContext(), "Lokasi minimal 1", Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun setUpDataLocation() {
        Log.d("SETUPDATALOCATION", "DIPANGGIL NIH AKU")
        //set recycler categories

        viewModelProductList.getLocation()
        viewModelProductList.location.observe(viewLifecycleOwner, Observer {
            items.clear()
            itemsLocation.clear()
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsLocation.add(it.data[item].provinceName!!)
            }
        })
        adapterTextLocation = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { _, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                locationsString.remove(item)
                locationsId.removeAt(position)
                adapterTextLocation.refresh()
            })

        val adapterLocation =
            ArrayAdapter(requireContext(), R.layout.item_list_dropdown_text, itemsLocation)
        binding.dropdownLokasi.setAdapter(adapterLocation)
        binding.dropdownLokasi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                for (i in items.indices) {
                    if (binding.dropdownLokasi.text.toString() == items[i].provinceName) {
                        var kondisi = true
                        if (locationsString.size > 0) {
                            for (j in locationsString.indices) {
                                if (binding.dropdownLokasi.text.toString() == locationsString[j]) {
                                    kondisi = false
                                }
                            }
                            if (kondisi) {
                                locationsId.add(items[i].id!!.toInt().toString())
                                locationsString.add(items[i].provinceName!!)
                            }
                        } else {
                            locationsString.add(items[i].provinceName!!)
                            locationsId.add(items[i].id!!.toInt().toString())
                        }
                        if (locationsString.size > 0) adapterTextLocation.data = locationsString
                        break
                    }
                }
            }
        })

        binding.rvLokasi.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvLokasi.adapter = adapterTextLocation
        if (locationsString.size > 0) adapterTextLocation.data = locationsString
    }

}