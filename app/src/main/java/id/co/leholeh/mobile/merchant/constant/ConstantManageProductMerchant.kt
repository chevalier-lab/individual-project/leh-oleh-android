package id.co.leholeh.mobile.merchant.constant

object ConstantManageProductMerchant {
    const val BASE_URL_PRODUCTS_MERCHANT = "merchant/manage/products"
    const val BASE_URL_GET_PRODUCT_MERCHANT = "merchant/manage/products/{id}"
    const val BASE_URL_PRODUCT_PHOTOS = "merchant/manage/products/image"
    const val BASE_URL_PRODUCT_DIMENSION = "merchant/product-info"
    const val BASE_URL_DETAIL_PRODUCT_DIMENSION = "merchant/product-info/{id}"
    const val BASE_URL_REMOVE_PRODUCT = "merchant/manage/products/{id}/remove"
    const val BASE_URL_PRODUCT_CATEGORIES_MERCHANT = "merchant/manage/products/{id}/categories"

    const val BASE_URL_PRODUCT_LOCATION_MERCHANT = "merchant/manage/products/locations/{id}"
    const val BASE_URL_PRODUCT_LOCATION_UPDATE_MERCHANT = "merchant/manage/products/{id}/location"
    const val BASE_URL_PRODUCT_LOCATION_DELETE_MERCHANT = "merchant/manage/products/locations/{id}"
    const val BASE_URL_PRODUCT_PHOTO_DELETE_MERCHANT = "merchant/manage/products/{id}/image"
    const val BASE_URL_PRODUCT_COVER_UPDATE_MERCHANT = "merchant/manage/products/{id}/cover"
    const val BASE_URL_PRODUCT_LOCATION_CREATE_MERCHANT = "merchant/manage/products/locations"
    const val BASE_URL_PRODUCT_DISCOUNT_UPDATE_MERCHANT = "merchant/manage/products/{id}/discount"

    const val ID_PRODUCT_MERCHANT = "id_product_merchant"
}