package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class RequestProof(

    @field:SerializedName("is_visible")
    val isVisible: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null
)