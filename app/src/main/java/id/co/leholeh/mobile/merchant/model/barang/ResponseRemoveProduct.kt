package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ResponseRemoveProduct(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Data(
	val any: Any? = null
)
