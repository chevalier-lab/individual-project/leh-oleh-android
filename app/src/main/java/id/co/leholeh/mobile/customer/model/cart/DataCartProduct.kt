package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class DataCartProduct(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: CartProduct? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)