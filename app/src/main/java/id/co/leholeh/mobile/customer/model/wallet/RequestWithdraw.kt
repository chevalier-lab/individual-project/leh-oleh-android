package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class RequestWithdraw(

    @field:SerializedName("account_number")
    val accountNumber: String? = null,

    @field:SerializedName("note")
    val note: String? = null,

    @field:SerializedName("balance_request")
    val balanceRequest: String? = null,

    @field:SerializedName("id_u_user_wallet")
    val idUUserWallet: String? = null,

    @field:SerializedName("wallet_name")
    val walletName: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("id_u_user_bank_account")
    val idUUserBankAccount: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("account_name")
    val accountName: String? = null,

    @field:SerializedName("bank_name")
    val bankName: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("wallet_description")
    val walletDescription: String? = null
)