package id.co.leholeh.mobile.merchant.adapter

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.co.leholeh.R
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.FinishPesananFragment
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.ProsesPesananFragment
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.RefundPesananFragment
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.VerifiedPesananFragment

class SectionsPagerAdapterPesanan(private val context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    @StringRes
    private val TAB_TITLES = intArrayOf(R.string.payment_verified, R.string.process, R.string.finish, R.string.refund)

    override fun getItem(position: Int): Fragment {
        var fragment : Fragment? = null
        when(position){
            0-> fragment =
                VerifiedPesananFragment()
            1-> fragment =
                ProsesPesananFragment()
            2-> fragment =
                FinishPesananFragment()
            3-> fragment =
                RefundPesananFragment()
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return TAB_TITLES.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }

}