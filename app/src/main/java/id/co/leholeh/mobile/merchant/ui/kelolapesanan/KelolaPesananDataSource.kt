package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.transaction.ListTransaction
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class KelolaPesananDataSource(coroutineContext : CoroutineContext, private val authorization: String, private val status : String) : PageKeyedDataSource<Int, ListTransaction>(){

    private val apiServiceTransaction: TransactionService by lazy {
        ApiService().createService(TransactionService::class.java)
    }
    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)
    val apiState = MutableLiveData<ApiStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ListTransaction>
    ) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiServiceTransaction.getListTranscationMerchant(authorization, FIRST_PAGE, status, "id", "DESC")
                val responseItem = response.body()!!.data
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, null, FIRST_PAGE + 1)
                        }
                    }
                }

            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_LST_PSNN_MRCHNT", e.localizedMessage!!)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ListTransaction>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiServiceTransaction.getListTranscationMerchant(authorization, params.key, status, "id", "DESC")
                val responseItem = response.body()!!.data
                val key = if(params.key > 0 ) params.key - 1 else -1
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                }
            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_LST_PSNN_MRCHNT", e.localizedMessage!!)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ListTransaction>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiServiceTransaction.getListTranscationMerchant(authorization, params.key, status, "id", "DESC")
                val responseItem = response.body()!!.data
                val key = params.key + 1
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_AFTER)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                }

            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d("ERR_REQ_LST_PSNN_MRCHNT", e.localizedMessage!!)
            }
        }
    }

    companion object {
        const val FIRST_PAGE = 0
    }

}