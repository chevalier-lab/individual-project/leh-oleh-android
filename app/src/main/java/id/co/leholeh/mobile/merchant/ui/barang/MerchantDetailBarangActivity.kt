package id.co.leholeh.mobile.merchant.ui.barang

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Paint
import android.os.*
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.*
import id.co.leholeh.mobile.customer.util.SliderAdapterUtil
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.*
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_category.view.*
import kotlinx.android.synthetic.main.slider_component.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.lang.StringBuilder

class MerchantDetailBarangActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMerchantDetailBarangBinding
    private lateinit var someHandler: Handler
    private lateinit var sliderAdapter: SliderAdapterUtil<String>
    private var sliderIndicators = mutableListOf<ImageView>()
    private lateinit var adapterTextCategory: AdapterUtil<String>
    private var sliderCount = 0
    private var id: Int? = null
    private val viewModelKelolaBarang: KelolaBarangViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(KelolaBarangViewModel::class.java)
    }
    private val viewModelProductList: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }
    val categoriesArray = JsonArray()
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private var oldCategories = arrayListOf<CategoriesItemDetailMerchant>()

    val valueLocation: ArrayList<String> = arrayListOf()
    private lateinit var adapterTextLocation: AdapterUtil<String>
    val locationArray = arrayListOf<String>()
    private val itemsOldLocation: ArrayList<DataLocationItem> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantDetailBarangBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        id = (intent.getStringExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT) as String).toInt()
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            setupToolbar()
            binding.viewPagerDetailProduct.addOnPageChangeListener(handler)
            loadDataDetailProduct()
            startAutoBanner()

        }
    }

    private fun setupToolbar() {
        setSupportActionBar(binding.toolbarDetailProduk)
        binding.collapsingToolbarDetailProduct.setCollapsedTitleTextColor(
            ContextCompat.getColor(this, android.R.color.white)
        )
        binding.collapsingToolbarDetailProduct.setExpandedTitleColor(Color.TRANSPARENT)

        supportActionBar!!.title = "Banana Banang"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_kelola_barang, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_ubah_barang -> {
                val intent = Intent(this, MerchantUbahBarangActivity::class.java)
                intent.putExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, id)
                startActivity(intent)
            }
            R.id.menu_atur_foto_barang -> {
                val intent = Intent(this, MerchantAturFotoActivity::class.java)
                intent.putExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, id)
                startActivity(intent)
            }
            R.id.menu_atur_kategori_barang -> {
                showDialogAturKategoriBarang(
                    getString(R.string.atur_kategori_barang),
                    getString(R.string.pilih_kategori)
                )
            }
            R.id.menu_atur_lokasi_barang -> {
                showDialogAturLokasiBarang(
                    getString(R.string.atur_lokasi_barang),
                    getString(R.string.pilih_lokasi)
                )
            }
            R.id.menu_hapus_barang -> {
                showDialogHapusBarang()
            }
            R.id.menu_discount_barang -> {
                showEditDiscountBarang()
            }
            R.id.menu_edit_cover -> {
                ImagePicker.create(this)
                    .single()
                    .start()
            }
        }
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun loadDataDetailProduct() {
        //        Location Product Merchant
        viewModelKelolaBarang.getDataLocationsProduct(auth.token!!, id!!)
        viewModelKelolaBarang.locationProduct.observe(this, Observer { location ->
            itemsOldLocation.clear()
            for (item in location.indices) {
                itemsOldLocation.add(location[item])
            }
        })

        //Slider
        val listSlider = arrayListOf<String>()
        Log.d("ID PRODUCT", id.toString())
        viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
        viewModelKelolaBarang.detailProduct.observe(this, Observer {
            binding.tvNamaProduk.text = it.productName
            binding.tvDetailDeksripsi.text = it.description

            val hargaDiskon =
                it.priceSelling!!.toDouble() - (it.priceSelling.toDouble() * it.discount!!.toDouble() / 100.0)
            if(it.discount != "0"){
                binding.tvPriceReal.visibility = View.VISIBLE
                binding.tvJumlahDiscount.visibility = View.VISIBLE
                binding.tvPriceReal.paintFlags =
                    binding.tvPriceReal.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                binding.tvPriceReal.text = changeToRupiah(it.priceSelling.toDouble())
                binding.tvJumlahDiscount.text = "-${it.discount}%"
                binding.tvPriceTotal.text = changeToRupiah(hargaDiskon)
            }else{
                binding.tvPriceReal.visibility = View.INVISIBLE
                binding.tvJumlahDiscount.visibility = View.INVISIBLE
                binding.tvPriceTotal.text = changeToRupiah(
                    it.priceSelling.toDouble()
                )
            }

            val locationStr = StringBuilder()

            it.location?.apply {
                for (index in this.indices) {
                    if (index == this.size - 1) {
                        locationStr.append(this[index].provinceName!!)
                    } else {
                        locationStr.append(this[index].provinceName!!).append(", ")
                    }
                }
            }
            binding.textViewLocation.text = locationStr.toString()


            val categoriesStr = StringBuilder()
            oldCategories.clear()
            it.categories?.apply {
                oldCategories.addAll(it.categories)
                for (index in this.indices) {
                    Log.d("DATA_CATEGORIES_CHANGE", "${it.categories[index].category}")
                    if (index == this.size - 1) {
                        categoriesStr.append(this[index].category!!)
                    } else {
                        categoriesStr.append(this[index].category!!).append(", ")
                    }
                }
            }
            binding.tvBookmarks.text = categoriesStr.toString()

            it.info?.apply{
                binding.tvPanjang.text = "Panjang: ${it.info.length} cm"
                binding.tvTinggi.text = "Tinggi: ${it.info.height} cm"
                binding.tvLebar.text = "Lebar: ${it.info.width} cm"
                binding.tvBerat.text = "Berat: ${it.info.weight} kg"
            }

            listSlider.clear()
            sliderIndicators.clear()
            binding.sliderIndicator.removeAllViews()

            if (listSlider.isEmpty()) {
                listSlider.add(it.uri!!)
            }
            it.photos?.apply {
                for (element in this.indices) {
                    listSlider.add(this[element].uri!!)
                }
            }
            Log.d("SizePhoto", listSlider.size.toString() + " , Isi : " + listSlider.toString())
            sliderAdapter =
                SliderAdapterUtil(R.layout.slider_component,
                    this,
                    arrayListOf(),
                    { itemView, item, position ->
                        Glide.with(this).load(item)
                            .into(itemView.imageViewBannerRekomendasi)
                    },
                    { item, position ->

                    })
            if (listSlider.size > 0) sliderAdapter.data = listSlider
            binding.viewPagerDetailProduct.adapter = sliderAdapter

            sliderCount = sliderAdapter.count
            //initiate slider indicator

            for (i in 0 until listSlider.size) {
                sliderIndicators.add(ImageView(this))
                this.sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(8, 0, 8, 0)
                if (this.sliderIndicators[i].parent != null) {
                    (this.sliderIndicators[i].parent as ViewGroup).removeView(this.sliderIndicators[i])
                }
                binding.sliderIndicator.addView(this.sliderIndicators[i], params)
            }
            sliderIndicators[0].setImageResource(R.drawable.shape_indicator_active)
        })
    }

    //handler slider
    private val handler = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            for (i in 0 until sliderCount)
                sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
            sliderIndicators[position].setImageResource(R.drawable.shape_indicator_active)
        }
    }

    private fun startAutoBanner() {
        var currentPage = 0
        someHandler = Handler(Looper.getMainLooper())
        someHandler.postDelayed(object : Runnable {
            override fun run() {
                if (currentPage == sliderCount) {
                    currentPage = 0
                }
                binding.viewPagerDetailProduct.setCurrentItem(currentPage++, true)
                someHandler.postDelayed(this, 3000)
            }
        }, 3000)
    }

    //3 parameters, param 3 should be dinamyc array list
    private fun showDialogAturLokasiBarang(title: String, formLabel: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_atur_kategori_barang)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogAturKategoriBarangBinding.inflate(layoutInflater)
        setDataLocation(binding, dialog)
        binding.buttonSubmit.setOnClickListener {
            dialog.dismiss()
        }

        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        binding.textViewTitle.text = title
        binding.textViewPilih.text = formLabel
    }

    private fun showDialogAturKategoriBarang(title: String, formLabel: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_atur_kategori_barang)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogAturKategoriBarangBinding.inflate(layoutInflater)
        setDataCategories(binding)
        binding.buttonSubmit.setOnClickListener {
            sendRequestupdateCategoriesProduct(dialog)
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        binding.textViewTitle.text = title
        binding.textViewPilih.text = formLabel
        binding.rvCategories.adapter = adapterTextCategory
        dialog.show()
    }

    //should get id of barang when delete it
    private fun showDialogHapusBarang() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_hapus_barang)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogHapusBarangBinding.inflate(layoutInflater)
        binding.buttonPositive.setOnClickListener {
            dialog.dismiss()
            sendRequestDeleteProduct()
        }
        binding.buttonNegative.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    private fun setDataCategories(binding: CustomDialogAturKategoriBarangBinding) {
        val tempCategories: ArrayList<Category> = arrayListOf()
        val categoriesString: ArrayList<String> = arrayListOf()
        val currentCategories = arrayListOf<String>()


        viewModelProductList.getCategories()
        viewModelProductList.categories.observe(this, Observer {
            tempCategories.clear()
            categoriesString.clear()
            for (item in it.data?.indices!!) {
                tempCategories.add(it.data[item])
                categoriesString.add(it.data[item].category!!)
            }
        })

        //set recycler categories
        adapterTextCategory = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { position, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                currentCategories.remove(item)
                categoriesArray.remove(position)
                adapterTextCategory.refresh()
                Log.d("CATEGORIES_CRNT_DEL", "$currentCategories")
                Log.d("CATEGORIES_STR_DEL", "$categoriesString")
                Log.d("CATEGORIES_ARR_DEL", "$categoriesArray")
            })

        categoriesArray.removeAll { true }
        Log.d("CATEGORIES_OLD", "DATA $currentCategories")
        if(oldCategories.isNotEmpty() && currentCategories.isEmpty()){
            Log.d("CATEGORIES_OLD", "TRUE $currentCategories")
            for (i in oldCategories.indices){
                categoriesArray.add(oldCategories[i].idMCategories)
                currentCategories.add(oldCategories[i].category!!)
            }
            adapterTextCategory.data = currentCategories
        }

        Log.d("CATEGORIES_RECYCLER", "$currentCategories")
        Log.d("CATEGORIES_ARR_REC", "$categoriesArray")

        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, categoriesString)
        binding.dropdownCategories.setAdapter(adapterCategory)
        binding.dropdownCategories.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                for (i in tempCategories.indices){
                    if(binding.dropdownCategories.text.toString() == tempCategories[i].category){
                        if(currentCategories.size > 0){
                            Log.d("CATEGORIES_ADA", "$currentCategories")
                            for (j in currentCategories.indices){
                                if(binding.dropdownCategories.text.toString() == currentCategories[j]){
                                    Toast.makeText(
                                        this@MerchantDetailBarangActivity,
                                        "Kategori sudah ada",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    Log.d("CATEGORIES_SAMA", "$currentCategories")
                                    break
                                }else{
                                    categoriesArray.add(tempCategories[i].id)
                                    currentCategories.add(tempCategories[i].category!!)
                                    Log.d("CATEGORIES_BEDA", "$currentCategories")
                                    break
                                }
                            }
                        }else{
                            Log.d("CATEGORIES_KOSONG", "$currentCategories")
                            categoriesArray.add(tempCategories[i].id)
                            currentCategories.add(tempCategories[i].category!!)
                        }
                        adapterTextCategory.data = currentCategories
                        break
                    }
                }

            }
        })

        binding.rvCategories.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvCategories.adapter = adapterTextCategory

    }


    private fun setDataLocation(binding: CustomDialogAturKategoriBarangBinding, dialog: Dialog) {
        val items: ArrayList<LocationProduct> = arrayListOf()
        val itemsLocation: ArrayList<String> = arrayListOf()

        viewModelProductList.getLocation()
        viewModelProductList.location.observe(this, Observer {
            itemsLocation.clear()
            items.clear()
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsLocation.add(it.data[item].provinceName!!)
            }
        })
        //set recycler categories
        adapterTextLocation = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { position, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                valueLocation.remove(item)
                Log.d("Location_Delete_Value", "$valueLocation")
                Log.d("Location_Delete", locationArray[position])
                sendRequestDeleteLocation(locationArray[position].toInt(), position)
                adapterTextLocation.refresh()
            })

        if (itemsOldLocation.isNotEmpty() && valueLocation.isEmpty()) {
            Log.d("LOCATION_OLD", "$valueLocation")
            for (item in itemsOldLocation.indices) {
                valueLocation.add(itemsOldLocation[item].provinceName!!)
                locationArray.add(itemsOldLocation[item].id.toString())
            }
        }
        if(valueLocation.size > 0) adapterTextLocation.data = valueLocation
        Log.d("LOCATION_VALUE_ADD", "$valueLocation")
        Log.d("LOCATION_ARRAY_ADD", "$locationArray")

        val adapterLocation =
            ArrayAdapter(this, R.layout.item_list_dropdown_text, itemsLocation)
        binding.dropdownCategories.setAdapter(adapterLocation)
        binding.dropdownCategories.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                Log.v("DataArrayFinal", "Data Array Sekarang $valueLocation")
                for (i in items.indices) {
                    if (binding.dropdownCategories.text.toString() == items[i].provinceName) {
                        Log.e("LOCATION_DATA_ISI", items[i].provinceName!!)
                        var kondisi = true
                        if (valueLocation.size > 0) {
                            for (j in valueLocation.indices) {
                                if (items[i].provinceName == valueLocation[j]) {
                                    kondisi=false
                                }
                            }
                            if(kondisi) {
                                locationArray.add(items[i].id!!.toInt().toString())
                                Log.d("LOCATION_DATA_TRUE", locationArray.toString())
                                Log.d("LOCATION_BEDA", "LOCATION_BEDA")
                                valueLocation.add(items[i].provinceName!!)
                                sendRequestCreateLocationMerchant(id!!,items[i].id!!.toInt())
                                Log.d("LOCATION_DATA_FINAL", "Salah Else atas $valueLocation")

                            }else{
                                Toast.makeText(
                                    this@MerchantDetailBarangActivity,
                                    "Lokasi sudah ada",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            locationArray.add(items[i].id!!.toInt().toString())
                            Log.d("LOCATION_DATA_ELSE", locationArray.toString())
                            valueLocation.add(items[i].provinceName!!)
                            sendRequestCreateLocationMerchant(id!!,items[i].id!!.toInt())
                            Log.d("LOCATION_DATA_FNL_ELSE", "Salah Else bawah $valueLocation")
                        }
                        if (valueLocation.size > 0) {
                            adapterTextLocation.data = valueLocation
                        }
                        break
                    }
                }
            }
        })

        binding.rvCategories.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvCategories.adapter = adapterTextLocation
        dialog.show()
    }

    private fun sendRequestupdateCategoriesProduct(dialog: Dialog) {
        Log.d("CATEGORIES_SEND", "$categoriesArray")
        if (categoriesArray.size() > 0) {
            val jsonObject = JsonObject()
            jsonObject.add("categories", categoriesArray)
            updateCategoriesProduct(jsonObject, {
                if (it.code == 200) {
                    Toast.makeText(
                        this,
                        "Berhasil mengubah kategori",
                        Toast.LENGTH_SHORT
                    ).show()
                    viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
                    dialog.dismiss()
                }
            }, {
                Toast.makeText(
                    this,
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            })
        } else {
            Toast.makeText(this, "Kategori minimal 1", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateCategoriesProduct(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateProductCategories) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseUpdateProductCategories> =
            apiService.updateCategoriesProductMerchant(auth.token!!, jsonObject, id!!)
        call.enqueue(object : Callback<ResponseUpdateProductCategories> {
            override fun onResponse(
                call: Call<ResponseUpdateProductCategories>,
                response: Response<ResponseUpdateProductCategories>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateProductCategories>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestDeleteProduct() {
        deleteProduct({
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    "Berhasil menghapus data",
                    Toast.LENGTH_SHORT
                ).show()
                finish()
            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun deleteProduct(
        onSuccess: (ResponseRemoveProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }

        val call: Call<ResponseRemoveProduct> = apiService.deleteProductMerchant(auth.token!!, id!!)
        call.enqueue(object : Callback<ResponseRemoveProduct> {
            override fun onResponse(
                call: Call<ResponseRemoveProduct>,
                response: Response<ResponseRemoveProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseRemoveProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    //Change Cover
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val imagePick: Image = ImagePicker.getFirstImageOrNull(data)
            try {
                val parcelFileDescriptor: ParcelFileDescriptor =
                    this.contentResolver.openFileDescriptor(imagePick.uri, "r")!!
                val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                val photoFile = createFileTemp(image)
                val photoProfile = MultipartBody.Part.createFormData(
                    "cover",
                    photoFile!!.name,
                    photoFile.asRequestBody("image/*".toMediaTypeOrNull())
                )

                setUpdatePhotoCover(photoProfile, {
                    Log.v("Cover_Upload", it.message!!)
                    if (it.code == 200) {
                        Toast.makeText(
                            this,
                            "Berhasil" + it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this,
                            "Gagal " + it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }, {
                    Toast.makeText(
                        this,
                        it,
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.v("Cover_Upload", it)

                })

                parcelFileDescriptor.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    //Change Cover Product
    private fun setUpdatePhotoCover(
        photoProfile: MultipartBody.Part,
        onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseAddPhotosProduct> =
            ApiService().createService(ManageProductService::class.java)
                .changeCoverMerchant(auth.token!!, id!!, photoProfile)

        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun createFileTemp(bitmap: Bitmap): File? {
        val file = File(
            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }


    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            Log.d("RESUME", "ON RESUME NIH")
            viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
            viewModelKelolaBarang.getDataLocationsProduct(auth.token!!, id!!)
        }
    }

    //Set Update Dicount
    //Change Cover Product

    //should get id of barang when delete it
    private fun showEditDiscountBarang() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_edit_discount)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogEditDiscountBinding.inflate(layoutInflater)
        viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
        viewModelKelolaBarang.detailProduct.observe(this, Observer {
            binding.etDiscount.setText(it?.discount)
        })

        binding.buttonPositive.setOnClickListener {
            if (binding.etDiscount.text.toString() != "") {
                sendRequestUpdateDiscount(binding.etDiscount.text.toString().toInt())
                dialog.dismiss()
            } else {
                Toast.makeText(
                    this,
                    "Isi Data Discount!",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    private fun updateDiscountProduct(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateLocationProductMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseUpdateLocationProductMerchant> =
            apiService.setDiscountProduct(auth.token!!, id, jsonObject)

        call.enqueue(object : Callback<ResponseUpdateLocationProductMerchant> {
            override fun onResponse(
                call: Call<ResponseUpdateLocationProductMerchant>,
                response: Response<ResponseUpdateLocationProductMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(
                call: Call<ResponseUpdateLocationProductMerchant>,
                t: Throwable
            ) {
                onFailed(t.message.toString())
            }
        })
    }

    //Change Cover Product Function
    private fun sendRequestUpdateDiscount(jumlahDiskon: Int) {
        val json = JsonObject()
        json.addProperty("discount", jumlahDiskon)
        updateDiscountProduct(json, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("Discount", "Berhasil" + it.message!!)
                viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
            } else {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("Disount", "Else Atas" + it.message!!)

            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    //Hapus Location Product
    private fun removeLocationProduct(
        id: Int, onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseAddPhotosProduct> =
            apiService.removeProductLocation(auth.token!!, id)

        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })
    }

    //Send Request Remove Product
    //Change Cover Product Function
    private fun sendRequestDeleteLocation(idLocation: Int, position: Int) {

        removeLocationProduct(idLocation, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                locationArray.removeAt(position)
                viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
                Log.d("Remove_location", "Berhasil" + it.message!!)

            } else {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("Remove_location", "Else Atas" + it.message!!)

            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()

        })

    }


    //Create Location Product
    private fun sendRequestCreateLocationMerchant(idProduct: Int, idLocation: Int) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("id_m_locations", idLocation)
        jsonObject.addProperty("id_u_product", idProduct)

        createLocation(jsonObject, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
            } else {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })

    }

    private fun createLocation(
        jsonObject: JsonObject,
        onSuccess: (ResponseCreateLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseCreateLocation> = apiService.createProductLocation(
            auth.token!!, jsonObject
        )
        call.enqueue(object : Callback<ResponseCreateLocation> {
            override fun onResponse(
                call: Call<ResponseCreateLocation>,
                response: Response<ResponseCreateLocation>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateLocation>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })
    }

}