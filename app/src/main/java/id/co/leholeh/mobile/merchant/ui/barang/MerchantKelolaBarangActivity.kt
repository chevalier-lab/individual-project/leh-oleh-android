package id.co.leholeh.mobile.merchant.ui.barang

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantKelolaBarangBinding
import id.co.leholeh.databinding.ComponentSeachingDialogBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.model.merchant.ProductsByMerchant
import id.co.leholeh.mobile.customer.ui.merchantcustomer.MerchantPreviewViewModel
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.ProductItemMerchant
import id.co.leholeh.mobile.merchant.ui.barang.tambah.MerchantTambahParentActivty
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.merchant.ui.profile.ProfileTokoViewModel
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_produk.view.*

class MerchantKelolaBarangActivity : AppCompatActivity() {

    private lateinit var productListAdapter: PagedAdapterUtil<ProductItemMerchant>
    private lateinit var productFilteredAdapter: PagedAdapterUtil<ProductsByMerchant>
    lateinit var binding : ActivityMerchantKelolaBarangBinding
    private val viewModelKelolaBarang : KelolaBarangViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(KelolaBarangViewModel::class.java)
    }
    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private val viewModelCategories: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }
    private val viewModelProductList: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }

    private val viewModelProductMerchantFilter: MerchantPreviewViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MerchantPreviewViewModel::class.java)
    }

    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth : Login
    private lateinit var idMerchant: String
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityMerchantKelolaBarangBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        else{
            auth = getAuth(cacheUtil)
            if(intent.getStringExtra(INTENT_DETAIL) != null){
                idMerchant = intent.getStringExtra(INTENT_DETAIL) as String
            }
            getListProductMerchant()
            //setup Toolbar
            setupToolbar()

            //check Location
            locationNullCheck()

            //add barang
            binding.floatingActionButtonTambahProduk.setOnClickListener {
                startActivity(Intent(this, MerchantTambahParentActivty::class.java))
            }

            binding.buttonClearFilter.setOnClickListener {
                getListProductMerchant()
                binding.buttonClearFilter.visibility = View.GONE
            }
        }
    }

    //check location null
    private fun locationNullCheck(){
        viewModel.getDataProfileToko(auth.token!!)
        viewModel.toko.observe(this , Observer { profileToko ->
            if (profileToko.location == null) {
                binding.tvAlert.visibility = View.VISIBLE
                binding.tvAlert.setOnClickListener {
                    startActivity(
                        Intent(
                            this,
                            MerchantLocationActivity::class.java
                        ).putExtra(INTENT_DETAIL, profileToko.id)
                            .putExtra(INTENT_SEARCH, auth.token)
                    )
                }
            }
        })
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Kelolah Barang"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        return true
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.menu_search -> {
            //setup dialog searching
            showDialogFilterProduct()
            true
        }

        else -> super.onOptionsItemSelected(item)
    }

    //show dialog
    private fun showDialogFilterProduct(){
        val categoriesTemp: ArrayList<Category> = arrayListOf()
        val categoriesString: ArrayList<String> = arrayListOf()
        val locationTemp: ArrayList<LocationProduct> = arrayListOf()
        val locationString: ArrayList<String> = arrayListOf()
        var pickedIdCategory = ""
        var pickedIdLocation = ""


        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_seaching_dialog)
        val layoutInflater = LayoutInflater.from(this)
        val binding = ComponentSeachingDialogBinding.inflate(layoutInflater)

        viewModelCategories.getCategories()
        viewModelCategories.categories.observe(this, Observer {
            categoriesTemp.clear()
            categoriesString.clear()
            for (item in it.data?.indices!!) {
                categoriesTemp.add(it.data[item])
                categoriesString.add(it.data[item].category!!)
            }
        })
        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, categoriesString)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                for (i in categoriesTemp.indices) {
                    if (binding.dropdownMakanan.text.toString() == categoriesTemp[i].category) {
                        pickedIdCategory = categoriesTemp[i].id!!
                        break
                    }
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        viewModelProductList.getLocation()
        viewModelProductList.location.observe(this, Observer {
            locationTemp.clear()
            locationString.clear()
            for (item in it.data?.indices!!) {
                locationTemp.add(it.data[item])
                locationString.add(it.data[item].provinceName!!)
            }
        })

        val adapterLocation =  ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, categoriesString)
        binding.dropdownLokasi.setAdapter(adapterLocation)
        binding.dropdownLokasi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                for (i in locationTemp.indices) {
                    if (binding.dropdownLokasi.text.toString() == locationTemp[i].provinceName) {
                        pickedIdLocation = locationTemp[i].id!!
                        break
                    }
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })


        var orderDirection = "ASC"
        //set dropdown urutan
        val itemsUrutan: ArrayList<String> = arrayListOf("ASC", "DESC")
        val arrayUrutan =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsUrutan)
        binding.dropdownUrutkan.setAdapter(arrayUrutan)
        binding.dropdownUrutkan.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                orderDirection = binding.dropdownUrutkan.text.toString()
            }

        })
        binding.tvCari.setOnClickListener {
            val searchQuery = binding.etKeyword.text.toString()
            Log.d("DATA FILTER", "$pickedIdCategory, $pickedIdLocation, $searchQuery, $orderDirection")
            setFilterProduct(pickedIdCategory, pickedIdLocation, searchQuery, orderDirection, dialog)
        }
        binding.tvBatal.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun getListProductMerchant(){
        viewModelKelolaBarang.setUpListMerchantProduct(auth.token!!)
        observerProductMerchant()

        productListAdapter =
            PagedAdapterUtil(
                R.layout.item_produk,
                { position, itemView, item ->
                    itemView.textViewNamaProduk.text = item.productName
//                itemView.textViewKategoriProduk.
                    val hargaDiskon =
                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                    itemView.textViewHargaDiskon.text =
                        changeToRupiah(
                            hargaDiskon
                        )
                    if (item.discount.toString().toDouble() > 0.0) {
                        itemView.textViewHargaAsliProduk.paintFlags =
                            itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        itemView.textViewHargaAsliProduk.text =
                            changeToRupiah(item.priceSelling.toDouble())

                    } else {
                        itemView.textViewHargaAsliProduk.visibility = View.INVISIBLE
                    }
                    itemView.textViewHargaAsliProduk.paintFlags =
                        itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    Glide.with(this).load(item.uri).into(itemView.imageViewProduk)

                    itemView.btBeliSekarang.text = "Lihat Detail"

                    //Categories
                    var categories = ""

                    for (i in item.categories!!.indices) {
                        categories += if (i < item.categories.size - 1) {
                            item.categories[i].category + ", "
                        } else {
                            item.categories[i].category
                        }
                    }
                    itemView.textViewKategoriProduk.text = categories



                },
                { _, item ->
                    val intent = Intent(
                        this@MerchantKelolaBarangActivity,
                        MerchantDetailBarangActivity::class.java
                    )
                    intent.putExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, item.id)
                    startActivity(intent)
                })

        binding.recyclerListProduct.layoutManager = GridLayoutManager(this@MerchantKelolaBarangActivity, 2)
        binding.recyclerListProduct.adapter = productListAdapter

    }

    private fun observerProductMerchant(){
        viewModelKelolaBarang.getDataListMerchantProduct().observe(this, Observer {
            productListAdapter.submitList(it)
        })

        viewModelKelolaBarang.statusMerchantProduct.observe(this, Observer {
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBarMerchantProduct,
                binding.recyclerListProduct,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Produk Kosong",
                R.drawable.ic_empty_cart
            )
        })
    }
    @SuppressLint("SetTextI18n")
    private fun getFilteredProducts(){
        productFilteredAdapter =  PagedAdapterUtil(
            R.layout.item_produk,
            { position, itemView, item ->
                itemView.textViewNamaProduk.text = item.productName
//                itemView.textViewKategoriProduk.
                val hargaDiskon =
                    item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                itemView.textViewHargaDiskon.text =
                    changeToRupiah(
                        hargaDiskon
                    )
                if (item.discount.toString().toDouble() > 0.0) {
                    itemView.textViewHargaAsliProduk.paintFlags =
                        itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    itemView.textViewHargaAsliProduk.text =
                        changeToRupiah(item.priceSelling.toDouble())

                } else {
                    itemView.textViewHargaAsliProduk.visibility = View.INVISIBLE
                }
                itemView.textViewHargaAsliProduk.paintFlags =
                    itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                Glide.with(this).load(item.uri).into(itemView.imageViewProduk)

                itemView.btBeliSekarang.text = "Lihat Detail"

                //Categories
                var categories = ""

                for (i in item.categories!!.indices) {
                    categories += if (i < item.categories.size - 1) {
                        item.categories[i].category + ", "
                    } else {
                        item.categories[i].category
                    }
                }
                itemView.textViewKategoriProduk.text = categories

            },
            { _, item ->
                val intent = Intent(
                    this@MerchantKelolaBarangActivity,
                    MerchantDetailBarangActivity::class.java
                )
                intent.putExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, item.id)
                startActivity(intent)
            })
        binding.recyclerListProduct.layoutManager = GridLayoutManager(this@MerchantKelolaBarangActivity, 2)
        binding.recyclerListProduct.adapter = productFilteredAdapter

    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            viewModelKelolaBarang.setUpListMerchantProduct(auth.token!!)
            observerProductMerchant()
            viewModel.getDataProfileToko(auth.token!!)
        }
    }

    private fun setFilterProduct(
        category: String,
        location:String,
        search: String,
        orderDirection: String,
        dialog: Dialog
    ) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("category", category)
        jsonObject.addProperty("location", location)

        viewModelProductMerchantFilter.setUpListMerchantFilterProduct(
            idMerchant.toInt(),
            search,
            orderDirection,
            jsonObject
        )

        Log.v(
            "CEKDATALFILTER",
            "Id : $idMerchant Serach : $search Order Direction : $orderDirection jsonObject : $jsonObject"
        )
        observeFilterProduct()
        binding.buttonClearFilter.visibility = View.VISIBLE
        dialog.hide()
//        requestFilterProduct(jsonObject, orderDirection, search, {
//            if (it.code == 200) {
//                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
//                if(it.data!!.isEmpty()){
//                    binding.tvDataKosong.visibility = View.VISIBLE
//                }
//                getFilteredProducts(it.data)
//                binding.buttonClearFilter.visibility = View.VISIBLE
//                dialog.dismiss()
//            } else {
//                Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
//            }
//        }, {
//            Toast.makeText(this, it,Toast.LENGTH_SHORT).show()
//        })
    }

    private fun observeFilterProduct() {
        getFilteredProducts()
        viewModelProductMerchantFilter.getDataListMerchantFilterProduct().observe(this, Observer {
            productFilteredAdapter.submitList(it)
            Log.v("CEKDATALFILTER", it.toString())
        })
        viewModelProductMerchantFilter.statusMerchantFilterProduct.observe(this, Observer {
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBarMerchantProduct,
                binding.recyclerListProduct,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Produk Tidak Ditemukan",
                R.drawable.ic_empty_cart
            )
        })

    }

//    private fun requestFilterProduct(
//        jsonObject: JsonObject,
//        orderDirection: String,
//        search: String,
//        onSuccess: (ResponseProductByMerchant) -> Unit,
//        onFailed: (String) -> Unit
//    ) {
//        val apiService: GuestMerchantsService by lazy {
//            ApiService().createService(GuestMerchantsService::class.java)
//        }
//
//        val call: retrofit2.Call<ResponseProductByMerchant> =
//            apiService.getFilterProductMerchant(
//                idMerchant.toInt(),
//                search,
//                orderDirection,
//                jsonObject
//            )
//        call.enqueue(object : Callback<ResponseProductByMerchant> {
//            override fun onResponse(
//                call: retrofit2.Call<ResponseProductByMerchant>,
//                response: Response<ResponseProductByMerchant>
//            ) {
//                Log.d("FILTER_PRODUCT", response.body().toString())
//                if (response.isSuccessful)
//                    if (response.body() != null) onSuccess(response.body()!!)
//                    else onFailed(response.message().toString())
//                else onFailed(response.message()).toString()
//            }
//
//            override fun onFailure(call: retrofit2.Call<ResponseProductByMerchant>, t: Throwable) {
//                onFailed(t.message.toString())
//            }
//
//        })
//    }

}