package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.ConstantFaq
import id.co.leholeh.mobile.customer.model.faq.ResponseFaq
import retrofit2.http.GET

interface FaqService {
    @GET(ConstantFaq.BASE_URL_FAQ)
    suspend fun getListFaq() : ResponseFaq
}