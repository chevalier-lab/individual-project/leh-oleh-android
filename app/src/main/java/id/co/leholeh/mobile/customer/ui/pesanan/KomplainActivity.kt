package id.co.leholeh.mobile.customer.ui.pesanan

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityKomplainBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.orders.detail.ProductsOrder
import id.co.leholeh.mobile.customer.model.orders.komplain.DataKomplain
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Callback
import retrofit2.Response

class KomplainActivity : AppCompatActivity() {
    lateinit var binding: ActivityKomplainBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var idPesananData: String
    private lateinit var idProductKomplain: String
    private val listProduct: ArrayList<ProductsOrder> = arrayListOf()
    private val viewModel: PesananCustomerViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(PesananCustomerViewModel::class.java)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKomplainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Actionbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Berikan Komplain"

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        //getdata Intent
        idPesananData = intent.getSerializableExtra(INTENT_DETAIL)!! as String
        idProductKomplain = intent.getSerializableExtra(INTENT_SEARCH)!! as String

        //initUI
        initUI()


    }

    fun initUI() {
        val auth = getAuth(cacheUtil)
        viewModel.getDetailPesanan(auth.token!!, idPesananData)
        viewModel.detailPesanan.observe(this, Observer {
            Log.v("KOMPLAIN", it.toString())
            var status = ""
            binding.tvIdTransaksiKomplain.text = it.transactionToken
            binding.tvHargaTransaksiKomplain.text = changeToRupiah(it.totalPrice!!.toDouble())
            when (it.status) {
                "0" -> {
                    status = "BELUM DIBAYAR"
                    binding.tvStatusTransaksiKomplain.setBackgroundResource(R.drawable.status_not_payment)
                }
                "1" -> {
                    status = "TERVERIFIKASI"
                    binding.tvStatusTransaksiKomplain.setBackgroundResource(R.drawable.status_payment_verified)
                }
                "2" -> {
                    status = "DIPROSES"
                    binding.tvStatusTransaksiKomplain.setBackgroundResource(R.drawable.status_diproses)
                }
                "3" -> {
                    status = "SELESAI"
                    binding.tvStatusTransaksiKomplain.setBackgroundResource(R.drawable.status_selesai)
                }
                "4" -> {
                    status = "REFUND"
                    binding.tvStatusTransaksiKomplain.setBackgroundResource(R.drawable.status_refund)
                }
            }

            binding.tvStatusTransaksiKomplain.text = status

            //setup time
            val tanggal = it.updatedAt!!.substring(0, 10)
            val jam = it.updatedAt.substring(10)
            binding.tvTglTransaksiKomplain.text = tanggal
            binding.tvJamTransaksiKomplain.text = jam

            for (i in it.products!!.indices) {
                if (it.products[i].idUProduct == idProductKomplain) {
                    listProduct.add(it.products[i])
                    break
                }
            }

            var statusProduct = ""
            when (listProduct[0].status) {
                "0" -> {
                    statusProduct = "BELUM DIBAYAR"
                    binding.tvStatusKomplain.setBackgroundResource(R.drawable.status_not_payment)
                }
                "1" -> {
                    statusProduct = "TERVERIFIKASI"
                    binding.tvStatusKomplain.setBackgroundResource(R.drawable.status_payment_verified)
                }
                "2" -> {
                    statusProduct = "DIPROSES"
                    binding.tvStatusKomplain.setBackgroundResource(R.drawable.status_diproses)
                }
                "3" -> {
                    statusProduct = "SELESAI"
                    binding.tvStatusKomplain.setBackgroundResource(R.drawable.status_selesai)
                }
                "4" -> {
                    statusProduct = "REFUND"
                    binding.tvStatusKomplain.setBackgroundResource(R.drawable.status_refund)
                }
            }
            binding.tvNamaProdukKomplain.text = listProduct[0].productName

            binding.tvHargaProdukKomplain.text =
                changeToRupiah(listProduct[0].subTotalPrice!!.toDouble())

            binding.tvJumlahProdukKomplain.text =
                StringBuilder("Qty : ").append(listProduct[0].qty.toString())
            binding.tvStatusKomplain.text = statusProduct
            Glide.with(this).load(listProduct[0].uri)
                .into(binding.imageViewProdukKomplain)

            //Categories
            var categories = ""

            for (i in listProduct[0].categories!!.indices) {
                categories += if (i < listProduct[0].categories!!.size - 1) {
                    listProduct[0].categories!![i]!!.category + ", "
                } else {
                    listProduct[0].categories!![i]!!.category
                }
            }

            binding.tvKategoriProdukDetailPesanan.text = categories

        })

        binding.buttonKirimKomplain.setOnClickListener {
            setKomplain()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    //Komplain
    private fun requestKomlpain(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataKomplain) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: retrofit2.Call<DataKomplain> =
            ApiService().serviceOrder.getKomplainOrders(auth, jsonObject)
        call.enqueue(object : Callback<DataKomplain> {
            override fun onFailure(call: retrofit2.Call<DataKomplain>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: retrofit2.Call<DataKomplain>,
                response: Response<DataKomplain>
            ) {
                Log.d("CHECKOUT", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }

    //Get Komplain
    private fun setKomplain() {
        val jsonObject = JsonObject()

        if (binding.editTextTextSubjectKomplain.text.toString() == "" || binding.editTextTextUlasanKomplain.text.toString() == "") {
            Toast.makeText(
                this,
                "Lengkapi Data",
                Toast.LENGTH_LONG
            ).show()
        } else {
            jsonObject.addProperty("id_u_user_transaction_products",listProduct[0].info!!.idUUserTransactionProducts)
            jsonObject.addProperty("subject",binding.editTextTextSubjectKomplain.text.toString())
            jsonObject.addProperty("description",binding.editTextTextUlasanKomplain.text.toString())
            jsonObject.addProperty("status",listProduct[0].status)

            requestKomlpain(getAuth(cacheUtil).token!!,jsonObject,{
                if(it.code==200){
                    Toast.makeText(this,"Berhasil "+ it.message, Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            },{
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })
        }

    }
}