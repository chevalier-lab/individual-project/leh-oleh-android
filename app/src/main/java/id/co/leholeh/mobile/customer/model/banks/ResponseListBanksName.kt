package id.co.leholeh.mobile.customer.model.banks

import com.google.gson.annotations.SerializedName

data class ResponseListBanksName(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<Bank>? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

