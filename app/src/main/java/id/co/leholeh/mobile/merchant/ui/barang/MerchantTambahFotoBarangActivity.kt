package id.co.leholeh.mobile.merchant.ui.barang

import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantTambahFotoBarangBinding
import id.co.leholeh.databinding.ComponentDialogConfirmationMerchantBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.*
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_text.view.*
import kotlinx.android.synthetic.main.item_photo_product_merchant.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class MerchantTambahFotoBarangActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMerchantTambahFotoBarangBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private lateinit var data: TempAddBarang
    private val listUri = arrayListOf<Uri>()
    private lateinit var adapterTextLocation: AdapterUtil<String>
    private lateinit var adapterTextCategories: AdapterUtil<String>
    private lateinit var adapterProductPhotos: AdapterUtil<Uri>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantTambahFotoBarangBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            data =
                intent.getSerializableExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT) as TempAddBarang
            Log.d("TEST_DATA", "TEST INI " + data.namaBarang)
//            idProduct = intent.getIntExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT,0)
            binding.btSimpan.setOnClickListener {
                if (listUri.size > 0) {
                    showConfirmationDialog()
                } else {
                    Toast.makeText(this, "Masukan foto terlebih dahulu", Toast.LENGTH_SHORT).show()
                }

//                startActivity(Intent(this, MerchantKelolaBarangActivity::class.java))
            }
            binding.cardViewaAddImage.setOnClickListener {
                if(listUri.size == 4){
                    Toast.makeText(this, "foto sudah 4", Toast.LENGTH_SHORT).show()
                }
                else{
                    ImagePicker.create(this)
                        .limit(4)
                        .start()
                    Toast.makeText(this, "Masukan 4 foto barang", Toast.LENGTH_SHORT).show()
                }
            }
        }

        setupToolbar()

    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Tambah Barang"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    //show dialog
    private fun showConfirmationDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_confirmation_merchant)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogConfirmationMerchantBinding.inflate(layoutInflater)
        binding.tvNamaBarang.text = StringBuilder("Nama Barang : ").append(data.namaBarang)
        binding.tvStok.text = StringBuilder("Jumlah Stok : ").append(data.jumlahStok)
        binding.tvDiskon.text = StringBuilder("Diskon : ").append(data.diskon).append(" %")
        binding.tvHargaModal.text =
            StringBuilder("Harga modal : ").append(changeToRupiah(data.hargaModal.toDouble()))
        binding.tvHargaJual.text =
            StringBuilder("Harga jual : ").append(changeToRupiah(data.hargaJual.toDouble()))
        binding.tvDeksripsi.text = StringBuilder("Deskripsi : ").append(data.deskripsi)
        Glide.with(binding.ivCover).load(data.cover).into(binding.ivCover)

        //set recycler categories
        adapterTextCategories = AdapterUtil(R.layout.item_list_text, data.kategoriName,
            { _, itemView, item ->
                itemView.tv_rv_text.text = StringBuilder("- ").append(item)
            }, { _, _ ->
            })
        binding.rvCategoryConfirmation.layoutManager = LinearLayoutManager(this)
        binding.rvCategoryConfirmation.adapter = adapterTextCategories


        //set recycler location
        adapterTextLocation = AdapterUtil(R.layout.item_list_text, data.lokasiName,
            { _, itemView, item ->
                itemView.tv_rv_text.text = StringBuilder("- ").append(item)
            }, { _, _ ->
            })
        binding.rvLokasiConfirmation.layoutManager = LinearLayoutManager(this)
        binding.rvLokasiConfirmation.adapter = adapterTextLocation

        //set recycler photos
        adapterProductPhotos = AdapterUtil(R.layout.item_photo_product_merchant, arrayListOf(),
            { _, itemView, item ->
                Glide.with(this).load(item).into(itemView.imageViewProductMerchant)
            }, { _, _ ->
            })
        binding.rvPhotoConfirmation.layoutManager = GridLayoutManager(this, 2)
        binding.rvPhotoConfirmation.adapter = adapterProductPhotos
        if (listUri.size > 0) adapterProductPhotos.data = listUri

        //cancel button
        binding.btBatal.setOnClickListener {
            dialog.dismiss()
        }
        binding.btSimpan.setOnClickListener {
            sendRequestCreateProductMerchant(dialog)
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    private fun sendRequestAddPhotoProduct(dialog : Dialog, idProduct: Int) {
        val drawableProduk1 = binding.imageProduk1.drawable as BitmapDrawable
        val bitmapProduk1 = drawableProduk1.bitmap
        val fileProduk1 = createTempFile(bitmapProduk1)
        val imageProduk1 = MultipartBody.Part.createFormData(
            "photo",
            fileProduk1!!.name,
            fileProduk1.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val drawableProduk2 = binding.imageProduk2.drawable as BitmapDrawable
        val bitmapProduk2 = drawableProduk2.bitmap
        val fileProduk2 = createTempFile(bitmapProduk2)
        val imageProduk2 = MultipartBody.Part.createFormData(
            "photo",
            fileProduk2!!.name,
            fileProduk2.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val drawableProduk3 = binding.imageProduk3.drawable as BitmapDrawable
        val bitmapProduk3 = drawableProduk3.bitmap
        val fileProduk3 = createTempFile(bitmapProduk3)
        val imageProduk3 = MultipartBody.Part.createFormData(
            "photo",
            fileProduk3!!.name,
            fileProduk3.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val drawableProduk4 = binding.imageProduk4.drawable as BitmapDrawable
        val bitmapProduk4 = drawableProduk4.bitmap
        val fileProduk4 = createTempFile(bitmapProduk4)
        val imageProduk4 = MultipartBody.Part.createFormData(
            "photo",
            fileProduk4!!.name,
            fileProduk4.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val partIdProduct = MultipartBody.Part.createFormData("id_u_product", idProduct.toString())
        val arrImage = arrayListOf(imageProduk1, imageProduk2, imageProduk3, imageProduk4)
        for (i in 0 until 4) {
            addPhotosProduct(arrImage[i], partIdProduct, {
                if (it.code == 200) {
                    Log.e("SIZE PHOTO", i.toString())
                    if (i == 3) {
                        Toast.makeText(
                            this,
                            "Berhasil menambah data!",
                            Toast.LENGTH_SHORT
                        ).show()
                        dialog.dismiss()
                        startActivity(Intent(this, MerchantKelolaBarangActivity::class.java))
                        finish()
                        finish()
                    }
                }
            },
                {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                })
        }

    }

    private fun addPhotosProduct(
        photos: MultipartBody.Part,
        idProduct: MultipartBody.Part,
        onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseAddPhotosProduct> =
            apiService.addPhotosProductMerchant(auth.token!!, photos, idProduct)
        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestCreateProductMerchant(dialog : Dialog) {
        val partName = MultipartBody.Part.createFormData("name", data.namaBarang)
        val slug = data.namaBarang.replace(" ", "-").toLowerCase(Locale.getDefault())
        val partSlug = MultipartBody.Part.createFormData("slug", slug)
        val partJumlah = MultipartBody.Part.createFormData("jumlah", data.jumlahStok)
        val partDiskon = MultipartBody.Part.createFormData("diskon", data.diskon)
        val partHargaJual = MultipartBody.Part.createFormData("harga_jual", data.hargaJual)
        val partHargaBeli = MultipartBody.Part.createFormData("harga_beli", data.hargaModal)
        val partDescription =
            MultipartBody.Part.createFormData("description", data.deskripsi)
        val partCategories =
            MultipartBody.Part.createFormData("categories", data.kategori.toString())
        val imageCover = MultipartBody.Part.createFormData(
            "cover",
            data.cover.name,
            data.cover.asRequestBody("image/*".toMediaTypeOrNull())
        )
        createProductMerchant(
            imageCover,
            partName,
            partSlug,
            partJumlah,
            partDiskon,
            partHargaJual,
            partHargaBeli,
            partDescription,
            partCategories,
            {
                if (it.code == 200) {
                    val idProduct = it.data
                    if (idProduct != null) {
                        Toast.makeText(this, "Memproses data...", Toast.LENGTH_SHORT)
                            .show()
                        sendRequestCreateLocationMerchant(dialog, idProduct)
                        val length = data.length
                        val width = data.width
                        val height = data.height
                        val weight = data.weight
                        sendRequestProductDimension(idProduct, length, width, height, weight)
                    }
                }
            },
            {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })
    }

    private fun createProductMerchant(
        cover: MultipartBody.Part,
        name: MultipartBody.Part,
        slug: MultipartBody.Part,
        jumlah: MultipartBody.Part,
        diskon: MultipartBody.Part,
        hargaJual: MultipartBody.Part,
        hargaBeli: MultipartBody.Part,
        description: MultipartBody.Part,
        categories: MultipartBody.Part,
        onSuccess: (ResponseCreateProductMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseCreateProductMerchant> = apiService.createProductMerchant(
            auth.token!!,
            cover,
            name,
            slug,
            jumlah,
            diskon,
            hargaJual,
            hargaBeli,
            description,
            categories
        )
        call.enqueue(object : Callback<ResponseCreateProductMerchant> {
            override fun onResponse(
                call: Call<ResponseCreateProductMerchant>,
                response: Response<ResponseCreateProductMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateProductMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestCreateLocationMerchant(dialog : Dialog, idProduct: Int) {
        val jsonObject = JsonObject()
        for (i in data.lokasi.indices) {
            jsonObject.addProperty("id_m_locations", data.lokasi[i])
            jsonObject.addProperty("id_u_product", idProduct)

            createLocation(jsonObject, {
                if (it.code == 200) {
                    if(i == data.lokasi.size-1){
                        sendRequestAddPhotoProduct(dialog, idProduct)
                    }
                } else {
                    Toast.makeText(
                        this,
                        it.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }, {
                Toast.makeText(
                    this,
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            })
        }
    }

    private fun createLocation(
        jsonObject: JsonObject,
        onSuccess: (ResponseCreateLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseCreateLocation> = apiService.createProductLocation(
            auth.token!!, jsonObject
        )
        call.enqueue(object : Callback<ResponseCreateLocation> {
            override fun onResponse(
                call: Call<ResponseCreateLocation>,
                response: Response<ResponseCreateLocation>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateLocation>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })

    }

    private fun createProductDimension(
        jsonObject: JsonObject,
        onSuccess: (ResponseCreateProductDimension) -> Unit,
        onFailed: (String) -> Unit
    ){
        val apiService : ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call : Call<ResponseCreateProductDimension> = apiService.createProductDimension(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseCreateProductDimension>{
            override fun onResponse(
                call: Call<ResponseCreateProductDimension>,
                responseCreate: Response<ResponseCreateProductDimension>
            ) {
                if (responseCreate.isSuccessful)
                    if (responseCreate.body() != null) onSuccess(responseCreate.body()!!)
                    else onFailed(responseCreate.message().toString())
                else onFailed(responseCreate.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateProductDimension>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestProductDimension(
        idProduct: Int,
        length: String,
        width: String,
        height: String,
        weight: String
    ){
        val jsonObject = JsonObject()
        jsonObject.apply {
            addProperty("id_u_product", idProduct)
            addProperty("length", length)
            addProperty("width", width)
            addProperty("height", height)
            addProperty("weight", weight)
        }
        createProductDimension(jsonObject,{
            if (it.code == 200){
                Toast.makeText(this, "Dimensi produk ditambahkan", Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
        },{
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image: List<Image> = ImagePicker.getImages(data)
            if (image.size < 4) {
                Toast.makeText(baseContext, "Foto barang harus 4", Toast.LENGTH_SHORT).show()
            } else {
                
                var index: Int
                for (i in image.indices) {
                    listUri.add(image[i].uri)
                    index = i
                    binding.cardViewImage1.visibility = View.VISIBLE
                    Glide.with(binding.imageProduk1).load(image[0].uri).into(binding.imageProduk1)
                    when {
                        index == 1 && binding.cardViewImage1.isVisible -> {
                            Glide.with(binding.imageProduk2).load(image[index].uri)
                                .into(binding.imageProduk2)
                            binding.cardViewImage2.visibility = View.VISIBLE
                        }
                        index == 2 && binding.cardViewImage2.isVisible -> {
                            Glide.with(binding.imageProduk3).load(image[index].uri)
                                .into(binding.imageProduk3)
                            binding.cardViewImage3.visibility = View.VISIBLE
                        }
                        index == 3 && binding.cardViewImage3.isVisible -> {
                            binding.cardViewImage4.visibility = View.VISIBLE
                            Glide.with(binding.imageProduk4).load(image[index].uri)
                                .into(binding.imageProduk4)
                        }
                    }
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }


}