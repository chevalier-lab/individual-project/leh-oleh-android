package id.co.leholeh.mobile.customer.ui.rekomendasi

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.banner.DataBannerRekomendasi
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.customer.model.product.ByLocation
import id.co.leholeh.mobile.customer.model.product.Product
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.rekomendasi.datasource.NewProductDataSource
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class RekomendasiViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    private val allProduct = MutableLiveData<List<Product>>()
    private val lokasiProduct = MutableLiveData<List<ByLocation>>()
    private val banner = MutableLiveData<List<DataBannerRekomendasi>>()
    private val status = MutableLiveData<ApiStatus>()
    private val statusListRandomMerchant = MutableLiveData<ApiStatus>()

    private val _listMerchantsRandom = MutableLiveData<List<Merchants>>()
    val listMerchantsRandom : LiveData<List<Merchants>>
        get() = _listMerchantsRandom


    //CONFIG
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    fun getBanner() : LiveData<List<DataBannerRekomendasi>> = banner

    fun getLokasiProduct() : LiveData<List<ByLocation>> = lokasiProduct

    fun getStatus() : LiveData<ApiStatus> = status

    init {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                requestDataProduct()
                requestDataListRandomMerchant()
                requestBanner()
            }
        }
    }

    private suspend fun requestDataListRandomMerchant(){
        try{
            statusListRandomMerchant.postValue(ApiStatus.LOADING)
            val apiService = ApiService().serviceGuestMerchants
            _listMerchantsRandom.postValue(apiService.getListRandomMerchant().data)
            statusListRandomMerchant.postValue(ApiStatus.SUCCESS)
        }catch (e:Exception){
            statusListRandomMerchant.postValue(ApiStatus.FAILED)
            Log.d("ERROR_REQ_LIST_MERCHANT", e.localizedMessage!!)
        }
    }

    private suspend fun requestBanner(){
        try{
            status.postValue(ApiStatus.LOADING)
            val apiService = ApiService().service
            banner.postValue(apiService.getBanner().data)
            Log.d("BANNER", apiService.getBanner().data.toString())
            status.postValue(ApiStatus.SUCCESS)
        }catch (e:Exception){
            status.postValue(ApiStatus.FAILED)
            Log.d("ERROR_REQ_LIST_MERCHANT", e.localizedMessage!!)
        }
    }

    private suspend fun requestDataProduct() {
        try {
            status.postValue(ApiStatus.LOADING)
            val productData = ApiService().service
            lokasiProduct.postValue(productData.getLokasiProduct("4").data)
            status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            status.postValue(ApiStatus.FAILED)
            Log.d("ProductData", e.message.toString())
        }
    }

    private lateinit var listNewProduct: LiveData<PagedList<Product>>
    lateinit var statusNewProduct: LiveData<ApiStatus>
    private val _listNewProductDataSource = MutableLiveData<NewProductDataSource>()

    //LIST MERCHANT
    fun setupListNewProduct() {
        statusNewProduct = Transformations.switchMap<NewProductDataSource, ApiStatus>(
            _listNewProductDataSource,
            NewProductDataSource::apiState
        )
        listNewProduct = initializedListNewProductBuilder(config).build()
    }

    fun getDataListNewProduct(): LiveData<PagedList<Product>> = listNewProduct

    private fun initializedListNewProductBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Product> {
        val dataSourceFactory = object : DataSource.Factory<Int, Product>() {
            override fun create(): DataSource<Int, Product> {
                val listNewProductDataSource = NewProductDataSource(Dispatchers.IO)
                _listNewProductDataSource.postValue(listNewProductDataSource)
                return listNewProductDataSource
            }
        }
        return LivePagedListBuilder<Int,Product>(dataSourceFactory,config)
    }

}