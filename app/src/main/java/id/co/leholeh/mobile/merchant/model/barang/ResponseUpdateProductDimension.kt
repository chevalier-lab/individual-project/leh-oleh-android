package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ResponseUpdateProductDimension(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataUpdateDimension? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataUpdateDimension(

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("length")
	val length: String? = null,

	@field:SerializedName("width")
	val width: String? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null,

	@field:SerializedName("height")
	val height: String? = null
)
