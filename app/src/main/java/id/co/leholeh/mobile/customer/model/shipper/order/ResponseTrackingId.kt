package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseTrackingId(

	@field:SerializedName("data")
	val data: DataTrackingId? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataTrackingId(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
