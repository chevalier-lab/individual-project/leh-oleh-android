package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataProduct(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<Product>? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
) : Serializable