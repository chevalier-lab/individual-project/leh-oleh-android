package id.co.leholeh.mobile.customer.ui.checkout.delivery

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Slide
import androidx.transition.TransitionManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityChooseDeliveryBinding
import id.co.leholeh.mobile.customer.constant.shipper.ConstantShipper
import id.co.leholeh.mobile.customer.model.shipper.rates.*
import id.co.leholeh.mobile.customer.ui.cart.KeranjangViewModel
import id.co.leholeh.mobile.customer.ui.checkout.CheckoutActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import kotlinx.android.synthetic.main.activity_choose_delivery.*
import kotlinx.android.synthetic.main.component_dialog_select_shipper.*
import kotlinx.android.synthetic.main.item_delivery_name.view.*
import kotlinx.android.synthetic.main.item_delivery_type.*
import kotlinx.android.synthetic.main.item_delivery_type.view.*
import kotlinx.android.synthetic.main.item_list_faq.view.*
import java.util.*

class ChooseDeliveryActivity : AppCompatActivity() {
    private lateinit var binding: ActivityChooseDeliveryBinding
    private var isLehOleh = false
    private var payment = ""

    private lateinit var deliveryTypeAdapter: AdapterUtil<String>
    private lateinit var deliveryExpressAdapter: AdapterUtil<ExpressItem>
    private lateinit var deliveryInstantAdapter: AdapterUtil<InstantItem>
    private lateinit var deliverySamedayAdapter: AdapterUtil<SameDayItem>
    private lateinit var deliveryRegularAdapter: AdapterUtil<RegularItem>
    private lateinit var deliveryTruckingAdapter: AdapterUtil<TruckingItem>

    private lateinit var listOfShipperPrice: ArrayList<Double>
    private lateinit var listOfShipperName: ArrayList<String>
    private lateinit var listOfShipperRatesID: ArrayList<Int>
    private lateinit var listDomesticType: ArrayList<String>

    private val listExpress = arrayListOf<ExpressItem>()
    private val listInstant = arrayListOf<InstantItem>()
    private val listSameDay = arrayListOf<SameDayItem>()
    private val listRegular = arrayListOf<RegularItem>()
    private val listTrucking = arrayListOf<TruckingItem>()

    private val viewModel: KeranjangViewModel by lazy {
        ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(KeranjangViewModel::class.java)
    }

    private fun setupToolbar() = supportActionBar?.apply {
        with(this) {
            title = "Opsi Pengiriman"
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChooseDeliveryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        listDomesticType = arrayListOf("Express", "Regular", "Same Day", "Instant", "Truck")

        if (intent.hasExtra("EXTRA_SHIPPER_LIST")) {
            intent.getParcelableExtra<ShippingList>("EXTRA_SHIPPER_LIST")?.let { item ->
                listOfShipperName = item.listOfShipperName
                listOfShipperPrice = item.listOfShipperPrice
                listOfShipperRatesID = item.listOfShipperRatesID

                Log.d(
                    "CHECK_SHIPPER_LISTS",
                    """
                    $listOfShipperName
                    $listOfShipperPrice
                    $listOfShipperRatesID
                    """.trimIndent()
                )
            }
        }

        if (intent.hasExtra("EXTRA_ISLEHOLEHPAY")) {
            isLehOleh = intent.getBooleanExtra("EXTRA_ISLEHOLEHPAY", false)

            payment = when (isLehOleh) {
                true -> "Leh-Oleh"
                false -> "Pembayaran lainnya"
            }
        }

        if (intent.hasExtra("EXTRA_SHIPPER_DATA")) {
            intent.getParcelableExtra<ShippingData>("EXTRA_SHIPPER_DATA")?.let { item ->
                Log.d("CHECK_SHIPPER_DATA", item.toString())

                deliveryTypeAdapter = AdapterUtil(
                    R.layout.item_delivery_type,
                    listDomesticType,
                    { position, itemView, type ->
                        with(itemView) {
                            tv_deliveryType.text = type
                            rv_deliveryName.visibility = View.GONE

                            data.setOnClickListener {
                                pb_loading.visibility = View.VISIBLE

                                setDeliveryData(
                                    item.EXTRA_ORIGIN,
                                    item.EXTRA_DEST,
                                    item.EXTRA_LENGTH,
                                    item.EXTRA_WIDTH,
                                    item.EXTRA_HEIGHT,
                                    item.EXTRA_WEIGHT,
                                    item.EXTRA_TOTAL,
                                    position,
                                    item.position,
                                    rv_deliveryName,
                                    pb_loading,
                                    not_found
                                )

                                if (this.rv_deliveryName.isGone) {
                                    toggleAnimation(
                                        true,
                                        this.iv_slidedown,
                                        this.rv_deliveryName,
                                        this.rv_deliveryName
                                    )
                                } else {
                                    toggleAnimation(
                                        false,
                                        this.iv_slidedown,
                                        this.rv_deliveryName,
                                        this.rv_deliveryName
                                    )
                                }
                            }
                        }
                    },
                    { _, _ -> }
                )

                with(binding.rvDeliveryType) {
                    layoutManager = LinearLayoutManager(
                        this@ChooseDeliveryActivity,
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                    adapter = deliveryTypeAdapter
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setDeliveryData(
        o: Int,
        d: Int,
        l: Float,
        w: Float,
        h: Float,
        wt: Float,
        v: Int,
        position: Int,
        productIndex: Int,
        view: RecyclerView,
        loadingbar: ProgressBar,
        notFoundText: TextView
    ) {
        // Adapter for express data
        when (position) {
            0 -> {
                with(viewModel) {
                    getDomesticRates(ConstantShipper.APIKEY_SHIPPER, o, d, l, w, h, wt, v)
                    shipperRates.observe(this@ChooseDeliveryActivity, Observer {
                        val logisticRoute = it.rates?.logistic

                        // LIST EXPRESS
                        if (listExpress.isNotEmpty()) listExpress.clear()
                        logisticRoute?.express?.forEach { item -> listExpress.add(item!!) }

                        Log.d("CHECK_setDeliveryData", "$listExpress")
                        loadingbar.visibility = View.GONE

                        if (logisticRoute?.express!!.isEmpty()) {
                            notFoundText.visibility = View.VISIBLE
                            notFoundText.text = "Shipper express tidak tersedia untuk merchant ini"
                        } else {
                            deliveryExpressAdapter = AdapterUtil(
                                R.layout.item_delivery_name,
                                listExpress,
                                { _, itemView, item ->
                                    with(itemView) {
                                        tv_deliveryName.text = item.name
                                        tv_deliveryPrice.text =
                                            changeToRupiah(item.finalRate!!.toDouble())

                                        btn_choose.setOnClickListener {
                                            listOfShipperName[productIndex] = item.name!!
                                            listOfShipperPrice[productIndex] =
                                                item.finalRate.toDouble()
                                            listOfShipperRatesID[productIndex] = item.rateId!!

                                            Log.d(
                                                "CHECK_SHIPPER_LISTS",
                                                """
                                            $listOfShipperName
                                            $listOfShipperPrice
                                            $listOfShipperRatesID
                                            """.trimIndent()
                                            )

                                            val listData = ShippingList(
                                                listOfShipperPrice,
                                                listOfShipperName,
                                                listOfShipperRatesID
                                            )

                                            startActivity(
                                                Intent(
                                                    this@ChooseDeliveryActivity,
                                                    CheckoutActivity::class.java
                                                ).apply {
                                                    putExtra("EXTRA_SET_LIST", listData)
                                                    putExtra("EXTRA_LEHOLEHPAY_RETURN", payment)
                                                }
                                            )
                                            finish()
                                        }
                                    }
                                },
                                { _, _ -> }
                            )

                            with(view) {
                                layoutManager = LinearLayoutManager(
                                    this@ChooseDeliveryActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                adapter = deliveryExpressAdapter
                            }
                        }
                    })
                }
            }
            1 -> {
                with(viewModel) {
                    getDomesticRates(ConstantShipper.APIKEY_SHIPPER, o, d, l, w, h, wt, v)
                    shipperRates.observe(this@ChooseDeliveryActivity, Observer {
                        val logisticRoute = it.rates?.logistic

                        // LIST REGULAR
                        if (listRegular.isNotEmpty()) listRegular.clear()
                        logisticRoute?.regular?.forEach { item -> listRegular.add(item!!) }

                        Log.d("CHECK_setDeliveryData", "$listRegular")
                        loadingbar.visibility = View.GONE

                        if (logisticRoute?.regular!!.isEmpty()) {
                            notFoundText.visibility = View.VISIBLE
                            notFoundText.text = "Shipper regular tidak tersedia untuk merchant ini"
                        } else {
                            deliveryRegularAdapter = AdapterUtil(
                                R.layout.item_delivery_name,
                                listRegular,
                                { _, itemView, item ->
                                    with(itemView) {
                                        tv_deliveryName.text = item.name
                                        tv_deliveryPrice.text =
                                            changeToRupiah(item.finalRate!!.toDouble())

                                        btn_choose.setOnClickListener {
                                            listOfShipperName[productIndex] = item.name!!
                                            listOfShipperPrice[productIndex] =
                                                item.finalRate.toDouble()
                                            listOfShipperRatesID[productIndex] = item.rateId!!

                                            Log.d(
                                                "CHECK_SHIPPER_LISTS",
                                                """
                                            $listOfShipperName
                                            $listOfShipperPrice
                                            $listOfShipperRatesID
                                            """.trimIndent()
                                            )

                                            val listData = ShippingList(
                                                listOfShipperPrice,
                                                listOfShipperName,
                                                listOfShipperRatesID
                                            )

                                            startActivity(
                                                Intent(
                                                    this@ChooseDeliveryActivity,
                                                    CheckoutActivity::class.java
                                                ).apply {
                                                    putExtra("EXTRA_SET_LIST", listData)
                                                    putExtra("EXTRA_LEHOLEHPAY_RETURN", payment)
                                                }
                                            )
                                            finish()
                                        }
                                    }
                                },
                                { _, _ -> }
                            )

                            with(view) {
                                layoutManager = LinearLayoutManager(
                                    this@ChooseDeliveryActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                adapter = deliveryRegularAdapter
                            }
                        }
                    })
                }
            }
            2 -> {
                with(viewModel) {
                    getDomesticRates(ConstantShipper.APIKEY_SHIPPER, o, d, l, w, h, wt, v)
                    shipperRates.observe(this@ChooseDeliveryActivity, Observer {
                        val logisticRoute = it.rates?.logistic

                        // LIST SAME DAY
                        if (listSameDay.isNotEmpty()) listSameDay.clear()
                        logisticRoute?.sameDay?.forEach { item -> listSameDay.add(item!!) }

                        Log.d("CHECK_setDeliveryData", "$listSameDay")
                        loadingbar.visibility = View.GONE

                        if (logisticRoute?.sameDay!!.isEmpty()) {
                            notFoundText.visibility = View.VISIBLE
                            notFoundText.text = "Shipper same day tidak tersedia untuk merchant ini"
                        } else {
                            deliverySamedayAdapter = AdapterUtil(
                                R.layout.item_delivery_name,
                                listSameDay,
                                { _, itemView, item ->
                                    with(itemView) {
                                        tv_deliveryName.text = item.name
                                        tv_deliveryPrice.text =
                                            changeToRupiah(item.finalRate!!.toDouble())

                                        btn_choose.setOnClickListener {
                                            listOfShipperName[productIndex] = item.name!!
                                            listOfShipperPrice[productIndex] =
                                                item.finalRate.toDouble()
                                            listOfShipperRatesID[productIndex] = item.rateId!!

                                            Log.d(
                                                "CHECK_SHIPPER_LISTS",
                                                """
                                            $listOfShipperName
                                            $listOfShipperPrice
                                            $listOfShipperRatesID
                                            """.trimIndent()
                                            )

                                            val listData = ShippingList(
                                                listOfShipperPrice,
                                                listOfShipperName,
                                                listOfShipperRatesID
                                            )

                                            startActivity(
                                                Intent(
                                                    this@ChooseDeliveryActivity,
                                                    CheckoutActivity::class.java
                                                ).apply {
                                                    putExtra("EXTRA_SET_LIST", listData)
                                                    putExtra("EXTRA_LEHOLEHPAY_RETURN", payment)
                                                }
                                            )
                                            finish()
                                        }
                                    }
                                },
                                { _, _ -> }
                            )

                            with(view) {
                                layoutManager = LinearLayoutManager(
                                    this@ChooseDeliveryActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                adapter = deliverySamedayAdapter
                            }
                        }
                    })
                }
            }
            3 -> {
                with(viewModel) {
                    getDomesticRates(ConstantShipper.APIKEY_SHIPPER, o, d, l, w, h, wt, v)
                    shipperRates.observe(this@ChooseDeliveryActivity, Observer {
                        val logisticRoute = it.rates?.logistic

                        // LIST INSTANT
                        if (listInstant.isNotEmpty()) listInstant.clear()
                        logisticRoute?.instant?.forEach { item -> listInstant.add(item!!) }

                        Log.d("CHECK_setDeliveryData", "$listInstant")
                        loadingbar.visibility = View.GONE

                        if (logisticRoute?.instant!!.isEmpty()) {
                            notFoundText.visibility = View.VISIBLE
                            notFoundText.text = "Shipper instant tidak tersedia untuk merchant ini"
                        } else {
                            deliveryInstantAdapter = AdapterUtil(
                                R.layout.item_delivery_name,
                                listInstant,
                                { _, itemView, item ->
                                    with(itemView) {
                                        tv_deliveryName.text = item.name
                                        tv_deliveryPrice.text =
                                            changeToRupiah(item.finalRate!!.toDouble())

                                        btn_choose.setOnClickListener {
                                            listOfShipperName[productIndex] = item.name!!
                                            listOfShipperPrice[productIndex] =
                                                item.finalRate.toDouble()
                                            listOfShipperRatesID[productIndex] = item.rateId!!

                                            Log.d(
                                                "CHECK_SHIPPER_LISTS",
                                                """
                                            $listOfShipperName
                                            $listOfShipperPrice
                                            $listOfShipperRatesID
                                            """.trimIndent()
                                            )

                                            val listData = ShippingList(
                                                listOfShipperPrice,
                                                listOfShipperName,
                                                listOfShipperRatesID
                                            )

                                            startActivity(
                                                Intent(
                                                    this@ChooseDeliveryActivity,
                                                    CheckoutActivity::class.java
                                                ).apply {
                                                    putExtra("EXTRA_SET_LIST", listData)
                                                    putExtra("EXTRA_LEHOLEHPAY_RETURN", payment)
                                                }
                                            )
                                            finish()
                                        }
                                    }
                                },
                                { _, _ -> }
                            )

                            with(view) {
                                layoutManager = LinearLayoutManager(
                                    this@ChooseDeliveryActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                adapter = deliveryInstantAdapter
                            }
                        }
                    })
                }
            }
            4 -> {
                with(viewModel) {
                    getDomesticRates(ConstantShipper.APIKEY_SHIPPER, o, d, l, w, h, wt, v)
                    shipperRates.observe(this@ChooseDeliveryActivity, Observer {
                        val logisticRoute = it.rates?.logistic

                        // LIST TRUCKING
                        if (listTrucking.isNotEmpty()) listTrucking.clear()
                        logisticRoute?.trucking?.forEach { item -> listTrucking.add(item!!) }

                        Log.d("CHECK_setDeliveryData", "$listTrucking")
                        loadingbar.visibility = View.GONE

                        if (logisticRoute?.trucking!!.isEmpty()) {
                            notFoundText.visibility = View.VISIBLE
                            notFoundText.text = "Shipper trucking tidak tersedia untuk merchant ini"
                        } else {
                            deliveryTruckingAdapter = AdapterUtil(
                                R.layout.item_delivery_name,
                                listTrucking,
                                { _, itemView, item ->
                                    with(itemView) {
                                        tv_deliveryName.text = item.name
                                        tv_deliveryPrice.text =
                                            changeToRupiah(item.finalRate!!.toDouble())

                                        btn_choose.setOnClickListener {
                                            listOfShipperName[productIndex] = item.name!!
                                            listOfShipperPrice[productIndex] =
                                                item.finalRate.toDouble()
                                            listOfShipperRatesID[productIndex] = item.rateId!!

                                            Log.d(
                                                "CHECK_SHIPPER_LISTS",
                                                """
                                            $listOfShipperName
                                            $listOfShipperPrice
                                            $listOfShipperRatesID
                                            """.trimIndent()
                                            )

                                            val listData = ShippingList(
                                                listOfShipperPrice,
                                                listOfShipperName,
                                                listOfShipperRatesID
                                            )

                                            startActivity(
                                                Intent(
                                                    this@ChooseDeliveryActivity,
                                                    CheckoutActivity::class.java
                                                ).apply {
                                                    putExtra("EXTRA_SET_LIST", listData)
                                                    putExtra("EXTRA_LEHOLEHPAY_RETURN", payment)
                                                }
                                            )
                                            finish()
                                        }
                                    }
                                },
                                { _, _ -> }
                            )

                            with(view) {
                                layoutManager = LinearLayoutManager(
                                    this@ChooseDeliveryActivity,
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                                adapter = deliveryTruckingAdapter
                            }
                        }
                    })
                }
            }
        }
    }

    private fun toggleAnimation(visible: Boolean, arrow: View, view: View, viewGroup: ViewGroup) {
        val transition = Slide(Gravity.TOP)

        with(transition) {
            duration = 300
            addTarget(view)
        }

        TransitionManager.beginDelayedTransition(viewGroup, transition)
        if (visible) {
            view.visibility = View.VISIBLE
            val rotation = RotateAnimation(
                0f,
                180f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )

            with(rotation) {
                duration = 300
                interpolator = LinearInterpolator()
                fillAfter = true
            }

            arrow.startAnimation(rotation)
        } else {
            view.visibility = View.GONE
            val rotation = RotateAnimation(
                180f,
                0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )

            with(rotation) {
                duration = 300
                interpolator = LinearInterpolator()
                fillAfter = true
            }

            arrow.startAnimation(rotation)
        }
    }
}