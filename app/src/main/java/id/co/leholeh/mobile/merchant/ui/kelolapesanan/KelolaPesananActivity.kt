package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.leholeh.databinding.ActivityKelolaPesananBinding
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.merchant.adapter.SectionsPagerAdapterPesanan
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.merchant.ui.profile.ProfileTokoViewModel

class KelolaPesananActivity : AppCompatActivity() {

    private lateinit var binding: ActivityKelolaPesananBinding
    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private lateinit var auth: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKelolaPesananBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val sectionsPagerAdapter =
            SectionsPagerAdapterPesanan(
                this,
                supportFragmentManager
            )
        binding.viewPager.adapter = sectionsPagerAdapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
        //Setup Toolbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Kelola Pesanan"

        //auth
        auth = intent.getSerializableExtra(INTENT_SEARCH) as String


        //checklocation
        locationNullCheck()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    //check location null
    private fun locationNullCheck() {
        viewModel.getDataProfileToko(
            auth
        )
        viewModel.toko.observe(this, Observer { profileToko ->
            if (profileToko.location == null) {
                binding.tvAlert.visibility = View.VISIBLE
                binding.tvAlert.setOnClickListener {
                    startActivity(
                        Intent(
                            this,
                            MerchantLocationActivity::class.java
                        ).putExtra(INTENT_DETAIL, profileToko.id)
                            .putExtra(
                                INTENT_SEARCH,
                                auth
                            )
                    )
                }
            }
        })
    }


    override fun onResume() {
        super.onResume()
        viewModel.getDataProfileToko(
            auth
        )
    }

}