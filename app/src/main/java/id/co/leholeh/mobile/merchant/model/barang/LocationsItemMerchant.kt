package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class LocationsItemMerchant(

	@field:SerializedName("id_m_locations")
	val idMLocations: String? = null,

	@field:SerializedName("id_u_product_location")
	val idUProductLocation: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)