package id.co.leholeh.mobile.merchant.model.barang

import java.io.File
import java.io.Serializable

data class TempAddBarang(
    val namaBarang : String,
    val jumlahStok :String,
    val diskon:String,
    val hargaModal:String,
    val hargaJual:String,
    val deskripsi:String,
    val kategori:ArrayList<String>,
    val lokasi : ArrayList<String>,
    val cover:File,
    val kategoriName:ArrayList<String>,
    val lokasiName:ArrayList<String>,
    val length : String,
    val width : String,
    val height : String,
    val weight : String
):Serializable