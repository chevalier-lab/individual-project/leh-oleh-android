package id.co.leholeh.mobile.customer.model.orders.review

import com.google.gson.annotations.SerializedName

data class DataReview(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Review? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)