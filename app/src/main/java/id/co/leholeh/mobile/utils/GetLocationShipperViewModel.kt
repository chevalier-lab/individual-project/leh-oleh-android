package id.co.leholeh.mobile.utils

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.constant.API_KEY_SHIPPER
import id.co.leholeh.mobile.customer.model.shipper.location.*
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.shipper.LocationService
import kotlinx.coroutines.launch

class GetLocationShipperViewModel:ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val _province = MutableLiveData<List<RowsItemProvince>>()
    val province: LiveData<List<RowsItemProvince>>
        get() = _province

    private val _cities = MutableLiveData<List<RowsItemCities>>()
    val cities: LiveData<List<RowsItemCities>>
        get() = _cities

    private val _suburbs = MutableLiveData<List<RowsItemSuburbs>>()
    val suburbs: LiveData<List<RowsItemSuburbs>>
        get() = _suburbs

    private val _areas = MutableLiveData<List<RowsItemAreas>>()
    val areas: LiveData<List<RowsItemAreas>>
        get() = _areas

    private val _search = MutableLiveData<List<RowsItemSearch>>()
    val search: LiveData<List<RowsItemSearch>>
        get() = _search


    private val shipperLocation : LocationService by lazy {
        ApiService().createServiceShipper(LocationService::class.java)
    }

    //get province
    private suspend fun requestProvince() {
        try {
            _status.postValue(ApiStatus.LOADING)
            _province.postValue(shipperLocation.getProvince(API_KEY_SHIPPER).data?.rows)
            Log.d("SHIPPER_PROVINCE", shipperLocation.getProvince(API_KEY_SHIPPER).data?.rows.toString())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("SHIPPER_PROVINCE", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)

        }
    }

    fun getProvince() {
        viewModelScope.launch {
            requestProvince()
        }
    }

    //get province
    private suspend fun requestCities(idProvince:String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _cities.postValue(shipperLocation.getCities(API_KEY_SHIPPER,"all",idProvince).data?.rows)
            Log.d("SHIPPER_CITIES", shipperLocation.getCities(API_KEY_SHIPPER,"all",idProvince).data?.rows.toString())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("SHIPPER_CITIES", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)

        }
    }

    fun getCities(idProvince:String) {
        viewModelScope.launch {
            requestCities(idProvince)
        }
    }

    //get province
    private suspend fun requestSuburbs(city:String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _suburbs.postValue(shipperLocation.getSuburbs(API_KEY_SHIPPER,city).data?.rows)
            Log.d("SHIPPER_SUBURBS", shipperLocation.getSuburbs(API_KEY_SHIPPER,city).data?.rows.toString())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("SHIPPER_SUBURBS", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)

        }
    }

    fun getSuburbs(city:String) {
        viewModelScope.launch {
            requestSuburbs(city)
        }
    }

    //get areas
    private suspend fun requestAreas(suburbs:String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _areas.postValue(shipperLocation.getAreas(API_KEY_SHIPPER,suburbs).data?.rows)
            Log.d("SHIPPER_AREAS", shipperLocation.getAreas(API_KEY_SHIPPER,suburbs).data?.rows.toString())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("SHIPPER_AREAS", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)

        }
    }

    fun getAreas(suburbs: String) {
        viewModelScope.launch {
            requestAreas(suburbs)
        }
    }

}