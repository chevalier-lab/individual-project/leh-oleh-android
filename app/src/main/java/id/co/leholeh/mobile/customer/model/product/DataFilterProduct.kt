package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataFilterProduct(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<FilterProduct>? = null,

    @field:SerializedName("error")
    val error: List<String>? = null,

    @field:SerializedName("message")
    val message: String? = null
) : Serializable