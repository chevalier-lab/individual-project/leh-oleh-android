package id.co.leholeh.mobile.customer.ui.merchantcustomer

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantPreviewBinding
import id.co.leholeh.databinding.CustomDialogSearchMerchantBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantGuestMerchants
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.cart.DataAddCart
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.customer.model.merchant.ProductsByMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.ui.produk.DetailProductActivity
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.customer.util.ShimmerUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_produk.view.*
import retrofit2.Callback
import retrofit2.Response

class MerchantPreviewActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMerchantPreviewBinding
    private lateinit var produkMerchantAdapter: PagedAdapterUtil<ProductsByMerchant>
    private val viewModel: MerchantPreviewViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(MerchantPreviewViewModel::class.java)
    }

    private val viewModelCategories: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }

    lateinit var data: Merchants
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantPreviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //cache util
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        data = intent.getSerializableExtra(ConstantGuestMerchants.DATA_MERCHANT) as Merchants
        setSupportActionBar(binding.toolbarMerchant)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.toolbarMerchant.inflateMenu(R.menu.search_cart_menu)
        binding.toolbarMerchant.setOnMenuItemClickListener {
            onOptionsItemSelected(it)
        }

        getDetailMerchant(data.id!!.toInt())

        //get List product
        viewModel.setUpListMerchantProduct(data.id!!.toInt())
        getListProductsMerchant()

        //ProgressBar
        viewModel.status.observe(this, Observer {
            ShimmerUtil.updateProgressConstaraintLayout(
                it,
                binding.progressBar,
                binding.tvNetworkError,
                binding.layoutmerchantPreview
            )
        })
    }

    @SuppressLint("SetTextI18n")
    private fun showSearchDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_dialog_search_merchant)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogSearchMerchantBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        //Categori
        val items: ArrayList<Category> = arrayListOf()
        val itemsCategory: ArrayList<String> = arrayListOf()
        viewModelCategories.getCategories()
        viewModelCategories.categories.observe(this, Observer {
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsCategory.add(it.data[item].category!!)
            }
        })


        //filter product
        val categoriesArray = ArrayList<Category>()

        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsCategory)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.tvMakananFilter.text = binding.dropdownMakanan.text.toString()
                binding.tvMakananFilter.visibility = View.VISIBLE
                for (i in items.indices) {
                    if (binding.tvMakananFilter.text.toString() == items[i].category) {
                        Log.e("DATA_ISI_CATEGORY", items[i].category!!)
                        if (categoriesArray.size > 0) {
                            categoriesArray.clear()
                        }
                        categoriesArray.add(items[i])
                        break
                    }
                }
            }
        })
        binding.tvMakananFilter.setOnClickListener {
            categoriesArray.clear()
            binding.tvMakananFilter.text = ""
            binding.tvMakananFilter.visibility = View.GONE
        }

        //ACS DESC
        //Urutan Product
        val itemsUrutan: ArrayList<String> = arrayListOf("ASC", "DESC")
        val arrayUrutan =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsUrutan)
        binding.dropdownUrutkan.setAdapter(arrayUrutan)

        binding.tvCari.setOnClickListener {
            if (categoriesArray.isEmpty()) {
                setFilterProduct(
                    "",
                    binding.etKeyword.text.toString(),
                    binding.dropdownUrutkan.text.toString(),
                    dialog
                )
            } else {
                setFilterProduct(
                    categoriesArray[0].id.toString(),
                    binding.etKeyword.text.toString(),
                    binding.dropdownUrutkan.text.toString(),
                    dialog
                )
            }
        }

        //batal onClick
        binding.tvBatal.setOnClickListener {
            dialog.hide()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_cart_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_search -> showSearchDialog()
        }
        return true
    }

    private fun getDetailMerchant(id: Int) {
        viewModel.getDataDetailMerchant(id)
        viewModel.previewMerchant.observe(this, Observer {
            binding.textViewNamaMerchantPreview.text = it.marketName
            binding.textViewAlamatMerchantPreview.text = it.marketAddress
            binding.toolbarMerchant.title = it.marketName
            Glide.with(this).load(it.uri).circleCrop().into(binding.imageViewMerchantPreview)
        })
    }


    private fun getListProductsMerchant() {
        viewModel.getDataListMerchantProduct().observe(this, Observer {
            produkMerchantAdapter.submitList(it)
        })
        viewModel.statusMerchantProduct.observe(this, Observer {
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBarMerchantProduct,
                binding.recyclerProdukMerchant,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Produk Kosong",
                R.drawable.ic_empty_cart
            )
        })
        adapterListProductMerchant()
    }

    private fun adapterListProductMerchant() {
        produkMerchantAdapter =
            PagedAdapterUtil(
                R.layout.item_produk,
                { position, itemView, item ->
                    //categories

                    itemView.textViewNamaProduk.text = item.productName
                    val hargaDiskon =
                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                    itemView.textViewHargaDiskon.text =
                        changeToRupiah(
                            hargaDiskon
                        )
                    if (item.discount.toString().toDouble() > 0.0) {
                        itemView.textViewHargaAsliProduk.paintFlags =
                            itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        itemView.textViewHargaAsliProduk.text =
                            changeToRupiah(item.priceSelling.toDouble())

                    } else {
                        itemView.textViewHargaAsliProduk.visibility = View.INVISIBLE
                    }

                    itemView.textViewHargaAsliProduk.paintFlags =
                        itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    Glide.with(this).load(item.uri).into(itemView.imageViewProduk)

                    //Categories
                    var categories = ""
                    if (item.categories != null) {
                        for (i in item.categories.indices) {
                            categories += if (i < item.categories.size - 1) {
                                item.categories[i].category + ", "
                            } else {
                                item.categories[i].category
                            }
                        }
                        itemView.textViewKategoriProduk.text = categories
                    } else {
                        itemView.textViewKategoriProduk.text = "-"

                    }

                    //Beli Sekarang
                    itemView.btBeliSekarang.setOnClickListener {
                        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
                            auth = getAuth(cacheUtil)

                            val json = JsonObject()
                            json.addProperty("idMProducts", item.id)
                            json.addProperty("qty", "1")

                            addToCart(auth.token!!, json, {
                                if (it.code == 200) {
                                    Toast.makeText(
                                        this,
                                        "Berhasil Menambahkan Keranjang",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    val intent = Intent(
                                        this,
                                        KeranjangActivity::class.java
                                    )
                                    startActivity(intent)
                                } else {
                                    Toast.makeText(
                                        this,
                                        it.message,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }, {
                                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                            })
                        } else {
                            Toast.makeText(
                                this,
                                "Silahkan Login Terlebih Dahulu",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                },
                { position, item ->
                    val intent = Intent(
                        this,
                        DetailProductActivity::class.java
                    )
                        .putExtra(
                            INTENT_DETAIL,
                            item.id
                        )
                    startActivity(intent)
                })

        this.binding.recyclerProdukMerchant.layoutManager = GridLayoutManager(this, 2)
        this.binding.recyclerProdukMerchant.adapter = produkMerchantAdapter

        binding.emptyView.layoutEmptyView.visibility = View.GONE
        binding.recyclerProdukMerchant.visibility = View.VISIBLE

        binding.tvClearFilter.setOnClickListener {
            //get List product
            binding.tvClearFilter.visibility = View.GONE

            viewModel.statusMerchantProduct.observe(this, Observer {
                stateUtilWithEmptyView(
                    this,
                    it,
                    binding.progressBarMerchantProduct,
                    binding.recyclerProdukMerchant,
                    binding.emptyView.tvEmptyView,
                    binding.emptyView.imEmptyView,
                    binding.emptyView.layoutEmptyView,
                    "Data Produk Kosong",
                    R.drawable.ic_empty_cart
                )
            })
            viewModel.setUpListMerchantProduct(data.id!!.toInt())
            viewModel.getDataListMerchantProduct().observe(this, Observer {
                getListProductsMerchant()
                produkMerchantAdapter.submitList(it)
            })
        }
    }

    //filter product Merchant
    private fun setFilterProduct(
        category: String,
        search: String,
        orderDirection: String,
        dialog: Dialog
    ) {

        val jsonObject = JsonObject()
        if (category != "") {
            jsonObject.addProperty("category", category)
        }

        viewModel.setUpListMerchantFilterProduct(
            data.id!!.toInt(),
            search,
            orderDirection,
            jsonObject
        )

        Log.v("CEKDATALFILTER", "Id : "+data.id+" Serach : "+search+" Order Direction : "+ orderDirection+" jsonObject : "+jsonObject)
        observeFilterProduct()
        binding.tvClearFilter.visibility = View.VISIBLE
        dialog.hide()


    }

    private fun observeFilterProduct() {
        adapterListProductMerchant()

        viewModel.getDataListMerchantFilterProduct().observe(this, Observer {
            produkMerchantAdapter.submitList(it)
            Log.v("CEKDATALFILTER", it.toString())
        })
        viewModel.statusMerchantFilterProduct.observe(this, Observer {
            Log.d("STATUS_FILTER_PRODUCT", it.toString())
            stateUtilWithEmptyView(
                this,
                it,
                binding.progressBarMerchantProduct,
                binding.recyclerProdukMerchant,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Produk Tidak Ditemukan",
                R.drawable.ic_empty_cart
            )
        })

    }


    //Request Filter Product
//    private fun requestFilterProduct(
//        jsonObject: JsonObject,
//        orderDirection: String,
//        search: String,
//        onSuccess: (ResponseProductByMerchant) -> Unit,
//        onFailed: (String) -> Unit
//    ) {
//        val apiService: GuestMerchantsService by lazy {
//            ApiService().createService(GuestMerchantsService::class.java)
//        }
//
//        val call: retrofit2.Call<ResponseProductByMerchant> =
//            apiService.getFilterProductMerchant(
//                data.id!!.toInt(),
//                search,
//                orderDirection,
//                jsonObject
//
//            )
//        call.enqueue(object : Callback<ResponseProductByMerchant> {
//            override fun onResponse(
//                call: retrofit2.Call<ResponseProductByMerchant>,
//                response: Response<ResponseProductByMerchant>
//            ) {
//                Log.d("FILTER_PRODUCT", response.body().toString())
//                if (response.isSuccessful)
//                    if (response.body() != null) onSuccess(response.body()!!)
//                    else onFailed(response.message().toString())
//                else onFailed(response.message()).toString()
//            }
//
//            override fun onFailure(call: retrofit2.Call<ResponseProductByMerchant>, t: Throwable) {
//                onFailed(t.message.toString())
//            }
//
//        })
//    }


    //Add Cart
    private fun addToCart(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataAddCart) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val call: retrofit2.Call<DataAddCart> =
            ApiService().serviceCart.addCart(auth, jsonObject)
        call.enqueue(object : Callback<DataAddCart> {
            override fun onFailure(call: retrofit2.Call<DataAddCart>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: retrofit2.Call<DataAddCart>,
                response: Response<DataAddCart>
            ) {
                Log.d("ADD_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }


}