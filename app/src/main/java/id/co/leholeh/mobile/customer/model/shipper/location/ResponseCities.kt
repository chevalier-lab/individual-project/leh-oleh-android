package id.co.leholeh.mobile.customer.model.shipper.location

import com.google.gson.annotations.SerializedName

data class ResponseCities(

	@field:SerializedName("data")
	val data: DataCities? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RowsItemCities(

	@field:SerializedName("province_id")
	val provinceId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)

data class DataCities(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItemCities>? = null,

	@field:SerializedName("content")
	val content: String? = null
)
