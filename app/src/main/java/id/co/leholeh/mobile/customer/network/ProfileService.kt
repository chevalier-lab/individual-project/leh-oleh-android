package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantProfile
import id.co.leholeh.mobile.customer.model.profile.ResponseChangePhotoProfile
import id.co.leholeh.mobile.customer.model.profile.ResponseProfile
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ProfileService {
    @GET(ConstantProfile.BASE_URL_PROFILE_USER)
    suspend fun getProfile(@Header("Authorization") authorization : String) : ResponseProfile

    @PUT(ConstantProfile.BASE_URL_PROFILE_USER)
    fun editProfile(@Header("Authorization") authorization : String, @Body jsonObject: JsonObject) : Call<ResponseProfile>

    @Multipart
    @POST(ConstantProfile.BASE_URL_CHANGE_PHOTO_PROFILE)
    fun changePhotoProfile(
        @Header("Authorization") authorization: String,
        @Part fotoProfile: MultipartBody.Part
    ): Call<ResponseChangePhotoProfile>
}