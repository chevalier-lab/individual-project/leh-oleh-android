package id.co.leholeh.mobile.customer.model.notification

import com.google.gson.annotations.SerializedName

data class ResponseNotifications(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataNotification>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataNotification(

	@field:SerializedName("is_broadcast")
	val isBroadcast: String? = null,

	@field:SerializedName("is_read")
	val isRead: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("title")
	val title: String? = null
)
