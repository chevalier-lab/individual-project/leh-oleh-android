package id.co.leholeh.mobile.customer.model.profile

import com.google.gson.annotations.SerializedName

data class ResponseChangePhotoProfile(

    @field:SerializedName("code")
	val code: Int? = null,

    @field:SerializedName("data")
	val data: FotoProfile? = null,

    @field:SerializedName("error")
	val error: List<Any?>? = null,

    @field:SerializedName("message")
	val message: String? = null
)


