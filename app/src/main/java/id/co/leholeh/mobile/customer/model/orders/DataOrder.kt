package id.co.leholeh.mobile.customer.model.orders

import com.google.gson.annotations.SerializedName

data class DataOrder (
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<Order>? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)