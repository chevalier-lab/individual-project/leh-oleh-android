package id.co.leholeh.mobile.merchant.ui.barang

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantTambahBarangBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.TempAddBarang
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_category.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class MerchantTambahBarangActivity : AppCompatActivity() {
    lateinit var binding: ActivityMerchantTambahBarangBinding
    private lateinit var adapterTextCategory: AdapterUtil<String>
    private val viewModelProductList: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    val categoriesArray = arrayListOf<String>()
    var categoriesArrayFinal = arrayListOf<String>()
    private lateinit var adapterTextLocation: AdapterUtil<String>
    val locationArray = arrayListOf<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantTambahBarangBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            setupToolbar()
            setDataCategories()
            setDataLocation()

            binding.inputCover.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
            }
            binding.btProses.setOnClickListener {
                sendRequestCreateProductMerchant()
            }
        }

    }

    val valueCategory: ArrayList<String> = arrayListOf()

    //Categories
    private fun setDataCategories() {
        val items: ArrayList<Category> = arrayListOf()
        val itemsCategory: ArrayList<String> = arrayListOf()
        viewModelProductList.getCategories()
        viewModelProductList.categories.observe(this, Observer {
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsCategory.add(it.data[item].category!!)
            }
        })
        //set recycler categories
        adapterTextCategory = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { position, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                valueCategory.remove(item)
                categoriesArray.removeAt(position)
                adapterTextCategory.refresh()
            })

        val adapterCategory =
            ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemsCategory)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                for (i in items.indices) {
                    if (binding.dropdownMakanan.text.toString() == items[i].category) {
                        Log.e("DATA_ISI_CATEGORY", items[i].category!!)
                        categoriesArray.add(items[i].id!!.toInt().toString())
                        Log.d("DATA_CATEGORY", categoriesArray.toString())
                        var kondisi = true
                        if (valueCategory.size > 0) {
                            for (j in valueCategory.indices) {
                                if (binding.dropdownMakanan.text.toString() == valueCategory[j]) {
                                    kondisi = false
                                }
                            }
                            if (kondisi) {
                                Log.d("KATEGORI BEDA", "KATEGORI BEDA")
                                valueCategory.add(items[i].category!!)
                            } else {
                                Toast.makeText(
                                    this@MerchantTambahBarangActivity,
                                    "Kategori sudah ada",
                                    Toast.LENGTH_SHORT
                                ).show()

                            }
                        } else {
                            valueCategory.add(items[i].category!!)
                        }
                        if (valueCategory.size > 0) adapterTextCategory.data = valueCategory
                        break
                    }
                }
                categoriesArrayFinal = categoriesArray
            }
        })

        Log.d("array kategori1", categoriesArrayFinal.toString())


        binding.rvMakananFilter.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvMakananFilter.adapter = adapterTextCategory
    }

    private fun sendRequestCreateProductMerchant() {

        val name = binding.etNamabarang.text.toString()
        val slug = name.replace(" ", "-").toLowerCase(Locale.getDefault())
        val jumlah = binding.etQty.text.toString()
        val hargaJual = binding.etHargaJual.text.toString()
        val hargaBeli = binding.etHargaModal.text.toString()
        val description = binding.etDeskripsi.text.toString()
        val diskon = binding.etDiskon.text.toString()
        val length = binding.etLength.text.toString()
        val width = binding.etWidth.text.toString()
        val height = binding.etHeight.text.toString()
        val weight = binding.etWeight.text.toString()

        if (name.isEmpty() || jumlah.isEmpty() || hargaJual.isEmpty() ||
            hargaBeli.isEmpty() || description.isEmpty() || diskon.isEmpty()
            || binding.imageViewCover.isGone
        ) {
            Toast.makeText(this, "Harap isi semua form!", Toast.LENGTH_SHORT).show()
        } else {
            when {
                diskon.toInt() > 100 -> {
                    Toast.makeText(this, "Diskon maksimal 100", Toast.LENGTH_SHORT).show()
                }
                categoriesArray.size <= 0 -> {
                    Toast.makeText(this, "Kategori minimal 1", Toast.LENGTH_SHORT).show()
                }
                locationArray.size <= 0 -> {
                    Toast.makeText(this, "Lokasi minimal 1", Toast.LENGTH_SHORT).show()
                }
                jumlah.toInt() <= 0 || hargaBeli.toInt() <= 0 || hargaJual.toInt() <= 0 -> {
                    Toast.makeText(this, "Ada input yg tidak valid", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    val drawableCover = binding.imageViewCover.drawable as BitmapDrawable
                    val bitmapCover = drawableCover.bitmap
                    val fileCover = createTempFile(bitmapCover)
                    val imageCover = MultipartBody.Part.createFormData(
                        "cover",
                        fileCover!!.name,
                        fileCover.asRequestBody("image/*".toMediaTypeOrNull())
                    )

                    //TODO add dimension property
                    val temp = TempAddBarang(
                        name, jumlah, diskon, hargaBeli, hargaJual,
                        description, categoriesArray, locationArray,
                        fileCover, valueCategory, valueLocation,
                        length, width, height, weight
                    )

                    val intent = Intent(this, MerchantTambahFotoBarangActivity::class.java)
                    intent.putExtra(
                        ConstantManageProductMerchant.ID_PRODUCT_MERCHANT,
                        temp
                    )
                    startActivity(intent)
                }
            }
        }
    }


    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Tambah Barang"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image: Image = ImagePicker.getFirstImageOrNull(data)
            Glide.with(binding.imageViewCover).load(image.uri).into(binding.imageViewCover)
            binding.imageViewCover.visibility = View.VISIBLE
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    //Location
    val valueLocation: ArrayList<String> = arrayListOf()

    private fun setDataLocation() {
        val items: ArrayList<LocationProduct> = arrayListOf()
        val itemsLocation: ArrayList<String> = arrayListOf()
        viewModelProductList.getLocation()
        viewModelProductList.location.observe(this, Observer {
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsLocation.add(it.data[item].provinceName!!)
            }
        })
        //set recycler categories
        adapterTextLocation = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { position, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                valueLocation.remove(item)
                locationArray.removeAt(position)
                adapterTextLocation.refresh()
            })

        val adapterLocation =
            ArrayAdapter(this, R.layout.item_list_dropdown_text, itemsLocation)
        binding.dropdownLokasi.setAdapter(adapterLocation)
        binding.dropdownLokasi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable?) {
                for (i in items.indices) {
                    if (binding.dropdownLokasi.text.toString() == items[i].provinceName) {
                        Log.e("DATA_ISI_LOCATION", items[i].provinceName!!)
                        locationArray.add(items[i].id!!.toInt().toString())
                        Log.d("DATA_CATEGORY", locationArray.toString())
                        var kondisi = true
                        if (valueLocation.size > 0) {
                            for (j in valueLocation.indices) {
                                if (binding.dropdownLokasi.text.toString() == valueLocation[j]) {
                                    kondisi = false
                                }
                            }
                            if (kondisi) {
                                Log.d("KATEGORI BEDA", "KATEGORI BEDA")
                                valueLocation.add(items[i].provinceName!!)
                            } else {
                                Toast.makeText(
                                    this@MerchantTambahBarangActivity,
                                    "Lokasi sudah ada",
                                    Toast.LENGTH_SHORT
                                ).show()
                                kondisi=true
                            }
                        } else {
                            valueLocation.add(items[i].provinceName!!)
                        }
                        if (valueLocation.size > 0) adapterTextLocation.data = valueLocation
                        break
                    }
                }
            }
        })

        binding.rvLokasi.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvLokasi.adapter = adapterTextLocation


    }


}