package id.co.leholeh.mobile.merchant.network

import id.co.leholeh.mobile.merchant.constant.ConstantDahboardMerchant
import id.co.leholeh.mobile.merchant.model.dashboard.ResponseIncome
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface DashboardService {

    @GET(ConstantDahboardMerchant.BASE_URL_INCOME)
    suspend fun getDataDashboardIncome(@Header("Authorization") auth : String, @Query("date") date : String) : ResponseIncome
}