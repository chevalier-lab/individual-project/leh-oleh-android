package id.co.leholeh.mobile.merchant.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ManageProductService {
    @GET(ConstantManageProductMerchant.BASE_URL_PRODUCTS_MERCHANT)
    suspend fun getListProductsMerchant(@Header("Authorization") auth : String, @Query("page") page : Int) : Response<ResponseProductsMerchant>

    @GET(ConstantManageProductMerchant.BASE_URL_GET_PRODUCT_MERCHANT)
    suspend fun getDetailProductMerchant(@Header("Authorization")  auth : String, @Path("id") id : Int) : ResponseDetailProductMerchant

    @Multipart
    @POST(ConstantManageProductMerchant.BASE_URL_PRODUCTS_MERCHANT)
    fun createProductMerchant(
        @Header("Authorization") auth: String,
        @Part cover : MultipartBody.Part,
        @Part name: MultipartBody.Part,
        @Part slug: MultipartBody.Part,
        @Part jumlah: MultipartBody.Part,
        @Part diskon: MultipartBody.Part,
        @Part hargaJual: MultipartBody.Part,
        @Part hargaBeli: MultipartBody.Part,
        @Part description: MultipartBody.Part,
        @Part jsonArray: MultipartBody.Part
    ) : Call<ResponseCreateProductMerchant>

    @POST(ConstantManageProductMerchant.BASE_URL_PRODUCT_DIMENSION)
    fun createProductDimension(
        @Header("Authorization") auth: String,
        @Body jsonObject: JsonObject
    ): Call<ResponseCreateProductDimension>

    @PUT(ConstantManageProductMerchant.BASE_URL_DETAIL_PRODUCT_DIMENSION)
    fun updateProductDimension(
        @Header("Authorization") auth: String,
        @Path("id") id : Int,
        @Body jsonObject: JsonObject
    ): Call<ResponseUpdateProductDimension>

    @Multipart
    @POST(ConstantManageProductMerchant.BASE_URL_PRODUCT_PHOTOS)
    fun addPhotosProductMerchant(
        @Header("Authorization") auth: String,
        @Part photos: MultipartBody.Part,
        @Part idProduct: MultipartBody.Part
    ): Call<ResponseAddPhotosProduct>

    @DELETE(ConstantManageProductMerchant.BASE_URL_REMOVE_PRODUCT)
    fun deleteProductMerchant(@Header("Authorization")  auth : String,  @Path("id") id : Int) : Call<ResponseRemoveProduct>

    @PUT(ConstantManageProductMerchant.BASE_URL_GET_PRODUCT_MERCHANT)
    fun updateProductMerchant(@Header("Authorization")  auth : String, @Body jsonObject: JsonObject, @Path("id") id : Int) : Call<ResponseUpdateProduct>

    @PUT(ConstantManageProductMerchant.BASE_URL_PRODUCT_CATEGORIES_MERCHANT)
    fun updateCategoriesProductMerchant(@Header("Authorization")  auth : String, @Body jsonObject: JsonObject, @Path("id") id : Int) : Call<ResponseUpdateProductCategories>

    @GET(ConstantManageProductMerchant.BASE_URL_PRODUCT_LOCATION_MERCHANT)
    suspend fun getLocationProducts(@Header("Authorization")  auth : String,@Path("id")id:Int):ResponseGetLocationProducts

    @PUT(ConstantManageProductMerchant.BASE_URL_PRODUCT_LOCATION_UPDATE_MERCHANT)
    fun updateProductLocations(@Header("Authorization")  auth : String,@Path("id")id:String,@Body jsonObject: JsonObject)

    @PUT(ConstantManageProductMerchant.BASE_URL_PRODUCT_DISCOUNT_UPDATE_MERCHANT)
    fun setDiscountProduct(@Header("Authorization")  auth : String, @Path("id") id: Int?, @Body jsonObject: JsonObject) : Call<ResponseUpdateLocationProductMerchant>

    @DELETE(ConstantManageProductMerchant.BASE_URL_PRODUCT_LOCATION_DELETE_MERCHANT)
    fun removeProductLocation(@Header("Authorization")  auth : String,@Path("id")id:Int):Call<ResponseAddPhotosProduct>

    @DELETE(ConstantManageProductMerchant.BASE_URL_PRODUCT_PHOTO_DELETE_MERCHANT)
    fun removeProductPhotos(@Header("Authorization")  auth : String,@Path("id")id:Int):Call<ResponseAddPhotosProduct>

    @Multipart
    @POST(ConstantManageProductMerchant.BASE_URL_PRODUCT_COVER_UPDATE_MERCHANT)
    fun changeCoverMerchant(@Header("Authorization")  auth : String,@Path("id")id:Int,@Part cover: MultipartBody.Part) : Call<ResponseAddPhotosProduct>

    @POST(ConstantManageProductMerchant.BASE_URL_PRODUCT_LOCATION_CREATE_MERCHANT)
    fun createProductLocation(@Header("Authorization")  auth : String,@Body jsonObject: JsonObject) : Call<ResponseCreateLocation>
}