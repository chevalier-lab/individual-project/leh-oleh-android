package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class AddCart(

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("id_m_users")
    val idMUsers: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("id_m_products")
    val idMProducts: String? = null,

    @field:SerializedName("product_name")
    val productName: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("username")
    val username: String? = null
)