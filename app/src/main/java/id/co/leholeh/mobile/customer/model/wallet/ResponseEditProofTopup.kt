package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseEditProofTopup(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: EditProofTopup? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)


