package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class CategoriesItemMerchant(

	@field:SerializedName("id_u_product_categories")
	val idUProductCategories: String? = null,

	@field:SerializedName("id_m_categories")
	val idMCategories: String? = null,

	@field:SerializedName("category")
	val category: String? = null
)