package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName

data class ResponseRegisterMerchant(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Data(

	@field:SerializedName("id_merchant")
	val idMerchant: Int? = null
)
