package id.co.leholeh.mobile.customer.ui.profile.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityLoginBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.DataLogin
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.register.MenuRegisterUserActivity
import id.co.leholeh.mobile.utils.CacheUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity() {

    private lateinit var cacheUtil: CacheUtil

    lateinit var binding: ActivityLoginBinding

    companion object {
        const val TAG = "LoginActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar!!.title = "Login"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        //Init Cache
        this.cacheUtil= CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        handlerInput()

        //Login With Google
        binding.llGoogle.setOnClickListener {
            Toast.makeText(
                this,
                getString(R.string.fitur_belum),
                Toast.LENGTH_SHORT
            ).show()
        }

        //Login With Facebook
        binding.llFacebook.setOnClickListener {
            Toast.makeText(
                this,
                getString(R.string.fitur_belum),
                Toast.LENGTH_SHORT
            ).show()
        }

        //Login With Lupa Password
        binding.textView8.setOnClickListener {
            Toast.makeText(
                this,
                getString(R.string.fitur_belum),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }

    private fun handlerInput() {
        loginValidation()
        binding.etUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                loginValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        binding.etPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                loginValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        binding.btProses.setOnClickListener {
            val jsonObject = JsonObject()
            jsonObject.apply {
                addProperty("username", binding.etUsername.text.toString())
                addProperty("password", binding.etPassword.text.toString())
            }
            setLogin(jsonObject,{
               if (it.code==200){
                   this.cacheUtil.set(ConstantAuth.AUTH,it.data)
                   finish()
                   startActivity(Intent(this@LoginActivity,
                       MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK))
               }else Toast.makeText(applicationContext, it.message, Toast.LENGTH_LONG).show()
            },{
                Toast.makeText(applicationContext, it, Toast.LENGTH_LONG).show()
            } )
        }

        binding.tvDaftar.setOnClickListener {
            val intent = Intent(this@LoginActivity, MenuRegisterUserActivity::class.java)
            startActivity(intent)
            finish()
        }

    }


    private fun setLogin(
        jsonObject: JsonObject,
        onSuccess: (DataLogin) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<DataLogin> = ApiService().serviceAuth.getLogin(jsonObject)
        call.enqueue(object : Callback<DataLogin> {
            override fun onFailure(call: Call<DataLogin>, t: Throwable) {
                onFailed(t.message.toString())
                Log.i(TAG, "${t.message} --- ${t.printStackTrace()}")
            }

            override fun onResponse(call: Call<DataLogin>, response: Response<DataLogin>) {
                Log.d("DataLogin", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }
        })
    }


    private fun loginValidation() {
        if (checkFormFilled()) binding.btProses.apply {
            isEnabled = true
            setBackgroundResource(R.drawable.rounded_corners)
            setTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
        }
        else binding.btProses.apply {
            isEnabled = false
            setBackgroundResource(R.drawable.rounded_grey_checkout)
            setTextColor(ContextCompat.getColor(applicationContext, R.color.colorBlack))
        }
    }

    private fun checkFormFilled(): Boolean =
        (!TextUtils.isEmpty(binding.etUsername.text.toString()) && !TextUtils.isEmpty(binding.etPassword.text.toString()))
}