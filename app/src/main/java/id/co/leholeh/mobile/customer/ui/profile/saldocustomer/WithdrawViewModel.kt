package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.wallet.DetailWithdraw
import id.co.leholeh.mobile.customer.model.wallet.Withdraw
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.WithdrawService
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup.RequestWithdrawDataSource
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup.WithdrawDataSource
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WithdrawViewModel : ViewModel() {
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()


    lateinit var statustListHistoryWithdraw: LiveData<ApiStatus>
    private lateinit var statusWithdrawHistoryList: LiveData<PagedList<Withdraw>>

    private val _statusWithdrawDataSource = MutableLiveData<WithdrawDataSource>()

    // Request
    lateinit var statusListRequestWithdraw: LiveData<ApiStatus>
    private lateinit var statusRequestWithdrawList: LiveData<PagedList<Withdraw>>

    private val _statusRequestWithdrawDataSource = MutableLiveData<RequestWithdrawDataSource>()

    private val _detailWithdraw = MutableLiveData<DetailWithdraw>()
    val detailWithdraw: LiveData<DetailWithdraw>
        get() = _detailWithdraw

    private val _statusDetailWithDraw = MutableLiveData<ApiStatus>()
    val statusDetailWithDraw: LiveData<ApiStatus>
        get() = _statusDetailWithDraw

    private val serviceWithdraw: WithdrawService by lazy {
        ApiService().createService(WithdrawService::class.java)
    }

    // Start of list history withdraw
    fun setUpListWithdraw(authorization: String) {
        statustListHistoryWithdraw = Transformations.switchMap<WithdrawDataSource, ApiStatus>(
            _statusWithdrawDataSource,
            WithdrawDataSource::apiState
        )
        statusWithdrawHistoryList = initPagedListWithdraw(authorization, config).build()
    }

    fun getListHistoryWithdraw(): LiveData<PagedList<Withdraw>> = statusWithdrawHistoryList

    private fun initPagedListWithdraw(
        authorization: String,
        config: PagedList.Config
    ): LivePagedListBuilder<Int, Withdraw> {
        val factory = object : DataSource.Factory<Int, Withdraw>() {
            override fun create(): DataSource<Int, Withdraw> {
                val withdrawDataSource = WithdrawDataSource(Dispatchers.IO, authorization)
                _statusWithdrawDataSource.postValue(withdrawDataSource)

                return withdrawDataSource
            }
        }
        return LivePagedListBuilder(factory, config)
    }
    // End of list history withdraw

    // Start of list request withdraw
    fun setUpListRequestWithdraw(authorization: String) {
        statusListRequestWithdraw = Transformations.switchMap<RequestWithdrawDataSource, ApiStatus>(
            _statusRequestWithdrawDataSource,
            RequestWithdrawDataSource::apiState
        )
        statusRequestWithdrawList = initPagedListRequestWithdraw(authorization, config).build()
    }

    fun getListRequestWithdraw(): LiveData<PagedList<Withdraw>> = statusRequestWithdrawList

    private fun initPagedListRequestWithdraw(
        authorization: String,
        config: PagedList.Config
    ): LivePagedListBuilder<Int, Withdraw> {
        val factory = object : DataSource.Factory<Int, Withdraw>() {
            override fun create(): DataSource<Int, Withdraw> {
                val requestWithdrawDataSource =
                    RequestWithdrawDataSource(Dispatchers.IO, authorization)
                _statusRequestWithdrawDataSource.postValue(requestWithdrawDataSource)

                return requestWithdrawDataSource
            }
        }
        return LivePagedListBuilder(factory, config)
    }
    // End of list request withdraw

    private suspend fun getDetailWithdraw(authorization: String, id: Int) = try {
        _statusDetailWithDraw.postValue(ApiStatus.LOADING)
        _detailWithdraw.postValue(serviceWithdraw.getDetailWithdraw(authorization, id).data)
        _statusDetailWithDraw.postValue(ApiStatus.SUCCESS)
    } catch (e: Exception) {
        _statusDetailWithDraw.postValue(ApiStatus.FAILED)
        Log.d("ERR_REQ_DETAIL_WD", e.localizedMessage!!)
    }


    fun getDataDetailWithdraw(authorization: String, id: Int) = viewModelScope.launch {
        getDetailWithdraw(authorization, id)
    }
}