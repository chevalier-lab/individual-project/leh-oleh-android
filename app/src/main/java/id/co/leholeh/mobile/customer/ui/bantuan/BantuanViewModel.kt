package id.co.leholeh.mobile.customer.ui.bantuan

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.faq.DataFaq
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.FaqService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class BantuanViewModel : ViewModel() {
    private val _getListFaq = MutableLiveData<List<DataFaq>>()
    val listFaq : LiveData<List<DataFaq>>
        get() = _getListFaq

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
        get() = _status

    private val serviceFaq : FaqService by lazy {
        ApiService().createService(FaqService::class.java)
    }

    private suspend fun requestListFaq(){
        try {
            _status.postValue(ApiStatus.LOADING)
            _getListFaq.postValue(serviceFaq.getListFaq().data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("REQ_FAQ", e.localizedMessage!!)
        }
    }

    fun getListFaq(){
        viewModelScope.launch {
            requestListFaq()
        }
    }

}