package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ResponseDetailProductMerchant(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataDetailProductMerchant? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class CategoriesItemDetailMerchant(

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id_m_categories")
	val idMCategories: String? = null,

	@field:SerializedName("category")
	val category: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null
)

data class DataDetailProductMerchant(

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("rating")
	val rating: Any? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("photos")
	val photos: List<PhotosItem>? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("location")
	val location: List<LocationsItem>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItemDetailMerchant>? = null,

	@field:SerializedName("id_cover")
	val idCover: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("info")
	val info: Info? = null
)
data class LocationsItem(

	@field:SerializedName("id_m_locations")
	val idMLocations: String? = null,

	@field:SerializedName("id_u_product_location")
	val idUProductLocation: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)

data class PhotosItem(

	@field:SerializedName("id_u_product_photos")
	val idUProductPhotos: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null
)

data class Info(

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("length")
	val length: String? = null,

	@field:SerializedName("width")
	val width: String? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("height")
	val height: String? = null
)


