package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class DataCart(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Cart? = null,

    @field:SerializedName("error")
    val error: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)