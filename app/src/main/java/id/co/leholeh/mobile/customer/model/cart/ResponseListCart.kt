package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class ResponseListCart(
	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataItemCart>? = null,

	@field:SerializedName("error")
	val error: String? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class ProductLocationItemCart(
	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)

data class DataItemCart(
	@field:SerializedName("id_u_user_merchant")
	val idUUserMerchant: String? = null,

	@field:SerializedName("total")
	val total: Int? = null,

	@field:SerializedName("id_merchant")
	val idMerchant: String? = null,

	@field:SerializedName("location")
	val location: LocationCart? = null,

	@field:SerializedName("id_m_users_merchant")
	val idMUsersMerchant: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null,

	@field:SerializedName("products")
	val products: List<ProductsItemCart>? = null,

	@field:SerializedName("market_phone_number")
	val marketPhoneNumber: String? = null
)

data class CategoriesItem(
	@field:SerializedName("category")
	val category: String? = null
)

data class LocationCart(
	@field:SerializedName("id_u_user_is_merchant")
	val idUUserIsMerchant: String? = null,

	@field:SerializedName("suburb_id")
	val suburbId: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("province")
	val province: String? = null,

	@field:SerializedName("province_id")
	val provinceId: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("suburb")
	val suburb: String? = null,

	@field:SerializedName("area_id")
	val areaId: String? = null,

	@field:SerializedName("city_id")
	val cityId: String? = null
)

data class ProductInfoCart(
	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("length")
	val length: String? = null,

	@field:SerializedName("width")
	val width: String? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("height")
	val height: String? = null
)

data class ProductsItemCart(
	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("product_info")
	val productInfo: ProductInfoCart? = null,

	@field:SerializedName("subTotal")
	val subTotal: Int? = null,

	@field:SerializedName("id_m_products")
	val idMProducts: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("id_m_users_merchant")
	val idMUsersMerchant: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("product_location")
	val productLocationCart: List<ProductLocationItemCart>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItem>? = null,

	@field:SerializedName("status")
	val status: String? = null
)
