package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityCustomerTopUpSaldoCustomerBinding
import id.co.leholeh.databinding.ComponentDialogTopupDetailBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.API_KEY_MIDTRANS
import id.co.leholeh.mobile.customer.constant.BASE_URL_INVOICE_MIDTRANS
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.midtransbody.ResponseMidtransTranscation
import id.co.leholeh.mobile.customer.model.wallet.ResponseRequestTopup
import id.co.leholeh.mobile.customer.model.wallet.ResponseUpdateTokenTopup
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.TopupService
import id.co.leholeh.mobile.customer.network.midtrans.MidtransService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.TagihanTopup
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_withdraw.view.*
import okhttp3.Credentials
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

class CustomerTopUpSaldoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCustomerTopUpSaldoCustomerBinding
    private lateinit var adapterTopup: PagedAdapterUtil<TagihanTopup>
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val viewModel: TopupViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(TopupViewModel::class.java)
    }
    private val basicAuth = Credentials.basic(API_KEY_MIDTRANS,"")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomerTopUpSaldoCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setup Toolbar
        setupToolbar()
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            getListTagihanTopup()
            sendTopupRequest()
        }
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.top_up)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.menu_history -> {
            startActivity(Intent(this, HistoryTopupActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    private fun showDialogDetailTopUp(tokenPayment: String, balanceTransfer: String, token: String, status : String) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.component_dialog_topup_detail)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogTopupDetailBinding.inflate(layoutInflater)
        binding.tvNominalTopup.text = changeToRupiah(balanceTransfer.toDouble())
        if(status == "2"){
            viewModel.getTransactionStatus(basicAuth, token)
            viewModel.transactionStatus.observe(this, Observer { responseStat ->
                if (responseStat.statusCode == "200" && responseStat.transactionStatus == "settlement") {
                    binding.tVLabelPaymentType.visibility = View.VISIBLE
                    binding.textViewPaymentType.visibility = View.VISIBLE
                    binding.textViewPaymentType.text = responseStat.paymentType?.replace("_", " ")
                    binding.btnBayarTagihan.text = "Ok"
                    binding.textViewStatus.text = getString(R.string.proses).toUpperCase(Locale.getDefault())
                    binding.textViewStatus.setBackgroundResource(R.drawable.status_diproses)
                    binding.btnBayarTagihan.setOnClickListener {
                        dialog.dismiss()
                    }
                    Log.d("TRUE_SETTLEMENT", "${responseStat.transactionStatus}")
                }else if(responseStat.statusCode == "407" && responseStat.transactionStatus == "expire"){
                    binding.tVLabelPaymentType.visibility = View.GONE
                    binding.textViewPaymentType.visibility = View.GONE
                    binding.textViewStatus.text = getString(R.string.kadaluarsa)
                    binding.textViewStatus.setBackgroundResource(R.drawable.status_refund)
                    binding.btnBayarTagihan.text = "Ok"
                    binding.btnBayarTagihan.setOnClickListener {
                        dialog.dismiss()
                    }
                    Log.d("TRUE_EXPIRE", "${responseStat.transactionStatus}")
                }else if(responseStat.statusCode == "404"){
                    binding.tVLabelPaymentType.visibility = View.GONE
                    binding.textViewPaymentType.visibility = View.GONE
                    binding.btnBayarTagihan.text = "Bayar Tagihan"
                    binding.textViewStatus.text = getString(R.string.diproses)
                    binding.textViewStatus.setBackgroundResource(R.drawable.status_not_payment)
                    binding.btnBayarTagihan.setOnClickListener {
                        val tabsIntent = CustomTabsIntent.Builder().build()
                        tabsIntent.launchUrl(this, Uri.parse(BASE_URL_INVOICE_MIDTRANS + tokenPayment))
                        dialog.dismiss()
                    }
                    Log.d("TRUE_NUNGGU_BYR", "${responseStat.transactionStatus}")
                } else{
                    binding.tVLabelPaymentType.visibility = View.GONE
                    binding.textViewPaymentType.visibility = View.GONE
                    binding.btnBayarTagihan.text = "Cek transaksi"
                    binding.textViewStatus.text = "Transaksi Bermasalah"
                    binding.textViewStatus.setBackgroundResource(R.drawable.status_refund)
                    binding.btnBayarTagihan.setOnClickListener {
                        val tabsIntent = CustomTabsIntent.Builder().build()
                        tabsIntent.launchUrl(this, Uri.parse(BASE_URL_INVOICE_MIDTRANS + tokenPayment))
                        dialog.dismiss()
                    }
                    Log.d("TRUE_NUNGGU_BYR", "${responseStat.transactionStatus}")
                }
            })
        }

        binding.btnBatal.setOnClickListener {
            dialog.dismiss()
        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    private fun getState() {
        viewModel.statusListTopup.observe(this, Observer {
            when (it) {
                ApiStatus.SUCCESS -> {
                    binding.progressBarTopup.visibility = View.GONE
                }
                ApiStatus.LOADING -> {
                    binding.progressBarTopup.visibility = View.VISIBLE
                }
                ApiStatus.EMPTY -> {
                    binding.progressBarTopup.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {
                    binding.progressBarTopup.visibility = View.GONE
                }
                ApiStatus.EMPTY_AFTER -> {
                    binding.progressBarTopup.visibility = View.GONE
                }
                ApiStatus.LOADED -> {
                    binding.progressBarTopup.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    binding.progressBarTopup.visibility = View.GONE
                    Toast.makeText(this, "Jaringan bermasalah", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    binding.progressBarTopup.visibility = View.GONE
                    Toast.makeText(this, "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    private fun observerListTopup(){
        viewModel.getDataListTagihanTop().observe(this, Observer {
            adapterTopup.submitList(it)
        })
        getState()
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun getListTagihanTopup() {
        viewModel.setUpListTagihanTopup(auth.token!!)
        observerListTopup()
        //init adapter
        adapterTopup =
            PagedAdapterUtil(
                R.layout.item_withdraw,
                { _, itemView, item ->
                    when (item.status) {
                        "0" -> {
                            itemView.tv_status.visibility = View.VISIBLE
                            itemView.tv_status.setBackgroundResource(R.drawable.status_refund)
                            itemView.tv_status.text = getString(R.string.failed)
                        }
                        else -> {
                            itemView.tv_status.visibility = View.GONE
                        }
                    }

                    itemView.tv_bank.text = item.walletName
                    itemView.tv_nominal.text = changeToRupiah(item.balanceRequest!!.toDouble())
                    val tglTransaksi =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.createdAt!!)
                    itemView.textViewTglTransaksi.text =
                        SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTransaksi!!)

                },
                { _, item ->
                    //setup showdialog
                    when (item.status) {
                        "2" -> {
                            //menunggu pembayaran
                            if (item.tokenPayment != null && item.token != null) {
                                showDialogDetailTopUp(item.tokenPayment, item.balanceTransfer!!, item.token, item.status)
                            } else {
                                Toast.makeText(
                                    this,
                                    "Permintaan topup tidak valid",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                    }
                })

        binding.rvTopUp.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvTopUp.adapter = adapterTopup

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.history_menu, menu)
        return true
    }

    private fun sendTopupRequest() {
        val nominal = binding.editTextNominalTopup.text
        binding.btProses.setOnClickListener {
            if (nominal.isNullOrEmpty()) {
                Toast.makeText(
                    this,
                    getString(R.string.message_nonnull_nominal),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val jsonObject = JsonObject()
                val balanceRequest = nominal.toString().toInt()
                val balanceTransfter = balanceRequest + Random.nextInt(0, 999)
                jsonObject.apply {
                    addProperty("balance_request", balanceRequest)
                    addProperty("balance_transfer", balanceTransfter)
                    addProperty("status", 2)
                }
                requestTopupSaldo(jsonObject, {
                    if (it.code == 200) {
                        val jsonObjectDetails = JsonObject()
                        val jsonObjectCreditCard = JsonObject()
                        val jsonObjectMidtrans = JsonObject()
                        //Transaction Details
                        jsonObjectDetails.addProperty("order_id", it.data!!.token)
                        jsonObjectDetails.addProperty("gross_amount", balanceTransfter)
                        jsonObjectMidtrans.add("transaction_details", jsonObjectDetails)
                        //Credit Card
                        jsonObjectCreditCard.addProperty("secure", true)
                        jsonObjectMidtrans.add("credit_card", jsonObjectCreditCard)

                        Log.v("TOPUPTOKEN", jsonObjectMidtrans.toString())

                        requestMidtransTransaction(jsonObjectMidtrans, { token ->
                            val jsonObjectUpdateTopup = JsonObject()
                            jsonObjectUpdateTopup.addProperty("token_payment", token.token)
                            Log.v("TOPUPTOKEN", "Token : " + token.token!!)

                            updatePaymentTokenTopup(auth.token!!,
                                jsonObjectUpdateTopup,
                                it.data.id!!,
                                { updateTopup ->
                                    if (updateTopup.code == 200) {
                                        Toast.makeText(
                                            this,
                                            getString(R.string.berhasil_topup),
                                            Toast.LENGTH_SHORT
                                        ).show()
                                        Log.v(
                                            "TOPUPTOKEN",
                                            "Sukses TopupToken : " + updateTopup.data.toString()
                                        )
                                        observerListTopup()
                                    }
                                },
                                { message ->
                                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                                })
                        }, { message ->
                            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                        })
                        nominal.clear()
                    } else Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()

                }, {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                })
            }
        }
    }

    private fun requestTopupSaldo(
        jsonObject: JsonObject,
        onSuccess: (ResponseRequestTopup) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceCreateProof: TopupService by lazy {
            ApiService().createService(TopupService::class.java)
        }
        val call: Call<ResponseRequestTopup> =
            serviceCreateProof.requestTopup(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseRequestTopup> {
            override fun onFailure(call: Call<ResponseRequestTopup>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRequestTopup>,
                response: Response<ResponseRequestTopup>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    // REQUEST Midtrans Transctions
    private fun requestMidtransTransaction(
        jsonObject: JsonObject,
        onSuccess: (ResponseMidtransTranscation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceTransaction: MidtransService by lazy {
            ApiService().createServiceMidtransTransaction(MidtransService::class.java)
        }
        val call = serviceTransaction.requestTransaction(basicAuth, jsonObject)
        call.enqueue(object : Callback<ResponseMidtransTranscation> {
            override fun onFailure(call: Call<ResponseMidtransTranscation>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<ResponseMidtransTranscation>,
                response: Response<ResponseMidtransTranscation>
            ) {
                if (response.code() == 400) {
                    Log.d("PAYMENTTOKEN", "Response Body if 400 : " + response.body().toString())
                    Log.d(
                        "PAYMENTTOKEN",
                        "Response Body if erro 400 : " + response.errorBody().toString()
                    )
                }
                Log.d("PAYMENTTOKEN", "Response Body : " + response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }

    //update token topup
    // UPDATE PAYMENT TOKEN
    private fun updatePaymentTokenTopup(
        auth: String,
        jsonObject: JsonObject,
        id : String,
        onSuccess: (ResponseUpdateTokenTopup) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceCreateProof: TopupService by lazy {
            ApiService().createService(TopupService::class.java)
        }
        val call = serviceCreateProof.updateTokenTopup(auth, jsonObject, id)
        call.enqueue(object : Callback<ResponseUpdateTokenTopup> {
            override fun onFailure(call: Call<ResponseUpdateTokenTopup>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<ResponseUpdateTokenTopup>,
                response: Response<ResponseUpdateTokenTopup>
            ) {
                Log.d("PAYMENTTOKEN", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }


    override fun onResume() {
        super.onResume()
        auth = getAuth(cacheUtil)
        viewModel.setUpListTagihanTopup(auth.token!!)
    }
}