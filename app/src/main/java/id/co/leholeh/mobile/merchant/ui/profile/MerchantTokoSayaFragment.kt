package id.co.leholeh.mobile.merchant.ui.profile

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentMerchantTokoSayaBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.ProfileMenuItem
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.ui.barang.MerchantKelolaBarangActivity
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.KelolaPesananActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_menu_akun.view.*


class MerchantTokoSayaFragment : Fragment() {

    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private var auth: Login? = null

    private lateinit var cacheUtil: CacheUtil
    private lateinit var adapter: AdapterUtil<ProfileMenuItem>
    private lateinit var binding: FragmentMerchantTokoSayaBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMerchantTokoSayaBinding.inflate(inflater, container, false)
        cacheUtil = CacheUtil()
        cacheUtil.start(context as Activity, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
        } else {
            getDataProfileToko()
            auth = getAuth(cacheUtil)
            binding.tvUbahAkun.setOnClickListener {
                /*val intent = Intent(requireContext(), RegisterMerchantFragment::class.java)
                intent.putExtra(ConstantProfileMerchant.DATA_EDIT_PROFILE_TOKO, ConstantProfileMerchant.ACTION_EDIT_PROFILE_TOKO)
                startActivity(intent)*/
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        binding.toolbar.title = "Toko Saya"


        //Setup Profil Menu
        binding.rvIconmenu.layoutManager = LinearLayoutManager(context)
        adapter =
            AdapterUtil(R.layout.item_list_menu_akun,
                listOf(
                    ProfileMenuItem(
                        "Kelola Barang",
                        R.drawable.ic_card_giftcard
                    ),
                    ProfileMenuItem(
                        "Kelola Pesanan",
                        R.drawable.ic_assignment
                    ),
                    ProfileMenuItem(
                        "Pesan Masuk",
                        R.drawable.ic_chat
                    ),
                    ProfileMenuItem(
                        "Keuangan",
                        R.drawable.ic_monetization
                    )
                ), { position, itemView, item ->
                    itemView.tv_menu!!.text = item.label
                    itemView.im_akun_icon!!.setImageResource(item.icon)
                    itemView.im_chevron_right.setImageResource(R.drawable.ic_chevron_right)
                }, { position, item ->
                    when (position) {
                        0 -> startActivity(
                            Intent(
                                context,
                                MerchantKelolaBarangActivity::class.java
                            )
                        )
                        1 -> startActivity(
                            Intent(
                                context,
                                KelolaPesananActivity::class.java
                            )
                        )
//                    2 -> startActivity(Intent(context, ProductListActivity::class.java))
//                    3 -> startActivity(Intent(context, ProductListActivity::class.java))
                    }
                })
        binding.rvIconmenu.adapter = adapter

        //init profil picture
        binding.imFotoProfil.setImageResource(R.drawable.ic_home_black_24dp)

        //ubah photo profil
        binding.tvUbahAkun.setOnClickListener {

        }



    }


    private fun getDataProfileToko() {
        val auth = getAuth(cacheUtil)
        viewModel.getDataProfileToko(auth.token!!)
        viewModel.toko.observe(viewLifecycleOwner, Observer {
            binding.tvNamaAkun.text = it.marketName
            Glide.with(this).load(it.authorUri).circleCrop().into(binding.imFotoProfil)
            Glide.with(this).load(it.marketUri).into(binding.imageViewHeader)
        })
    }


}