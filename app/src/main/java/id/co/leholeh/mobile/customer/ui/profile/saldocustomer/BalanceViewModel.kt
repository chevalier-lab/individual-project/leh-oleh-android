package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.wallet.Balance
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.BalanceService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class BalanceViewModel : ViewModel() {
    private val _statusGetBalance = MutableLiveData<ApiStatus>()
    val statusGetBalance: LiveData<ApiStatus>
        get() = _statusGetBalance

    private val _balance = MutableLiveData<Balance>()
    val balance: LiveData<Balance>
        get() = _balance

    private val serviceBalance: BalanceService by lazy {
        ApiService().createService(BalanceService::class.java)
    }

    private suspend fun getBalance(authorization: String) = try {
        _statusGetBalance.postValue(ApiStatus.LOADING)
        _balance.postValue(serviceBalance.getBalance(authorization).data)
        _statusGetBalance.postValue(ApiStatus.SUCCESS)
    } catch (e: Exception) {
        Log.d("ERR_REQ_BALANCE", e.localizedMessage!!)
        _statusGetBalance.postValue(ApiStatus.FAILED)
    }

    fun getDataBalance(authorization: String) = viewModelScope.launch { getBalance(authorization) }
}