package id.co.leholeh.mobile.customer.model.category

import com.google.gson.annotations.SerializedName

data class DataCategory(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<Category>? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)