package id.co.leholeh.mobile.customer.model.auth

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class DataLogin(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Login? = null,

    @field:SerializedName("error")
    val error: List<String?>? = listOf(),

    @field:SerializedName("message")
    val message: String? = null
):Serializable