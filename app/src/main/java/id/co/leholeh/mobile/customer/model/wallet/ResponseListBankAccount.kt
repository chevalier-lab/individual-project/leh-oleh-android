package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName
import id.co.leholeh.mobile.customer.model.banks.BankAccount

data class ResponseListBankAccount(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<BankAccount>? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)
