package id.co.leholeh.mobile.customer.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

class SliderAdapterUtil<T>(
    private val layout: Int,
    private val context: Context,
    private var items: List<T>,
    var view: (View, T, Int) -> Unit,
    var handler: (T, Int) -> Unit
) : PagerAdapter() {
    private lateinit var layoutInflater: LayoutInflater

    var data = this.items
        set(value){
            field = value
            notifyDataSetChanged()
        }

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = data.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        this.layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.layoutInflater.inflate(layout, null).let {
            view(it, data[position], position)
            it.setOnClickListener { handler(data[position], position) }
            (container as ViewPager).addView(it, 0)
            return it
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) =
        (container as ViewPager).removeView((`object` as View))

    override fun getPageWidth(position: Int): Float = 1f
}