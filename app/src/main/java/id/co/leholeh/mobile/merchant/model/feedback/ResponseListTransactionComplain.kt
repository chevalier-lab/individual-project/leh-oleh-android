package id.co.leholeh.mobile.merchant.model.feedback

import com.google.gson.annotations.SerializedName

data class ResponseListTransactionComplain(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataTransactionComplain>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataTransactionComplain(

	@field:SerializedName("sub_total_price")
	val subTotalPrice: String? = null,

	@field:SerializedName("subject")
	val subject: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("id_m_products")
	val idMProducts: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("username")
	val username: String? = null
)
