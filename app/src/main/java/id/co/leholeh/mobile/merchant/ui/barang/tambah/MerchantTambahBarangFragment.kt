package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.databinding.FragmentMerchantTambahBarangBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.merchant.model.barang.BarangTemporary
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class MerchantTambahBarangFragment : Fragment() {


    lateinit var binding : FragmentMerchantTambahBarangBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val barangTemporary = BarangTemporary()
    private var fileCover : File? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMerchantTambahBarangBinding.inflate(inflater,container,false)
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(requireActivity(), LoginActivity::class.java))
        } else {
            auth = getAuth(cacheUtil)
            binding.inputCover.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
            }

        }
    }

    fun validasiForm() : Boolean{
        val name = binding.etNamabarang.text.toString()
        val jumlah = binding.etQty.text.toString()
        val hargaJual = binding.etHargaJual.text.toString()
        val hargaBeli = binding.etHargaModal.text.toString()
        val description = binding.etDeskripsi.text.toString()
        val diskon = binding.etDiskon.text.toString()
        val length = binding.etLength.text.toString()
        val width = binding.etWidth.text.toString()
        val height = binding.etHeight.text.toString()
        val weight = binding.etWeight.text.toString()
        if (name.isEmpty() || jumlah.isEmpty() || hargaJual.isEmpty() ||
            hargaBeli.isEmpty() || description.isEmpty() || diskon.isEmpty()
            || binding.imageViewCover.drawable == null || length.isEmpty() || width.isEmpty() ||
            height.isEmpty() || weight.isEmpty()
        ) {
            Toast.makeText( requireContext(), "Harap isi semua form", Toast.LENGTH_SHORT).show()
            return false
        }else {
            when {
                hargaJual.toInt() < hargaBeli.toInt() -> {
                    Toast.makeText(
                        requireContext(),
                        "Harga jual tidak boleh kurang dari harga beli",
                        Toast.LENGTH_SHORT
                    ).show()
                    return false
                }
                diskon.toInt() > 100 -> {
                    Toast.makeText(requireContext(), "Diskon maksimal 100", Toast.LENGTH_SHORT)
                        .show()
                    return false
                }
                jumlah.toInt() <= 0 || hargaBeli.toInt() <= 0 || hargaJual.toInt() <= 0 -> {
                    Toast.makeText(requireContext(), "Ada input yg tidak valid", Toast.LENGTH_SHORT)
                        .show()
                    return false
                }
                else -> {
                    return true
                }
            }

        }
    }

    fun getInformasiBarang() : BarangTemporary{
        val drawableCover = binding.imageViewCover.drawable as BitmapDrawable
        val bitmapCover = drawableCover.bitmap
        fileCover = createTempFile(bitmapCover)!!

        val name = binding.etNamabarang.text.toString()
        val jumlah = binding.etQty.text.toString()
        val hargaJual = binding.etHargaJual.text.toString()
        val hargaBeli = binding.etHargaModal.text.toString()
        val description = binding.etDeskripsi.text.toString()
        val diskon = binding.etDiskon.text.toString()
        val length = binding.etLength.text.toString()
        val width = binding.etWidth.text.toString()
        val height = binding.etHeight.text.toString()
        val weight = binding.etWeight.text.toString()
        barangTemporary.cover = fileCover
        barangTemporary.namaBarang = name
        barangTemporary.jumlahStok = jumlah
        barangTemporary.hargaJual = hargaJual
        barangTemporary.hargaModal = hargaBeli
        barangTemporary.deskripsi = description
        barangTemporary.diskon = diskon
        barangTemporary.length = length
        barangTemporary.width = width
        barangTemporary.height = height
        barangTemporary.weight = weight
        return barangTemporary
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image: Image = ImagePicker.getFirstImageOrNull(data)
            if(!image.equals(null)){
                Glide.with(binding.imageViewCover).load(image.uri).into(binding.imageViewCover)
                binding.imageViewCover.visibility = View.VISIBLE
                binding.coverBackgroud.background = null
                binding.inputCover.visibility = View.GONE
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        if(fileCover != null){
            Glide.with(binding.imageViewCover).load(fileCover).into(binding.imageViewCover)
            binding.imageViewCover.visibility = View.VISIBLE
            binding.coverBackgroud.background = null
            binding.inputCover.visibility = View.GONE
        }
        super.onResume()
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

}