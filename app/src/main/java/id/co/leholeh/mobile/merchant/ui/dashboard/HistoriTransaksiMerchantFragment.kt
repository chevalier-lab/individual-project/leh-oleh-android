package id.co.leholeh.mobile.merchant.ui.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentHistoriTransaksiMerchantBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.merchant.model.transaction.ListTransaction
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.DetailPesananMerchantActivity
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.TransactionViewModel
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_transaksi_kelola_pesanan.view.*
import java.text.SimpleDateFormat
import java.util.*

class HistoriTransaksiMerchantFragment : Fragment() {
    private lateinit var binding: FragmentHistoriTransaksiMerchantBinding
    private lateinit var adapterHistoriTransaksi: PagedAdapterUtil<ListTransaction>
    private val viewModel: TransactionViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(TransactionViewModel::class.java)
    }
    private lateinit var cacheUtil : CacheUtil
    private lateinit var auth : Login
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentHistoriTransaksiMerchantBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        if (getAuth(cacheUtil).token!!.isNotEmpty()) {
            auth = getAuth(cacheUtil)
            getHistoriTransaksi()
        }else{
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getHistoriTransaksi() {
        viewModel.setUpListTransaction(auth.token!!, "3")
        observerListTransaction()
        adapterHistoriTransaksi =
            PagedAdapterUtil(
                R.layout.item_transaksi_kelola_pesanan,
                { _, itemView, item ->
                    itemView.textViewNamaPemesan.text = "Nama pemesan : ${item.fullName}"
                    itemView.textViewTotalTransaksi.text =
                        changeToRupiah(item.totalPrice?.toDouble()!!)
                    itemView.textViewJumlahProduk.text = "Jumlah produk : 0"
                    itemView.textViewNoTransaksi.text = item.transactionToken
                    itemView.textViewStatusPesanan.text = getString(R.string.payment_verified)
                    val orderDate = SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss",
                        Locale("ID")
                    ).parse(item.createdAt!!)
                    val formatedOrderDate =
                        SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale("ID")).format(orderDate!!)
                    itemView.textViewTanggalPesanan.text =
                        "Tanggal pesanan dibuat: $formatedOrderDate"
                },
                { _, item ->
                    val intent = Intent(
                        requireContext(),
                        DetailPesananMerchantActivity::class.java
                    )
                    intent.putExtra(
                        ConstantMerchant.ID_PESANAN,
                        item.id
                    )
                    intent.putExtra(
                        ConstantMerchant.VERIFIED_PESANAN,
                        ConstantMerchant.IS_VERIFIED_PESANAN
                    )
                    intent.putExtra(ConstantMerchant.STATUS_PESANAN, item.status)
                    startActivity(intent)
                })

        binding.recyclerHistoriTransaksi.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerHistoriTransaksi.adapter = adapterHistoriTransaksi
    }

    private fun getState() {
        viewModel.statusListTransaction.observe(viewLifecycleOwner, Observer {
            Log.d("STATUS_TRANS_HISTORY", it.toString())
            when (it) {
                ApiStatus.SUCCESS -> {

                }
                ApiStatus.LOADING -> {

                }
                ApiStatus.EMPTY -> {
                    binding.emptyView.visibility = View.VISIBLE
                    binding.recyclerHistoriTransaksi.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {

                }
                ApiStatus.EMPTY_AFTER -> {

                }
                ApiStatus.LOADED ->{
                    binding.recyclerHistoriTransaksi.visibility = View.VISIBLE
                    binding.emptyView.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    Toast.makeText(requireContext(), "Jaringan bermasalah", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    Toast.makeText(requireContext(), "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    private fun observerListTransaction() {
        viewModel.getDataListTransaction().observe(viewLifecycleOwner, Observer {
            adapterHistoriTransaksi.submitList(it)
        })
        getState()
    }
}