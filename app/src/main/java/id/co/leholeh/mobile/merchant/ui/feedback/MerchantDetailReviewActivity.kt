package id.co.leholeh.mobile.merchant.ui.feedback

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantDetailReviewBinding
import id.co.leholeh.databinding.ComponentDialogBalasanBinding
import id.co.leholeh.mobile.customer.util.AdapterUtil

class MerchantDetailReviewActivity : AppCompatActivity() {
    lateinit var adapter : AdapterUtil<MerchantDetailKomplainActivity.Dummy>

    lateinit var binding:ActivityMerchantDetailReviewBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMerchantDetailReviewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setupToolbar
        setupToolbar()

        //adapter
  /*      adapter =
            AdapterUtil(R.layout.item_list_feedback_complain,
                listOf(
                    MerchantDetailKomplainActivity.Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    MerchantDetailKomplainActivity.Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        0
                    ),
                    MerchantDetailKomplainActivity.Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    MerchantDetailKomplainActivity.Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    MerchantDetailKomplainActivity.Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        0
                    )
                ), { position, itemView, item ->
                    itemView.tv_chat.text = item.komen
                    itemView.tv_tanggal.text = item.tanggal
                    itemView.tv_jam.text = item.jam

                    val colorGreen = ContextCompat.getColor(this, R.color.colorGreenApp)
                    val colorGrey = ContextCompat.getColor(this, R.color.colorGreyChat)

                    if (item.user == 1) {
                        itemView.list_feedback.setBackgroundColor(colorGrey)


                    } else if (item.user == 0) {
                        itemView.list_feedback.setBackgroundColor(colorGreen)
                        itemView.tv_tanggal.setTextColor(resources.getColor(R.color.colorWhite))
                        itemView.tv_chat.setTextColor(resources.getColor(R.color.colorWhite))
                        itemView.tv_jam.setTextColor(resources.getColor(R.color.colorWhite))
                    }

                }, { position, item ->

                })
*/
        binding.rvHistory.adapter=adapter
        binding.rvHistory.layoutManager = LinearLayoutManager(this@MerchantDetailReviewActivity)



    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Detail Review"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem:MenuItem = menu!!.findItem(R.id.menu_search)
        MenuItemCompat.getActionView(menuItem)
        menuItem.setIcon(R.drawable.ic_send)
        return super.onCreateOptionsMenu(menu)
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.menu_search->{
            showDialog()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    //show dialog
    private fun showDialog(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_balasan)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogBalasanBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

}