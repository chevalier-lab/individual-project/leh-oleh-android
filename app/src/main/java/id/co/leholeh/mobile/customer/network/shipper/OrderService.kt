package id.co.leholeh.mobile.customer.network.shipper

import id.co.leholeh.mobile.customer.constant.shipper.ConstantOrder
import id.co.leholeh.mobile.customer.model.shipper.order.*
import retrofit2.Call
import retrofit2.http.*

interface OrderService {
    @POST(ConstantOrder.BASE_URL_DOMESTIC_ORDER)
    fun createDomesticOrder(@Query("apiKey") apiKey: String): Call<ResponseDomesticOrder>

    @GET(ConstantOrder.BASE_URL_ORDER_DETAIL)
    suspend fun getOrderDetail(
        @Path("id") id: String,
        @Query("apiKey") apiKey: String
    ): ResponseOrderDetail

    @PUT(ConstantOrder.BASE_URL_UPDATE_ORDER_WEIGHT)
    fun updateOrderWeight(
        @Path("id") id: String,
        @Query("apiKey") apiKey: String
    ): ResponseUpdateOrderWeight

    @PUT(ConstantOrder.BASE_URL_ORDER_ACTIVATIONS)
    fun editOrderActivation(
        @Path("id") id: String,
        @Query("apiKey") apiKey: String
    ): Call<ResponseOrderActivations>

    @PUT(ConstantOrder.BASE_URL_CANCEL_ORDER)
    fun cancelOrder(
        @Path("id") orderId: String,
        @Query("apiKey") apiKey: String
    ): Call<ResponseCancelOrder>

    @GET(ConstantOrder.BASE_URL_TRACKING_ID)
    suspend fun getTrackingId(
        @Query("apiKey") apiKey: String,
        @Query("id") id: String
    ): ResponseTrackingId

    @GET(ConstantOrder.BASE_URL_LIST_TRACK_STAT)
    suspend fun getListTrackStatus(
        @Query("apiKey") apiKey: String
    ): ResponseListTrackStat
}