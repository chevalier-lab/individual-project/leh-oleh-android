package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class OrderCheckout(
	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class TransactionMerchants(
	@field:SerializedName("id_u_user_is_merchant")
	val idUUserIsMerchant: String? = null,

	@field:SerializedName("additional")
	val additional: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("list_id_m_product")
	val listIdMProduct: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null
)

data class DataItem(
	@field:SerializedName("transaction_merchants")
	val transactionMerchants: TransactionMerchants? = null,

	@field:SerializedName("transaction_products")
	val transactionProducts: List<Any?>? = null,

	@field:SerializedName("transaction_info")
	val transactionInfo: TransactionInfo? = null,

	@field:SerializedName("transaction")
	val transaction: Transaction? = null
)

data class TransactionInfo(
	@field:SerializedName("id_u_user_location")
	val idUUserLocation: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class Transaction(
	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)