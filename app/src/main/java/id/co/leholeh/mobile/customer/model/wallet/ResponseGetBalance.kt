package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseGetBalance(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: Balance? = null,

    @field:SerializedName("error")
    val error: List<Any> = listOf(),

    @field:SerializedName("message")
    val message: String? = null
)


