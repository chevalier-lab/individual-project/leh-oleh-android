package id.co.leholeh.mobile.customer.ui.notifications

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.notification.DataNotification
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers

class NotificationsViewModel : ViewModel() {



    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .build()

    // REQUEST USER LOCATION LIST
    lateinit var statusListNotification: LiveData<ApiStatus>
    private lateinit var listNotification: LiveData<PagedList<DataNotification>>

    private val _listNotificationDataSource = MutableLiveData<NotificationDataSource>()

    //LIST NOTIFICATIONS
    fun setUpListNotfications(authorization: String){
        statusListNotification = Transformations.switchMap<NotificationDataSource, ApiStatus>(_listNotificationDataSource, NotificationDataSource::apiState)
        listNotification  = initializedNotificationPagedListBuilder(authorization,config).build()
    }

    fun getDataListNotifications() : LiveData<PagedList<DataNotification>> = listNotification

    private fun initializedNotificationPagedListBuilder(authorization: String, config: PagedList.Config):
            LivePagedListBuilder<Int, DataNotification> {

        val dataSourceFactory = object : DataSource.Factory<Int, DataNotification>() {
            override fun create(): DataSource<Int, DataNotification> {
                val notificationDataSource = NotificationDataSource(Dispatchers.IO, authorization)
                _listNotificationDataSource.postValue(notificationDataSource)
                return notificationDataSource
            }
        }
        return LivePagedListBuilder<Int, DataNotification>(dataSourceFactory, config)
    }
    //END OF NOTIFICATIONS
}