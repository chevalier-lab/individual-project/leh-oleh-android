package id.co.leholeh.mobile.customer.ui.register

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityStatusRegisterMerchantBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.merchant.ResponseDeleteReqMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.merchant.constant.ConstantProfileMerchant
import id.co.leholeh.mobile.utils.CacheUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StatusRegisterMerchantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityStatusRegisterMerchantBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStatusRegisterMerchantBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Setup Toolbar
        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            if (intent.getStringExtra(ConstantProfileMerchant.INTENT_MERCHANT_STATUS) != null) {
                if (intent.getStringExtra(ConstantProfileMerchant.INTENT_MERCHANT_STATUS) == ConstantProfileMerchant.MERCHANT_REJECTED) {
                    binding.buttonDeleteReqMerchant.visibility = View.VISIBLE
                    binding.textViewStatusRegisMerchantProses.text =
                        getString(R.string.status_regis_merchant_gagal)
                    Glide.with(this).load(R.drawable.ic_reject)
                        .into(binding.imageViewStatusRegisMerchantProses)
                    binding.buttonDeleteReqMerchant.setOnClickListener {
                        deleteRequestMerchant(auth.token!!, {
                            if (it.code == 200) {
                                Toast.makeText(
                                    this,
                                    getString(R.string.berhasil_menghapus_request),
                                    Toast.LENGTH_SHORT
                                ).show()

                            }
                        }, {
                            Toast.makeText(baseContext, it, Toast.LENGTH_LONG).show()
                        })
                    }
                }
            }

        }
    }

    private fun deleteRequestMerchant(
        authorization: String, onSuccess: (ResponseDeleteReqMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseDeleteReqMerchant> =
            ApiService().serviceGuestMerchants.deleteRequestMerchant(authorization)
        call.enqueue(object : Callback<ResponseDeleteReqMerchant> {
            override fun onFailure(call: Call<ResponseDeleteReqMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseDeleteReqMerchant>,
                response: Response<ResponseDeleteReqMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Status Merchant"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            startActivity(Intent(this, MainActivity::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

}