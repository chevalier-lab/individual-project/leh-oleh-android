package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.model.wallet.ResponseHistorySaldo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface HistorySaldoService {

    @GET(ConstantWallet.BASE_URL_GET_HISTORY_SALDO)
    suspend fun getListHistoryTopup(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): Response<ResponseHistorySaldo>
}