package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class DataDeleteCart(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: DeleteCart? = null,

    @field:SerializedName("error")
    val error: String? = null,

    @field:SerializedName("message")
    val message: String? = null
)