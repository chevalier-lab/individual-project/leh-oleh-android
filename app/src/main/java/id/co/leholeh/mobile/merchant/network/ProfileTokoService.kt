package id.co.leholeh.mobile.merchant.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.merchant.constant.ConstantProfileMerchant
import id.co.leholeh.mobile.merchant.model.setting.ResponseProfileToko
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdateLocationMerchant
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdatePhotoProfileMerchant
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdateProfileToko
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface ProfileTokoService {
    @GET(ConstantProfileMerchant.BASE_URL_PROFILE_TOKO)
    suspend fun getProfileToko(@Header("Authorization") auth : String) : ResponseProfileToko

    @PUT(ConstantProfileMerchant.BASE_URL_PROFILE_TOKO)
    fun updateProfileToko(@Header("Authorization") auth : String, @Body jsonObject: JsonObject) : Call<ResponseUpdateProfileToko>

    @Multipart
    @POST(ConstantProfileMerchant.BASE_URL_AUTHOR_PHOTO)
    fun updatePhotoAuthor(
        @Header("Authorization") authorization: String,
        @Part fotoProfile: MultipartBody.Part
    ):Call<ResponseUpdatePhotoProfileMerchant>

    @Multipart
    @POST(ConstantProfileMerchant.BASE_URL_IDENTITY_PHOTO)
    fun updatePhotoIdentity(
        @Header("Authorization") authorization: String,
        @Part fotoProfile: MultipartBody.Part
    ):Call<ResponseUpdatePhotoProfileMerchant>

    @Multipart
    @POST(ConstantProfileMerchant.BASE_URL_MARKET_PHOTO)
    fun updatePhotoMarket(
        @Header("Authorization") authorization: String,
        @Part fotoProfile: MultipartBody.Part
    ):Call<ResponseUpdatePhotoProfileMerchant>

    @POST(ConstantProfileMerchant.BASE_URL_CREATE_LOCATION_MERCHANT)
    fun createLocationMerchant(
        @Body jsonObject: JsonObject
        ):Call<ResponseUpdateLocationMerchant>

    @PUT(ConstantProfileMerchant.BASE_URL_UPDATE_LOCATION_MERCHANT)
    fun updateLocationMerchant(
        @Path("id") id:String,
        @Body jsonObject: JsonObject
    ):Call<ResponseUpdateLocationMerchant>
}