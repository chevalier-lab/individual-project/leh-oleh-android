package id.co.leholeh.mobile.merchant.constant

object ConstantDahboardMerchant {
    const val BASE_URL_INCOME = "merchant/dashboard/income"
    const val BASE_URL_BANNER = "merchant/dashboard/banner"
}