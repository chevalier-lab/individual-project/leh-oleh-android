package id.co.leholeh.mobile.merchant.ui.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.setting.ProfileToko
import id.co.leholeh.mobile.merchant.network.ProfileTokoService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch

class ProfileTokoViewModel : ViewModel() {

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
        get() = _status

    private val _toko = MutableLiveData<ProfileToko>()
    val toko : LiveData<ProfileToko>
        get() = _toko

    private val profileTokoServoce : ProfileTokoService by lazy {
        ApiService().createService(ProfileTokoService::class.java)
    }
    private suspend fun getProfileToko(auth : String){
        try{
            _status.postValue(ApiStatus.LOADING)
            _toko.postValue(profileTokoServoce.getProfileToko(auth).data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("ERR_REQ_PROF_TOKO", e.localizedMessage!!)
        }
    }

    fun getDataProfileToko(auth: String){
        viewModelScope.launch {
            getProfileToko(auth)
        }
    }

    override fun onCleared() {
        SupervisorJob().cancel()
        super.onCleared()
    }
}