package id.co.leholeh.mobile.merchant.model.transaction

import com.google.gson.annotations.SerializedName

data class ResponsePickupRequest(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataPickupRequest>? = null,

	@field:SerializedName("error")
	val error: List<String>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataPickupRequest(

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
