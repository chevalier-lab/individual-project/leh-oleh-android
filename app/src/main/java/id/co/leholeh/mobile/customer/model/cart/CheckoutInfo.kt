package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class CheckoutInfo(
    @field:SerializedName("zip")
    val zip: String? = null,

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("state")
    val state: String? = null,

    @field:SerializedName("first_name")
    val firstName: String? = null,

    @field:SerializedName("id_u_user_transaction")
    val idUUserTransaction: String? = null
)