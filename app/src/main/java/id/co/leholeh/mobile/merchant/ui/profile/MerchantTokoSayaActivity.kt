package id.co.leholeh.mobile.merchant.ui.profile

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantTokoSayaBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.ProfileMenuItem
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.ui.register.RegisterMerchantActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantProfileMerchant
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdatePhotoProfileMerchant
import id.co.leholeh.mobile.merchant.network.ProfileTokoService
import id.co.leholeh.mobile.merchant.ui.barang.MerchantKelolaBarangActivity
import id.co.leholeh.mobile.merchant.ui.dashboard.KeuanganMerchantActivity
import id.co.leholeh.mobile.merchant.ui.kelolapesanan.KelolaPesananActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_menu_akun.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class MerchantTokoSayaActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMerchantTokoSayaBinding
    private lateinit var auth: Login
    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private lateinit var idMerchant: String
    private lateinit var cacheUtil: CacheUtil
    private lateinit var adapter: AdapterUtil<ProfileMenuItem>
    private var uploadPhoto = ""
    private var photoProfile = "PhotoProfil"
    private var photoBackground = "PhotoBackground"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantTokoSayaBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (getAuth(cacheUtil).token!!.isNotEmpty()) {
            auth = getAuth(cacheUtil)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            getDataProfileToko()

            title = "Toko Saya"
            setUpProfileToko()
            //init profil picture
            binding.imFotoProfil.setImageResource(R.drawable.ic_home_black_24dp)

            //update Photo
            binding.imFotoProfil.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
                uploadPhoto = photoProfile
            }
        } else {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun setUpProfileToko(){
        //Setup Profil Menu
        binding.rvIconmenu.layoutManager = LinearLayoutManager(this)
        adapter =
            AdapterUtil(R.layout.item_list_menu_akun,
                listOf(
                    ProfileMenuItem(
                        "Kelola Produk",
                        R.drawable.ic_kelola_produk
                    ),
                    ProfileMenuItem(
                        "Kelola Pesanan",
                        R.drawable.ic_kelola_pesanan
                    ),
                    ProfileMenuItem(
                        "Pesan Masuk",
                        R.drawable.ic_chat
                    ),
                    ProfileMenuItem(
                        "Keuangan",
                        R.drawable.ic_monetization
                    ),
                    ProfileMenuItem(
                        "Update Photo KTP",
                        R.drawable.ic_camera_alt_24px
                    ),
                    ProfileMenuItem(
                        "Lokasi",
                        R.drawable.ic_add
                    )
                ), { position, itemView, item ->
                    itemView.tv_menu!!.text = item.label
                    itemView.im_akun_icon!!.setImageResource(item.icon)
                    itemView.im_chevron_right.setImageResource(R.drawable.ic_chevron_right)
                }, { position, _ ->
                    when (position) {
                        0 -> startActivity(
                                Intent(
                                    this,
                                    MerchantKelolaBarangActivity::class.java
                                ).putExtra(INTENT_DETAIL, idMerchant)
                            )
                        1 -> startActivity(
                            Intent(
                                this,
                                KelolaPesananActivity::class.java
                            ).putExtra(INTENT_SEARCH, auth.token)

                        )
                        3 -> startActivity(
                            Intent(
                                this,
                                KeuanganMerchantActivity::class.java
                            ).putExtra(INTENT_SEARCH, auth.token)
                        )
                        4 -> startActivity(
                            Intent(
                                this,
                                UpdateKTP::class.java
                            ).putExtra(INTENT_SEARCH, auth.token)
                        )
                        5 ->
                            startActivity(
                                Intent(
                                    this,
                                    MerchantLocationActivity::class.java
                                ).putExtra(INTENT_DETAIL, idMerchant)
                                    .putExtra(INTENT_SEARCH, auth.token)
                            )
//                    2 -> startActivity(Intent(context, ProductListActivity::class.java))
//                    3 -> startActivity(Intent(context, ProductListActivity::class.java))
                    }
                })
        binding.rvIconmenu.adapter = adapter

    }

    @SuppressLint("SetTextI18n")
    private fun getDataProfileToko() {
        viewModel.getDataProfileToko(auth.token!!)
        viewModel.toko.observe(this, Observer { profileToko ->
            //idmerchant
            idMerchant = profileToko.id!!

            //tv alert
            if (profileToko.location == null) {
                binding.tvAlert.visibility = View.VISIBLE
                binding.tvAlert.setOnClickListener {
                    startActivity(
                        Intent(
                            this,
                            MerchantLocationActivity::class.java
                        ).putExtra(INTENT_DETAIL, idMerchant)
                    )
                }
            }else{
                val address = profileToko.location.address
                val area = profileToko.location.area
                val subUrb = profileToko.location.suburb
                val city = profileToko.location.city
                val province = profileToko.location.province
                val postCode = profileToko.location.postcode

                binding.tvAlamatToko.text = "$address, $area,\n$subUrb, $city, $province, $postCode"
            }
            profileToko.marketPhoneNumber?.apply {
                binding.tvNoTelepon.text = profileToko.marketPhoneNumber
            }
            binding.tvNamaAkun.text = profileToko.marketName
            Glide.with(this).load(profileToko.authorUri).circleCrop().into(binding.imFotoProfil)
            //Glide.with(this).load(profileToko.marketUri).into(binding.imageViewHeader)
            binding.tvUbahAkun.setOnClickListener {
                val intent = Intent(this, RegisterMerchantActivity::class.java)
                intent.putExtra(ConstantProfileMerchant.INTENT_DATA_PROFILE_TOKO, profileToko)
                intent.putExtra(
                    ConstantProfileMerchant.INTEN_DATA_EDIT_PROFILE_TOKO,
                    ConstantProfileMerchant.KONDISI_EDIT_PROFILE_TOKO
                )
                startActivity(intent)
            }
        })
    }

    override fun onResume() {
        viewModel.getDataProfileToko(auth.token!!)
        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val imagePick: Image = ImagePicker.getFirstImageOrNull(data)
            try {
                val parcelFileDescriptor: ParcelFileDescriptor =
                    this.contentResolver.openFileDescriptor(imagePick.uri, "r")!!
                val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                val photoFile = createFileTemp(image)
                val photoProfile = MultipartBody.Part.createFormData(
                    "photo",
                    photoFile!!.name,
                    photoFile.asRequestBody("image/*".toMediaTypeOrNull())
                )
                if (uploadPhoto.equals(photoProfile)) {
                    setUpdatePhotoProfile(auth.token!!, photoProfile, {
                        if (it.code == 200) {
                            Toast.makeText(
                                this,
                                getString(R.string.berhasil_mengubah_foto),
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Upload_photo", "Berhasil" + it.message!!)

                            viewModel.getDataProfileToko(auth.token!!)
                        } else {
                            Toast.makeText(
                                this,
                                it.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Upload_photo", "Else Atas" + it.message!!)

                        }
                    }, {
                        Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                        Log.d("Upload_photo", "Gagal $it")
                    })
                    parcelFileDescriptor.close()
                } else if (uploadPhoto == photoBackground) {
                    setUpdateBackgroundProfile(auth.token!!, photoProfile, {
                        if (it.code == 200) {
                            Toast.makeText(
                                this,
                                getString(R.string.berhasil_mengubah_foto),
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Upload_photo", "Berhasil" + it.message!!)

                            viewModel.getDataProfileToko(auth.token!!)
                        } else {
                            Toast.makeText(
                                this,
                                it.message,
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Upload_photo", "Else Atas" + it.message!!)

                        }
                    }, {
                        Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                        Log.d("Upload_photo", "Gagal $it")
                    })
                    parcelFileDescriptor.close()
                }
                uploadPhoto = ""
            } catch (e: IOException) {
                e.printStackTrace()
                uploadPhoto = ""
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setUpdatePhotoProfile(
        authorization: String,
        photoProfile: MultipartBody.Part,
        onSuccess: (ResponseUpdatePhotoProfileMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseUpdatePhotoProfileMerchant> =
            ApiService().createService(ProfileTokoService::class.java)
                .updatePhotoAuthor(authorization, photoProfile)
        call.enqueue(object : Callback<ResponseUpdatePhotoProfileMerchant> {
            override fun onFailure(call: Call<ResponseUpdatePhotoProfileMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseUpdatePhotoProfileMerchant>,
                response: Response<ResponseUpdatePhotoProfileMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun setUpdateBackgroundProfile(
        authorization: String,
        photoProfile: MultipartBody.Part,
        onSuccess: (ResponseUpdatePhotoProfileMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseUpdatePhotoProfileMerchant> =
            ApiService().createService(ProfileTokoService::class.java)
                .updatePhotoMarket(authorization, photoProfile)
        call.enqueue(object : Callback<ResponseUpdatePhotoProfileMerchant> {
            override fun onFailure(call: Call<ResponseUpdatePhotoProfileMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseUpdatePhotoProfileMerchant>,
                response: Response<ResponseUpdatePhotoProfileMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun createFileTemp(bitmap: Bitmap): File? {
        val file = File(
            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}