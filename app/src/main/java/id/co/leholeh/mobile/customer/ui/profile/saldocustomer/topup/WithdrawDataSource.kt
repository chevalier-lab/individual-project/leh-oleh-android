package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import id.co.leholeh.mobile.customer.model.wallet.Withdraw
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.WithdrawService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class WithdrawDataSource(
    coroutineContext: CoroutineContext,
    private val authorization: String
) :
    PageKeyedDataSource<Int, Withdraw>() {
    private val serviceWithdraw: WithdrawService by lazy {
        ApiService().createService(WithdrawService::class.java)
    }

    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)
    val apiState = MutableLiveData<ApiStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Withdraw>
    ) {
        scope.launch {
            try {
                val response = serviceWithdraw.getListHistoryWithdraw(authorization, FIRST_PAGE)
                val responseItem = response.body()!!.data!!

                apiState.postValue(ApiStatus.LOADING)

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (it.isEmpty()) apiState.postValue(ApiStatus.EMPTY)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, null, FIRST_PAGE + 1)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Withdraw>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)

                val response = serviceWithdraw.getListHistoryWithdraw(authorization, params.key)
                val responseItem = response.body()!!.data!!
                val key = if (params.key > 0) params.key - 1 else -1

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (it.isEmpty()) apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, key)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Withdraw>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)

                val response = serviceWithdraw.getListHistoryWithdraw(authorization, params.key)
                val responseItem = response.body()!!.data!!
                val key = params.key + 1

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (responseItem.isEmpty()) apiState.postValue(ApiStatus.EMPTY_AFTER)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, key)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    companion object {
        const val FIRST_PAGE = 0
    }
}

class RequestWithdrawDataSource(
    coroutineContext: CoroutineContext,
    private val authorization: String
) :
    PageKeyedDataSource<Int, Withdraw>() {
    private val serviceWithdraw: WithdrawService by lazy {
        ApiService().createService(WithdrawService::class.java)
    }

    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)
    val apiState = MutableLiveData<ApiStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Withdraw>
    ) {
        scope.launch {
            try {
                val response = serviceWithdraw.getListPermintaanWithdraw(authorization, FIRST_PAGE)
                val responseItem = response.body()!!.data!!

                apiState.postValue(ApiStatus.LOADING)

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (it.isEmpty()) apiState.postValue(ApiStatus.EMPTY)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, null, FIRST_PAGE + 1)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Withdraw>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)

                val response = serviceWithdraw.getListPermintaanWithdraw(authorization, params.key)
                val responseItem = response.body()!!.data!!
                val key = if (params.key > 0) params.key - 1 else -1

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (it.isEmpty()) apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, key)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Withdraw>) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)

                val response = serviceWithdraw.getListPermintaanWithdraw(authorization, params.key)
                val responseItem = response.body()!!.data!!
                val key = params.key + 1

                when {
                    response.isSuccessful -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem.let {
                            if (responseItem.isEmpty()) apiState.postValue(ApiStatus.EMPTY_AFTER)
                            else apiState.postValue(ApiStatus.LOADED)

                            callback.onResult(responseItem, key)
                        }
                    }
                }
            } catch (e: Exception) {
                apiState.postValue(ApiStatus.FAILED)
            }
        }
    }

    companion object {
        const val FIRST_PAGE = 0
    }
}

