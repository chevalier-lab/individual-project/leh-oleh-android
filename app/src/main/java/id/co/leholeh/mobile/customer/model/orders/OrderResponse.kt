package id.co.leholeh.mobile.customer.model.orders

import com.google.gson.annotations.SerializedName


data class DataOrderResponse (
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<OrderResponse>? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data class OrderResponse(
    @field:SerializedName("consigneeName")
    val consigneeName: String? = null,

    @field:SerializedName("id_u_user_location")
    val idUUserLocation: Int? = null,

    @field:SerializedName("shipper_agent_id")
    val shipperAgentId: Int? = null,

    @field:SerializedName("shipper_agent_name")
    val shipperAgentName: String? = null,

    @field:SerializedName("destinationAddress")
    val destinationAddress: String? = null,

    @field:SerializedName("contents")
    val contents: String? = null,

    @field:SerializedName("id_user_transaction")
    val idUserTransaction: Int? = null,

    @field:SerializedName("consigneePhoneNumber")
    val consigneePhoneNumber: String? = null,

    @field:SerializedName("destinationDirection")
    val destinationDirection: String? = null,

    @field:SerializedName("packageType")
    val packageType: Int? = null,

    @field:SerializedName("rateID")
    val rateID: Int? = null
)
