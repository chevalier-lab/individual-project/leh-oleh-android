package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName

data class DataGetStatusMerchant(

    @field:SerializedName("is_visible")
    val isVisible: String? = null
)