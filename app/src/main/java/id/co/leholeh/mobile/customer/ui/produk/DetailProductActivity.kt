package id.co.leholeh.mobile.customer.ui.produk

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityDetailProductBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.cart.DataAddCart
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.ShimmerUtil
import id.co.leholeh.mobile.customer.util.SliderAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_bottomsheet_beli.*
import kotlinx.android.synthetic.main.slider_component.view.*
import retrofit2.Callback
import retrofit2.Response


class DetailProductActivity : AppCompatActivity() {

    lateinit var binding: ActivityDetailProductBinding
    private lateinit var sliderAdapter: SliderAdapterUtil<String>
    private lateinit var someHandler: Handler
    private var sliderIndicators = mutableListOf<ImageView>()
    private var sliderCount = 0
    private lateinit var itemData: String
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    private val viewModel: DetailProductViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(DetailProductViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //init bindung
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        //Data
        itemData = intent.getSerializableExtra(INTENT_DETAIL)!! as String

        //toolbar
        setupToolbar()

        //slider
        binding.viewPagerDetailProduct.addOnPageChangeListener(handler)
        loadDataSlider()
        startAutoBanner()

        //ProgeressBarShimmer
        viewModel.status.observe(this, Observer {
            ShimmerUtil.updateProgressRelativeLayout(
                it,
                binding.progressBar,
                binding.tvNetworkError,
                binding.layoutDetailProduct
            )
        })

        //Bottom sheet
        bottomSheet()

        //After Login
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            binding.tvAlert.visibility = View.GONE
            binding.lyButton.visibility = View.VISIBLE
            auth = getAuth(cacheUtil)
        } else {
            binding.tvAlert.visibility = View.VISIBLE
            binding.lyButton.visibility = View.GONE
            //Login
            binding.tvAlert.setOnClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
            }

        }

        //onClick Beli Sekarang
        binding.btBeliSekarang.setOnClickListener {
            val json = JsonObject()
            json.addProperty("idMProducts", itemData)
            json.addProperty("qty", "1")

            addToCart(auth.token!!, json, {
                if (it.code == 200) {
                    Toast.makeText(this, "Berhasil Menambahkan Keranjang", Toast.LENGTH_LONG).show()
                    val intent = Intent(this@DetailProductActivity, KeranjangActivity::class.java)
                    startActivity(intent)
                } else {
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }, {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })

        }

        initUI()

    }

    //initUI
    @SuppressLint("SetTextI18n")
    fun initUI() {
        viewModel.getDetailProduct(itemData)
        viewModel.detailProduct.observe(this, Observer {
            Log.d("DataDetail", it.toString())
            binding.tvNamaProduk.text = it?.productName
            binding.tvDetailDeksripsi.text = it?.description
            //Categories
            var categories = ""

            for (i in it.categories!!.indices) {
                categories += if (i < it.categories.size - 1) {
                    it.categories[i].category + ", "
                } else {
                    it.categories[i].category
                }
            }
            binding.tvCategory.text = categories

            //Location
            var location = ""

            for (i in it.location!!.indices) {
                location += if (i < it.location.size - 1) {
                    it.location[i].provinceName + ", "
                } else {
                    it.location[i].provinceName
                }
            }
            binding.tvLocation.text = location

            //Rating
            if (it?.rating == null) {
                binding.tvBintang.text = "-"
            } else {
                binding.tvBintang.text = it.rating.toString()
            }
            //price
            val harga_diskon =
                it.priceSelling!!.toDouble() - (it.priceSelling.toDouble() * it.discount!!.toDouble() / 100.0)
            if (it.discount != "0") {
                binding.tvPriceReal.paintFlags =
                    binding.tvPriceReal.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                binding.tvPriceReal.text = changeToRupiah(it.priceSelling.toDouble())
                binding.tvJumlahDiscount.text = StringBuilder("-").append(it.discount).append("%")
                binding.tvPriceTotal.text = StringBuilder("Total : ").append(
                    changeToRupiah(
                        harga_diskon
                    )
                )
            } else {
                binding.tvPriceReal.visibility = View.GONE
                binding.tvJumlahDiscount.visibility = View.GONE
                binding.tvPriceTotal.text = changeToRupiah(
                    it.priceSelling.toDouble()
                )
            }

            //Total Order
            tv_price_old.text = StringBuilder("Total : ").append(
                changeToRupiah(
                    harga_diskon
                )
            )

            //Dimensi Produk
            binding.tvPanjang.text = "Panjang: ${it.info!!.length} cm"
            binding.tvTinggi.text = "Tinggi: ${it.info.height} cm"
            binding.tvLebar.text = "Lebar: ${it.info.width} cm"
            binding.tvBerat.text = "Berat: ${it.info.weight} kg"

            //proses data
            if (tv_qtyBottom.text == "1") {
                bt_minBottom.isEnabled = false
                bt_minBottom.setTextColor(
                    ContextCompat.getColor(
                        applicationContext,
                        R.color.colorBlack
                    )
                )
                bt_minBottom.setBackgroundResource(R.drawable.rounded_grey_checkout)

            }
            tv_qtyBottom.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    val harga =
                        tv_qtyBottom.text.toString().toDouble() * it.priceSelling.toString()
                            .toDouble()
                    val hargaDiskon = harga - (harga * it.discount.toDouble() / 100)
                    tv_price_old.text = StringBuilder("Total : ").append(
                        changeToRupiah(
                            hargaDiskon
                        )
                    )

                    if (tv_qtyBottom.text.toString().toInt() <= 1) {
                        bt_minBottom.isEnabled = false
                        bt_minBottom.setBackgroundResource(R.drawable.rounded_grey_checkout)
                        bt_minBottom.setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.colorBlack
                            )
                        )
                    } else {
                        bt_minBottom.isEnabled = true
                        bt_minBottom.setBackgroundResource(R.drawable.rounded_red_button_corner)
                        bt_minBottom.setTextColor(
                            ContextCompat.getColor(
                                applicationContext,
                                R.color.colorWhite
                            )
                        )
                    }
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
            var qty = 1
            bt_plusBottom.setOnClickListener {
                qty = tv_qtyBottom.text.toString().toInt() + 1
                tv_qtyBottom.text = qty.toString()
            }
            bt_minBottom.setOnClickListener {
                qty = tv_qtyBottom.text.toString().toInt() - 1
                tv_qtyBottom.text = qty.toString()
            }

        })
    }

    //Add Cart
    private fun addToCart(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataAddCart) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val call: retrofit2.Call<DataAddCart> =
            ApiService().serviceCart.addCart(auth, jsonObject)
        call.enqueue(object : Callback<DataAddCart> {
            override fun onFailure(call: retrofit2.Call<DataAddCart>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: retrofit2.Call<DataAddCart>,
                response: Response<DataAddCart>
            ) {
                Log.d("ADD_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }

    //Setup Toolbar
    private fun setupToolbar() {
        setSupportActionBar(binding.toolbarDetailProduk)
        binding.collapsingToolbarDetailProduct.setCollapsedTitleTextColor(
            ContextCompat.getColor(this@DetailProductActivity, android.R.color.white)
        )
        binding.collapsingToolbarDetailProduct.setExpandedTitleColor(Color.TRANSPARENT)

        supportActionBar!!.title = "Banana Banang"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    //slider load Data
    private fun loadDataSlider() {

        viewModel.getDetailProduct(itemData)
        viewModel.detailProduct.observe(this@DetailProductActivity, Observer {
            val listSlider = arrayListOf<String>()
            listSlider.add(it.uri!!)


            if (it.photos!!.isNotEmpty()) {
                for (element in it.photos.indices) {
                    listSlider.add(it.photos[element].uri!!)
                }
            }

            //initiate slider indicator
            for (i in 0 until listSlider.size) {
                sliderIndicators.add(ImageView(this@DetailProductActivity))
                this.sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)

                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(8, 0, 8, 0)
                if (this.sliderIndicators[i].parent != null) {
                    (this.sliderIndicators[i].parent as ViewGroup).removeView(this.sliderIndicators[i])
                }
                binding.sliderIndicator.addView(this.sliderIndicators[i], params)
            }
            sliderIndicators[0].setImageResource(R.drawable.shape_indicator_active)


            //Adapter Slider
            sliderAdapter =
                SliderAdapterUtil(R.layout.slider_component,
                    this@DetailProductActivity,
                    listSlider,
                    { itemView, item, position ->
                        Glide.with(this).load(item).into(itemView.imageViewBannerRekomendasi)
                    },
                    { item, position ->
                        //intent
                    })

            sliderCount = sliderAdapter.count


            binding.viewPagerDetailProduct.adapter = sliderAdapter
        })

    }

    //handler slider
    private val handler = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            for (i in 0 until sliderCount)
                sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
            sliderIndicators[position].setImageResource(R.drawable.shape_indicator_active)
        }
    }

    //auto banner slider
    private fun startAutoBanner() {
        var currentPage = 0
        someHandler = Handler(Looper.getMainLooper())
        someHandler.postDelayed(object : Runnable {
            override fun run() {
                if (currentPage == sliderCount) {
                    currentPage = 0
                }
                binding.viewPagerDetailProduct.setCurrentItem(currentPage++, true)
                someHandler.postDelayed(this, 3000)
            }
        }, 3000)
    }


    private lateinit var bottomSheet: BottomSheetBehavior<RelativeLayout>

    //bottom sheetHandler
    private fun bottomSheet() {
        bottomSheet = BottomSheetBehavior.from(bottomSheetBeli)
        bottomSheet.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {

            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
        binding.btTambahKeranjang.setOnClickListener {
            if (bottomSheet.state == BottomSheetBehavior.STATE_EXPANDED)
                bottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED
            else
                bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
        }


        //Beli Barang with tambah keranjang
        bt_beli_sekarangBottom.setOnClickListener {
            val json = JsonObject()
            json.addProperty("idMProducts", itemData)
            json.addProperty("qty", tv_qtyBottom.text.toString())


            addToCart(auth.token!!, json, {
                if (it.code == 200) {
                    Toast.makeText(this, "Berhasil Menambahkan Keranjang", Toast.LENGTH_LONG).show()
                    bottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED

                } else {
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            }, {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })

        }

    }

    override fun onResume() {
        super.onResume()
        //After Login
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            binding.tvAlert.visibility = View.GONE
            binding.lyButton.visibility = View.VISIBLE
            auth = getAuth(cacheUtil)
        } else {
            binding.tvAlert.visibility = View.VISIBLE
            binding.lyButton.visibility = View.GONE
            //Login
            binding.tvAlert.setOnClickListener {
                startActivity(Intent(this, LoginActivity::class.java))
            }

        }
    }

}