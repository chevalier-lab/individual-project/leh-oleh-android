package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityHistoryWithdrawBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.wallet.Withdraw
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_withdraw.view.*
import java.text.SimpleDateFormat

class HistoryWithdrawActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHistoryWithdrawBinding
    private lateinit var adapterHistoryWithDraw: PagedAdapterUtil<Withdraw>

    private val viewModel: WithdrawViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(WithdrawViewModel::class.java)
    }

    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryWithdrawBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            getListHistory()
        }
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.history_withdraw)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    fun getListHistory() {
        // Start of History Withdraw
        with(viewModel) {
            setUpListWithdraw(auth.token.toString())
            statustListHistoryWithdraw.observe(this@HistoryWithdrawActivity, Observer {
                stateUtilWithEmptyView(
                    this@HistoryWithdrawActivity,
                    it,
                    binding.progressBar,
                    binding.recyclerHistoryWithdraw,
                    binding.emptyView.tvEmptyView,
                    binding.emptyView.imEmptyView,
                    binding.emptyView.layoutEmptyView,
                    "Riwayat Withdraw Kosong!",
                    R.drawable.ic_monetization
                )
            })
        }

        adapterHistoryWithDraw = PagedAdapterUtil(
            R.layout.item_withdraw,
            { _, itemView, item ->
                when (item.status) {
                    "0" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_refund)
                        text = getString(R.string.failed)
                    }
                    "1" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_selesai)
                        text = getString(R.string.success)
                    }
                    "2" -> with(itemView.tv_status) {
                        setBackgroundResource(R.drawable.status_diproses)
                        text = getString(R.string.withdrawmemunggu)
                    }
                }
                with(itemView) {
                    tv_bank.text = item.bankName
                    tv_nominal.text = changeToRupiah(item.balanceRequest!!.toDouble())

                    val tglTrans =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.createdAt!!)
                    textViewTglTransaksi.text =
                        SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTrans!!)
                }
            },
            { _, item ->
                val intentDetail = Intent(this, DetailWithdrawActivity::class.java)
                intentDetail.putExtra(INTENT_DETAIL, item)
                startActivity(intentDetail)
            })

        with(binding) {
            recyclerHistoryWithdraw.layoutManager = LinearLayoutManager(
                this@HistoryWithdrawActivity,
                LinearLayoutManager.VERTICAL,
                false
            )
            recyclerHistoryWithdraw.adapter = adapterHistoryWithDraw
        }

        viewModel.getListHistoryWithdraw().observe(this, Observer {
            adapterHistoryWithDraw.submitList(it)
        })
        // End of History Withdraw

//        adapterHistoryWithDraw = AdapterUtil(
//            R.layout.item_withdraw,
//            arrayListOf(),
//            { position, itemView, item ->
//                when (item.status) {
//                    "0" -> {
//                        itemView.tv_status.setBackgroundResource(R.drawable.status_refund)
//                        itemView.tv_status.text = getString(R.string.failed)
//                    }
//                    "1" -> {
//                        itemView.tv_status.setBackgroundResource(R.drawable.status_selesai)
//                        itemView.tv_status.text = getString(R.string.success)
//                    }
//                    "2" -> {
//                        itemView.tv_status.setBackgroundResource(R.drawable.status_diproses)
//                        itemView.tv_status.text = getString(R.string.withdrawmemunggu)
//                    }
//                }
//                itemView.tv_bank.text = item.bankName
//                itemView.tv_nominal.text = changeToRupiah(item.balanceRequest!!.toDouble())
//                val tglTransaksi = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.createdAt!!)
//                itemView.textViewTglTransaksi.text =
//                    SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTransaksi!!)
//                //itemView.tv_jam_transaksi.text = item.jamWithdraw
//
//            },
//            { position, item ->
//                //intent
//                val intentDetail = Intent(this, DetailWithdrawActivity::class.java)
//                intentDetail.putExtra(INTENT_DETAIL, item)
//                startActivity(intentDetail)
//
//            })
//        viewModel.getDataListHistoryWithdraw(auth.token!!)
//        viewModel.listHistoryWithdraw.observe(this, Observer {
//            Log.d("DATA_LIST_HISTORY_WD", it.toString())
//            adapterHistoryWithDraw.data = it
//        })
//        binding.recyclerHistoryWithdraw.layoutManager =
//            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
//        binding.recyclerHistoryWithdraw.adapter = adapterHistoryWithDraw
    }

}