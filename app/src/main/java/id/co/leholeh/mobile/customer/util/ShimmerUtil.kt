package id.co.leholeh.mobile.customer.util

import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.facebook.shimmer.ShimmerFrameLayout
import id.co.leholeh.mobile.utils.ApiStatus

object ShimmerUtil {
     fun updateProgressCoordinatorLayout(
        status: ApiStatus,
        shimmer: ShimmerFrameLayout,
        tvNetworkError: TextView,
        layout: CoordinatorLayout
    ) {
        when (status) {
            ApiStatus.LOADING -> {
                shimmer.visibility = View.VISIBLE
                layout.visibility = View.GONE
            }
            ApiStatus.SUCCESS -> {
                shimmer.visibility = View.GONE
                layout.visibility = View.VISIBLE
            }
            ApiStatus.FAILED -> {
                shimmer.visibility = View.GONE
                tvNetworkError.visibility = View.VISIBLE
            }
        }
    }

    fun updateProgressRelativeLayout(
        status: ApiStatus,
        shimmer: ShimmerFrameLayout,
        tvNetworkError: TextView,
        layout: RelativeLayout
    ) {
        when (status) {
            ApiStatus.LOADING -> {
                shimmer.visibility = View.VISIBLE
                layout.visibility = View.GONE
            }
            ApiStatus.SUCCESS -> {
                shimmer.visibility = View.GONE
                layout.visibility = View.VISIBLE
            }
            ApiStatus.FAILED -> {
                shimmer.visibility = View.GONE
                tvNetworkError.visibility = View.VISIBLE
            }
        }
    }

    fun updateProgressConstaraintLayout(
        status: ApiStatus,
        shimmer: ShimmerFrameLayout,
        tvNetworkError: TextView,
        layout: ConstraintLayout
    ) {
        when (status) {
            ApiStatus.LOADING -> {
                shimmer.visibility = View.VISIBLE
                layout.visibility = View.GONE
            }
            ApiStatus.SUCCESS -> {
                shimmer.visibility = View.GONE
                layout.visibility = View.VISIBLE
            }
            ApiStatus.FAILED -> {
                shimmer.visibility = View.GONE
                tvNetworkError.visibility = View.VISIBLE
            }
        }
    }

    fun updateProgressLinearLayout(
        status: ApiStatus,
        shimmer: ShimmerFrameLayout,
        tvNetworkError: TextView,
        layout: LinearLayout
    ) {
        when (status) {
            ApiStatus.LOADING -> {
                shimmer.visibility = View.VISIBLE
                layout.visibility = View.GONE
            }
            ApiStatus.SUCCESS -> {
                shimmer.visibility = View.GONE
                layout.visibility = View.VISIBLE
            }
            ApiStatus.FAILED -> {
                shimmer.visibility = View.GONE
                tvNetworkError.visibility = View.VISIBLE
            }
        }
    }
}