package id.co.leholeh.mobile.customer.ui.register

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.databinding.FragmentRegisterMerchantBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.merchant.ResponseRegisterMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class RegisterMerchantFragment : Fragment() {
    private lateinit var binding : FragmentRegisterMerchantBinding
    private val viewModel: RegisterMerchantViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(RegisterMerchantViewModel::class.java)
    }
    private lateinit var clicked : String
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentRegisterMerchantBinding.inflate(inflater, container, false)
        //getStatusMerchant()
        val cacheUtil = CacheUtil()
        cacheUtil.start(context as Activity, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
        }
        binding.inputFotoKtp.setOnClickListener {
            ImagePicker.create(this)
                .single()
                .start()
            clicked = "KTP"
        }
        binding.inputFotoPemilik.setOnClickListener {
            ImagePicker.create(this)
                .single()
                .start()
            clicked = "pemilik"
        }
        binding.inputFotoToko.setOnClickListener {
            ImagePicker.create(this)
                .single()
                .start()
            clicked = "toko"
        }

        binding.buttonAjukan.setOnClickListener {

            val drawableFotoKtp = binding.imageViewFotoKtp.drawable as BitmapDrawable
            val bitmapFotoKtp = drawableFotoKtp.bitmap
            val fileKtp = createTempFile(bitmapFotoKtp)
            val imageKtp = MultipartBody.Part.createFormData(
                "foto_ktp",
                fileKtp!!.name,
                fileKtp.asRequestBody("image/*".toMediaTypeOrNull())
            )

            val drawableFotoPemilik = binding.imageViewPemilik.drawable as BitmapDrawable
            val bitmapFotoPemilik = drawableFotoPemilik.bitmap
            val filePemilik = createTempFile(bitmapFotoPemilik)
            val imagePemilik = MultipartBody.Part.createFormData(
                "foto_pengguna",
                filePemilik!!.name,
                filePemilik.asRequestBody("image/*".toMediaTypeOrNull())
            )

            val drawableFotoToko = binding.imageViewToko.drawable as BitmapDrawable
            val bitmapFotoToko = drawableFotoToko.bitmap
            val fileToko = createTempFile(bitmapFotoToko)
            val imageToko = MultipartBody.Part.createFormData(
                "foto_toko",
                fileToko!!.name,
                fileToko.asRequestBody("image/*".toMediaTypeOrNull())
            )

            val noKtp = MultipartBody.Part.createFormData("no_ktp", binding.editTextNomorKtp.text.toString())
            val namaToko = MultipartBody.Part.createFormData("nama_toko", binding.editTextNamaToko.text.toString())
            val alamatToko = MultipartBody.Part.createFormData("alamat", binding.editTextAlamatToko.text.toString())
            val noHp = MultipartBody.Part.createFormData("no_hp", binding.editTextNomorHp.text.toString())

            val  auth = getAuth(cacheUtil)

            setRegisterMerchant(auth.token!!,imageKtp,imagePemilik, imageToko, namaToko, noHp, alamatToko, noKtp,{
                if(it.code == 200){
                    Toast.makeText(
                        context,
                        "Berhasil Mendaftar!",
                        Toast.LENGTH_SHORT
                    ).show()

                }
            },{
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            })

        }

        getStatusMerchant()

        return binding.root
    }

   private fun getStatusMerchant(){
        val cacheUtil = CacheUtil()
        cacheUtil.start(context as Activity, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
        }
        val  auth = getAuth(cacheUtil)
        viewModel.getDataStatusMerchant(auth.token!!)
        viewModel.statusMerchant.observe(viewLifecycleOwner, Observer {
            if(it.isVisible!!.isNotEmpty()){

            }
            else{
                Log.d("DATA_STATUS", "BELUM LOGIN")
            }

        })
    }

    private fun setRegisterMerchant(
        authorization : String,
        fotoKtp : MultipartBody.Part,
        fotoPemilik : MultipartBody.Part,
        fotoToko : MultipartBody.Part,
        namaToko : MultipartBody.Part,
        noHP : MultipartBody.Part,
        alamat : MultipartBody.Part,
        noKtp : MultipartBody.Part,
        onSuccess: (ResponseRegisterMerchant) -> Unit,
        onFailed: (String) -> Unit){

        val call: Call<ResponseRegisterMerchant> = ApiService().serviceGuestMerchants.registerMerchant(authorization,fotoKtp,
        fotoPemilik, fotoToko, namaToko, noHP, alamat, noKtp)
        call.enqueue(object : Callback<ResponseRegisterMerchant>{
            override fun onFailure(call: Call<ResponseRegisterMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRegisterMerchant>,
                response: Response<ResponseRegisterMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })

    }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
                val image : Image = ImagePicker.getFirstImageOrNull(data)
                when(clicked){
                    "KTP" -> {
                        Glide.with(binding.imageViewFotoKtp).load(image.uri).into(binding.imageViewFotoKtp)
                        binding.imageViewFotoKtp.visibility = View.VISIBLE
                    }
                    "pemilik" -> {
                        Glide.with(binding.imageViewPemilik).load(image.uri).into(binding.imageViewPemilik)
                        binding.imageViewPemilik.visibility = View.VISIBLE
                    }
                    "toko" -> {
                        Glide.with(binding.imageViewToko).load(image.uri).into(binding.imageViewToko)
                        binding.imageViewToko.visibility = View.VISIBLE
                    }
                }

            }
            super.onActivityResult(requestCode, resultCode, data)
        }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            , System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

}