package id.co.leholeh.mobile.customer.constant

object ConstantGuestMerchants {
    const val DATA_MERCHANT = "data_merchant"
    const val BASE_URL_GUEST_MERCHANT = "guest/merchant"
    const val BASE_URL_GUEST_MERCHANT_RANDOM = "guest/merchant/random"
    const val BASE_URL_GUEST_MERCHANT_DETAIL = "guest/merchant/{id}"
    const val BASE_URL_GUEST_MERCHANT_PRODUCT_LIST = "guest/merchant/{id}/products"
    const val BASE_URL_GUEST_MERCHANT_STATUS = "guest/merchant/status"
    const val BASE_URL_GUEST_MERCHANT_REQUEST_MERCHANT = "guest/merchant/request"
    //const val BASE_URL_GUEST_MERCHANT_PRODUK = "guest/merchant/{id}/produk"

}