package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantOrders
import id.co.leholeh.mobile.customer.constant.ConstantWallet
import id.co.leholeh.mobile.customer.model.orders.midtrans.ResponseGetStatusTransaction
import id.co.leholeh.mobile.customer.model.wallet.*
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup.ResponseHistoryTopup
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.ResponseTagihanTopup
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface TopupService {
    @GET(ConstantWallet.BASE_URL_LIST_HISTORY_TOPUP)
    suspend fun getListHistoryTopup(@Header("Authorization") authorization : String, @Query("page") page : Int) : Response<ResponseHistoryTopup>

    @GET(ConstantWallet.BASE_URL_LIST_TAGIHAN_TOPUP)
    suspend fun getListTagihanTopup(@Header("Authorization") authorization: String, @Query("page") page: Int) : Response<ResponseTagihanTopup>

    @POST(ConstantWallet.BASE_URL_REQUEST_TOPUP)
    fun requestTopup(@Header("Authorization") authorization: String, @Body jsonObject: JsonObject) : Call<ResponseRequestTopup>

    @PUT(ConstantWallet.BASE_URL_UPDATE_TOKEN)
    fun updateTokenTopup(@Header("Authorization") authorization: String, @Body jsonObject: JsonObject,@Path ("id")id: String) : Call<ResponseUpdateTokenTopup>

    @PUT(ConstantWallet.BASE_URL_EDIT_PROOF)
    fun editProof(@Header("Authorization") authorization: String, @Body jsonObject: JsonObject) : Call<ResponseEditProofTopup>

    @Multipart
    @POST(ConstantWallet.BASE_URL_REQ_PROOF)
    fun requestProof(@Header("Authorization") authorization: String, @Part photo : MultipartBody.Part) : Call<ResponseCreateProof>

    @GET(ConstantWallet.BASE_URL_DETAIL_BANK)
    suspend fun getDetailBanks(@Header("Authorization") authorization: String,@Path("id")id:String) : ResponseDetailBank

    @GET(ConstantWallet.BASE_URL_BANK)
    suspend fun getListBanks(@Header("Authorization") authorization: String) : ResponseListBank

    @GET(ConstantWallet.BASE_URL_STEP_BANK)
    suspend fun getStepBanks(@Header("Authorization") authorization: String,@Path("id")id:String) : ResponseStepBanks

    @GET(ConstantOrders.BASE_URL_GET_STATUS_MIDTRANS)
    suspend fun getTransactionStatus(
        @Header("Authorization") auth: String,
        @Path("token") token: String
    ): ResponseGetStatusTransaction
}