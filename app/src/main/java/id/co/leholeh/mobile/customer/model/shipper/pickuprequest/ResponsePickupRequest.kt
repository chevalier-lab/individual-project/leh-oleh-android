package id.co.leholeh.mobile.customer.model.shipper.pickuprequest

import com.google.gson.annotations.SerializedName

data class ResponsePickupRequest(

	@field:SerializedName("data")
	val data: DataPickupRequest? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataPickupRequest(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
