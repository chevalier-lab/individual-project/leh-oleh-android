package id.co.leholeh.mobile.merchant.ui.profile

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantLocationBinding
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdateLocationMerchant
import id.co.leholeh.mobile.merchant.network.ProfileTokoService
import id.co.leholeh.mobile.utils.GetLocationShipperViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MerchantLocationActivity : AppCompatActivity() {
    lateinit var binding: ActivityMerchantLocationBinding
    private val viewModelLocation: GetLocationShipperViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(GetLocationShipperViewModel::class.java)
    }

    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }

    private lateinit var auth: String

    private lateinit var idMerchant: String
    private var arrayIdProvinceLocation = arrayListOf<PenampungLocation>()
    private var arrayIdSuburbsLocation = arrayListOf<PenampungLocation>()
    private var arrayIdCitiesLocation = arrayListOf<PenampungLocation>()
    private var arrayIdAreaLocation = arrayListOf<PenampungLocation>()

    private val itemProvinceName: ArrayList<String> = arrayListOf()
    val itemCitiesName: ArrayList<String> = arrayListOf()
    val itemAreasName: ArrayList<String> = arrayListOf()
    val itemSuburbsName: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //get id merchant
        idMerchant = intent.getSerializableExtra(INTENT_DETAIL) as String
        Log.v("MerchantLocation", "Id Merchant : $idMerchant")
        //Auth
        auth = intent.getSerializableExtra(INTENT_SEARCH) as String

        //setup Toolbar
        setupToolbar()

        //shipper handler
        locationShiiper()

        //submit location
        locationUI()




    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Merchant Location"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    //send create Location
    private fun sendRequestCreateLocation() {
        val provinsi = binding.dropdownProvinsi.text.toString()
        val cities = binding.dropdownKota.text.toString()
        val suburbs = binding.dropdownSuburb.text.toString()
        val area = binding.dropdownArea.text.toString()
        val kodePos = binding.dropdownKodepos.text.toString()
        val alamat = binding.etAlamat.text.toString()

        if (provinsi.isEmpty() || cities.isEmpty() || suburbs.isEmpty() || area.isEmpty() || kodePos.isEmpty() || alamat.isEmpty()) {
            Toast.makeText(this, "Isi data dengan lengkap!", Toast.LENGTH_SHORT).show()
        } else {
            val jsonObject = JsonObject()
            jsonObject.apply {
                addProperty("id_u_user_is_merchant", idMerchant)
                addProperty("province_id", arrayIdProvinceLocation[0].id)
                addProperty("province", arrayIdProvinceLocation[0].name)
                addProperty("city_id", arrayIdCitiesLocation[0].id)
                addProperty("city", arrayIdCitiesLocation[0].name)
                addProperty("suburb_id", arrayIdSuburbsLocation[0].id)
                addProperty("suburb", arrayIdSuburbsLocation[0].name)
                addProperty("area_id", arrayIdAreaLocation[0].id)
                addProperty("area", arrayIdAreaLocation[0].name)
                addProperty("postcode", kodePos)
                addProperty("address", alamat)

                Log.v("MerchantLocation", "Json IdMerchant $idMerchant")
                Log.v("MerchantLocation", "Json province_id " + arrayIdProvinceLocation[0])
                Log.v("MerchantLocation", "Json province $provinsi")
                Log.v("MerchantLocation", "Json city_id " + arrayIdCitiesLocation[0])
                Log.v("MerchantLocation", "Json city $cities")
                Log.v("MerchantLocation", "Json suburb_id " + arrayIdSuburbsLocation[0])
                Log.v("MerchantLocation", "Json suburb $suburbs")
                Log.v("MerchantLocation", "Json area_id " + arrayIdAreaLocation[0])
                Log.v("MerchantLocation", "Json area $area")
                Log.v("MerchantLocation", "Json postcode $kodePos")
                Log.v("MerchantLocation", "Json address $alamat")

                //eksekusi nya
                createLocationMerchant(jsonObject, {
                    if (it.code == 200) {
                        Toast.makeText(
                            this@MerchantLocationActivity,
                            "Berhasil Membuat Lokasi!",
                            Toast.LENGTH_SHORT
                        ).show()

                        startActivity(
                            Intent(
                                this@MerchantLocationActivity,
                                MerchantTokoSayaActivity::class.java
                            )
                        )
                    } else {
                        Toast.makeText(
                            this@MerchantLocationActivity,
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }, {
                    Toast.makeText(this@MerchantLocationActivity, it, Toast.LENGTH_LONG).show()
                })
            }
        }
    }

    private fun createLocationMerchant(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateLocationMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ProfileTokoService by lazy {
            ApiService().createService(ProfileTokoService::class.java)
        }
        val call: Call<ResponseUpdateLocationMerchant> =
            apiService.createLocationMerchant(jsonObject)
        call.enqueue(object : Callback<ResponseUpdateLocationMerchant> {
            override fun onResponse(
                call: Call<ResponseUpdateLocationMerchant>,
                response: Response<ResponseUpdateLocationMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateLocationMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })
    }

    private fun locationShiiper() {
        var idCities: String
        var idProvince: String
        var idSuburbs: String
        //province
        viewModelLocation.getProvince()
        viewModelLocation.province.observe(this, Observer { itemProvince ->
            itemProvinceName.clear()
            itemCitiesName.clear()
            itemAreasName.clear()
            itemSuburbsName.clear()
            for (item in itemProvince) {
                itemProvinceName.add(item.name!!)
            }
            val adapterProvince =
                ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemProvinceName)
            binding.dropdownProvinsi.setAdapter(adapterProvince)
            binding.dropdownProvinsi.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    //cities
                    for (i in itemProvince.indices) {
                        if (binding.dropdownProvinsi.text.toString() == itemProvince[i].name) {
                            //get id province
                            arrayIdProvinceLocation.clear()
                            arrayIdProvinceLocation.add(
                                PenampungLocation(
                                    itemProvince[i].id.toString(),
                                    itemProvince[i].name.toString()
                                )
                            )
                            Log.v("MerchantLocation", "Id Province : $arrayIdProvinceLocation")


                            viewModelLocation.getCities(itemProvince[i].id.toString())
                            idProvince = itemProvince[i].id.toString()
                            Log.d("IDProvincsi", "Dalam $idProvince")
                            viewModelLocation.cities.observe(
                                this@MerchantLocationActivity,
                                Observer { itemCities ->
                                    itemCitiesName.clear()
                                    itemAreasName.clear()
                                    itemSuburbsName.clear()
                                    //cleartext
                                    binding.dropdownKota.setText("")
                                    binding.dropdownArea.setText("")
                                    binding.dropdownSuburb.setText("")
                                    binding.dropdownKodepos.setText("")

                                    for (itCities in itemCities) {
                                        itemCitiesName.add(itCities.name!!)
                                    }
                                    val adapterCities =
                                        ArrayAdapter(
                                            applicationContext,
                                            R.layout.item_list_dropdown_text,
                                            itemCitiesName
                                        )
                                    binding.dropdownKota.setAdapter(adapterCities)
                                })

                            break
                        }
                    }
                }

            })
        })

        viewModelLocation.cities.observe(
            this@MerchantLocationActivity,
            Observer { itemCities ->
                binding.dropdownKota.addTextChangedListener(object :
                    TextWatcher {
                    override fun beforeTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun onTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        Log.d("IDProvincsi", "Citites $itemCities")
                        //surburbs
                        for (iSub in itemCities.indices) {
                            if (binding.dropdownKota.text.toString() == itemCities[iSub].name) {
                                arrayIdCitiesLocation.clear()
                                arrayIdCitiesLocation.add(
                                    PenampungLocation(
                                        itemCities[iSub].id.toString(),
                                        itemCities[iSub].name.toString()
                                    )
                                )

                                Log.v("MerchantLocation", "Id Cities : $arrayIdCitiesLocation")



                                idSuburbs = itemCities[iSub].id.toString()
                                Log.d("IDProvincsi", "SUBURUBS ")
                                viewModelLocation.getSuburbs(idSuburbs)
                                viewModelLocation.suburbs.observe(
                                    this@MerchantLocationActivity,
                                    Observer { itemSuburbs ->
                                        itemSuburbsName.clear()
                                        itemAreasName.clear()
                                        //cleartext
                                        binding.dropdownArea.setText("")
                                        binding.dropdownSuburb.setText("")
                                        binding.dropdownKodepos.setText("")

                                        for (itSuburbs in itemSuburbs) {
                                            itemSuburbsName.add(itSuburbs.name!!)
                                        }
                                        val adapterSuburbs =
                                            ArrayAdapter(
                                                applicationContext,
                                                R.layout.item_list_dropdown_text,
                                                itemSuburbsName
                                            )
                                        binding.dropdownSuburb.setAdapter(
                                            adapterSuburbs
                                        )
                                    })
                                break
                            }
                        }
                    }
                })
            })


        viewModelLocation.suburbs.observe(
            this@MerchantLocationActivity,
            Observer { itemSuburbs ->
                binding.dropdownSuburb.addTextChangedListener(
                    object : TextWatcher {
                        override fun beforeTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun onTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun afterTextChanged(p0: Editable?) {
                            //Areas
                            for (iAreas in itemSuburbs.indices) {
                                if (binding.dropdownSuburb.text.toString() == itemSuburbs[iAreas].name) {

                                    arrayIdSuburbsLocation.clear()
                                    arrayIdSuburbsLocation.add(
                                        PenampungLocation(
                                            itemSuburbs[iAreas].id.toString(),
                                            itemSuburbs[iAreas].name.toString()
                                        )
                                    )

                                    Log.v(
                                        "MerchantLocation",
                                        "Id Suburbs : $arrayIdSuburbsLocation"
                                    )

                                    viewModelLocation.getAreas(
                                        itemSuburbs[iAreas].id.toString()
                                    )
                                    viewModelLocation.areas.observe(
                                        this@MerchantLocationActivity,
                                        Observer { itemAreas ->
                                            itemAreasName.clear()
                                            //cleartext
                                            binding.dropdownArea.setText("")
                                            binding.dropdownKodepos.setText("")
                                            for (itAreas in itemAreas) {
                                                itemAreasName.add(
                                                    itAreas.name!!
                                                )
                                            }
                                            val adapterAreas =
                                                ArrayAdapter(
                                                    applicationContext,
                                                    R.layout.item_list_dropdown_text,
                                                    itemAreasName
                                                )
                                            binding.dropdownArea.setAdapter(
                                                adapterAreas
                                            )

                                        })
                                    break
                                }
                            }
                        }
                    })
            })

        viewModelLocation.areas.observe(
            this@MerchantLocationActivity,
            Observer { itemAreas ->
                binding.dropdownArea.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        for (i in itemAreas.indices) {
                            if (itemAreas[i].name == binding.dropdownArea.text.toString()) {
                                arrayIdAreaLocation.clear()
                                arrayIdAreaLocation.add(
                                    PenampungLocation(
                                        itemAreas[i].id.toString(),
                                        itemAreas[i].name.toString()
                                    )
                                )
                                Log.v("MerchantLocation", "Id Areas : $arrayIdAreaLocation")
                                binding.dropdownKodepos.setText(itemAreas[i].postcode)
                                break
                            }
                        }
                    }

                })
            })
    }

    //data location exist
    private fun locationUI() {
        viewModel.getDataProfileToko(auth)
        viewModel.toko.observe(this, Observer { it ->
            if (it.location == null) {
                binding.btSubmit.setOnClickListener {
                    sendRequestCreateLocation()
                }
            } else {
                binding.btSubmit.text = "Update"

                arrayIdProvinceLocation.add(
                    PenampungLocation(
                        it.location.provinceId.toString(),
                        it.location.province.toString()
                    )
                )
                arrayIdCitiesLocation.add(
                    PenampungLocation(
                        it.location.cityId.toString(),
                        it.location.city.toString()
                    )
                )
                arrayIdSuburbsLocation.add(
                    PenampungLocation(
                        it.location.suburbId.toString(),
                        it.location.suburb.toString()
                    )
                )
                arrayIdAreaLocation.add(
                    PenampungLocation(
                        it.location.areaId.toString(),
                        it.location.area.toString()
                    )
                )

                binding.dropdownProvinsi.setText(it.location.province)
                binding.dropdownKota.setText(it.location.city)
                binding.dropdownSuburb.setText(it.location.suburb)
                binding.dropdownArea.setText(it.location.area)
                binding.dropdownKodepos.setText(it.location.postcode)
                binding.etAlamat.setText(it.location.address)


                //GetCities Update
                viewModelLocation.getCities(it.location.provinceId.toString())
                viewModelLocation.cities.observe(
                    this@MerchantLocationActivity,
                    Observer { itemCities ->
                        itemCitiesName.clear()
                        for (itCities in itemCities) {
                            itemCitiesName.add(itCities.name!!)
                        }
                        val adapterCities =
                            ArrayAdapter(
                                applicationContext,
                                R.layout.item_list_dropdown_text,
                                itemCitiesName
                            )
                        binding.dropdownKota.setAdapter(adapterCities)
                    })

                //GetListSuburbs Update
                viewModelLocation.getSuburbs(it.location.cityId.toString())
                viewModelLocation.suburbs.observe(
                    this@MerchantLocationActivity,
                    Observer { itemSuburbs ->
                        itemSuburbsName.clear()
                        for (itSuburbs in itemSuburbs) {
                            itemSuburbsName.add(itSuburbs.name!!)
                        }
                        val adapterSuburbs =
                            ArrayAdapter(
                                applicationContext,
                                R.layout.item_list_dropdown_text,
                                itemSuburbsName
                            )
                        binding.dropdownSuburb.setAdapter(
                            adapterSuburbs
                        )
                    })

                //GetListAreas Update
                viewModelLocation.getAreas(
                    it.location.suburbId.toString()
                )
                viewModelLocation.areas.observe(
                    this@MerchantLocationActivity,
                    Observer { itemAreas ->
                        itemAreasName.clear()
                        for (itAreas in itemAreas) {
                            itemAreasName.add(
                                itAreas.name!!
                            )
                        }
                        val adapterAreas =
                            ArrayAdapter(
                                applicationContext,
                                R.layout.item_list_dropdown_text,
                                itemAreasName
                            )
                        binding.dropdownArea.setAdapter(
                            adapterAreas
                        )
                    })

                //if button update onclick
                binding.btSubmit.setOnClickListener {
                    val provinsi = binding.dropdownProvinsi.text.toString()
                    val cities = binding.dropdownKota.text.toString()
                    val suburbs = binding.dropdownSuburb.text.toString()
                    val area = binding.dropdownArea.text.toString()
                    val kodePos = binding.dropdownKodepos.text.toString()
                    val alamat = binding.etAlamat.text.toString()

                    if (provinsi.isEmpty() || cities.isEmpty() || suburbs.isEmpty() || area.isEmpty() || kodePos.isEmpty() || alamat.isEmpty()) {
                        Toast.makeText(this, "Isi data dengan lengkap!", Toast.LENGTH_SHORT).show()
                    } else {
                        val jsonObject = JsonObject()
                        jsonObject.apply {
                            addProperty("province_id", arrayIdProvinceLocation[0].id)
                            addProperty("province", arrayIdProvinceLocation[0].name)
                            addProperty("city_id", arrayIdCitiesLocation[0].id)
                            addProperty("city", arrayIdCitiesLocation[0].name)
                            addProperty("suburb_id", arrayIdSuburbsLocation[0].id)
                            addProperty("suburb", arrayIdSuburbsLocation[0].name)
                            addProperty("area_id", arrayIdAreaLocation[0].id)
                            addProperty("area", arrayIdAreaLocation[0].name)
                            addProperty("postcode", kodePos)
                            addProperty("address", alamat)
                        }

                        //eksekusi Update Location
                        updateLocationMerchant(jsonObject, { response ->
                            if (response.code == 200) {
                                Toast.makeText(
                                    this@MerchantLocationActivity,
                                    "Berhasil Menupdate Lokasi!",
                                    Toast.LENGTH_SHORT
                                ).show()

                                startActivity(
                                    Intent(
                                        this,
                                        MerchantTokoSayaActivity::class.java
                                    )
                                )
                            } else {
                                Toast.makeText(
                                    this@MerchantLocationActivity,
                                    response.message,
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }, { response ->
                            Toast.makeText(
                                this@MerchantLocationActivity,
                                response,
                                Toast.LENGTH_LONG
                            )
                                .show()
                        })

                    }
                }
            }
        })
    }

    //Update Location

    private fun updateLocationMerchant(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateLocationMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ProfileTokoService by lazy {
            ApiService().createService(ProfileTokoService::class.java)
        }
        val call: Call<ResponseUpdateLocationMerchant> =
            apiService.updateLocationMerchant(idMerchant, jsonObject)
        call.enqueue(object : Callback<ResponseUpdateLocationMerchant> {
            override fun onResponse(
                call: Call<ResponseUpdateLocationMerchant>,
                response: Response<ResponseUpdateLocationMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateLocationMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getDataProfileToko(auth)
    }

    data class PenampungLocation(
        var id: String,
        var name: String
    )


}