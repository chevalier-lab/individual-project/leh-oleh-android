package id.co.leholeh.mobile.customer.ui.location

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityEditUserLocationBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.shipper.ConstantShipper
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.userlocation.DataCurrentUserLocation
import id.co.leholeh.mobile.customer.model.userlocation.DataUserLocation
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.GetLocationShipperViewModel
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditUserLocationActivity : AppCompatActivity() {
    lateinit var binding: ActivityEditUserLocationBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    private val viewModel: UserLocationViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(UserLocationViewModel::class.java)
    }

    private val viewModelLocation: GetLocationShipperViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(GetLocationShipperViewModel::class.java)
    }

    private var arrayIdProvinceLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdSuburbsLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdCitiesLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdAreaLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()

    private val itemProvinceName: ArrayList<String> = arrayListOf()
    val itemCitiesName: ArrayList<String> = arrayListOf()
    val itemAreasName: ArrayList<String> = arrayListOf()
    val itemSuburbsName: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditUserLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Init cacheUtil
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        auth = getAuth(cacheUtil)

        //setupToolbar
        setupToolbar()

        val idLocation = intent.getSerializableExtra(INTENT_DETAIL) as String

        //location shipper
        locationShiiper(binding)

        updateCurrentLocation(auth.token!!, idLocation)
    }

    @SuppressLint("LongLogTag", "SetTextI18n")
    private fun updateCurrentLocation(auth: String, idLocation: String) {
        var status = ""
        val statusLama = ""

        //LayoutStatus
        binding.layoutStatus.visibility = View.VISIBLE

        with(viewModel) {
            // GET EXISTING DATA
            getUserListLocation(auth)
            listLocation.observe(this@EditUserLocationActivity, Observer {
                for (i in it.data.indices) {
                    if (it.data[i].id == idLocation) {
                        binding.tvAdd.text = "Update"
//                        statusLama = it.data[i].is_active

                        arrayIdProvinceLocation.add(
                            MerchantLocationActivity.PenampungLocation(
                                it.data[i].province_id,
                                it.data[i].province
                            )
                        )
                        arrayIdCitiesLocation.add(
                            MerchantLocationActivity.PenampungLocation(
                                it.data[i].city_id,
                                it.data[i].city
                            )
                        )
                        arrayIdSuburbsLocation.add(
                            MerchantLocationActivity.PenampungLocation(
                                it.data[i].suburb_id,
                                it.data[i].suburb
                            )
                        )
                        arrayIdAreaLocation.add(
                            MerchantLocationActivity.PenampungLocation(
                                it.data[i].area_id,
                                it.data[i].area
                            )
                        )

                        binding.etAlamat.setText(it.data[i].address)

                        if (it.data[i].is_active == "1") {
                            binding.rbActive.isChecked = true
                        } else if (it.data[i].is_active == "0") {
                            binding.rbNonActive.isChecked = true
                        }
                        binding.dropdownProvinsi.setText(it.data[i].province)

                        Log.v("DropdownProvinsi", it.data[i].province)
                        Log.v("DropdownProvinsi", "Kota " + it.data[i].city)

                        binding.dropdownKota.setText(it.data[i].city)
                        binding.dropdownSuburb.setText(it.data[i].suburb)
                        binding.dropdownArea.setText(it.data[i].area)
                        binding.dropdownKodepos.setText(it.data[i].postcode)

                        //Province Update
                        viewModelLocation.getProvince()
                        viewModelLocation.province.observe(
                            this@EditUserLocationActivity,
                            Observer { itemProvince ->
                                itemProvinceName.clear()
                                for (itProvince in itemProvince) {
                                    itemProvinceName.add(itProvince.name!!)
                                }
                                val adapterProvince =
                                    ArrayAdapter(
                                        applicationContext,
                                        R.layout.item_list_dropdown_text,
                                        itemProvinceName
                                    )
                                binding.dropdownProvinsi.setAdapter(adapterProvince)
                            })

                        //GetCities Update
                        viewModelLocation.getCities(it.data[i].province_id)
                        viewModelLocation.cities.observe(
                            this@EditUserLocationActivity,
                            Observer { itemCities ->
                                itemCitiesName.clear()
                                for (itCities in itemCities) {
                                    itemCitiesName.add(itCities.name!!)
                                }
                                val adapterCities =
                                    ArrayAdapter(
                                        applicationContext,
                                        R.layout.item_list_dropdown_text,
                                        itemCitiesName
                                    )
                                binding.dropdownKota.setAdapter(adapterCities)
                            })

                        //GetListSuburbs Update
                        viewModelLocation.getSuburbs(it.data[i].city_id)
                        viewModelLocation.suburbs.observe(
                            this@EditUserLocationActivity,
                            Observer { itemSuburbs ->
                                itemSuburbsName.clear()
                                for (itSuburbs in itemSuburbs) {
                                    itemSuburbsName.add(itSuburbs.name!!)
                                }
                                val adapterSuburbs =
                                    ArrayAdapter(
                                        applicationContext,
                                        R.layout.item_list_dropdown_text,
                                        itemSuburbsName
                                    )
                                binding.dropdownSuburb.setAdapter(
                                    adapterSuburbs
                                )
                            })

                        //GetListAreas Update
                        viewModelLocation.getAreas(
                            it.data[i].suburb_id
                        )
                        viewModelLocation.areas.observe(
                            this@EditUserLocationActivity,
                            Observer { itemAreas ->
                                itemAreasName.clear()
                                for (itAreas in itemAreas) {
                                    itemAreasName.add(
                                        itAreas.name!!
                                    )
                                }
                                val adapterAreas =
                                    ArrayAdapter(
                                        applicationContext,
                                        R.layout.item_list_dropdown_text,
                                        itemAreasName
                                    )
                                binding.dropdownArea.setAdapter(adapterAreas)
                            })
                        break
                    }
                }
            })
        }

        binding.tvAdd.setOnClickListener {
            val provinsi = binding.dropdownProvinsi.text.toString()
            val cities = binding.dropdownKota.text.toString()
            val suburbs = binding.dropdownSuburb.text.toString()
            val area = binding.dropdownArea.text.toString()
            val kodePos = binding.dropdownKodepos.text.toString()
            val alamat = binding.etAlamat.text.toString()

            if (provinsi.isEmpty() || cities.isEmpty() || suburbs.isEmpty() || area.isEmpty() || kodePos.isEmpty() || alamat.isEmpty()) {
                Toast.makeText(this, "Isi data dengan lengkap!", Toast.LENGTH_SHORT).show()
            } else {
                // Set JsonObject
                val jsonObject = JsonObject()

                when {
                    binding.rbActive.isChecked -> status = "1"
                    binding.rbNonActive.isChecked -> status = "0"
                }

                jsonObject.apply {
                    addProperty("province_id", arrayIdProvinceLocation[0].id)
                    addProperty("province", arrayIdProvinceLocation[0].name)
                    addProperty("city_id", arrayIdCitiesLocation[0].id)
                    addProperty("city", arrayIdCitiesLocation[0].name)
                    addProperty("suburb_id", arrayIdSuburbsLocation[0].id)
                    addProperty("suburb", arrayIdSuburbsLocation[0].name)
                    addProperty("area_id", arrayIdAreaLocation[0].id)
                    addProperty("area", arrayIdAreaLocation[0].name)
                    addProperty("postcode", kodePos)
                    addProperty("address", alamat)
                    addProperty("is_active", status)
                }

                requestEditLocation(
                    auth,
                    idLocation,
                    jsonObject,
                    {
                        if (it.code == 200) {
                            Toast.makeText(this, "Berhasil mengubah lokasi!", Toast.LENGTH_SHORT)
                                .show()
                            viewModel.getUserListLocation(auth)

                            onBackPressed()
                        } else {
//                            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                            Log.d("Update_location", it.message)
                        }
                    },
                    {
//                        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                        Log.d("Update_location", it)
                    }
                )

                if (status != statusLama) {
                    if (status == "1") {
                        requestEditCurrentLocation(
                            auth,
                            idLocation,
                            {
                                if (it.code == 200) {
                                    Toast.makeText(
                                        this,
                                        "Berhasil Mengubah Lokasi Sekarang",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                    viewModel.getUserListLocation(auth)
                                    onBackPressed()
                                } else {
//                                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                                    Log.d("Update_current", it.message.toString())
                                }
                            },
                            {
//                                Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                                Log.d("Update_current", it)
                            }
                        )
                    }
                }
            }
        }

        binding.tvCancel.setOnClickListener {
            onBackPressed()
        }
    }

    private fun locationShiiper(binding: ActivityEditUserLocationBinding) {
        var idProvince: String
        var idSuburbs: String
        //province
        viewModel.getListProvince(ConstantShipper.APIKEY_SHIPPER)
        viewModel.listShipperProvince.observe(this, Observer { itemProvince ->
            itemProvinceName.clear()
            for (item in itemProvince) {
                itemProvinceName.add(item.name!!)
            }
            val adapterProvince =
                ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemProvinceName)
            binding.dropdownProvinsi.setAdapter(adapterProvince)
            binding.dropdownProvinsi.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    //cities
                    for (i in itemProvince.indices) {
                        if (binding.dropdownProvinsi.text.toString() == itemProvince[i].name) {
                            //get id province
                            arrayIdProvinceLocation.clear()
                            arrayIdProvinceLocation.add(
                                MerchantLocationActivity.PenampungLocation(
                                    itemProvince[i].id.toString(),
                                    itemProvince[i].name.toString()
                                )
                            )
                            Log.v("MerchantLocation", "Id Province : $arrayIdProvinceLocation")


                            viewModelLocation.getCities(itemProvince[i].id.toString())
                            idProvince = itemProvince[i].id.toString()
                            Log.d("IDProvincsi", "Dalam $idProvince")
                            viewModelLocation.cities.observe(
                                this@EditUserLocationActivity,
                                Observer { itemCities ->
                                    itemCitiesName.clear()
                                    //cleartext
                                    binding.dropdownKota.setText("")
                                    binding.dropdownArea.setText("")
                                    binding.dropdownSuburb.setText("")
                                    binding.dropdownKodepos.setText("")

                                    for (itCities in itemCities) {
                                        itemCitiesName.add(itCities.name!!)
                                    }
                                    val adapterCities =
                                        ArrayAdapter(
                                            applicationContext,
                                            R.layout.item_list_dropdown_text,
                                            itemCitiesName
                                        )
                                    binding.dropdownKota.setAdapter(adapterCities)
                                })

                            break
                        }
                    }
                }

            })
        })

        viewModelLocation.cities.observe(
            this@EditUserLocationActivity,
            Observer { itemCities ->
                binding.dropdownKota.addTextChangedListener(object :
                    TextWatcher {
                    override fun beforeTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun onTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        Log.d("IDProvincsi", "Citites $itemCities")
                        //surburbs
                        for (iSub in itemCities.indices) {
                            if (binding.dropdownKota.text.toString() == itemCities[iSub].name) {
                                arrayIdCitiesLocation.clear()
                                arrayIdCitiesLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemCities[iSub].id.toString(),
                                        itemCities[iSub].name.toString()
                                    )
                                )

                                Log.v("MerchantLocation", "Id Cities : $arrayIdCitiesLocation")



                                idSuburbs = itemCities[iSub].id.toString()
                                Log.d("IDProvincsi", "SUBURUBS ")
                                viewModelLocation.getSuburbs(idSuburbs)
                                viewModelLocation.suburbs.observe(
                                    this@EditUserLocationActivity,
                                    Observer { itemSuburbs ->
                                        itemSuburbsName.clear()
                                        //cleartext
                                        binding.dropdownArea.setText("")
                                        binding.dropdownSuburb.setText("")
                                        binding.dropdownKodepos.setText("")

                                        for (itSuburbs in itemSuburbs) {
                                            itemSuburbsName.add(itSuburbs.name!!)
                                        }
                                        val adapterSuburbs =
                                            ArrayAdapter(
                                                applicationContext,
                                                R.layout.item_list_dropdown_text,
                                                itemSuburbsName
                                            )
                                        binding.dropdownSuburb.setAdapter(
                                            adapterSuburbs
                                        )
                                    })
                                break
                            }
                        }
                    }
                })
            })


        viewModelLocation.suburbs.observe(
            this@EditUserLocationActivity,
            Observer { itemSuburbs ->
                binding.dropdownSuburb.addTextChangedListener(
                    object : TextWatcher {
                        override fun beforeTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun onTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun afterTextChanged(p0: Editable?) {
                            //Areas
                            for (iAreas in itemSuburbs.indices) {
                                if (binding.dropdownSuburb.text.toString() == itemSuburbs[iAreas].name) {

                                    arrayIdSuburbsLocation.clear()
                                    arrayIdSuburbsLocation.add(
                                        MerchantLocationActivity.PenampungLocation(
                                            itemSuburbs[iAreas].id.toString(),
                                            itemSuburbs[iAreas].name.toString()
                                        )
                                    )

                                    Log.v(
                                        "MerchantLocation",
                                        "Id Suburbs : $arrayIdSuburbsLocation"
                                    )

                                    viewModelLocation.getAreas(
                                        itemSuburbs[iAreas].id.toString()
                                    )
                                    viewModelLocation.areas.observe(
                                        this@EditUserLocationActivity,
                                        Observer { itemAreas ->
                                            itemAreasName.clear()
                                            //cleartext
                                            binding.dropdownArea.setText("")
                                            binding.dropdownKodepos.setText("")
                                            for (itAreas in itemAreas) {
                                                itemAreasName.add(
                                                    itAreas.name!!
                                                )
                                            }
                                            val adapterAreas =
                                                ArrayAdapter(
                                                    applicationContext,
                                                    R.layout.item_list_dropdown_text,
                                                    itemAreasName
                                                )
                                            binding.dropdownArea.setAdapter(
                                                adapterAreas
                                            )

                                        })
                                    break
                                }
                            }
                        }
                    })
            })

        viewModelLocation.areas.observe(
            this@EditUserLocationActivity,
            Observer { itemAreas ->
                binding.dropdownArea.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        for (i in itemAreas.indices) {
                            if (itemAreas[i].name == binding.dropdownArea.text.toString()) {
                                arrayIdAreaLocation.clear()
                                arrayIdAreaLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemAreas[i].id.toString(),
                                        itemAreas[i].name.toString()
                                    )
                                )
                                Log.v("MerchantLocation", "Id Areas : $arrayIdAreaLocation")
                                binding.dropdownKodepos.setText(itemAreas[i].postcode)
                                break
                            }
                        }
                    }

                })
            })
    }


    private fun requestEditLocation(
        auth: String,
        id: String,
        jsonObject: JsonObject,
        onSuccess: (DataUserLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        try {
            ApiService().service.putUserLocation(auth, id, jsonObject).enqueue(
                object : Callback<DataUserLocation> {
                    override fun onResponse(
                        call: Call<DataUserLocation>,
                        response: Response<DataUserLocation>
                    ) {
                        Log.i("User Current Location", response.body().toString())
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message()).toString()
                    }

                    override fun onFailure(call: Call<DataUserLocation>, t: Throwable) =
                        onFailed(t.message.toString())
                }
            )
        } catch (e: Exception) {
            Log.i("User Current Location", "${e.message} --- ${e.printStackTrace()}")
        }
    }

    private fun requestEditCurrentLocation(
        auth: String,
        id: String,
        onSuccess: (DataCurrentUserLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        try {
            ApiService().service.putCurrentUserLocation(auth, id).enqueue(
                object : Callback<DataCurrentUserLocation> {
                    override fun onResponse(
                        call: Call<DataCurrentUserLocation>,
                        response: Response<DataCurrentUserLocation>
                    ) {
                        Log.i("User Current Location", response.body().toString())
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message()).toString()
                    }

                    override fun onFailure(call: Call<DataCurrentUserLocation>, t: Throwable) =
                        onFailed(t.message.toString())
                }
            )
        } catch (e: Exception) {
            Log.i("User Current Location", "${e.message} --- ${e.printStackTrace()}")
        }
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            with(this) {
                title = "Ubah Lokasi"
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }
    }

}