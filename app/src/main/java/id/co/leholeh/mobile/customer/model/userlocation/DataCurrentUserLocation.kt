package id.co.leholeh.mobile.customer.model.userlocation

data class DataCurrentUserLocation(
    val code: Int? = null,
    val data: CurrentUserLocation? = null,
    val error: List<Any> = listOf(),
    val message: String? = null
)