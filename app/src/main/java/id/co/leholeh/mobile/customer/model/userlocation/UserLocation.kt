package id.co.leholeh.mobile.customer.model.userlocation

import com.google.gson.annotations.SerializedName

data class UserLocation(
    @field:SerializedName("address")
    val address: String = "",

    @field:SerializedName("area")
    val area: String = "",

    @field:SerializedName("area_id")
    val area_id: String = "",

    @field:SerializedName("city")
    val city: String = "",

    @field:SerializedName("city_id")
    val city_id: String = "",

    @field:SerializedName("id")
    val id: String = "",

    @field:SerializedName("id_m_users")
    val id_m_users: String = "",

    @field:SerializedName("is_active")
    val is_active: String = "",

    @field:SerializedName("postcode")
    val postcode: String = "",

    @field:SerializedName("province")
    val province: String = "",

    @field:SerializedName("province_id")
    val province_id: String = "",

    @field:SerializedName("suburb")
    val suburb: String = "",

    @field:SerializedName("suburb_id")
    val suburb_id: String = ""
)