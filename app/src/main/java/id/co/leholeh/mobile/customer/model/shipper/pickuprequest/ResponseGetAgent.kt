package id.co.leholeh.mobile.customer.model.shipper.pickuprequest

import com.google.gson.annotations.SerializedName

data class ResponseGetAgent(

	@field:SerializedName("data")
	val data: DataGetAgent? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class Agent(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class DataItemGetAgent(

	@field:SerializedName("agent")
	val agent: Agent? = null,

	@field:SerializedName("city")
	val city: City? = null,

	@field:SerializedName("contact")
	val contact: Contact? = null,

	@field:SerializedName("location")
	val location: Location? = null
)

data class City(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Contact(

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null
)

data class DataGetAgent(

	@field:SerializedName("data")
	val data: List<DataItemGetAgent>? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Location(

	@field:SerializedName("latitude")
	val latitude: Double? = null,

	@field:SerializedName("longitude")
	val longitude: Double? = null
)
