package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName

data class ResponseProductByMerchant(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<ProductsByMerchant>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class LocationsItemFilter(

	@field:SerializedName("id_m_locations")
	val idMLocations: String? = null,

	@field:SerializedName("id_u_product_location")
	val idUProductLocation: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)

data class CategoriesItemFilter(

	@field:SerializedName("id_u_product_categories")
	val idUProductCategories: String? = null,

	@field:SerializedName("id_m_categories")
	val idMCategories: String? = null,

	@field:SerializedName("category")
	val category: String? = null
)

data class ProductsByMerchant(

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("id_merchant")
	val idMerchant: String? = null,

	@field:SerializedName("length")
	val length: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("weight")
	val weight: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("width")
	val width: String? = null,

	@field:SerializedName("locations")
	val locations: List<LocationsItemFilter>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItemFilter>? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("height")
	val height: String? = null
)
