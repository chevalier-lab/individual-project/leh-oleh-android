package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable


data class ByLocation(
    @field:SerializedName("id_m_locations")
    val idMLocations: String? = null,

    @field:SerializedName("lon")
    val lon: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("lat")
    val lat: String? = null,

    @field:SerializedName("province_name")
    val provinceName: String? = null,

    @field:SerializedName("products")
    val products: List<ByLocationProduct>? = listOf()

) : Serializable