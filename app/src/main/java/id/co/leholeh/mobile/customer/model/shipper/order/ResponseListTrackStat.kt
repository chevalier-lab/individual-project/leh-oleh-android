package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseListTrackStat(

	@field:SerializedName("data")
	val data: DataTrackStatus? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RowsItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class DataTrackStatus(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItem?>? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
