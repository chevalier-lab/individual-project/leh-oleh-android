package id.co.leholeh.mobile.customer.constant

object ConstantProfile {
    const val BASE_URL_PROFILE_USER = "guest/profile"
    const val INTENT_DATA_PROFILE = "data_profile"
    const val INTENT_DATA_EDIT = "edit"
    const val INTENT_EDIT_CONDITION = "edit_condition"
    const val BASE_URL_CHANGE_PHOTO_PROFILE = "guest/profile/picture"
}