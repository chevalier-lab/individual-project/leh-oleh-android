package id.co.leholeh.mobile.customer.ui.checkout

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.userlocation.CurrentUserLocation
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class CheckoutViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val apiServiceGuestLocation = ApiService().service

    // REQUEST CURRENT USER LOCATION
    private val _currentLocation = MutableLiveData<CurrentUserLocation>()
    val currentLocation: LiveData<CurrentUserLocation>
        get() = _currentLocation

    private suspend fun requestCurrentUserLocation(auth: String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _currentLocation.postValue(apiServiceGuestLocation.getCurrentUserLocation(auth).data)

            Log.i(
                CURRENT_USER_LOCATION_TAG,
                "${apiServiceGuestLocation.getCurrentUserLocation(auth).data}"
            )
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.i(
                CURRENT_USER_LOCATION_TAG,
                "${apiServiceGuestLocation.getUserLocation(auth).data}"
            )
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getCurrentUserLocation(auth: String) =
        viewModelScope.launch { requestCurrentUserLocation(auth) }

    companion object {
        private const val CURRENT_USER_LOCATION_TAG = "Current User Location"
    }
}