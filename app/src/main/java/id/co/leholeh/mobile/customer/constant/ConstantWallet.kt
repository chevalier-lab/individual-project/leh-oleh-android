package id.co.leholeh.mobile.customer.constant

import com.google.gson.Gson
import id.co.leholeh.mobile.utils.CacheUtil

object ConstantWallet {
    //walltet log
    const val BASE_URL_GET_HISTORY_SALDO = "guest/wallet/logs"


    const val BASE_URL_GET_BALANCE = "guest/wallet/balance"
    const val BASE_URL_HISTORY_WITHDRAW = "guest/wallet/withdraw/history"
    const val BASE_URL_PERMINTAAN_WITHDRAW = "guest/wallet/withdraw"
    const val BASE_URL_DETAIL_WITHDRAW = "guest/wallet/withdraw/{id}"
    const val BASE_URL_REQUEST_WITHDRAW = "guest/wallet/withdraw/request"

    const val BASE_URL_LIST_HISTORY_TOPUP = "guest/wallet/top-up/history"
    const val BASE_URL_LIST_TAGIHAN_TOPUP = "guest/wallet/top-up/bills"
    const val BASE_URL_BANK = "api/admin/master/Banks/getBanks"
    const val BASE_URL_DETAIL_BANK = "master/bank/account//{id}"
    const val BASE_URL_STEP_BANK = "master/ubank/{id}"
    const val BASE_URL_REQUEST_TOPUP = "guest/wallet/top-up"
    const val BASE_URL_EDIT_PROOF = "guest/wallet/top-up/proof"
    const val BASE_URL_REQ_PROOF = "master/medias"
    const val BASE_URL_UPDATE_TOKEN = "guest/wallet/top-up/{id}/payment-token"

    const val ID_WALLET = "id_wallet"
    const val BALANCE_WALLET = "data_balance_wallet"

    fun getBalance(cacheUtil: CacheUtil): String =  Gson().fromJson(cacheUtil.get(BALANCE_WALLET), String::class.java)
    fun getIdWallet(cacheUtil: CacheUtil): String = Gson().fromJson(cacheUtil.get(ID_WALLET)!!, String::class.java)
}