package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.*
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiService {
    private var retrofit: Retrofit? = null
    private val okHttpBuilder = OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(120, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)
        .build()

    private val okHttpBuilderShipper = OkHttpClient.Builder()
        .addInterceptor(Interceptor { chain ->
            val request: Request =
                chain.request().newBuilder().addHeader("User-Agent", "/Shipper").build()
            chain.proceed(request)
        })
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(120, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)
        .build()

    private val okHttpBuilderMidtrans = OkHttpClient.Builder()
        .addInterceptor(BasicAuthInterceptor(API_KEY_MIDTRANS,""))
        .connectTimeout(120, TimeUnit.SECONDS)
        .readTimeout(120, TimeUnit.SECONDS)
        .writeTimeout(120, TimeUnit.SECONDS)
        .build()

    fun <S> createService(serviceClass: Class<S>?): S {
        if (retrofit == null) {
            retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(okHttpBuilder)
                .build()
        }
        return retrofit!!.create(serviceClass!!)
    }

    fun <S> createServiceShipper(serviceClass: Class<S>): S {
        if (retrofit == null) {
            retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL_SHIPPER)
                .client(okHttpBuilderShipper)
                .build()
        }
        return retrofit!!.create(serviceClass)
    }

    fun <S> createServiceMidtrans(serviceClass: Class<S>): S {
        if (retrofit == null) {
            retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL_MIDTRANS)
                .client(okHttpBuilderShipper)
                .build()
        }
        return retrofit!!.create(serviceClass)
    }

    fun <S> createServiceMidtransTransaction(serviceClass: Class<S>): S {
        if (retrofit == null) {
            retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL_MIDTRANS_TRANSACTION)
                .client(okHttpBuilderMidtrans)
                .build()
        }
        return retrofit!!.create(serviceClass)
    }

    val service: GuestService by lazy {
        createService(GuestService::class.java)
    }

    val serviceGlobal: GlobalService by lazy {
        createService(GlobalService::class.java)
    }

    val serviceAuth: AuthService by lazy {
        createService(AuthService::class.java)
    }

    val serviceGuestMerchants: GuestMerchantsService by lazy {
        createService(GuestMerchantsService::class.java)
    }

    val serviceProfileUser: ProfileService by lazy {
        createService(ProfileService::class.java)
    }

    val serviceOrder: OrdersService by lazy {
        createService(OrdersService::class.java)
    }

    val serviceCart: CartService by lazy {
        createService(CartService::class.java)
    }

    val serviceBank: BanksService by lazy {
        createService(BanksService::class.java)
    }
}
class BasicAuthInterceptor(username:String , password:String):Interceptor{
    private var crendentials:String = Credentials.basic(username,password)
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder().header("Authorization",crendentials).build()
        return chain.proceed(request)
    }

}
