package id.co.leholeh.mobile.customer.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMenuRegisterUserBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity

class MenuRegisterUserActivity : AppCompatActivity() {
    lateinit var binding: ActivityMenuRegisterUserBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuRegisterUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //setuptoolbar
        setupToolbar()

        //register with email
        binding.tvLogin.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        //Login With Email
        binding.llEmail.setOnClickListener {
            startActivity(Intent(this, RegisterUserActivity::class.java))
            finish()
        }

        //Login With Google
        binding.llGoogle.setOnClickListener {
            Toast.makeText(
                this,
                getString(R.string.fitur_belum),
                Toast.LENGTH_SHORT
            ).show()
        }

        //Login With Facebook
        binding.llFacebook.setOnClickListener {
            Toast.makeText(
                this,
                getString(R.string.fitur_belum),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Register"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}