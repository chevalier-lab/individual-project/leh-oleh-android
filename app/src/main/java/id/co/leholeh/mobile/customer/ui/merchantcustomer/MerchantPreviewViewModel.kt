package id.co.leholeh.mobile.customer.ui.merchantcustomer

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.model.merchant.DetailMerchant
import id.co.leholeh.mobile.customer.model.merchant.ProductsByMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MerchantPreviewViewModel : ViewModel(){
    private val _previewMerchant = MutableLiveData<DetailMerchant>()
    val previewMerchant: LiveData<DetailMerchant>
        get() = _previewMerchant

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    //LIST MERCHANT
    private lateinit var listMerchantsProduct: LiveData<PagedList<ProductsByMerchant>>
    lateinit var statusMerchantProduct: LiveData<ApiStatus>
    private val _listMerchantDataSource = MutableLiveData<MerchantPreviewDataSource>()

    //LIST MERCHANT FILTER PRODUCT
    private lateinit var listMerchantsFilterProduct: LiveData<PagedList<ProductsByMerchant>>
    lateinit var statusMerchantFilterProduct: LiveData<ApiStatus>
    private val _listMerchantFilterDataSource = MutableLiveData<MerchantFilterPreviewDataSource>()

    //CONFIG
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .build()

    private val apiService = ApiService().serviceGuestMerchants
    private suspend fun getDetailMerchant(id: Int) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _previewMerchant.postValue(apiService.getPreviewMerchant(id).data)
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("FAI_REQ_DETAIL_MRCHNT", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataDetailMerchant(id: Int) {
        viewModelScope.launch {
            getDetailMerchant(id)
        }
    }

    //LIST PRODUCT MERCHANT
    fun setUpListMerchantProduct(idMerchant: Int){
        statusMerchantProduct = Transformations.switchMap<MerchantPreviewDataSource,ApiStatus>(_listMerchantDataSource,
            MerchantPreviewDataSource::apiState)
        listMerchantsProduct = initializedMerchantProductPagedListBuilder(config,idMerchant).build()
    }

    fun getDataListMerchantProduct() : LiveData<PagedList<ProductsByMerchant>> = listMerchantsProduct

    private fun initializedMerchantProductPagedListBuilder(config: PagedList.Config,idMerchant:Int) : LivePagedListBuilder<Int, ProductsByMerchant> {
        val dataSourceFactory = object : DataSource.Factory<Int, ProductsByMerchant>(){
            override fun create(): DataSource<Int, ProductsByMerchant> {
                val merchantProductDataSource = MerchantPreviewDataSource(Dispatchers.IO,idMerchant)
                _listMerchantDataSource.postValue(merchantProductDataSource)
                return merchantProductDataSource
            }
        }
        return LivePagedListBuilder<Int, ProductsByMerchant>(dataSourceFactory,config)
    }
    //END OF LIST PRODUCT MERCHANT


    //LIST PRODUCT MERCHANT
    fun setUpListMerchantFilterProduct(idMerchant: Int,search:String,orderDirection:String, jsonObject: JsonObject){
        statusMerchantFilterProduct = Transformations.switchMap<MerchantFilterPreviewDataSource,ApiStatus>(_listMerchantFilterDataSource,
            MerchantFilterPreviewDataSource::apiState)
        listMerchantsFilterProduct = initializedMerchantFilterProductPagedListBuilder(config,idMerchant,search,orderDirection,jsonObject).build()
    }

    fun getDataListMerchantFilterProduct() : LiveData<PagedList<ProductsByMerchant>> = listMerchantsFilterProduct

    private fun initializedMerchantFilterProductPagedListBuilder(config: PagedList.Config,idMerchant:Int,search:String,orderDirection:String, jsonObject: JsonObject) : LivePagedListBuilder<Int, ProductsByMerchant> {
        val dataSourceFactory = object : DataSource.Factory<Int, ProductsByMerchant>(){
            override fun create(): DataSource<Int, ProductsByMerchant> {
                val merchantProductDataSource = MerchantFilterPreviewDataSource(idMerchant,search,orderDirection,jsonObject)
                _listMerchantFilterDataSource.postValue(merchantProductDataSource)
                return merchantProductDataSource
            }
        }
        return LivePagedListBuilder<Int, ProductsByMerchant>(dataSourceFactory,config)
    }
    //END OF LIST PRODUCT MERCHANT



}