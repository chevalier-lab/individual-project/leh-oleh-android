package id.co.leholeh.mobile.customer.model.location

import com.google.gson.annotations.SerializedName

data class DataLocation(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<LocationProduct>? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
)