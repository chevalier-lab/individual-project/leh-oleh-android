package id.co.leholeh.mobile.customer.ui.bantuan

import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Slide
import androidx.transition.TransitionManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityBantuanBinding
import id.co.leholeh.mobile.customer.model.faq.DataFaq
import id.co.leholeh.mobile.customer.util.AdapterUtil
import kotlinx.android.synthetic.main.item_list_faq.view.*


class BantuanActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanBinding
    private lateinit var adapter: AdapterUtil<DataFaq>
    private val viewModel: BantuanViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(BantuanViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBantuanBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = "FAQ"
        getListFaq()
    }

    private fun getListFaq() {
        viewModel.getListFaq()
        viewModel.listFaq.observe(this, Observer {
            adapter.data = it
        })
        adapter = AdapterUtil(R.layout.item_list_faq, arrayListOf(), { _, view, item ->
            view.textViewQuestion.text = item.question
            view.textViewAnswer.text = item.answer
            view.ivShrinkArrow.setOnClickListener {
                if (view.textViewAnswer.isGone) {
                    toggleAnimation(true, view.ivShrinkArrow, view.textViewAnswer, view.linearAnswer)
                } else {
                    toggleAnimation(false, view.ivShrinkArrow,view.textViewAnswer, view.linearAnswer)
                }
            }
            view.textViewQuestion.setOnClickListener {
                if (view.textViewAnswer.isGone) {
                    toggleAnimation(true, view.ivShrinkArrow, view.textViewAnswer, view.linearAnswer)
                } else {
                    toggleAnimation(false, view.ivShrinkArrow,view.textViewAnswer, view.linearAnswer)
                }
            }
        }, { _, _ ->
        })
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerFaq.layoutManager = layoutManager
        binding.recyclerFaq.adapter = adapter
    }

    private fun toggleAnimation(visible : Boolean, imageView : View, view : View, viewGroup : ViewGroup){
        val transition = Slide(Gravity.TOP)
        transition.duration = 300
        transition.addTarget(view)
        TransitionManager.beginDelayedTransition(viewGroup, transition)
        if(visible){
            view.visibility = View.VISIBLE
            val rotate = RotateAnimation(
                0f,
                90f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            rotate.duration = 300
            rotate.interpolator = LinearInterpolator()
            rotate.fillAfter = true
            imageView.startAnimation(rotate)
        }else{
            view.visibility = View.GONE
            val rotate = RotateAnimation(
                90f,
                0f,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f
            )
            rotate.duration = 300
            rotate.interpolator = LinearInterpolator()
            rotate.fillAfter = true
            imageView.startAnimation(rotate)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return true
    }
}