package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class TransactionBanner(

	@field:SerializedName("total_payment_verified")
	val totalPaymentVerified: Int? = null,

	@field:SerializedName("total_process")
	val totalProcess: Int? = null,

	@field:SerializedName("total_refund")
	val totalRefund: Int? = null,

	@field:SerializedName("total_finish")
	val totalFinish: Int? = null
)