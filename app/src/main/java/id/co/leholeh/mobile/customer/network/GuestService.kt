package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.*
import id.co.leholeh.mobile.customer.model.banner.ResponseBanner
import id.co.leholeh.mobile.customer.model.product.DataDetailProduct
import id.co.leholeh.mobile.customer.model.product.DataFilterProduct
import id.co.leholeh.mobile.customer.model.product.DataProduct
import id.co.leholeh.mobile.customer.model.product.DataProductbyLocation
import id.co.leholeh.mobile.customer.model.userlocation.DataCurrentUserLocation
import id.co.leholeh.mobile.customer.model.userlocation.DataUserLocation
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface GuestService {
    @GET(BASE_URL_GUEST_PRODUCT)
    suspend fun getAllProduct(): DataProduct

    @GET(BASE_URL_BANNER)
    suspend fun getBanner(): ResponseBanner

    @GET(BASE_URL_GUEST_NEW_PRODUCT)
    suspend fun getNewProduct(
        @Query("page") page: Int?,
        @Query("order-by") orderBy: String?,
        @Query("order-direction") orderDirection: String?
    ): Response<DataProduct>

    @GET(BASE_URL_GUEST_PRODUCT_BYLOCATION)
    suspend fun getLokasiProduct(@Query("limit") limit: String?): DataProductbyLocation


    @GET(BASE_URL_GUEST_PRODUCT_DETAIL)
    suspend fun getDetailProduct(@Path("id") id: String): DataDetailProduct

    @POST(BASE_URL_GUEST_PRODUCT_FILTER)
    fun getFilterProduct(
        @Query("page") page: Int?,
        @Query("order-by") orderBy: String?,
        @Query("order-direction") orderDirection: String?,
        @Query("search") search: String?,
        @Body jsonObject: JsonObject
    ): Call<DataFilterProduct>

    @GET(BASE_URL_GUEST_USER_LOCATION)
    suspend fun getUserLocation(
        @Header("Authorization") authorization: String
    ): DataUserLocation

    @PUT(BASE_URL_GUEST_UPDATE_USER_LOCATION)
    fun putUserLocation(
        @Header("Authorization") authorization: String,
        @Path("id") id: String,
        @Body jsonObject: JsonObject
    ): Call<DataUserLocation>

    @PUT(BASE_URL_GUEST_UPDATE_USER_CURRENT_LOCATION)
    fun putCurrentUserLocation(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<DataCurrentUserLocation>

    @GET(BASE_URL_GUEST_USER_CURRENT_LOCATION)
    suspend fun getCurrentUserLocation(
        @Header("Authorization") authorization: String
    ): DataCurrentUserLocation

    @POST(BASE_URL_GUEST_USER_LOCATION)
    fun postUserLocation(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<DataUserLocation>

    @DELETE(BASE_URL_GUEST_UPDATE_USER_LOCATION)
    fun deleteUserLocation(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<DataUserLocation>
}