package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseDomesticOrder(

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("data")
	val dataDomesticOrder: DataDomesticOrder? = null

)

data class DataDomesticOrder(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
