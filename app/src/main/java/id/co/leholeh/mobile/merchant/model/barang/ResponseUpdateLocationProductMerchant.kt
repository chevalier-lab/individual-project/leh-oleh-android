package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ResponseUpdateLocationProductMerchant(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: UpdateLocation? = null,

    @field:SerializedName("error")
    val error: List<Any?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)

data  class UpdateLocation (
    val any:Any?=null
)

