package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseOrderActivations(

	@field:SerializedName("data")
	val dataOrderActivations: DataOrderActivations? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataOrderActivations(

	@field:SerializedName("message")
	val title: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
