package id.co.leholeh.mobile.customer.model.profile

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Profile(
    @field:SerializedName("jk")
    val jk: String? = null,

    @field:SerializedName("api_token")
    val apiToken: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("id_photo")
    val idPhoto: String? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("tgl_lahir")
    val tglLahir: String? = null,

    @field:SerializedName("alamat")
    val alamat: String? = null,

    @field:SerializedName("password")
    val password: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("type_before_banned")
    val typeBeforeBanned: Any? = null,

    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("username")
    val username: String? = null
) : Serializable