package id.co.leholeh.mobile.customer.ui.pesanan

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.mobile.customer.constant.PESANAN_DATA
import id.co.leholeh.databinding.FragmentPesananCustomerBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.orders.Order
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.*
import kotlinx.android.synthetic.main.item_pesanan.view.*

class PesananCustomerFragment : Fragment() {

    private lateinit var adapterPesanan: PagedAdapterUtil<Order>
    private var _binding: FragmentPesananCustomerBinding? = null
    private val binding get() = _binding!!
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login

    private val viewModel: PesananCustomerViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(PesananCustomerViewModel::class.java)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        _binding = FragmentPesananCustomerBinding.inflate(inflater, container, false)
        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        //Toolbar
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbarPesanan)
        binding.toolbarPesanan.title = getString(R.string.pesanansaya)
        binding.toolbarPesanan.inflateMenu(R.menu.search_cart_menu)
        binding.toolbarPesanan.inflateMenu(R.menu.cart_time_menu)
        binding.toolbarPesanan.setOnMenuItemClickListener {
            onOptionsItemSelected(it)
        }

        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            auth = getAuth(cacheUtil)
            initUI()
        } else {
            binding.emptyView.tvEmptyView.text = "Anda Belum Login!"
            binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_red_pesanan)
            binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
            binding.recyclerPesanan.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
        }
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun observerListPesanan() {
        viewModel.getDataListPesanan().observe(viewLifecycleOwner, Observer {
            adapterPesanan.submitList(it)
        })
        viewModel.statusListPesanan.observe(viewLifecycleOwner, Observer {
            stateUtilWithEmptyView(
                requireContext(),
                it,
                binding.progressBar,
                binding.recyclerPesanan,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Data Pesanan Kosong",
                R.drawable.ic_red_pesanan
            )
        })
    }


    @SuppressLint("SetTextI18n")
    private fun initUI() {

        viewModel.setUpListPesanan(auth.token!!)
        observerListPesanan()
        adapterPesanan = PagedAdapterUtil(
            R.layout.item_pesanan,
            { _, itemView, item ->
                when (item.status) {
                    "0" -> {
                        itemView.textViewStatusTransaksi.text = "BELUM DIBAYAR"
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_not_payment)
                    }
                    "1" -> {
                        itemView.textViewStatusTransaksi.text = "TERVERIFIKASI"
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_payment_verified)
                    }
                    "2" -> {
                        itemView.textViewStatusTransaksi.text = "DIPROSES"
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_diproses)
                    }
                    "3" -> {
                        itemView.textViewStatusTransaksi.text = "SELESAI"
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_selesai)
                    }
                    "4" -> {
                        itemView.textViewStatusTransaksi.text = "REFUND"
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_refund)
                    }
                }
                itemView.textViewIdTransaksi.text = item.transactionToken
                itemView.textViewHargaTransaksi.text =
                    changeToRupiah(item.totalPrice!!.toDouble())

                val tanggal = item.updatedAt!!.substring(0, 10)
                val jam = item.updatedAt.substring(10)
                itemView.textViewTglTransaksi.text = tanggal
                itemView.textViewJamTransaksi.text = jam
            },
            { _, item ->
                //intent
                val intent = Intent(
                    requireContext(),
                    DetailPesananActivity::class.java
                )
                intent.putExtra(
                    PESANAN_DATA,
                    item.id
                )

                startActivity(intent)
            })
        binding.recyclerPesanan.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)

        binding.recyclerPesanan.adapter = adapterPesanan

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.cart_menu, menu)
        inflater.inflate(R.menu.cart_time_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_cart -> startActivity(Intent(requireContext(), KeranjangActivity::class.java))
            R.id.menu_time -> startActivity(
                Intent(
                    requireContext(),
                    RiwayatPesananCustomerActivity::class.java
                )
            )
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            viewModel.setUpListPesanan(auth.token!!)
            observerListPesanan()
        }
    }
}