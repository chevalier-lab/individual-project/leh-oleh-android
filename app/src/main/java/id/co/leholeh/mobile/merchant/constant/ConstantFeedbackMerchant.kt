package id.co.leholeh.mobile.merchant.constant

object ConstantFeedbackMerchant {
    const val BASE_URL_REVIEW = "merchant/manage/transaction/review"
    const val BASE_URL_COMPLAIN = "merchant/manage/transaction/complain"
    const val BASE_URL_DETAIL_REVIEW = "merchant/manage/transaction/review/{id}"
}