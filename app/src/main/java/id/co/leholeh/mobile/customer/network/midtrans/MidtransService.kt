package id.co.leholeh.mobile.customer.network.midtrans

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.midtrans.ConstantMidtrans
import id.co.leholeh.mobile.customer.model.midtransbody.ResponseMidtransTranscation
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface MidtransService {

    @POST(ConstantMidtrans.BASE_URL_MIDTRANS_TRANSACTION)
//    @Headers(
//        "Accept: application/json",
//        "Content-Type: application/json")
    fun requestTransaction(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<ResponseMidtransTranscation>



}