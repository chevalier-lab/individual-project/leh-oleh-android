package id.co.leholeh.mobile.customer.model.midtransbody

import com.google.gson.annotations.SerializedName

data class ResponsePaymentToken(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataPaymentToken? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataPaymentToken(

	@field:SerializedName("id_u_user_location")
	val idUUserLocation: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("token_payment")
	val tokenPayment: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
