package id.co.leholeh.mobile.merchant.model.setting

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProfileToko(

	@field:SerializedName("id_user_photo")
	val idUserPhoto: String? = null,

	@field:SerializedName("market_phone_number")
	val marketPhoneNumber: String? = null,

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("identity_label")
	val identityLabel: String? = null,

	@field:SerializedName("id_market_photo")
	val idMarketPhoto: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id_identity_photo")
	val idIdentityPhoto: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null,

	@field:SerializedName("market_label")
	val marketLabel: String? = null,

	@field:SerializedName("author_label")
	val authorLabel: String? = null,

	@field:SerializedName("author_uri")
	val authorUri: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("market_uri")
	val marketUri: String? = null,

	@field:SerializedName("id_u_user")
	val idUUser: String? = null,

	@field:SerializedName("no_identity")
	val noIdentity: String? = null,

	@field:SerializedName("market_address")
	val marketAddress: String? = null,

	@field:SerializedName("identity_uri")
	val identityUri: String? = null,

	@field:SerializedName("location")
	val location: LocationMerchant? = null,

	@field:SerializedName("id")
	val id: String? = null
) : Serializable

data class LocationMerchant(

	@field:SerializedName("id_u_user_is_merchant")
	val idUUserIsMerchant: String? = null,

	@field:SerializedName("suburb_id")
	val suburbId: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("province")
	val province: String? = null,

	@field:SerializedName("province_id")
	val provinceId: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("suburb")
	val suburb: String? = null,

	@field:SerializedName("area_id")
	val areaId: String? = null,

	@field:SerializedName("city_id")
	val cityId: String? = null
) : Serializable