package id.co.leholeh.mobile.customer.model.banner

import com.google.gson.annotations.SerializedName

data class ResponseBanner(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataBannerRekomendasi>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataBannerRekomendasi(

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null
)
