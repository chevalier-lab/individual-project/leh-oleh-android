package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantTambahParentActivtyBinding

class MerchantTambahParentActivty : AppCompatActivity() {
    lateinit var binding: ActivityMerchantTambahParentActivtyBinding
    private var currentFragPosition = 0
    var sliderCount = 0
    private lateinit var pagerAdapter: PagerAdapterTambahBarang

    private lateinit var fragmentMerchantTambahKategori: MerchantTambahKategoriFragment
    private lateinit var fragmentMerchantTambahBarang: MerchantTambahBarangFragment
    private lateinit var fragmentMerchantTambahLokasi: MerchantTambahLokasiFragment
    private lateinit var fragmentMerchantTambahFoto: MerchantTambahFotoFragment
    private lateinit var fragmentMerchantSubmitBarang: MerchantSubmitBarangFragment

    private var sliderIndicators = mutableListOf<ImageView>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantTambahParentActivtyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //Toolbar
        setToolbar()
        //Tabpager
        initPagerAdapter()
        setSliderIndicators()
        nextOrBackPageAction()
    }

    private fun initPagerAdapter() {
        fragmentMerchantTambahBarang = MerchantTambahBarangFragment()
        fragmentMerchantTambahKategori = MerchantTambahKategoriFragment()
        fragmentMerchantTambahLokasi = MerchantTambahLokasiFragment()
        fragmentMerchantTambahFoto = MerchantTambahFotoFragment()
        fragmentMerchantSubmitBarang = MerchantSubmitBarangFragment()

        val pages = listOf(
            fragmentMerchantTambahBarang, fragmentMerchantTambahKategori,
            fragmentMerchantTambahLokasi, fragmentMerchantTambahFoto, fragmentMerchantSubmitBarang
        )

        pagerAdapter = PagerAdapterTambahBarang(this, supportFragmentManager, pages)
        binding.viewPager.adapter = pagerAdapter
        binding.viewPager.addOnPageChangeListener(handlerIndicator)
        sliderCount = pages.size
    }

    private fun setSliderIndicators(){
        for (i in 0 until sliderCount) {
            sliderIndicators.add(ImageView(this))
            this.sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            if (this.sliderIndicators[i].parent != null) {
                (this.sliderIndicators[i].parent as ViewGroup).removeView(this.sliderIndicators[i])
            }
            binding.sliderIndicator.addView(this.sliderIndicators[i], params)
        }
        sliderIndicators[0].setImageResource(R.drawable.shape_indicator_active)
    }

    private val handlerIndicator = object : ViewPager.OnPageChangeListener {
         override fun onPageScrollStateChanged(state: Int) {}

         override fun onPageScrolled(
             position: Int,
             positionOffset: Float,
             positionOffsetPixels: Int
         ) {
         }

         override fun onPageSelected(position: Int) {
             for (i in 0 until sliderCount)
                 sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
             sliderIndicators[position].setImageResource(R.drawable.shape_indicator_active)
         }
     }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setToolbar() {
        supportActionBar?.apply {
            title = "Tambah Barang"
            setDisplayHomeAsUpEnabled(true)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun nextOrBackPageAction() {
        when (currentFragPosition) {
            0 -> {
                binding.buttonKembali.visibility = View.GONE
            }
        }
        binding.buttonLanjut.setOnClickListener {
            if (currentFragPosition <= 3) {
                if (currentFragPosition == 0) {
                    val isFormFilled = fragmentMerchantTambahBarang.validasiForm()
                    if (isFormFilled) {
                        currentFragPosition++
                        moveNextFragment(currentFragPosition)
                        binding.buttonKembali.visibility = View.VISIBLE
                        val informasiBarang = fragmentMerchantTambahBarang.getInformasiBarang()
                        fragmentMerchantTambahKategori.parsingInformasiBarang(informasiBarang)
                    }
                }
                else if (currentFragPosition == 1) {
                    val isCategoriNotNull = fragmentMerchantTambahKategori.isCategoryNotNull()
                    if (isCategoriNotNull) {
                        currentFragPosition++
                        moveNextFragment(currentFragPosition)
                        val dataBarang = fragmentMerchantTambahKategori.getDataBarang()
                        fragmentMerchantTambahLokasi.setDataBarang(dataBarang)
                    }
                }
                else if (currentFragPosition == 2) {
                    val isLocationNotNull = fragmentMerchantTambahLokasi.isLocationNotNull()
                    if (isLocationNotNull) {
                        currentFragPosition++
                        moveNextFragment(currentFragPosition)
                        val dataBarang = fragmentMerchantTambahLokasi.getDataBarang()
                        fragmentMerchantTambahFoto.setDataBarang(dataBarang)
                    }
                }
                else if (currentFragPosition == 3) {
                    val isPhotoNoNull = fragmentMerchantTambahFoto.isPhotoNotNull()
                    if (isPhotoNoNull) {
                        currentFragPosition++
                        moveNextFragment(currentFragPosition)
                        val dataBarang = fragmentMerchantTambahFoto.getDataBarang()
                        fragmentMerchantSubmitBarang.setDataBarangToForm(dataBarang)
                    }
                }
                if (currentFragPosition == 4) {
                    binding.buttonLanjut.text = "SUBMIT"
                    binding.buttonLanjut.setCompoundDrawablesWithIntrinsicBounds(
                        null,
                        null,
                        null,
                        null
                    )
                }
                Log.d("FRAG NEXT", "$currentFragPosition")
            } else {
                val dataBarang = fragmentMerchantTambahFoto.getDataBarang()
                fragmentMerchantSubmitBarang.submitDataBarang(dataBarang)
            }
        }
        binding.buttonKembali.setOnClickListener {
            currentFragPosition--
            if (currentFragPosition > 0) {
                if (currentFragPosition == 0) {
                    binding.buttonKembali.visibility = View.GONE
                }
               /* else if (currentFragPosition == 1) {

                    moveNextFragment(currentFragPosition)
                    val dataBarang = fragmentMerchantTambahKategori.getDataBarang()
                    fragmentMerchantTambahLokasi.setDataBarang(dataBarang)
                }
                else if (currentFragPosition == 2) {
                    currentFragPosition--
                    moveNextFragment(currentFragPosition)
                    val dataBarang = fragmentMerchantTambahLokasi.getDataBarang()
                    fragmentMerchantTambahFoto.setDataBarang(dataBarang)
                }
                else if (currentFragPosition == 3) {
                    currentFragPosition--
                    moveNextFragment(currentFragPosition)
                    val dataBarang = fragmentMerchantTambahFoto.getDataBarang()
                    fragmentMerchantSubmitBarang.setDataBarangToForm(dataBarang)
                }else if(currentFragPosition == 4){
                    currentFragPosition--
                    moveNextFragment(currentFragPosition)
                }*/
                binding.buttonLanjut.text = "Lanjut"
                binding.buttonLanjut.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_next,
                    0
                )
                moveNextFragment(currentFragPosition)
                Log.d("FRAG BACK", "$currentFragPosition")
            }
        }
    }

    private fun moveNextFragment(position: Int) {
        binding.viewPager.setCurrentItem(position, true)
    }
}