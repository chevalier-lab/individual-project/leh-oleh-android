package id.co.leholeh.mobile.customer.constant.shipper

object ConstantLocation {
    const val BASE_URL_LOCATION_PROVINCE= "public/v1/provinces"
    const val BASE_URL_LOCATION_CITIES= "public/v1/cities"
    const val BASE_URL_LOCATION_SUBURBS = "public/v1/suburbs"
    const val BASE_URL_LOCATION_AREAS = "public/v1/areas"
    const val BASE_URL_LOCATION_SEARCH = "public/v1/details/{SUBSTRING}"

}