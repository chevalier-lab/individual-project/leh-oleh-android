package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.orders.midtrans.ResponseGetStatusTransaction
import id.co.leholeh.mobile.customer.model.wallet.StepBanks
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.TopupService
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.TagihanTopup
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TopupViewModel : ViewModel() {

    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .build()

    private val serviceTopup : TopupService by lazy {
        ApiService().createService(TopupService::class.java)
    }

    private val serviceMidtrans : TopupService by lazy {
        ApiService().createServiceMidtrans(TopupService::class.java)
    }
    private val _stepBank = MutableLiveData<List<StepBanks>>()
    val stepBank : LiveData<List<StepBanks>>
        get() = _stepBank

    private val _transactionStatus = MutableLiveData<ResponseGetStatusTransaction>()
    val transactionStatus: LiveData<ResponseGetStatusTransaction>
        get() = _transactionStatus

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private lateinit var listHistoryTopup: LiveData<PagedList<HistoryTopup>>
    private lateinit var listTagihanTopup: LiveData<PagedList<TagihanTopup>>
    lateinit var statusListTopup: LiveData<ApiStatus>
    lateinit var statusListHistoryTopup: LiveData<ApiStatus>
    private val _tagihanTopupDataSource = MutableLiveData<TopupDataSource>()
    private val _historyTopupDataSource = MutableLiveData<HistoryTopupDataSource>()

    //TOPUP
    fun setUpListTagihanTopup(authorization: String) {
        statusListTopup = Transformations.switchMap<TopupDataSource, ApiStatus>(
            _tagihanTopupDataSource,
            TopupDataSource::apiState
        )
        listTagihanTopup = initializedTopupPagedListBuilder(authorization, config).build()
    }

    fun getDataListTagihanTop() : LiveData<PagedList<TagihanTopup>> = listTagihanTopup

    private fun initializedTopupPagedListBuilder(authorization: String, config: PagedList.Config):
            LivePagedListBuilder<Int, TagihanTopup> {

        val dataSourceFactory = object : DataSource.Factory<Int, TagihanTopup>() {
            override fun create(): DataSource<Int, TagihanTopup> {
                val topupDataSource = TopupDataSource(Dispatchers.IO, authorization)
                _tagihanTopupDataSource.postValue(topupDataSource)
                return topupDataSource
            }
        }
        return LivePagedListBuilder<Int, TagihanTopup>(dataSourceFactory, config)
    }
    //END OF TOPUP

    //HISTORY TOPUP
    fun setUpListHistoryTopup(authorization: String){
        statusListHistoryTopup = Transformations.switchMap<HistoryTopupDataSource, ApiStatus>(_historyTopupDataSource, HistoryTopupDataSource::apiState)
        listHistoryTopup  = initializedHistoryTopupPagedListBuilder(authorization,config).build()
    }

    fun getDataListHistoryTopUp() : LiveData<PagedList<HistoryTopup>> = listHistoryTopup

    private fun initializedHistoryTopupPagedListBuilder(authorization: String, config: PagedList.Config):
            LivePagedListBuilder<Int, HistoryTopup> {

        val dataSourceFactory = object : DataSource.Factory<Int, HistoryTopup>() {
            override fun create(): DataSource<Int, HistoryTopup> {
                val historyTopupDataSource = HistoryTopupDataSource(Dispatchers.IO, authorization)
                _historyTopupDataSource.postValue(historyTopupDataSource)
                return historyTopupDataSource
            }
        }
        return LivePagedListBuilder<Int, HistoryTopup>(dataSourceFactory, config)
    }
    fun listHistoryTopupIsEmpty() : Boolean{
        return listHistoryTopup.value?.isEmpty() ?: true
    }
    //END OF HISTORY TOPUP

    private suspend fun requestTransactionStatus(auth: String, token: String){
        try {
            _status.postValue(ApiStatus.LOADING)
            _transactionStatus.postValue(serviceMidtrans.getTransactionStatus(auth, token))
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("TRANSACTION_STATUS", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getTransactionStatus(auth: String, token: String){
        viewModelScope.launch {
            requestTransactionStatus(auth, token)
        }
    }

    //StepBank
    private suspend fun getStepBank(authorization: String,idBank:String){
        try {
            _stepBank.postValue(serviceTopup.getStepBanks(authorization,idBank).data) //page should be dynamic
        }catch (e : Exception){
            Log.d("ERR_REQ_HISTORY_WD", e.localizedMessage!!)
        }
    }

    fun getDataStepBanks(authorization: String,idBank:String){
        viewModelScope.launch {
            getStepBank(authorization,idBank)
        }
    }

}