package id.co.leholeh.mobile.customer.model.profile

import com.google.gson.annotations.SerializedName

data class ResponseProfile(
    @field:SerializedName("code")
	val code: Int? = null,

    @field:SerializedName("data")
	val data: Profile? = null,

    @field:SerializedName("error")
	val error: List<Any?>? = null,

    @field:SerializedName("message")
	val message: String? = null
)



