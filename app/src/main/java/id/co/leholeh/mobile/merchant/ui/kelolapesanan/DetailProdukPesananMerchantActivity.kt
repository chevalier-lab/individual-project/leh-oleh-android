package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.Window
import android.widget.Toast
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityDetailProdukPesananMerchantBinding
import id.co.leholeh.databinding.CustomDialogRejectProcessPesananBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.merchant.model.transaction.DetailProductsItem
import id.co.leholeh.mobile.merchant.model.transaction.ResponseUpdateTransaction
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.merchant.ui.feedback.MerchantKomplainActivity
import id.co.leholeh.mobile.merchant.ui.feedback.MerchantReviewActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailProdukPesananMerchantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailProdukPesananMerchantBinding
    private val rejectPesanan = "Reject Pesanan"
    private val reject = "reject"
    private val processPesanan = "Proses Pesanan"
    private val process = "proses"
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private lateinit var dataProduk : DetailProductsItem
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProdukPesananMerchantBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        setUpToolbar()
        //Auth
        auth= getAuth(cacheUtil)
        if(intent.getSerializableExtra(ConstantMerchant.DATA_PRODUK) != null){
            dataProduk = intent.getSerializableExtra(ConstantMerchant.DATA_PRODUK) as DetailProductsItem
        }
        checkStatusPesanan()
        initUI()

        binding.btnNegativeAction.setOnClickListener {
            if (dataProduk.status == ConstantMerchant.STATUS_PESANAN_FINISH || dataProduk.status == ConstantMerchant.STATUS_PESANAN_REFUND) {
                startActivity(Intent(this, MerchantKomplainActivity::class.java))
            } else {
                showActionDialog(rejectPesanan, getString(R.string.confirm_kelola_pesanan, reject))
            }
        }
        binding.btnPositiveAction.setOnClickListener {
            if (dataProduk.status == ConstantMerchant.STATUS_PESANAN_FINISH || dataProduk.status == ConstantMerchant.STATUS_PESANAN_REFUND) {
                startActivity(Intent(this, MerchantReviewActivity::class.java))
            } else {
                showActionDialog(
                    processPesanan,
                    getString(R.string.confirm_kelola_pesanan, process)
                )
            }
        }
    }

    private fun initUI(){
        binding.tvNamaProduk.text = dataProduk.productName
        binding.tvDetailDeksripsi.text = dataProduk.description
        binding.tvTotalPrice.text = StringBuilder("Total : ").append(changeToRupiah(dataProduk.subTotalPrice!!.toDouble()))
        binding.tvQty.text = StringBuilder("Qty : ").append(dataProduk.qty)

        val categoriesStr = StringBuilder()
        //Categories
        dataProduk.categories?.apply {
            for (index in this.indices) {
                if (index == this.size - 1) {
                    categoriesStr.append(this[index].category!!)
                } else {
                    categoriesStr.append(this[index].category!!).append(", ")
                }
            }
        }
        binding.tvBookmarks.text = categoriesStr.toString()
        //Location
        val locationStr = StringBuilder()
        dataProduk.location?.apply {
            for (index in this.indices) {
                if (index == this.size - 1) {
                    locationStr.append(this[index].provinceName!!)
                } else {
                    locationStr.append(this[index].provinceName!!).append(", ")
                }
            }
        }
        binding.tvLocation.text = locationStr

        Glide.with(this).load(dataProduk.uri).into(binding.imPhoto)
    }

    @ExperimentalStdlibApi
    private fun showActionDialog(title: String, desc: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogRejectProcessPesananBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        binding.tvDescDialogRejectProcess.text = desc
        if (title == rejectPesanan) {
            binding.tVTitleDialogRejectProcess.text = rejectPesanan
        } else {
            binding.tVTitleDialogRejectProcess.text = processPesanan
        }

        binding.btnnDialogActRejectProc.setOnClickListener {
            if (title==processPesanan) {
                sendRequestUpdateStatusProses(dialog)
            }else if (title==rejectPesanan){
                sendRequestUpdateStatusRefund(dialog)
            }
        }
        binding.buttonNegative.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    @SuppressLint("SetTextI18n")
    private fun checkStatusPesanan(): String {
        when (dataProduk.status) {
            ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED -> {
                binding.btnPositiveAction.text = getString(R.string.accept)
                binding.btnNegativeAction.text = getString(R.string.reject)
                return ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED
            }
            ConstantMerchant.STATUS_PESANAN_PROCESS -> {
                binding.btnPositiveAction.text = getString(R.string.accept)
                binding.btnNegativeAction.text = getString(R.string.reject)
                binding.btnNegativeAction.setBackgroundColor(resources.getColor(R.color.colorGrey))
                binding.btnPositiveAction.setBackgroundColor(resources.getColor(R.color.colorGreyCheckout))
                binding.btnNegativeAction.setTextColor(resources.getColor(R.color.colorBlack))
                binding.btnPositiveAction.setTextColor(resources.getColor(R.color.colorBlack))
                binding.btnPositiveAction.isEnabled = false
                binding.btnNegativeAction.isEnabled = false
                return ConstantMerchant.STATUS_PESANAN_PROCESS
            }
            ConstantMerchant.STATUS_PESANAN_FINISH -> {
                binding.btnPositiveAction.text = getString(R.string.review) + "(1)"
                binding.btnNegativeAction.text = getString(R.string.complain) + "(1)"
                return ConstantMerchant.STATUS_PESANAN_FINISH
            }
            ConstantMerchant.STATUS_PESANAN_REFUND -> {
                binding.btnPositiveAction.text = getString(R.string.review) + "(1)"
                binding.btnNegativeAction.text = getString(R.string.complain) + "(1)"
                return ConstantMerchant.STATUS_PESANAN_REFUND
            }
        }
        return ""
    }

    //Update Status Pesanan
    private fun sendRequestUpdateStatusProses(dialog: Dialog) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("status", "2")
        updatePesananProses(jsonObject, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    "Berhasil mengubah Status Proses",
                    Toast.LENGTH_SHORT
                ).show()
                dialog.dismiss()
                finish()
            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    //Update Status Pesanan
    private fun sendRequestUpdateStatusRefund(dialog: Dialog) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("status", "4")
        updatePesananProses(jsonObject, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    "Berhasil mengubah Status Refund",
                    Toast.LENGTH_SHORT
                ).show()
                dialog.dismiss()
                finish()
            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }


    private fun updatePesananProses(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateTransaction) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: TransactionService by lazy {
            ApiService().createService(TransactionService::class.java)
        }
        val call: Call<ResponseUpdateTransaction> =
            apiService.updateTransactionMerchant(auth.token!!, jsonObject,dataProduk.id!!)
        call.enqueue(object : Callback<ResponseUpdateTransaction> {
            override fun onResponse(
                call: Call<ResponseUpdateTransaction>,
                response: Response<ResponseUpdateTransaction>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }
            override fun onFailure(call: Call<ResponseUpdateTransaction>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return true
    }

    private fun setUpToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }
}