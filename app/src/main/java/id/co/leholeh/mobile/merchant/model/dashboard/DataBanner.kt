package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class DataBanner(

	@field:SerializedName("product")
	val product: ProductBanner? = null,

	@field:SerializedName("rating_point")
	val ratingPoint: RatingPointBanner? = null,

	@field:SerializedName("complain")
	val complain: ComplainBanner? = null,

	@field:SerializedName("transaction")
	val transaction: TransactionBanner? = null
)