package id.co.leholeh.mobile.customer.model.banks

import com.google.gson.annotations.SerializedName

data class BankAccount(

	@field:SerializedName("id_m_banks")
	val idMBanks: String? = null,

	@field:SerializedName("account_number")
	val accountNumber: String? = null,

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("account_name")
	val accountName: String? = null,

	@field:SerializedName("bank_name")
	val bankName: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)