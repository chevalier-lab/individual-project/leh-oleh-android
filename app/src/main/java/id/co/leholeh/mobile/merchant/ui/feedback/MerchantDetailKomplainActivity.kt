package id.co.leholeh.mobile.merchant.ui.feedback

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantDetailKomplainBinding
import id.co.leholeh.databinding.ComponentDialogBalasanBinding
import id.co.leholeh.databinding.ComponentDialogUbasStatusBinding
import id.co.leholeh.mobile.customer.util.AdapterUtil

class MerchantDetailKomplainActivity : AppCompatActivity() {
    lateinit var binding : ActivityMerchantDetailKomplainBinding
    lateinit var adapter : AdapterUtil<Dummy>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMerchantDetailKomplainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //setupToolbar
        setupToolbar()

        /*adapter =
            AdapterUtil(
                R.layout.item_list_feedback_complain,
                listOf(
                    Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        0
                    ),
                    Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        1
                    ),
                    Dummy(
                        getString(R.string.lorem_ipsum),
                        "30 Agustus 2020",
                        "14:06:1",
                        0
                    )
                ), { position, itemView, item ->
                    itemView.tv_chat.text = item.komen
                    itemView.tv_tanggal.text = item.tanggal
                    itemView.tv_jam.text = item.jam
                    val colorGreen = ContextCompat.getColor(this, R.color.colorGreenApp)
                    val colorGrey = ContextCompat.getColor(this, R.color.colorGreyChat)

                    if (item.user == 1) {
                        itemView.list_feedback.setBackgroundColor(colorGrey)

                    } else {
                        itemView.list_feedback.setBackgroundColor(colorGreen)
                        itemView.tv_tanggal.setTextColor(resources.getColor(R.color.colorWhite))
                        itemView.tv_chat.setTextColor(resources.getColor(R.color.colorWhite))
                        itemView.tv_jam.setTextColor(resources.getColor(R.color.colorWhite))
                    }

                }, { position, item ->

                })


        binding.rvHistory.adapter=adapter
        binding.rvHistory.layoutManager = LinearLayoutManager(this@MerchantDetailKomplainActivity)*/

        binding.tvStatus.setOnClickListener {
            //setupDialogStatus
            showDialogStatus()
        }
    }

    data class Dummy(
        val komen:String,
        val tanggal:String,
        val jam : String,
        val user : Int
    //if user 1 = customer
    //if user 0 = merchant
    )

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Detail Komplain"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)
        val menuItem:MenuItem = menu!!.findItem(R.id.menu_search)
        MenuItemCompat.getActionView(menuItem)
        menuItem.setIcon(R.drawable.ic_send)
        return super.onCreateOptionsMenu(menu)
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        R.id.menu_search->{
            showDialog()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }


    //show dialog
    private fun showDialog(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_balasan)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogBalasanBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    //show dialog
    private fun showDialogStatus(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_ubas_status)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogUbasStatusBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }
}