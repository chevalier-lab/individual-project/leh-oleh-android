package id.co.leholeh.mobile.customer.model.userlocation

import com.google.gson.annotations.SerializedName

data class CurrentUserLocation(
    @field:SerializedName("area")
    val area: String = "",
    @field:SerializedName("address")
    val address: String = "",
    @field:SerializedName("is_active")
    val isActive: String = "",
    @field:SerializedName("city")
    val city: String = "",
    @field:SerializedName("id_m_users")
    val idMUsers: String = "",
    @field:SerializedName("postcode")
    val postcode: String = "",
    @field:SerializedName("area_id")
    val areaId: String = "",
    @field:SerializedName("suburb_id")
    val suburbId: String = "",
    @field:SerializedName("province")
    val province: String = "",
    @field:SerializedName("province_id")
    val provinceId: String = "",
    @field:SerializedName("suburb")
    val suburb: String = "",
    @field:SerializedName("id")
    val id: String = "",
    @field:SerializedName("city_id")
    val cityId: String = ""
)