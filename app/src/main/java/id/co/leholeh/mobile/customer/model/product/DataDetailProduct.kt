package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DataDetailProduct(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val detailProduct: DetailProduct? = null,

    @field:SerializedName("error")
    val error: List<Any>? = null,

    @field:SerializedName("message")
    val message: String? = null
) : Serializable