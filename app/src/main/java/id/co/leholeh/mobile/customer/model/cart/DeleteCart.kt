package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class DeleteCart(

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("cart")
    val cart: List<DeleteCartItem?>? = null
)