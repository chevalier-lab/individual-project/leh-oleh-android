package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ProductItemlDetailMerchant(

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("id_cover")
	val idCover: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null
)