package id.co.leholeh.mobile.customer.model.shipper.location

import com.google.gson.annotations.SerializedName

data class ResponseProvince(

	@field:SerializedName("data")
	val data: DataProvince? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RowsItemProvince(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class DataProvince(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItemProvince>? = null,

	@field:SerializedName("content")
	val content: String? = null
)
