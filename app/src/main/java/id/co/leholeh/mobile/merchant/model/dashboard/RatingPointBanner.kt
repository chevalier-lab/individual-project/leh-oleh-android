package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class RatingPointBanner(

	@field:SerializedName("rating")
	val rating: String? = null
)