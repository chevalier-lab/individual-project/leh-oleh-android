package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseOrderDetail(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class LogisticStatusItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class DetailsItem(

	@field:SerializedName("itemID")
	val itemID: Int? = null,

	@field:SerializedName("itemQTY")
	val itemQTY: Int? = null,

	@field:SerializedName("itemName")
	val itemName: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null
)

data class Detail(

	@field:SerializedName("handler_ids")
	val handlerIds: List<Any?>? = null,

	@field:SerializedName("voucher")
	val voucher: String? = null,

	@field:SerializedName("origin")
	val origin: Origin? = null,

	@field:SerializedName("destination")
	val destination: Destination? = null,

	@field:SerializedName("isLabelPrinted")
	val isLabelPrinted: Int? = null,

	@field:SerializedName("channel")
	val channel: String? = null,

	@field:SerializedName("externalID")
	val externalID: String? = null,

	@field:SerializedName("source")
	val source: String? = null,

	@field:SerializedName("isActive")
	val isActive: Int? = null,

	@field:SerializedName("isAutomateOrder")
	val isAutomateOrder: Int? = null,

	@field:SerializedName("paymentType")
	val paymentType: String? = null,

	@field:SerializedName("consigner")
	val consigner: Consigner? = null,

	@field:SerializedName("readyTime")
	val readyTime: String? = null,

	@field:SerializedName("partitionKey")
	val partitionKey: Int? = null,

	@field:SerializedName("activeDate")
	val activeDate: String? = null,

	@field:SerializedName("id")
	val idDetail: String? = null,

	@field:SerializedName("JOBNumber")
	val jOBNumber: String? = null,

	@field:SerializedName("useInsurance")
	val useInsurance: Int? = null,

	@field:SerializedName("stickerNumber")
	val stickerNumber: String? = null,

	@field:SerializedName("consignee")
	val consignee: Consignee? = null,

	@field:SerializedName("package")
	val jsonMemberPackage: JsonMemberPackage? = null,

	@field:SerializedName("labelChecksum")
	val labelChecksum: String? = null,

	@field:SerializedName("groupID")
	val groupID: Int? = null,

	@field:SerializedName("rates")
	val rates: Rates? = null,

	@field:SerializedName("pickUpTime")
	val pickUpTime: String? = null,

	@field:SerializedName("batchID")
	val batchID: Int? = null,

	@field:SerializedName("creationDate")
	val creationDate: String? = null,

	@field:SerializedName("specialID")
	val specialID: String? = null,

	@field:SerializedName("awbNumber")
	val awbNumber: String? = null,

	@field:SerializedName("internalStatus")
	val internalStatus: InternalStatus? = null,

	@field:SerializedName("lastUpdatedDate")
	val lastUpdatedDate: String? = null,

	@field:SerializedName("isHubless")
	val isHubless: Int? = null,

	@field:SerializedName("isJobMarketplace")
	val isJobMarketplace: Int? = null,

	@field:SerializedName("driver")
	val driver: Driver? = null,

	@field:SerializedName("isAutoTrack")
	val isAutoTrack: Int? = null,

	@field:SerializedName("courier")
	val courier: Courier? = null,

	@field:SerializedName("createdBy")
	val createdBy: Int? = null,

	@field:SerializedName("shipmentArea")
	val shipmentArea: String? = null,

	@field:SerializedName("requestID")
	val requestID: String? = null,

	@field:SerializedName("domain")
	val domain: String? = null,

	@field:SerializedName("cod")
	val cod: Cod? = null,

	@field:SerializedName("isCustomAWB")
	val isCustomAWB: Int? = null,

	@field:SerializedName("_id")
	val id: String? = null,

	@field:SerializedName("externalStatus")
	val externalStatus: ExternalStatus? = null,

	@field:SerializedName("whitelabel")
	val whitelabel: String? = null,

	@field:SerializedName("zonaID")
	val zonaID: Int? = null,

	@field:SerializedName("shipmentStatus")
	val shipmentStatus: ShipmentStatus? = null
)

data class ActualRate(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class EscrowCost(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Dimension(

	@field:SerializedName("length")
	val length: Length? = null,

	@field:SerializedName("width")
	val width: Width? = null,

	@field:SerializedName("height")
	val height: Height? = null
)

data class Courier(

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("actualID")
	val actualID: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("actualRate")
	val actualRate: ActualRate? = null,

	@field:SerializedName("rate")
	val rate: Rate? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("shipmentType")
	val shipmentType: Int? = null
)

data class Origin(

	@field:SerializedName("suburbID")
	val suburbID: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("stopID")
	val stopID: Int? = null,

	@field:SerializedName("cityID")
	val cityID: Int? = null,

	@field:SerializedName("provinceID")
	val provinceID: Int? = null,

	@field:SerializedName("countryID")
	val countryID: Int? = null,

	@field:SerializedName("areaID")
	val areaID: Int? = null,

	@field:SerializedName("suburbName")
	val suburbName: String? = null,

	@field:SerializedName("cityName")
	val cityName: String? = null,

	@field:SerializedName("areaName")
	val areaName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("provinceName")
	val provinceName: String? = null,

	@field:SerializedName("countryName")
	val countryName: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("direction")
	val direction: String? = null
)

data class ExternalStatus(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Consigner(

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)

data class Shipment(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Cod(

	@field:SerializedName("freeDelivery")
	val freeDelivery: Int? = null,

	@field:SerializedName("order")
	val order: Int? = null
)

data class Consignee(

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)

data class Width(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class ShipmentStatus(

	@field:SerializedName("updateDate")
	val updateDate: String? = null,

	@field:SerializedName("updatedBy")
	val updatedBy: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Length(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Driver(

	@field:SerializedName("feedback")
	val feedback: Feedback? = null,

	@field:SerializedName("agentCityID")
	val agentCityID: String? = null,

	@field:SerializedName("agentID")
	val agentID: Int? = null,

	@field:SerializedName("phoneNumber")
	val phoneNumber: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("vehicleNumber")
	val vehicleNumber: String? = null,

	@field:SerializedName("agentName")
	val agentName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("vehicleType")
	val vehicleType: String? = null,

	@field:SerializedName("isPaymentCollected")
	val isPaymentCollected: Int? = null
)

data class ActualInsurance(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class JsonMemberPackage(

	@field:SerializedName("itemType")
	val itemType: Int? = null,

	@field:SerializedName("itemName")
	val itemName: String? = null,

	@field:SerializedName("contents")
	val contents: String? = null,

	@field:SerializedName("cubicalWeight")
	val cubicalWeight: CubicalWeight? = null,

	@field:SerializedName("price")
	val price: Price? = null,

	@field:SerializedName("pictureURL")
	val pictureURL: String? = null,

	@field:SerializedName("weight")
	val weight: Weight? = null,

	@field:SerializedName("isConfirmed")
	val isConfirmed: Int? = null,

	@field:SerializedName("details")
	val details: List<DetailsItem?>? = null,

	@field:SerializedName("fragile")
	val fragile: Int? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("dimension")
	val dimension: Dimension? = null
)

data class ItemPrice(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Data(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("order")
	val order: Order? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)

data class Rate(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Insurance(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class TrackingItem(

	@field:SerializedName("internalStatus")
	val internalStatus: InternalStatus? = null,

	@field:SerializedName("createdDate")
	val createdDate: String? = null,

	@field:SerializedName("orderID")
	val orderID: String? = null,

	@field:SerializedName("createdBy")
	val createdBy: String? = null,

	@field:SerializedName("trackStatus")
	val trackStatus: TrackStatus? = null,

	@field:SerializedName("_id")
	val _idTracking: String? = null,

	@field:SerializedName("id")
	val idTracking: String? = null,

	@field:SerializedName("externalStatus")
	val externalStatus: ExternalStatus? = null,

	@field:SerializedName("logisticStatus")
	val logisticStatus: List<LogisticStatusItem?>? = null,

	@field:SerializedName("uniqueID")
	val uniqueID: String? = null
)

data class CubicalWeight(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Double? = null
)

data class InternalStatus(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Price(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class FulfillmentCost(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class PaidInsurance(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Weight(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class TrackStatus(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Discount(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Rates(

	@field:SerializedName("insurance")
	val insurance: Insurance? = null,

	@field:SerializedName("paidInsurance")
	val paidInsurance: PaidInsurance? = null,

	@field:SerializedName("shipment")
	val shipment: Shipment? = null,

	@field:SerializedName("fulfillmentCost")
	val fulfillmentCost: FulfillmentCost? = null,

	@field:SerializedName("paidShipment")
	val paidShipment: PaidShipment? = null,

	@field:SerializedName("actualShipment")
	val actualShipment: ActualShipment? = null,

	@field:SerializedName("discount")
	val discount: Discount? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: ItemPrice? = null,

	@field:SerializedName("actualInsurance")
	val actualInsurance: ActualInsurance? = null,

	@field:SerializedName("escrowCost")
	val escrowCost: EscrowCost? = null
)

data class Order(

	@field:SerializedName("detail")
	val detail: Detail? = null,

	@field:SerializedName("tracking")
	val tracking: List<TrackingItem?>? = null
)

data class PaidShipment(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Destination(

	@field:SerializedName("suburbID")
	val suburbID: Int? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("lng")
	val lng: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("stopID")
	val stopID: Int? = null,

	@field:SerializedName("cityID")
	val cityID: Int? = null,

	@field:SerializedName("provinceID")
	val provinceID: Int? = null,

	@field:SerializedName("countryID")
	val countryID: Int? = null,

	@field:SerializedName("areaID")
	val areaID: Int? = null,

	@field:SerializedName("suburbName")
	val suburbName: String? = null,

	@field:SerializedName("cityName")
	val cityName: String? = null,

	@field:SerializedName("areaName")
	val areaName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("provinceName")
	val provinceName: String? = null,

	@field:SerializedName("countryName")
	val countryName: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("direction")
	val direction: String? = null
)

data class ActualShipment(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)

data class Feedback(

	@field:SerializedName("score")
	val score: Int? = null,

	@field:SerializedName("comment")
	val comment: String? = null
)

data class Height(

	@field:SerializedName("UoM")
	val uoM: String? = null,

	@field:SerializedName("value")
	val value: Int? = null
)
