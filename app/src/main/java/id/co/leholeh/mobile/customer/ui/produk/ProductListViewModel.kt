package id.co.leholeh.mobile.customer.ui.produk

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.model.category.DataCategory
import id.co.leholeh.mobile.customer.model.location.DataLocation
import id.co.leholeh.mobile.customer.model.product.FilterProduct
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class ProductListViewModel : ViewModel() {

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val _categories = MutableLiveData<DataCategory>()
    val categories: LiveData<DataCategory>
        get() = _categories

    private val _location = MutableLiveData<DataLocation>()
    val location: LiveData<DataLocation>
        get() = _location

    val apiService = ApiService().service
    private val apiServiceGlobal = ApiService().serviceGlobal

    private suspend fun requestDataCategories() {
        try {
            _status.postValue(ApiStatus.LOADING)
            _categories.postValue(apiServiceGlobal.getCategories())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("Categories", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getCategories() {
        viewModelScope.launch {
            requestDataCategories()
        }
    }


    private suspend fun requestDataLocation() {
        try {
            _status.postValue(ApiStatus.LOADING)
            _location.postValue(apiServiceGlobal.getLocation())
            _status.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("Categories", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getLocation() {
        viewModelScope.launch {
            requestDataLocation()
        }
    }

    //CONFIG
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    //LIST PRODUCT SEARCH
    private lateinit var listProductSearch: LiveData<PagedList<FilterProduct>>
    lateinit var statusProductSearch: LiveData<ApiStatus>
    private val _listProductSearchDataSource = MutableLiveData<ProductListActivityDataSource>()


    //LIST PRODUCT SEARCH
    fun setUpListProductSearch(orderDirection: String, search: String, jsonObject: JsonObject) {
        statusProductSearch = Transformations.switchMap<ProductListActivityDataSource, ApiStatus>(
            _listProductSearchDataSource,
            ProductListActivityDataSource::apiState
        )
        listProductSearch = initializedProductSearchPagedListBuilder(
            config,
            orderDirection,
            search,
            jsonObject
        ).build()
    }

    fun getDataListProductSearch() : LiveData<PagedList<FilterProduct>> = listProductSearch

    private fun initializedProductSearchPagedListBuilder(config: PagedList.Config,orderDirection:String, search:String , jsonObject: JsonObject) : LivePagedListBuilder<Int, FilterProduct> {
        val dataSourceFactory = object : DataSource.Factory<Int, FilterProduct>(){
            override fun create(): DataSource<Int, FilterProduct> {
                val productSearchDataSource = ProductListActivityDataSource(orderDirection,search,jsonObject)
                _listProductSearchDataSource.postValue(productSearchDataSource)
                return productSearchDataSource
            }
        }
        return LivePagedListBuilder<Int, FilterProduct>(dataSourceFactory,config)
    }
    //END OF LIST PRODUCT SEARCH

}