package id.co.leholeh.mobile.customer.network.shipper

import id.co.leholeh.mobile.customer.constant.shipper.ConstantLocation
import id.co.leholeh.mobile.customer.model.shipper.location.*
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface LocationService {
    @GET(ConstantLocation.BASE_URL_LOCATION_PROVINCE)
    @Headers("User-Agent: Shipper/")
    suspend fun getProvince(@Query("apiKey") apiKey: String): ResponseProvince

    @GET(ConstantLocation.BASE_URL_LOCATION_CITIES)
    @Headers("User-Agent: Shipper/")
    suspend fun getCities(
        @Query("apiKey") apiKey: String,
        @Query("origin") origin: String,
        @Query("province") province: String
    ): ResponseCities

    @GET(ConstantLocation.BASE_URL_LOCATION_CITIES)
    @Headers("User-Agent: Shipper/")
    suspend fun getCities(
        @Query("apiKey") apiKey: String,
        @Query("province") province: Int
    ): ResponseCities

    @GET(ConstantLocation.BASE_URL_LOCATION_SUBURBS)
    @Headers("User-Agent: Shipper/")
    suspend fun getSuburbs(
        @Query("apiKey") apiKey: String,
        @Query("city") city: String
    ): ResponseSuburbs

    @GET(ConstantLocation.BASE_URL_LOCATION_SUBURBS)
    @Headers("User-Agent: Shipper/")
    suspend fun getSuburbs(
        @Query("apiKey") apiKey: String,
        @Query("city") city: Int
    ): ResponseSuburbs

    @GET(ConstantLocation.BASE_URL_LOCATION_AREAS)
    @Headers("User-Agent: Shipper/")
    suspend fun getAreas(
        @Query("apiKey") apiKey: String,
        @Query("suburb") suburb: String
    ): ResponseAreas

    @GET(ConstantLocation.BASE_URL_LOCATION_AREAS)
    @Headers("User-Agent: Shipper/")
    suspend fun getAreas(
        @Query("apiKey") apiKey: String,
        @Query("suburb") suburb: Int
    ): ResponseAreas

    @GET(ConstantLocation.BASE_URL_LOCATION_SEARCH)
    @Headers("User-Agent: Shipper/")
    suspend fun getSearch(@Path("SUBSTRING") subtring: String): ResponseSearch
}