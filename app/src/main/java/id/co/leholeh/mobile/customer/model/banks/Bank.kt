package id.co.leholeh.mobile.customer.model.banks

import com.google.gson.annotations.SerializedName

data class Bank(

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("bank_name")
	val bankName: String? = null,

	@field:SerializedName("index")
	val index: Int? = null,

	@field:SerializedName("logo")
	val logo: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)
