package id.co.leholeh.mobile.customer.model.auth

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Login(

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("level")
    val level: String? = null,

    @field:SerializedName("username")
    val username: String? = null,

    @field:SerializedName("token")
    val token: String? = null
):Serializable