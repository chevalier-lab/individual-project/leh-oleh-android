package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.BASE_URL_LOGIN
import id.co.leholeh.mobile.customer.constant.BASE_URL_REGISTER
import id.co.leholeh.mobile.customer.model.auth.DataLogin
import id.co.leholeh.mobile.customer.model.auth.DataRegister
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface AuthService {
    @Headers(
        "Accept: application/json",
        "Cache-Control: no-store, no-cache, must-revalidate",
        "Content-Type: application/json")
    @POST(BASE_URL_LOGIN)
    fun getLogin(@Body login : JsonObject) : Call<DataLogin>

    @Headers(
        "Accept: application/json",
        "Cache-Control: no-store, no-cache, must-revalidate",
        "Content-Type: application/json")
    @POST(BASE_URL_REGISTER)
    fun getRegister(@Body register: JsonObject) : Call<DataRegister>

}