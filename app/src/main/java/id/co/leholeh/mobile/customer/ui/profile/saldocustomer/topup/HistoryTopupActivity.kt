package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityHistoryTopupBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_withdraw.view.*
import java.text.SimpleDateFormat

class HistoryTopupActivity : AppCompatActivity() {
    private lateinit var binding : ActivityHistoryTopupBinding
    private lateinit var adapterTopupHistory: PagedAdapterUtil<HistoryTopup>
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth : Login
    private val viewModel: TopupViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(TopupViewModel::class.java)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHistoryTopupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
        else{
            auth = getAuth(cacheUtil)
            getListHistoryTopup()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun observerListHistoryTopup(){
        viewModel.getDataListHistoryTopUp().observe(this, Observer {
            adapterTopupHistory.submitList(it)
        })
        getState()

    }

    @SuppressLint("SetTextI18n")
    private fun getState() {
        viewModel.statusListHistoryTopup.observe(this, Observer {
            Log.d("STATUS_HISTORY", it.toString())
            when (it) {
                ApiStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                ApiStatus.EMPTY -> {
                    binding.emptyView.tvEmptyView.text = "Riwayat Tambah Saldo Kosong!"
                    binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_red_pesanan)
                    binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
                    binding.recyclerHistoryTopup.visibility = View.GONE
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.EMPTY_AFTER -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.LOADED -> {
                    binding.emptyView.layoutEmptyView.visibility = View.GONE
                    binding.recyclerHistoryTopup.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, "Jaringan bermasalah", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun getListHistoryTopup() {
        viewModel.setUpListHistoryTopup(auth.token!!)
        observerListHistoryTopup()
        //init adapter
        adapterTopupHistory =
            PagedAdapterUtil(
                R.layout.item_withdraw,
                { _, itemView, item ->
                    when (item.status) {
                        "1" -> {
                            itemView.tv_status.text = getString(R.string.success)
                        }
                        "0" -> {
                            itemView.tv_status.text = getString(R.string.failed)
                            itemView.tv_status.setBackgroundResource(R.drawable.status_refund)

                        }
                    }
                    itemView.tv_bank.text = item.walletName
                    itemView.tv_nominal.text = changeToRupiah(item.balanceRequest!!.toDouble())
                    val tglTransaksi =
                        SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(item.createdAt!!)
                    itemView.textViewTglTransaksi.text =
                        SimpleDateFormat("dd MMMM yyyy  HH:mm:ss").format(tglTransaksi!!)

                },
                { _, _ ->

                })

        binding.recyclerHistoryTopup.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerHistoryTopup.adapter = adapterTopupHistory
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.history_topup)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            auth = getAuth(cacheUtil)
            //observeListHistoryTopup()
        }
    }
}