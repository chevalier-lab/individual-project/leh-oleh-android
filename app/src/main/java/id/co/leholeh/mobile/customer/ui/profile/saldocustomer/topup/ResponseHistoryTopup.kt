package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import com.google.gson.annotations.SerializedName

data class ResponseHistoryTopup(

    @field:SerializedName("code")
	val code: Int? = null,

    @field:SerializedName("data")
	val data: List<HistoryTopup>? = null,

    @field:SerializedName("error")
	val error: List<Any?>? = null,

    @field:SerializedName("message")
	val message: String? = null
)


