package id.co.leholeh.mobile.customer.ui.cart

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.cart.DataItemCart
import id.co.leholeh.mobile.customer.model.shipper.rates.DataTrackingItem
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.shipper.DomesticRatesService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class KeranjangViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    // Updated
    private val _listCart = MutableLiveData<List<DataItemCart>>()
    private val _shipperRates = MutableLiveData<DataTrackingItem>()

    val listCart: LiveData<List<DataItemCart>>
        get() = _listCart
    val shipperRates: LiveData<DataTrackingItem>
        get() = _shipperRates

    private val apiServiceKeranjang = ApiService().serviceCart
    private val serviceShipper by lazy {
        ApiService().createServiceShipper(DomesticRatesService::class.java)
    }

    private suspend fun requestListCart(token: String) = try {
        _status.postValue(ApiStatus.LOADING)
        _listCart.postValue(apiServiceKeranjang.getListCart(token).data)
        Log.d("CART", apiServiceKeranjang.getListCart(token).data.toString())
        _status.postValue(ApiStatus.SUCCESS)

    } catch (e: Exception) {
        Log.d("CART", e.localizedMessage!!)
        _status.postValue(ApiStatus.FAILED)
    }

    private suspend fun requestShipperRates(
        apiKey: String,
        o: Int,
        d: Int,
        l: Float,
        w: Float,
        h: Float,
        wt: Float,
        v: Int
    ) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _shipperRates.postValue(
                serviceShipper.getDomesticRates(apiKey, o, d, l, w, h, wt, v).data
            )
            Log.i(
                "SHIPPER_RATES",
                serviceShipper.getDomesticRates(apiKey, o, d, l, w, h, wt, v).data.toString()
            )
            _status.postValue(ApiStatus.SUCCESS)

        } catch (e: Exception) {
            Log.d("SHIPPER_RATES", "${e.message} --- ${e.printStackTrace()}")
            Log.d("SHIPPER_RATES_2", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getListCart(token: String) = viewModelScope.launch { requestListCart(token) }
    fun getDomesticRates(
        apiKey: String, o: Int,
        d: Int,
        l: Float,
        w: Float,
        h: Float,
        wt: Float,
        v: Int
    ) = viewModelScope.launch { requestShipperRates(apiKey, o, d, l, w, h, wt, v) }
}