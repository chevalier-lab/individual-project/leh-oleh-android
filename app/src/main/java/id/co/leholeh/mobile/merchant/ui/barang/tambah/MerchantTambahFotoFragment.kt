package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentMerchantTambahFotoBinding
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.barang.BarangTemporary
import kotlinx.android.synthetic.main.item_grid_photo.view.*

class MerchantTambahFotoFragment : Fragment() {

    private lateinit var binding : FragmentMerchantTambahFotoBinding
    private var barangTemporary = BarangTemporary()
    private lateinit var adapterPhoto: AdapterUtil<Uri>
    private var listUriPhoto = arrayListOf<Uri>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentMerchantTambahFotoBinding.inflate(inflater,container,false)
        binding.imAddCamera.setOnClickListener {
            if(listUriPhoto.size == 4){
                Toast.makeText(requireContext(), "Photo barang sudah 4 ya :)", Toast.LENGTH_SHORT).show()
            }else{
                ImagePicker.create(this)
                    .single()
                    .start()
            }
        }
        setUpPhotoBarang()
        return binding.root
    }

    fun isPhotoNotNull() : Boolean {
        return if(listUriPhoto.size > 0){
            true
        }else{
            Toast.makeText(requireContext(), "Masukkan minimal 1 photo barang", Toast.LENGTH_SHORT).show()
            false
        }
    }

    fun setDataBarang(barangTemp : BarangTemporary){
        barangTemporary = barangTemp
        Log.d("NAMA BARANG FOTO", "CANDA BARANG ${barangTemp.namaBarang}")
    }

    fun getDataBarang() : BarangTemporary {
        barangTemporary.photoProduct = listUriPhoto
        return barangTemporary
    }

    private fun setUpPhotoBarang(){
        adapterPhoto = AdapterUtil(
            R.layout.item_grid_photo,
            arrayListOf(),
            { position, itemView, item ->
                Glide.with(this).load(item)
                    .into(itemView.imageViewProductPhoto)
                itemView.imDeletePhoto.setOnClickListener {
                    listUriPhoto.removeAt(position)
                    adapterPhoto.refresh()
                }
            },
            { _, _ ->

            })
        if(listUriPhoto.size > 0 ) adapterPhoto.data = listUriPhoto
        binding.rvFoto.layoutManager = GridLayoutManager(requireContext(), 3)
        binding.rvFoto.adapter = adapterPhoto
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image : Image = ImagePicker.getFirstImageOrNull(data)
            if(!image.equals(null)){
                listUriPhoto.add(image.uri)
                adapterPhoto.data = listUriPhoto
                adapterPhoto.refresh()
            }
        }
    }
}