package id.co.leholeh.mobile.merchant.model.barang

import android.net.Uri
import java.io.File
import java.io.Serializable

data class BarangTemporary(
    var cover: File? = null,
    var namaBarang : String? = null,
    var jumlahStok :String? = null,
    var diskon:String? = null,
    var hargaModal:String? = null,
    var hargaJual:String? = null,
    var deskripsi:String? = null,
    var length : String? = null,
    var width : String? = null,
    var height : String? = null,
    var weight : String? = null,
    var kategoriId:ArrayList<String>? = null,
    var kategoriString:ArrayList<String>? = null,
    var lokasiId : ArrayList<String>? = null,
    var lokasiString : ArrayList<String>? = null,
    var photoProduct : ArrayList<Uri>? = null
): Serializable