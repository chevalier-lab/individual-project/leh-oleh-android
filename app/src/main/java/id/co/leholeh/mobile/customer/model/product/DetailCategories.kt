package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DetailCategories (
    @field:SerializedName("id_u_product_categories")
    val idUProductCategories: String? = null,

    @field:SerializedName("id_m_categories")
    val idMCategories: String? = null,

    @field:SerializedName("category")
    val category: String? = null
) : Serializable