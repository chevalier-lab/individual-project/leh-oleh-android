package id.co.leholeh.mobile.merchant.constant

object ConstantProfileMerchant {
    const val BASE_URL_PROFILE_TOKO = "merchant/manage/general/my-market"
    const val BASE_URL_IDENTITY_PHOTO = "merchant/manage/general/identity-photo"
    const val BASE_URL_MARKET_PHOTO = "merchant/manage/general/market-photo"
    const val BASE_URL_AUTHOR_PHOTO = "merchant/manage/general/author-photo"
    const val BASE_URL_CREATE_LOCATION_MERCHANT = "auth/merchant/location"
    const val BASE_URL_UPDATE_LOCATION_MERCHANT = "auth/merchant/location/{id}"

    const val INTENT_DATA_PROFILE_TOKO = "data_profile_toko"
    const val INTEN_DATA_EDIT_PROFILE_TOKO = "data_edit_profile_toko"
    const val KONDISI_EDIT_PROFILE_TOKO = "kondisi_edit_profile_toko"
    const val INTENT_MERCHANT_STATUS = "intent_merchant_status"
    const val MERCHANT_REJECTED = "merchant_rejected"
    const val MERCHANT_PROSES = "merchant_proses"
}