package id.co.leholeh.mobile.customer.ui.rekomendasi

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ComponentSeachingDialogBinding
import id.co.leholeh.databinding.FragmentRekomendasiBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantGuestMerchants
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.constant.shipper.ConstantShipper
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.banner.DataBannerRekomendasi
import id.co.leholeh.mobile.customer.model.cart.DataAddCart
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.model.location.LocationProduct
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.customer.model.product.ByLocationProduct
import id.co.leholeh.mobile.customer.model.product.Product
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.ui.listmerchants.ListMerchantsActivity
import id.co.leholeh.mobile.customer.ui.location.UserLocationViewModel
import id.co.leholeh.mobile.customer.ui.merchantcustomer.MerchantPreviewActivity
import id.co.leholeh.mobile.customer.ui.produk.DetailProductActivity
import id.co.leholeh.mobile.customer.ui.produk.ProductListActivity
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.customer.util.ShimmerUtil
import id.co.leholeh.mobile.customer.util.SliderAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilFragment
import kotlinx.android.synthetic.main.item_merchant.view.*
import kotlinx.android.synthetic.main.item_produk.view.*
import kotlinx.android.synthetic.main.item_produk_fix_size.view.*
import kotlinx.android.synthetic.main.slider_component.view.*
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable


class RekomendasiFragment : Fragment() {

    private val viewModel: RekomendasiViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(RekomendasiViewModel::class.java)
    }
    private val viewModelCategories: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }

    private val viewModelLocation: UserLocationViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(UserLocationViewModel::class.java)
    }

    private lateinit var sliderAdapter: SliderAdapterUtil<DataBannerRekomendasi>
    private lateinit var someHandler: Handler
    private lateinit var produkTerbaruAdapter: PagedAdapterUtil<Product>
    private lateinit var produkByLokasiAdapter: AdapterUtil<ByLocationProduct>

    private lateinit var merchantRandomAdapter: AdapterUtil<Merchants>


    private var sliderIndicators = mutableListOf<ImageView>()

    private var _binding: FragmentRekomendasiBinding? = null
    private val binding get() = _binding!!
    private var sliderCount = 0

    private var listCariLehOleh = arrayListOf<ModelSearching>()

    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        _binding = FragmentRekomendasiBinding.inflate(inflater, container, false)
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbarRekomendasi)

        //cache util
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)

        binding.toolbarRekomendasi.title = getString(R.string.untuk_kamu)
        binding.toolbarRekomendasi.inflateMenu(R.menu.search_cart_menu)
        binding.toolbarRekomendasi.setOnMenuItemClickListener {
            onOptionsItemSelected(it)
        }
        binding.collapsingToolbarRekomendasi.setCollapsedTitleTextColor(
            ContextCompat.getColor(context as Activity, android.R.color.white)
        )
        binding.collapsingToolbarRekomendasi.setExpandedTitleColor(Color.TRANSPARENT)

        sliderAdapter =
            SliderAdapterUtil(R.layout.slider_component,
                requireContext(),
                arrayListOf(),
                { itemView, item, position ->
                    Glide.with(requireContext()).load(item.uri)
                        .into(itemView.imageViewBannerRekomendasi)
                },
                { _, position ->

                })
        binding.viewPagerRekomendasi.addOnPageChangeListener(handler)
        loadDataSlider()
        startAutoBanner()
        getListNewProduct()
        getListProductByLocation()
        getListRandomMerchant()

        binding.textViewAllMerchant.setOnClickListener {
            startActivity(Intent(requireContext(), ListMerchantsActivity::class.java))
        }

        viewModel.getStatus().observe(requireActivity(), Observer {
            ShimmerUtil.updateProgressCoordinatorLayout(
                it,
                binding.progressBar,
                binding.tvNetworkError,
                binding.layoutRekomendasi
            )
        })

        return binding.root
    }

    private fun getListNewProduct() {
        viewModel.setupListNewProduct()
        viewModel.getDataListNewProduct().observe(viewLifecycleOwner, Observer {
            produkTerbaruAdapter.submitList(it)
        })

//        getStateNewProduct()
        viewModel.statusNewProduct.observe(viewLifecycleOwner, Observer {
            stateUtilFragment(requireContext(),it,binding.progressBarNewProduct,binding.recyclerProdukTerbaru)
        })


        produkTerbaruAdapter =
            PagedAdapterUtil(
                R.layout.item_produk,
                { position, itemView, item ->
                    itemView.textViewNamaProduk.text = item.productName

                    val hargaDiskon =
                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                    itemView.textViewHargaDiskon.text =
                        changeToRupiah(
                            hargaDiskon
                        )
                    if (item.discount.toString().toDouble() > 0.0) {
                        itemView.textViewHargaAsliProduk.paintFlags =
                            itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        itemView.textViewHargaAsliProduk.text =
                            changeToRupiah(item.priceSelling.toDouble())

                    } else {
                        itemView.textViewHargaAsliProduk.visibility = View.INVISIBLE
                    }

                    itemView.textViewHargaAsliProduk.paintFlags =
                        itemView.textViewHargaAsliProduk.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    Glide.with(requireContext()).load(item.uri).into(itemView.imageViewProduk)

                    //Categories
                    var categories = ""

                    for (i in item.categories!!.indices) {
                        categories += if (i < item.categories.size - 1) {
                            item.categories[i].category + ", "
                        } else {
                            item.categories[i].category
                        }
                    }
                    itemView.textViewKategoriProduk.text = categories

                    //Beli sekarang item

                    //Beli Sekarang
                    itemView.btBeliSekarang.setOnClickListener {
                        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
                            auth = getAuth(cacheUtil)

                            val json = JsonObject()
                            json.addProperty("idMProducts", item.id)
                            json.addProperty("qty", "1")

                            addToCart(auth.token!!, json, {
                                if (it.code == 200) {
                                    Toast.makeText(
                                        requireContext(),
                                        "Berhasil Menambahkan Keranjang",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    val intent = Intent(
                                        requireContext(),
                                        KeranjangActivity::class.java
                                    )
                                    startActivity(intent)
                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        it.message,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }, {
                                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                            })
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Silahkan Login Terlebih Dahulu",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                },
                { _, item ->
                    val intent = Intent(
                        requireContext(),
                        DetailProductActivity::class.java
                    )
                        .putExtra(
                            INTENT_DETAIL,
                            item.id
                        )
                    startActivity(intent)
                })
        binding.recyclerProdukTerbaru.layoutManager = GridLayoutManager(requireContext(), 2)    
        binding.recyclerProdukTerbaru.adapter = produkTerbaruAdapter

    }

    private fun getListProductByLocation() {
        viewModel.getLokasiProduct().observe(viewLifecycleOwner, Observer {

            for (i in it.indices) {
                Log.d("DATADALEMLIST", "Perulanagn ke " + i + " " + it[i].toString())

                if (it[i].products!!.isNotEmpty()) {
                    Glide.with(this).load(it[0].uri).into(binding.imageViewLokasi)
                    produkByLokasiAdapter.data = it[i].products!!
                    binding.textViewLokasi.text =
                        StringBuilder("Lokasi (").append(it[i].provinceName + ")")
                    binding.emptyView.layoutEmptyView.visibility = View.GONE

                    break
                } else {
                    binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_location)
                    binding.emptyView.tvEmptyView.text = getString(R.string.data_kosong)
                    binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
                }
            }
        })


        produkByLokasiAdapter =
            AdapterUtil(
                R.layout.item_produk_fix_size,
                arrayListOf(),
                { position, itemView, item ->
                    itemView.textViewNamaProdukFix.text = item.productName
                    Log.d("data nama produk", item.productName!!)
                    Glide.with(requireContext()).load(item.uri).into(itemView.imageViewProdukFix)
                    //Categories
                    var categories = ""

                    for (i in item.categories!!.indices) {
                        categories += if (i < item.categories.size - 1) {
                            item.categories[i].category + ", "
                        } else {
                            item.categories[i].category
                        }
                    }
                    itemView.textViewKategoriProdukFix.text = categories


                    val harga_diskon =
                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
                    itemView.textViewHargaDiskonFix.text =
                        changeToRupiah(
                            harga_diskon
                        )
                    if (item.discount.toString().toDouble() > 0.0) {
                        itemView.textViewHargaAsliProdukFix.paintFlags =
                            itemView.textViewHargaAsliProdukFix.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        itemView.textViewHargaAsliProdukFix.text =
                            changeToRupiah(item.priceSelling.toDouble())

                    } else {
                        itemView.textViewHargaAsliProdukFix.visibility = View.INVISIBLE
                    }

                    itemView.btBeliSekarangFixSize.setOnClickListener {
                        //Beli Sekarang
                        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
                            auth = getAuth(cacheUtil)

                            val json = JsonObject()
                            json.addProperty("idMProducts", item.idMProducts)
                            json.addProperty("qty", "1")

                            addToCart(auth.token!!, json, {
                                if (it.code == 200) {
                                    Toast.makeText(
                                        requireContext(),
                                        "Berhasil Menambahkan Keranjang",
                                        Toast.LENGTH_LONG
                                    ).show()
                                    val intent = Intent(
                                        requireContext(),
                                        KeranjangActivity::class.java
                                    )
                                    startActivity(intent)
                                } else {
                                    Toast.makeText(
                                        requireContext(),
                                        it.message,
                                        Toast.LENGTH_LONG
                                    )
                                        .show()
                                }
                            }, {
                                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
                            })
                        } else {
                            Toast.makeText(
                                requireContext(),
                                "Silahkan Login Terlebih Dahulu",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }

                },
                { position, item ->
                    val intent = Intent(
                        requireContext(),
                        DetailProductActivity::class.java
                    )
                        .putExtra(
                            INTENT_DETAIL,
                            item.idMProducts
                        )
                    startActivity(intent)
                })

        binding.recyclerProdukByLokasi.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerProdukByLokasi.adapter = produkByLokasiAdapter
    }

    private fun getListRandomMerchant(){
        viewModel.listMerchantsRandom.observe(viewLifecycleOwner, Observer {
            merchantRandomAdapter.data = it
        })
        merchantRandomAdapter = AdapterUtil(
            R.layout.item_merchant,
            arrayListOf(),
            { _, itemView, item ->
                itemView.textViewNamaMerchantFix.text = item.marketName
                itemView.textViewAlamatMerchantFix.text = item.marketAddress
                Glide.with(requireContext()).load(item.marketUri)
                    .into(itemView.imageViewMerchantFix)
            },
            { _, item ->
                val intent = Intent(
                    requireContext(),
                    MerchantPreviewActivity::class.java
                )
                intent.putExtra(
                    ConstantGuestMerchants.DATA_MERCHANT,
                    item
                )
                startActivity(intent)
            })
        binding.recyclerViewMerchantKami.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerViewMerchantKami.adapter = merchantRandomAdapter
    }


    private fun loadDataSlider() {
        binding.viewPagerRekomendasi.adapter = sliderAdapter

        viewModel.getBanner().observe(viewLifecycleOwner, Observer {
            sliderAdapter.data = it

            binding.viewPagerRekomendasi.offscreenPageLimit = it.size

            val listSlider = arrayListOf<DataBannerRekomendasi>()
            for (i in it.indices) {
                listSlider.add(it[i])
            }

            if (listSlider.size > 0) sliderAdapter.data = listSlider
            sliderCount = sliderAdapter.count

            //initiate slider indicator

            for (i in 0 until sliderCount) {
                sliderIndicators.add(ImageView(requireContext()))
                this.sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(8, 0, 8, 0)
                if (this.sliderIndicators[i].parent != null) {
                    (this.sliderIndicators[i].parent as ViewGroup).removeView(this.sliderIndicators[i])
                }
                binding.sliderIndicator.addView(this.sliderIndicators[i], params)
            }
            sliderIndicators[0].setImageResource(R.drawable.shape_indicator_active)
        })


    }

    private val handler = object : ViewPager.OnPageChangeListener {
        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(
            position: Int,
            positionOffset: Float,
            positionOffsetPixels: Int
        ) {
        }

        override fun onPageSelected(position: Int) {
            for (i in 0 until sliderCount)
                sliderIndicators[i].setImageResource(R.drawable.shape_indicator_inactive)
            sliderIndicators[position].setImageResource(R.drawable.shape_indicator_active)
        }
    }

    private fun startAutoBanner() {
        var currentPage = 0
        someHandler = Handler(Looper.getMainLooper())
        someHandler.postDelayed(object : Runnable {
            override fun run() {
                if (currentPage == sliderCount) {
                    currentPage = 0
                }
                binding.viewPagerRekomendasi.setCurrentItem(currentPage++, true)
                someHandler.postDelayed(this, 3000)
            }

        }, 3000)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_cart_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_search ->
                showDialog()

            R.id.menu_cart -> startActivity(Intent(requireContext(), KeranjangActivity::class.java))
        }
        return true
    }

    //show dialog
    private fun showDialog() {

        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_seaching_dialog)
        val layoutInflater = LayoutInflater.from(requireContext())
        //binding here
        val binding = ComponentSeachingDialogBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        //gone view
        binding.tvLocationFilter.visibility = View.GONE
        binding.tvMakananFilter.visibility = View.GONE
        //Categori
        val items: ArrayList<Category> = arrayListOf()
        val itemsCategory: ArrayList<String> = arrayListOf()
        viewModelCategories.getCategories()
        viewModelCategories.categories.observe(viewLifecycleOwner, Observer {
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsCategory.add(it.data[item].category!!)
            }
        })

        val categoriesArray = ArrayList<String>()
        categoriesArray.add(0, "")

        val adapterCategory =
            ArrayAdapter(requireContext(), R.layout.item_list_dropdown_text, itemsCategory)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.tvMakananFilter.text = binding.dropdownMakanan.text.toString()
                binding.tvMakananFilter.visibility = View.VISIBLE
                for (i in items.indices) {
                    if (binding.tvMakananFilter.text.toString() == items[i].category) {
                        Log.e("DATA_ISI_CATEGORY", items[i].category!!)
                        if (categoriesArray.size > 0) {
                            categoriesArray.clear()
                        }
                        categoriesArray.add(items[i].id.toString())
                        break
                    }
                }
            }
        })
        binding.tvMakananFilter.setOnClickListener {
            categoriesArray.clear()
            binding.tvMakananFilter.text = ""
            binding.tvMakananFilter.visibility = View.GONE
        }

        //ACS DESC
        //Urutan Product
        val itemsUrutan: ArrayList<String> = arrayListOf("ASC", "DESC")
        val arrayUrutan =
            ArrayAdapter(requireContext(), R.layout.item_list_dropdown_text, itemsUrutan)
        binding.dropdownUrutkan.setAdapter(arrayUrutan)


        //Location
        val locationArray = ArrayList<String>()
        locationArray.add(0, "")
        val itemsLocation: ArrayList<LocationProduct> = arrayListOf()
        val itemsLocationProvince: ArrayList<String> = arrayListOf()
        viewModelLocation.getListProvince(ConstantShipper.APIKEY_SHIPPER)
        viewModelCategories.getLocation()
        viewModelCategories.location.observe(requireActivity(), Observer {
            for (item in it.data!!.indices) {
                itemsLocation.add(it.data[item])
                itemsLocationProvince.add(it.data[item].provinceName!!)
            }
        })

        val arrayLocation =
            ArrayAdapter(
                requireContext(),
                R.layout.item_list_dropdown_text,
                itemsLocationProvince
            )
        binding.dropdownLokasi.setAdapter(arrayLocation)
        binding.dropdownLokasi.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                binding.tvLocationFilter.text = binding.dropdownLokasi.text.toString()
                binding.tvLocationFilter.visibility = View.VISIBLE
                for (i in itemsLocation.indices) {
                    if (binding.tvLocationFilter.text.toString() == itemsLocation[i].provinceName) {
                        Log.e("DATA_ISI_CATEGORY", itemsLocation[i].provinceName!!)
                        locationArray.clear()
                        locationArray.add(itemsLocation[i].id.toString())
                        break
                    }
                }
            }
        })

        binding.tvLocationFilter.setOnClickListener {
            locationArray.clear()
            binding.tvLocationFilter.text = ""
            binding.tvLocationFilter.visibility = View.GONE
        }


        binding.tvCari.setOnClickListener {
            listCariLehOleh.clear()
            Log.v("IDLOCATION", "id location : " + locationArray[0])
            Log.v("IDLOCATION", "id categirt : " + categoriesArray[0])

            listCariLehOleh.add(
                ModelSearching(
                    binding.etKeyword.text.toString(),
                    categoriesArray[0],
                    locationArray[0],
                    binding.dropdownUrutkan.text.toString()
                )
            )
            startActivity(
                Intent(
                    requireContext(),
                    ProductListActivity::class.java
                ).putExtra(INTENT_SEARCH, listCariLehOleh)
            )
        }
        binding.tvBatal.setOnClickListener {
            dialog.hide()
        }
    }

    data class ModelSearching(
        val keyword: String = "",
        val makanan: String = "",
        val lokasi: String = "",
        val urutan: String = ""
    ) : Serializable


    //Add Cart
    private fun addToCart(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataAddCart) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val call: retrofit2.Call<DataAddCart> =
            ApiService().serviceCart.addCart(auth, jsonObject)
        call.enqueue(object : Callback<DataAddCart> {
            override fun onFailure(call: retrofit2.Call<DataAddCart>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: retrofit2.Call<DataAddCart>,
                response: Response<DataAddCart>
            ) {
                Log.d("ADD_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }

}
