package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class ProductBanner(

	@field:SerializedName("total_pending")
	val totalPending: Int? = null,

	@field:SerializedName("total_active")
	val totalActive: Int? = null
)