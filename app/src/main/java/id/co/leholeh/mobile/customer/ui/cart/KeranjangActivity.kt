package id.co.leholeh.mobile.customer.ui.cart

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityKeranjangBinding
import id.co.leholeh.databinding.ComponentDialogDeleteProductCartBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.cart.DataDeleteCart
import id.co.leholeh.mobile.customer.model.cart.DataItemCart
import id.co.leholeh.mobile.customer.model.cart.ProductsItemCart
import id.co.leholeh.mobile.customer.model.cart.ResponseListCart
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.checkout.CheckoutActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_cart_produk.view.*
import kotlinx.android.synthetic.main.item_list_checkout.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// Updated
class KeranjangActivity : AppCompatActivity() {
    private lateinit var binding: ActivityKeranjangBinding
    private lateinit var keranjangAdapter: AdapterUtil<DataItemCart>
    private lateinit var perMerchantAdapter: AdapterUtil<ProductsItemCart>
    private lateinit var cacheUtil: CacheUtil

    private val viewModel: KeranjangViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(KeranjangViewModel::class.java)
    }

//    private var productList = arrayListOf<ProductsItemCart>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // init binding
        binding = ActivityKeranjangBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        // setupToolbar
        setupToolbar()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            binding.rlKeranjang.visibility = View.VISIBLE

            // init adapter
            setData()

            binding.btBeli.setOnClickListener {
                val intent = Intent(this, CheckoutActivity::class.java)
                startActivity(intent)
            }
        } else {
            binding.ivDataKosong.layoutEmptyView.visibility = View.VISIBLE
            binding.ivDataKosong.imEmptyView.setImageResource(R.drawable.ic_empty_cart)
            binding.ivDataKosong.tvEmptyView.text = getString(R.string.keranjang_kosong)
            binding.rlKeranjang.visibility = View.GONE
        }
    }

    //init toolbar
    private fun setupToolbar() = supportActionBar?.apply {
        this.title = "Keranjang Saya"
        this.setDisplayHomeAsUpEnabled(true)
        this.setDisplayShowHomeEnabled(true)
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        var qty: Int
        var qtyTotal: Int
//        var hargaTotal = 0.0

        val auth = getAuth(cacheUtil)
        val arrCategories = arrayListOf<String>()

        with(viewModel) {
            getListCart(auth.token!!)
            listCart.observe(this@KeranjangActivity, Observer {
                qtyTotal = 0
                qty = 0
                var totalHarga = 0.0

                if (it.isNullOrEmpty()) {
                    with(binding) {
                        ivDataKosong.layoutEmptyView.visibility = View.VISIBLE
                        binding.ivDataKosong.imEmptyView.setImageResource(R.drawable.ic_empty_cart)
                        binding.ivDataKosong.tvEmptyView.text = getString(R.string.keranjang_kosong)
                        rlKeranjang.visibility = View.GONE
                        tvQty.text = "Total (0)"
                        tvSubtotal.text = "-"
                    }
                } else {
                    with(binding) {
                        ivDataKosong.layoutEmptyView.visibility = View.GONE
                        rvKeranjang.visibility = View.VISIBLE
                    }

                    it.forEach { item -> totalHarga += item.total!! }
                    binding.tvSubtotal.text = changeToRupiah(totalHarga)

                    keranjangAdapter = AdapterUtil(
                        R.layout.item_list_checkout,
                        it,
                        { _, itemView, item ->
                            with(itemView) {
                                iv_icon_shipper.visibility = View.GONE
                                tv_harga_shipper.visibility = View.GONE
                                tv_nama_shipper.visibility = View.GONE
                                tv_opsi_pengiriman.visibility = View.GONE
                                separator.visibility = View.GONE

                                tv_merchant_name.text = item.marketName
                            }

                            perMerchantAdapter = AdapterUtil(
                                R.layout.item_cart_produk,
                                item.products!!,
                                { _, v, product ->
                                    with(v) {
                                        tv_namacart_produk.text = product.productName
                                        qty = product.qty!!.toInt()

                                        qtyTotal += qty
                                        v.tv_quantity.text = qty.toString()
                                    }

                                    //delete item
                                    v.imDeleteItem.setOnClickListener {
                                        showDeleteDialog(auth.token,product.id.toString(),product.productName!!)
                                    }

                                    for (j in product.categories!!.indices)
                                        arrCategories.add(product.categories[j].category!!)

                                    arrCategories.forEach { cat ->
                                        v.tv_kategori_cartproduk.text = "$cat "
                                    }

                                    binding.tvQty.text = StringBuilder().append("Total (")
                                        .append(qtyTotal.toString()).append("pcs )")

                                    Glide.with(this@KeranjangActivity)
                                        .load(product.uri)
                                        .into(v.im_cart_produk)

                                    val hargaDiskon =
                                        product.priceSelling!!.toDouble() - (product.priceSelling.toDouble() * product.discount!!.toDouble() / 100.0)
                                    v.tv_cart_harga.text = changeToRupiah(hargaDiskon)

                                    // button min & plus
                                    v.bt_min.setOnClickListener {
                                        var qtySekarang = v.tv_quantity.text.toString().toInt()

                                        qtySekarang -= 1

                                        // Handler Mines Button
                                        if (v.tv_quantity.text.toString().toDouble() <= 1) {
                                            with(v.bt_min) {
                                                isEnabled = false
                                                setBackgroundResource(
                                                    R.drawable.shape_indicator_inactive
                                                )
                                                setTextColor(
                                                    ContextCompat.getColor(
                                                        applicationContext, R.color.colorBlack
                                                    )
                                                )
                                            }
                                            setDelete(auth.token, product.id.toString())

                                        } else {
                                            //set Qty Change
                                            setUpdateQty(
                                                auth.token,
                                                qtySekarang,
                                                product.id.toString()
                                            )
                                        }
                                    }

                                    v.bt_plus.setOnClickListener {
                                        var qtySekarang = v.tv_quantity.text.toString().toInt()
                                        qtySekarang += 1
////                                            qtyTotal += 1
//                                            v.tv_quantity.text = qty.toString()
//                                            binding.tvQty.text =
//                                                StringBuilder("Total (").append(qtyTotal.toString())
//                                                    .append(" pcs)")
//                                            //harga total
//                                            hargaTotal += hargaDiskon
                                        //subtotal
//                                            binding.tvSubtotal.text = changeToRupiah(hargaTotal)

                                        //set Qty Change
                                        setUpdateQty(auth.token, qtySekarang, product.id.toString())

                                        //Handler Plus Button
                                        if (v.tv_quantity.text.toString().toDouble() > 0) {
                                            with(v.bt_min) {
                                                isEnabled = true
                                                setBackgroundResource(
                                                    R.drawable.shape_indicator_active
                                                )
                                                setTextColor(
                                                    ContextCompat.getColor(
                                                        applicationContext, R.color.colorWhite
                                                    )
                                                )
                                            }
                                        }
                                    }
                                },
                                { _, _ -> })

                            with(itemView.rv_item_checkouts) {
                                layoutManager = LinearLayoutManager(this@KeranjangActivity)
                                adapter = perMerchantAdapter
                            }

//                                perMerchantAdapter.data = item.products
                        },
                        { _, _ -> })

                    with(binding.rvKeranjang) {
                        layoutManager = LinearLayoutManager(this@KeranjangActivity)
                        adapter = keranjangAdapter
                    }

                    keranjangAdapter.data = it
                }
            })
        }
    }

    //Init UI
//    @SuppressLint("SetTextI18n")
//    private fun initUI() {
//        var qtyTotal = 0
//        var hargaTotal = 0.0
//
//        val auth = getAuth(cacheUtil)
//        viewModel.getListCart(auth.token!!)
//        viewModel.listCart.observe(this, Observer {
//            Log.d("TEST_GAN", it.toString())
//
//            if (it.isNullOrEmpty()) {
//                with(binding) {
//                    ivDataKosong.visibility = View.VISIBLE
//                    rlKeranjang.visibility = View.GONE
//                    tvQty.text = "Total (0)"
//                    tvSubtotal.text = "-"
//                }
//            } else {
//                //tampung qty Total
//                with(binding) {
//                    ivDataKosong.visibility = View.GONE
//                    rlKeranjang.visibility = View.VISIBLE
//                }

    //subtotal
//                for (i in it.indices) {
//                    hargaTotal += it[i].total!!.toDouble()
//                    keranjangAdapter.data = it[i].products!!
//
//                    for (j in it[i].products!!.indices) {
//                        qtyTotal += it[i].products?.get(j)?.qty!!.toInt()
//                    }
//                }
//
//                with(binding) {
//                    tvSubtotal.text = changeToRupiah(hargaTotal)
//                    tvQty.text =
//                        StringBuilder("Total (").append(qtyTotal.toString()).append(" pcs)")
//                }
//            }
//        })

//        keranjangAdapter =
//            AdapterUtil(
//                R.layout.item_cart_produk,
//                arrayListOf(),
//                { _, itemView, item ->
//
//                    itemView.tv_kategori_cartproduk.text = "Makanan, cemilan"
//                    itemView.tv_namacart_produk.text = item.productName
//                    Glide.with(this).load(item.uri).into(itemView.im_cart_produk)
//
//                    var qty = item.qty!!.toInt()
//                    itemView.tv_qty.text = qty.toString()
//
//                    // diskon
//                    val hargaDiskon =
//                        item.priceSelling!!.toDouble() - (item.priceSelling.toDouble() * item.discount!!.toDouble() / 100.0)
//                    itemView.tv_cart_harga.text = changeToRupiah(hargaDiskon)
//
//                    // button min & plus
//                    itemView.bt_min.setOnClickListener {
//                        qty -= 1
//                        qtyTotal -= 1
//                        itemView.tv_qty.text = qty.toString()
//                        binding.tvQty.text =
//                            StringBuilder("Total (").append(qtyTotal.toString()).append(" pcs)")
//
//                        //total harga change
//                        //harga total
//                        hargaTotal -= hargaDiskon
//
//                        //subtotal
//                        binding.tvSubtotal.text = changeToRupiah(hargaTotal)
//
//                        // Handler Mines Button
//                        if (itemView.tv_qty.text.toString().toDouble() <= 0) {
//                            with(itemView.bt_min) {
//                                isEnabled = false
//                                setBackgroundResource(R.drawable.shape_indicator_inactive)
//                                setTextColor(
//                                    ContextCompat.getColor(
//                                        applicationContext,
//                                        R.color.colorBlack
//                                    )
//                                )
//                            }
//                            setDelete(auth.token, item.id.toString())
//
//                        } else {
//                            //set Qty Change
//                            setUpdateQty(auth.token, qty, item.id.toString())
//                        }
//                    }
//
//                    itemView.bt_plus.setOnClickListener {
//                        qty += 1
//                        qtyTotal += 1
//                        itemView.tv_qty.text = qty.toString()
//                        binding.tvQty.text =
//                            StringBuilder("Total (").append(qtyTotal.toString())
//                                .append(" pcs)")
//
//                        //harga total
//                        hargaTotal += hargaDiskon
//                        //subtotal
//                        binding.tvSubtotal.text = changeToRupiah(hargaTotal)
//
//                        //set Qty Change
//                        setUpdateQty(auth.token, qty, item.id.toString())
//
//                        //Handler Plus Button
//                        if (itemView.tv_qty.text.toString().toDouble() > 0) {
//                            with(itemView.bt_min) {
//                                isEnabled = true
//                                setBackgroundResource(R.drawable.shape_indicator_active)
//                                setTextColor(
//                                    ContextCompat.getColor(
//                                        applicationContext,
//                                        R.color.colorWhite
//                                    )
//                                )
//                            }
//                        }
//                    }
//                },
//                { _, _ -> })
//
//        with(binding.rvKeranjang) {
//            layoutManager = LinearLayoutManager(this@KeranjangActivity)
//            adapter = keranjangAdapter
//        }
//    }

    //Update QTY
    private fun updateQty(
        auth: String,
        jsonObject: JsonObject,
        id: String,
        onSuccess: (ResponseListCart) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseListCart> =
            ApiService().serviceCart.updateQty(auth, jsonObject, id)
        call.enqueue(object : Callback<ResponseListCart> {
            override fun onFailure(call: Call<ResponseListCart>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<ResponseListCart>,
                response: Response<ResponseListCart>
            ) {
                Log.d("QTY_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }

    private fun setUpdateQty(auth: String, qty: Int, id: String) {
        //onChangeQty
        val jsonObject = JsonObject()
        jsonObject.addProperty("qty", qty)

        updateQty(auth, jsonObject, id, {
            if (it.code == 200) {
                Log.d("CART.setUpdateQty", "Berhasil Update Qty Menjadi :  $qty")
                viewModel.getListCart(auth)
            } else Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    //Delete Cart
    private fun deleteCart(
        auth: String,
        id: String,
        onSuccess: (DataDeleteCart) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<DataDeleteCart> =
            ApiService().serviceCart.deleteCart(auth, id)
        call.enqueue(object : Callback<DataDeleteCart> {
            override fun onFailure(call: Call<DataDeleteCart>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<DataDeleteCart>,
                response: Response<DataDeleteCart>
            ) {
                Log.d("DELETE_CART", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }

    private fun setDelete(auth: String, id: String) {
        deleteCart(auth, id, {
            if (it.code == 200) {
                Toast.makeText(this, "Berhasil Menghapus Data", Toast.LENGTH_LONG).show()
//                viewModel.getListCart(auth)
                setData()
            } else Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
        }, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    //set Dialog Hapus
    private fun showDeleteDialog(auth:String,id:String,namaBarang:String){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_delete_product_cart)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogDeleteProductCartBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()

        //Text Barang
        binding.tvMessage.text= StringBuilder("Anda yakin ingin menghapus barang $namaBarang")

        binding.tvDelete.setOnClickListener {
            setDelete(auth, id)
            dialog.dismiss()
        }

        //Cancel Button
        binding.tvCancel.setOnClickListener {
            dialog.dismiss()
        }



    }
}