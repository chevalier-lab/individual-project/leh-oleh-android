package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentMerchantSubmitBarangBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.barang.*
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_grid_photo.view.*
import kotlinx.android.synthetic.main.item_list_text_category.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*
import java.util.*


class MerchantSubmitBarangFragment : Fragment() {
    private lateinit var binding : FragmentMerchantSubmitBarangBinding
    private lateinit var adapterPhoto: AdapterUtil<Uri>
    private lateinit var adapterTextCategory: AdapterUtil<String>
    private lateinit var adapterTextLocation: AdapterUtil<String>
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private lateinit var listUri: ArrayList<Uri>
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =FragmentMerchantSubmitBarangBinding.inflate(inflater,container,false)
        // Inflate the layout for this fragment
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            auth = getAuth(cacheUtil)
        }
        return binding.root
    }

    fun setDataBarangToForm(barangTemp : BarangTemporary){
        setUpInformasiBarang(barangTemp)
        setUpPhotoBarang(barangTemp)
        setKategoriBarang(barangTemp)
        setLokasiBarang(barangTemp)
    }

    fun submitDataBarang(barangTemp: BarangTemporary){
        sendRequestCreateProductMerchant(barangTemp)
    }

    private fun setUpInformasiBarang(barangTemp : BarangTemporary){
        Glide.with(requireContext()).load(barangTemp.cover).into(binding.imageViewCover)
        binding.etNamabarang.setText(barangTemp.namaBarang)
        binding.etQty.setText(barangTemp.jumlahStok)
        binding.etHargaModal.setText(barangTemp.hargaModal)
        binding.etHargaJual.setText(barangTemp.hargaJual)
        binding.etDeskripsi.setText(barangTemp.deskripsi)
        binding.etDiskon.setText(barangTemp.diskon)
        binding.etLength.setText(barangTemp.length)
        binding.etWidth.setText(barangTemp.width)
        binding.etHeight.setText(barangTemp.height)
        binding.etWeight.setText(barangTemp.weight)
    }

    private fun setLokasiBarang(barangTemp: BarangTemporary){
        adapterTextLocation = AdapterUtil(
            R.layout.item_list_text_category,
            barangTemp.lokasiString as ArrayList,
            { _, itemview, item ->
                itemview.tv_text_category.text = item
            },
            { _, _ ->

            })
        binding.recyclerLokasi.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerLokasi.adapter = adapterTextLocation
    }
    private fun setKategoriBarang(barangTemp: BarangTemporary){
        adapterTextCategory = AdapterUtil(
            R.layout.item_list_text_category,
            barangTemp.kategoriString as ArrayList,
            { _, itemview, item ->
                itemview.tv_text_category.text = item
            },
            { _, _ ->

            })
        binding.recyclerKategori.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerKategori.adapter = adapterTextCategory
    }

    private fun setUpPhotoBarang(barangTemp: BarangTemporary){
        adapterPhoto = AdapterUtil(
            R.layout.item_grid_photo,
            arrayListOf(),
            { _, itemView, item ->
                itemView.imDeletePhoto.visibility = View.GONE
                Glide.with(requireContext()).load(item).into(itemView.imageViewProductPhoto)
            },
            { _, _ ->

            })
        if(barangTemp.photoProduct?.size!! > 0) adapterPhoto.data = barangTemp.photoProduct!!
        listUri = barangTemp.photoProduct!!
        binding.recyclerPhoto.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerPhoto.adapter = adapterPhoto
    }


    private fun sendRequestAddPhotoProduct(barangTemp: BarangTemporary, id : Int){
        try {
            for (i in 0 until barangTemp.photoProduct!!.size){
                val parcelFileDescriptor: ParcelFileDescriptor =
                    requireContext().contentResolver.openFileDescriptor(barangTemp.photoProduct!![i], "r")!!
                val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                val imageBitmap: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                val photoFile = createTempFile(imageBitmap)
                val photoProfile = MultipartBody.Part.createFormData(
                    "photo",
                    photoFile!!.name,
                    photoFile.asRequestBody("image/*".toMediaTypeOrNull())
                )
                val partIdProduct =
                    MultipartBody.Part.createFormData("id_u_product", id.toString())
                addPhotosProduct(photoProfile, partIdProduct, {
                    if (it.code == 200) {
                        if(i==barangTemp.photoProduct!!.size - 1){
                            Toast.makeText(
                                requireContext(),
                                "Data berhasil ditambahkan",
                                Toast.LENGTH_SHORT
                            ).show()
                            requireActivity().finish()
                        }
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Gagal " + it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }, {
                    Toast.makeText(
                        requireContext(),
                        it,
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.v("Cover_Upload", it)

                })

                parcelFileDescriptor.close()
            }
        }catch (e : Exception){
            e.printStackTrace()
        }
    }

    private fun addPhotosProduct(
        photos: MultipartBody.Part,
        idProduct: MultipartBody.Part,
        onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseAddPhotosProduct> =
            apiService.addPhotosProductMerchant(auth.token!!, photos, idProduct)
        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

//
//
//
    private fun sendRequestCreateProductMerchant(barangTemp: BarangTemporary) {
        val partName = MultipartBody.Part.createFormData("name", barangTemp.namaBarang!!)
        val slug = barangTemp.namaBarang!!.replace(" ", "-").toLowerCase(Locale.getDefault())
        val partSlug = MultipartBody.Part.createFormData("slug", slug)
        val partJumlah = MultipartBody.Part.createFormData("jumlah", barangTemp.jumlahStok!!)
        val partDiskon = MultipartBody.Part.createFormData("diskon", barangTemp.diskon!!)
        val partHargaJual = MultipartBody.Part.createFormData("harga_jual", barangTemp.hargaJual!!)
        val partHargaBeli = MultipartBody.Part.createFormData("harga_beli", barangTemp.hargaModal!!)
        val partDescription =
            MultipartBody.Part.createFormData("description", barangTemp.deskripsi!!)
        val partCategories =
            MultipartBody.Part.createFormData("categories", barangTemp.kategoriId.toString())
        val imageCover = MultipartBody.Part.createFormData(
            "cover",
            barangTemp.cover!!.name,
            barangTemp.cover!!.asRequestBody("image/*".toMediaTypeOrNull())
        )
        createProductMerchant(
            imageCover,
            partName,
            partSlug,
            partJumlah,
            partDiskon,
            partHargaJual,
            partHargaBeli,
            partDescription,
            partCategories,
            {
                if (it.code == 200) {
                    val idProduct = it.data
                    if (idProduct != null) {
                        Toast.makeText(requireContext(), "Memproses data...", Toast.LENGTH_SHORT)
                            .show()
                        sendRequestCreateLocationMerchant(barangTemp,idProduct)
                        val length = barangTemp.length!!
                        val width = barangTemp.width!!
                        val height = barangTemp.height!!
                        val weight = barangTemp.weight!!
                        sendRequestProductDimension(idProduct, length, width, height, weight)
                    }
                }
            },
            {
                Toast.makeText(requireContext(), it, Toast.LENGTH_LONG).show()
            })
    }

    private fun createProductMerchant(
        cover: MultipartBody.Part,
        name: MultipartBody.Part,
        slug: MultipartBody.Part,
        jumlah: MultipartBody.Part,
        diskon: MultipartBody.Part,
        hargaJual: MultipartBody.Part,
        hargaBeli: MultipartBody.Part,
        description: MultipartBody.Part,
        categories: MultipartBody.Part,
        onSuccess: (ResponseCreateProductMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseCreateProductMerchant> = apiService.createProductMerchant(
            auth.token!!,
            cover,
            name,
            slug,
            jumlah,
            diskon,
            hargaJual,
            hargaBeli,
            description,
            categories
        )
        call.enqueue(object : Callback<ResponseCreateProductMerchant> {
            override fun onResponse(
                call: Call<ResponseCreateProductMerchant>,
                response: Response<ResponseCreateProductMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateProductMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestCreateLocationMerchant(barangTemp: BarangTemporary, idProduct: Int) {
        Log.d("IDPRODUK", idProduct.toString())
        val jsonObject = JsonObject()
        for (i in barangTemp.lokasiId!!.indices) {
            jsonObject.addProperty("id_m_locations", barangTemp.lokasiId!![i])
            jsonObject.addProperty("id_u_product", idProduct.toString())

            createLocation(jsonObject, {
                if (it.code == 200) {
                    if(i == barangTemp.lokasiId!!.size-1){
                        sendRequestAddPhotoProduct(barangTemp, idProduct)
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }, {
                Toast.makeText(
                    requireContext(),
                    it,
                    Toast.LENGTH_SHORT
                ).show()
            })
        }
    }

    private fun createLocation(
        jsonObject: JsonObject,
        onSuccess: (ResponseCreateLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseCreateLocation> = apiService.createProductLocation(
            auth.token!!, jsonObject
        )
        call.enqueue(object : Callback<ResponseCreateLocation> {
            override fun onResponse(
                call: Call<ResponseCreateLocation>,
                response: Response<ResponseCreateLocation>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateLocation>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })

    }

    private fun createProductDimension(
        jsonObject: JsonObject,
        onSuccess: (ResponseCreateProductDimension) -> Unit,
        onFailed: (String) -> Unit
    ){
        val apiService : ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call : Call<ResponseCreateProductDimension> = apiService.createProductDimension(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseCreateProductDimension> {
            override fun onResponse(
                call: Call<ResponseCreateProductDimension>,
                responseCreate: Response<ResponseCreateProductDimension>
            ) {
                if (responseCreate.isSuccessful)
                    if (responseCreate.body() != null) onSuccess(responseCreate.body()!!)
                    else onFailed(responseCreate.message().toString())
                else onFailed(responseCreate.message().toString())
            }

            override fun onFailure(call: Call<ResponseCreateProductDimension>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestProductDimension(
        idProduct: Int,
        length: String,
        width: String,
        height: String,
        weight: String
    ){
        val jsonObject = JsonObject()
        jsonObject.apply {
            addProperty("id_u_product", idProduct)
            addProperty("length", length)
            addProperty("width", width)
            addProperty("height", height)
            addProperty("weight", weight)
        }
        createProductDimension(jsonObject,{
            if (it.code == 200){
                Toast.makeText(requireContext(), "Dimensi produk ditambahkan", Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(
                    requireContext(),
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
            }
        },{
            Toast.makeText(
                requireContext(),
                it,
                Toast.LENGTH_SHORT
            ).show()
        })
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }
}