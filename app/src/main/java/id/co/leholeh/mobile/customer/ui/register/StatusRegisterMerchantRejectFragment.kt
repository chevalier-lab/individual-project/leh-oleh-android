package id.co.leholeh.mobile.customer.ui.register

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import id.co.leholeh.R
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.merchant.ResponseDeleteReqMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.databinding.FragmentStatusRegisterMerchantRejectBinding
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StatusRegisterMerchantRejectFragment : Fragment() {

    private lateinit var binding : FragmentStatusRegisterMerchantRejectBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val cacheUtil = CacheUtil()
        cacheUtil.start(context as Activity, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(requireContext(), LoginActivity::class.java))
        }
        binding = FragmentStatusRegisterMerchantRejectBinding.inflate(inflater, container, false)
        binding.buttonDeleteReqMerchant.setOnClickListener {
            val auth = getAuth(cacheUtil)
            deleteRequestMerchant(auth.token!!,{
                if(it.code == 200){
                    Toast.makeText(
                        context,
                        getString(R.string.berhasil_menghapus_request),
                        Toast.LENGTH_SHORT
                    ).show()

                }
            },{
                Toast.makeText(context, it, Toast.LENGTH_LONG).show()
            })
        }
        return binding.root
    }

    private fun deleteRequestMerchant(
        authorization: String, onSuccess: (ResponseDeleteReqMerchant) -> Unit,
        onFailed: (String) -> Unit
    ){
        val call : Call<ResponseDeleteReqMerchant> = ApiService().serviceGuestMerchants.deleteRequestMerchant(authorization)
        call.enqueue(object : Callback<ResponseDeleteReqMerchant>{
            override fun onFailure(call: Call<ResponseDeleteReqMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseDeleteReqMerchant>,
                response: Response<ResponseDeleteReqMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }
}