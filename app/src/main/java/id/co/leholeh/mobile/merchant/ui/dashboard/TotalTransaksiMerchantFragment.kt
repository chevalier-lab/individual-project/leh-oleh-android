package id.co.leholeh.mobile.merchant.ui.dashboard

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentTotalTransaksiMerchantBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.dashboard.MonthsItem
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_total_transaksi_merchant.view.*
import java.text.SimpleDateFormat
import java.util.*

class TotalTransaksiMerchantFragment : Fragment() {
    private lateinit var binding: FragmentTotalTransaksiMerchantBinding
    private lateinit var adapter : AdapterUtil<MonthsItem>
    private val viewModel: DashboardMerchantViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(
            DashboardMerchantViewModel::class.java
        )
    }
    private lateinit var cacheUtil : CacheUtil
    private lateinit var auth : Login
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentTotalTransaksiMerchantBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)
        if (getAuth(cacheUtil).token!!.isNotEmpty()) {
            auth = getAuth(cacheUtil)
            getDataIncome()
        }else{
            startActivity(Intent(context, LoginActivity::class.java))
        }
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDataIncome(){
        val date = SimpleDateFormat("dd-MM-yyyy").format(Date())
        viewModel.getDataTotalTransaksi(auth.token!!, date)
        viewModel.totalTransaksi.observe(viewLifecycleOwner, Observer {
            adapter.data = it
        })
        adapter = AdapterUtil(
            R.layout.item_total_transaksi_merchant,
            arrayListOf(),
            { _, itemView, item ->
                if (!item.data!!.isNullOrEmpty()){
                    itemView.textViewTotalPendapatan.text = item.data.size.toString()
                }
                val bulan = SimpleDateFormat("MM", Locale("ID")).parse(item.month.toString())
                val bulanStr = SimpleDateFormat("MMMM", Locale("ID")).format(bulan!!)
                itemView.textViewBulan.text = bulanStr
            },
            { _, _ ->
            })
        binding.recyclerTotalTransaksi.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerTotalTransaksi.adapter = adapter
    }

}