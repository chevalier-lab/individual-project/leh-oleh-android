package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityListBankUserBinding
import id.co.leholeh.databinding.ComponentDialogAkunBankBinding
import id.co.leholeh.databinding.ComponentDialogEditBankBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.banks.BankAccount
import id.co.leholeh.mobile.customer.model.banks.ResponseRequestBankAccount
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.BanksService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_bank_account.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListBankUserActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListBankUserBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val viewModelBanks: BanksViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(BanksViewModel::class.java)
    }
    private lateinit var adapterListBank: AdapterUtil<BankAccount>
    private var idBank: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListBankUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            binding.floatingActionButton.setOnClickListener {
                showDialogCreateBankAccount()
            }
            getListBankAccountUser()
        }

    }

    //show dialog

    private fun showDialogCreateBankAccount() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_akun_bank)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogAkunBankBinding.inflate(layoutInflater)
        viewModelBanks.getDataListBankName(auth.token!!)
        viewModelBanks.listBanksName.observe(this, Observer { dataBank ->
            val listItemBanks: ArrayList<String> = arrayListOf()
            for (item in dataBank.indices) {
                listItemBanks.add(dataBank[item].bankName!!)
            }
            val adapterBank =
                ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, listItemBanks)
            binding.dropdownBank.setAdapter(adapterBank)

            binding.dropdownBank.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(
                    s: CharSequence?, start: Int, count: Int, after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    for (item in dataBank.indices) {
                        if (binding.dropdownBank.text.toString() == dataBank[item].bankName) {
                            idBank = dataBank[item].id
                        }
                    }
                }

            })

            //binding batal
            binding.tvBatal.setOnClickListener {
                dialog.hide()
            }

            //submit create bank account
            binding.buttonSubmit.setOnClickListener {
                val accountNumber = binding.etNamaPemilik.text.toString()
                val accountName = binding.etNorekening.text.toString()
                val bankName = binding.dropdownBank.text.toString()
                var status = ""
                when {
                    binding.rbActive.isChecked -> {
                        status = "1"
                    }
                    binding.rbNonActive.isChecked -> {
                        status = "0"
                    }
                }

                if (accountName.isEmpty() || accountNumber.isEmpty() || bankName.isEmpty()) {
                    Toast.makeText(this, "Harap isi semua data", Toast.LENGTH_SHORT).show()
                } else {
                    val jsonObject = JsonObject()
                    Log.d("ID_BANK_NAME", idBank!!)
                    jsonObject.apply {
                        addProperty("id_m_banks", idBank)
                        addProperty("account_number", binding.etNorekening.text.toString())
                        addProperty("account_name", binding.etNamaPemilik.text.toString())
                        addProperty("is_visible", status)
                    }
                    requestCreateBankAccount(jsonObject, {
                        if (it.code == 200) {
                            Toast.makeText(this, "Berhasil menambahkan akun!", Toast.LENGTH_SHORT)
                                .show()
                            getListBankAccountUser()
                        }
                    }, {
                        Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
                    })
                    dialog.dismiss()
                }

            }
        })

        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = getString(R.string.list_banks)
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun requestCreateBankAccount(
        jsonObject: JsonObject,
        onSuccess: (ResponseRequestBankAccount) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val serviceBank: BanksService by lazy {
            ApiService().createService(BanksService::class.java)
        }
        val call: Call<ResponseRequestBankAccount> =
            serviceBank.requestBankAccount(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseRequestBankAccount> {
            override fun onFailure(call: Call<ResponseRequestBankAccount>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRequestBankAccount>,
                response: Response<ResponseRequestBankAccount>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }


        })
    }


    private fun getListBankAccountUser() {
        viewModelBanks.getDataListBankAccount(auth.token!!)
        viewModelBanks.listBanksAccount.observe(this, Observer { it ->

            if (it.isEmpty()) {
                binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
                binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_bank)
                binding.emptyView.tvEmptyView.text = getString(R.string.kamu_belum_memiliki_akun_bank_terdaftar)
                binding.recyclerBankAccount.visibility = View.GONE
            } else {
                binding.emptyView.layoutEmptyView.visibility = View.GONE
                binding.recyclerBankAccount.visibility = View.VISIBLE
                adapterListBank = AdapterUtil(
                    R.layout.item_bank_account,
                    it.sortedByDescending { it.isVisible },
                    { position, itemView, item ->

                        itemView.textViewAccountNumber.text = item.accountNumber
                        itemView.textViewBankAccountName.text = item.accountName
                        itemView.textViewBankName.text = item.bankName
                        var statusBank = ""
                        when (item.isVisible) {
                            "0" -> {
                                statusBank = "NON AKTIF"
                            }
                            "1" -> {
                                statusBank = "AKTIF"
                            }
                            "2" -> {
                                statusBank = "PENDING"
                            }
                        }
                        itemView.tv_status.text = statusBank

                        if (item.isVisible == "0") {
                            itemView.textViewAccountNumber.setTextColor(resources.getColor(R.color.colorGreyNonActive))
                            itemView.textViewBankAccountName.setTextColor(resources.getColor(R.color.colorGreyNonActive))
                            itemView.textViewBankName.setTextColor(resources.getColor(R.color.colorGreyNonActive))
                        }


                    },
                    { position, item ->
                        //intent
                        editDeleteBank(item)
                    })
                binding.recyclerBankAccount.layoutManager =
                    LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
                binding.recyclerBankAccount.adapter = adapterListBank
            }

        })

    }

    private fun editDeleteBank(item: BankAccount) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_edit_bank)
        val layoutInflater = LayoutInflater.from(this)
        //binding here
        val binding = ComponentDialogEditBankBinding.inflate(layoutInflater)
        binding.etNamaPemilik.setText(item.accountName.toString())
        binding.etNorekening.setText(item.accountNumber.toString())

        when (item.isVisible) {
            "0" -> {
                binding.rbNonActive.isChecked = true
            }
            "1" -> {
                binding.rbActive.isChecked = true

            }
        }
        val listItemBankEdit = arrayListOf<String>()
        viewModelBanks.listBanksName.observe(this, Observer { dataBank ->
            for (i in dataBank.indices) {
                listItemBankEdit.add(dataBank[i].bankName!!)
            }
            val adapterBank =
                ArrayAdapter(
                    applicationContext,
                    R.layout.item_list_dropdown_text,
                    listItemBankEdit
                )
            binding.dropdownBank.setAdapter(adapterBank)
        })
        binding.dropdownBank.setText(item.bankName.toString())
        //submit create bank account
        binding.buttonSubmit.setOnClickListener {
            if (binding.etNamaPemilik.text.toString() == "" || binding.etNorekening.text.toString() == "" || binding.dropdownBank.text.toString() == "") {
                Toast.makeText(
                    this,
                    "Lengkapi Data",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                val jsonObject = JsonObject()

                jsonObject.addProperty("id_m_banks", item.idMBanks)
                jsonObject.addProperty(
                    "account_number",
                    binding.etNorekening.text.toString()
                )
                jsonObject.addProperty(
                    "account_name",
                    binding.etNamaPemilik.text.toString()
                )
                var status = ""
                when {
                    binding.rbActive.isChecked -> {
                        status = "1"
                    }
                    binding.rbNonActive.isChecked -> {
                        status = "0"
                    }

                }
                jsonObject.addProperty("is_visible", status)

                requestEditBank(getAuth(cacheUtil).token!!, jsonObject, item.id!!, {
                    if (it.code == 200) {
                        Toast.makeText(this, "Berhasil " + it.message, Toast.LENGTH_LONG).show()
                        getListBankAccountUser()
                    } else {
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                }, {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                })
            }
            dialog.dismiss()
        }
        binding.tvBatal.setOnClickListener {
            dialog.hide()
        }
//        binding.buttonHapus.setOnClickListener {
//            setDeleteBank(item.id!!)
//            dialog.dismiss()
//        }
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }

    //Delete Bank
    private fun requestDeleteBank(
        auth: String,
        id: String,
        onSuccess: (ResponseRequestBankAccount) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseRequestBankAccount> =
            ApiService().serviceBank.deleteBank(auth, id)
        call.enqueue(object : Callback<ResponseRequestBankAccount> {
            override fun onFailure(call: Call<ResponseRequestBankAccount>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRequestBankAccount>,
                response: Response<ResponseRequestBankAccount>
            ) {
                Log.d("DELETE_BANK", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }

    //Get Delete Bank
    private fun setDeleteBank(id: String) {
        requestDeleteBank(getAuth(cacheUtil).token!!, id, {
            if (it.code == 200) {
                Toast.makeText(this, "Berhasil " + it.message, Toast.LENGTH_LONG).show()
                viewModelBanks.getDataListBankAccount(auth.token!!)
            } else {
                Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
            }
        }, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    //Edit Bank
    private fun requestEditBank(
        auth: String,
        jsonObject: JsonObject,
        id: String,
        onSuccess: (ResponseRequestBankAccount) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseRequestBankAccount> =
            ApiService().serviceBank.updateBank(auth, jsonObject, id)
        call.enqueue(object : Callback<ResponseRequestBankAccount> {
            override fun onFailure(call: Call<ResponseRequestBankAccount>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRequestBankAccount>,
                response: Response<ResponseRequestBankAccount>
            ) {
                Log.d("EDIT_BANK", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

        })
    }

    override fun onResume() {
        super.onResume()
        viewModelBanks.getDataListBankAccount(auth.token!!)
    }
}


