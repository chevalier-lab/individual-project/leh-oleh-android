package id.co.leholeh.mobile.customer.model.shipper.pickuprequest

import com.google.gson.annotations.SerializedName

data class ResponseCancelPickup(

	@field:SerializedName("data")
	val data: DataCancelPickup? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataCancelPickup(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
