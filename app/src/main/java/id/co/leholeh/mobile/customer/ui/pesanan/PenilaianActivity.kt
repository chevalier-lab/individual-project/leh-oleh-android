package id.co.leholeh.mobile.customer.ui.pesanan

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityPenilaianBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.orders.detail.ProductsOrder
import id.co.leholeh.mobile.customer.model.orders.review.DataReview
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Callback
import retrofit2.Response

class PenilaianActivity : AppCompatActivity() {
    lateinit var binding: ActivityPenilaianBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var idPesananData: String
    private lateinit var idProductKomplain: String
    private val listProduct: ArrayList<ProductsOrder> = arrayListOf()
    private lateinit var viewModel: PesananCustomerViewModel

    private val arrayStatus = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPenilaianBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Setup Toolbar
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Berikan Penilaian"

        //Setup Cache Util
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        //getdata Intent
        idPesananData = intent.getSerializableExtra(INTENT_DETAIL)!! as String
        idProductKomplain = intent.getSerializableExtra(INTENT_SEARCH)!! as String

        //initUI
        initUI()
    }

    fun initUI(){
        viewModel = ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(PesananCustomerViewModel::class.java)
        val auth = getAuth(cacheUtil)
        viewModel.getDetailPesanan(auth.token!!, idPesananData)
        viewModel.detailPesanan.observe(this, Observer {

            var status = ""
            binding.tvIdTransaksiPenilaian.text = it.transactionToken
            binding.tvHargaTransaksiPenilaian.text = changeToRupiah(it.totalPrice!!.toDouble())
            when (it.status) {
                "0" -> {
                    status = "BELUM DIBAYAR"
                    binding.tvStatusTransaksiPenilaian.setBackgroundResource(R.drawable.status_not_payment)
                }
                "1" -> {
                    status = "TERVERIFIKASI"
                    binding.tvStatusTransaksiPenilaian.setBackgroundResource(R.drawable.status_payment_verified)
                }
                "2" -> {
                    status = "DIPROSES"
                    binding.tvStatusTransaksiPenilaian.setBackgroundResource(R.drawable.status_diproses)
                }
                "3" -> {
                    status = "SELESAI"
                    binding.tvStatusTransaksiPenilaian.setBackgroundResource(R.drawable.status_selesai)
                }
                "4" -> {
                    status = "REFUND"
                    binding.tvStatusTransaksiPenilaian.setBackgroundResource(R.drawable.status_refund)
                }
            }

            arrayStatus.clear()
            arrayStatus.add(it.status.toString())
            binding.tvStatusTransaksiPenilaian.text = status

            //setup time
            val tanggal = it.updatedAt!!.substring(0, 10)
            val jam = it.updatedAt.substring(10)
            binding.tvTglTransaksiPenilaian.text = tanggal
            binding.tvJamTransaksiPenilaian.text = jam

            for (i in it.products!!.indices) {
                if (it.products[i].idUProduct == idProductKomplain) {
                    listProduct.add(it.products[i])
                    Log.v("DETAILPESANAN", "IUUSERProduct" + it.products[i].idUProduct.toString())

                    break
                }
            }

            var statusProduct = ""
            when (listProduct[0].status) {
                "0" -> {
                    statusProduct = "Belum Dibayar"
                    binding.tvStatusPenilaian.setBackgroundResource(R.drawable.status_not_payment)
                }
                "1" -> {
                    statusProduct = "Terverifikasi"
                    binding.tvStatusPenilaian.setBackgroundResource(R.drawable.status_payment_verified)
                }
                "2" -> {
                    statusProduct = "Diproses"
                    binding.tvStatusPenilaian.setBackgroundResource(R.drawable.status_diproses)
                }
                "3" -> {
                    statusProduct = "Selesai"
                    binding.tvStatusPenilaian.setBackgroundResource(R.drawable.status_selesai)
                }
                "4" -> {
                    statusProduct = "Refund"
                    binding.tvStatusPenilaian.setBackgroundResource(R.drawable.status_refund)
                }
            }
            binding.tvNamaProdukPenilaian.text = listProduct[0].productName

            binding.tvHargaProdukPenilaian.text =
                changeToRupiah(listProduct[0].subTotalPrice!!.toDouble())


            //Categories
            var categories = ""

            for (i in listProduct[0].categories!!.indices) {
                categories += if (i < listProduct[0].categories!!.size - 1) {
                    listProduct[0].categories!![i]!!.category + ", "
                } else {
                    listProduct[0].categories!![i]!!.category
                }
            }

            binding.tvKategoriProdukDetailPesanan.text = categories
            binding.tvJumlahProdukPenilaian.text =
                StringBuilder("Qty : ").append(listProduct[0].qty.toString())
            binding.tvStatusPenilaian.text = statusProduct
            Glide.with(this).load(listProduct[0].uri)
                .into(binding.imageViewProdukPenilaian)

        })

        binding.buttonKirimPenilaian.setOnClickListener {
            setPenilaian()
        }
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            android.R.id.home -> finish()
        }
        return true
    }

    //Komplain
    private fun requestPenilaian(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataReview) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: retrofit2.Call<DataReview> =
            ApiService().serviceOrder.getReviewOrders(auth, jsonObject)
        call.enqueue(object : Callback<DataReview> {
            override fun onResponse(
                call: retrofit2.Call<DataReview>,
                response: Response<DataReview>
            ) {
                Log.d("CHECKOUT", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }

            override fun onFailure(call: retrofit2.Call<DataReview>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    //Get Komplain
    private fun setPenilaian() {
        val jsonObject = JsonObject()
        if (binding.editTextTextUlasanPenilaian.text.toString() == "") {
            Toast.makeText(
                this,
                "Lengkapi Data",
                Toast.LENGTH_LONG
            ).show()
        } else {
            jsonObject.addProperty("id_u_user_transaction_products",listProduct[0].info!!.idUUserTransactionProducts)
            jsonObject.addProperty("rating",binding.ratingBarPenilaian.rating)
            jsonObject.addProperty("review",binding.editTextTextUlasanPenilaian.text.toString())
            jsonObject.addProperty("status",arrayStatus[0])

            requestPenilaian(getAuth(cacheUtil).token!!,jsonObject,{
                if(it.code==200){
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }else{
                    Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                }
            },{
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })
        }

    }

}