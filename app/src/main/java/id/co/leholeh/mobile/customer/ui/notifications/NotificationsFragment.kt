package id.co.leholeh.mobile.customer.ui.notifications


import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentNotificationsBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.notification.DataNotification
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import id.co.leholeh.mobile.utils.stateUtilWithEmptyView
import kotlinx.android.synthetic.main.item_notification.view.*

class NotificationsFragment : Fragment() {

    private lateinit var adapterNotification: PagedAdapterUtil<DataNotification>
    private lateinit var cacheUtil: CacheUtil
    private var auth: Login? = null

    private val viewModel: NotificationsViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(NotificationsViewModel::class.java)
    }

    private lateinit var binding: FragmentNotificationsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationsBinding.inflate(inflater, container, false)
        // Setup Cache
        cacheUtil = CacheUtil()
        cacheUtil.start(requireActivity(), ConstantAuth.PREFERENCES)


        if (!TextUtils.isEmpty(cacheUtil.get(ConstantAuth.AUTH))) {
            auth = getAuth(cacheUtil)
            initUI()
        } else {
            binding.emptyView.tvEmptyView.text = getString(R.string.anda_belum_login)
            binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_notifications_red)
            binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
            binding.progressBarNotification.visibility = View.GONE
            binding.recyclerNotification.visibility = View.GONE
        }

        return binding.root
    }

    fun initUI() {

        viewModel.setUpListNotfications(auth!!.token!!)
        observerNotifications()

        //init Adapter
        adapterNotification =
            PagedAdapterUtil(
                R.layout.item_notification,
                { _, itemView, item ->
                    itemView.tvJudulNotifikasi.text = item.title
                    itemView.tvDeskripsiNotifikasi.text = item.description
                    //setup time
                    val tanggal = item.updatedAt!!.substring(0, 10)
                    val jam = item.updatedAt.substring(10)
                    itemView.tvTglNotifikasi.text = tanggal
                    itemView.tvJamNotifikasi.text = jam
                },
                { _, _ ->
                    //intent
                })
        binding.recyclerNotification.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.recyclerNotification.adapter = adapterNotification




    }

    private fun observerNotifications(){

        viewModel.getDataListNotifications().observe(viewLifecycleOwner, Observer {
//            if (it.isEmpty()) {
//                binding.emptyView.tvEmptyView.text = getString(R.string.notifikasi_kosong)
//                binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_notifications_red)
//                binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
//                binding.recyclerNotification.visibility = View.GONE
//            } else {
//                binding.emptyView.layoutEmptyView.visibility = View.GONE
//                binding.recyclerNotification.visibility = View.VISIBLE
            adapterNotification.submitList(it)
//            }
        })

        viewModel.statusListNotification.observe(viewLifecycleOwner, Observer {
            stateUtilWithEmptyView(
                requireContext(),
                it,
                binding.progressBarNotification,
                binding.recyclerNotification,
                binding.emptyView.tvEmptyView,
                binding.emptyView.imEmptyView,
                binding.emptyView.layoutEmptyView,
                "Notifikasi Kosong",
                R.drawable.ic_notifications_red
            )

        })
    }


}

