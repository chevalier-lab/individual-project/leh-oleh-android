package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class ResponseBanner(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DataBanner? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)








