package id.co.leholeh.mobile.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import java.text.NumberFormat
import java.util.*

fun changeToRupiah(uang: Double): String {
    val localeID =  Locale("in", "ID")
    val numberFormat = NumberFormat.getCurrencyInstance(localeID)
    return numberFormat.format(uang).toString()
}
fun getAuth(cacheUtil: CacheUtil): Login =
    Gson().fromJson(cacheUtil.get(ConstantAuth.AUTH), Login::class.java)

fun stateUtilFragment(context:Context,apiStatus: ApiStatus, progressBar: ProgressBar, recyclerView: RecyclerView){
    Log.d("STATUS_HISTORY", apiStatus.toString())
    when (apiStatus) {
        ApiStatus.SUCCESS -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.LOADING -> {
            progressBar.visibility = View.VISIBLE
        }
        ApiStatus.EMPTY -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.EMPTY_BEFORE -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.EMPTY_AFTER -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.LOADED -> {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        ApiStatus.FAILED -> {
            progressBar.visibility = View.GONE
            Toast.makeText(context, "Jaringan bermasalah", Toast.LENGTH_SHORT)
                .show()
        }
        else -> {
            progressBar.visibility = View.GONE
            Toast.makeText(context, "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                .show()
        }
    }
}

fun stateUtilWithEmptyView(context:Context,apiStatus: ApiStatus, progressBar: ProgressBar, recyclerView: RecyclerView,tvEmptyView:TextView,imEmptyView:ImageView,layoutEmptyView:View,textEmptyView:String,imageEmpty:Int ){
    Log.d("STATUS_HISTORY", apiStatus.toString())
    when (apiStatus) {
        ApiStatus.SUCCESS -> {
            progressBar.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
        }
        ApiStatus.LOADING -> {
            progressBar.visibility = View.VISIBLE
        }
        ApiStatus.EMPTY -> {
            tvEmptyView.text = textEmptyView
            imEmptyView.setImageResource(imageEmpty)
            layoutEmptyView.visibility = View.VISIBLE
            recyclerView.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
        ApiStatus.EMPTY_BEFORE -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.EMPTY_AFTER -> {
            progressBar.visibility = View.GONE
        }
        ApiStatus.LOADED -> {
            layoutEmptyView.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
        ApiStatus.FAILED -> {
            progressBar.visibility = View.GONE
            Toast.makeText(context, "Jaringan bermasalah", Toast.LENGTH_SHORT)
                .show()
        }
        else -> {
            progressBar.visibility = View.GONE
            Toast.makeText(context, "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                .show()
        }
    }
}