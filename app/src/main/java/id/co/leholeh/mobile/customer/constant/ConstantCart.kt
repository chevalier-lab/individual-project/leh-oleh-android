package id.co.leholeh.mobile.customer.constant

object ConstantCart {
    const val BASE_URL_LIST_CART = "guest/cart"
    const val BASE_URL_ADD_CART = "guest/cart/pesan"
    const val BASE_URL_CHECKOUT_CART = "guest/cart/checkout"
    const val BASE_URL_QTY_CART = "guest/cart/product/{id}"
    const val BASE_URL_DELETE_CART = "guest/cart/{id}"
}