package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseTagihanTopup(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<TagihanTopup>? = null,

	@field:SerializedName("error")
	val error: List<Any?>? = null,

	@field:SerializedName("message")
	val message: String? = null
):Serializable

data class TagihanTopup(

	@field:SerializedName("balance_transfer")
	val balanceTransfer: String? = null,

	@field:SerializedName("balance_request")
	val balanceRequest: String? = null,

	@field:SerializedName("id_u_user_wallet")
	val idUUserWallet: String? = null,

	@field:SerializedName("wallet_name")
	val walletName: String? = null,

	@field:SerializedName("proof_id")
	val proofId: Any? = null,

	@field:SerializedName("token_payment")
	val tokenPayment: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("balance")
	val balance: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("wallet_description")
	val walletDescription: String? = null
):Serializable
