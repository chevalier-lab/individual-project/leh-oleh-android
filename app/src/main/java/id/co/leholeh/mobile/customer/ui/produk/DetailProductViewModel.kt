package id.co.leholeh.mobile.customer.ui.produk

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.product.DetailProduct
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class DetailProductViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    private val _detailProduct = MutableLiveData<DetailProduct>()
    val detailProduct: LiveData<DetailProduct>
        get() = _detailProduct

    private val apiServiceProduct = ApiService().service

    private suspend fun requestDetailProduct(id: String) {
        try {
            _status.postValue(ApiStatus.LOADING)
            _detailProduct.postValue(apiServiceProduct.getDetailProduct(id).detailProduct)
            Log.d("DATA_DETAIL", apiServiceProduct.getDetailProduct(id).detailProduct.toString())

            _status.postValue(ApiStatus.SUCCESS)

        } catch (e: Exception) {
            Log.d("DETAIL_PRODUCT", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }


    fun getDetailProduct(id: String) {
        viewModelScope.launch {
            requestDetailProduct(id)
        }
    }
}