package id.co.leholeh.mobile.merchant.model.dashboard

import com.google.gson.annotations.SerializedName

data class ComplainBanner(

	@field:SerializedName("total_waiting")
	val totalWaiting: Int? = null,

	@field:SerializedName("total_process")
	val totalProcess: Int? = null,

	@field:SerializedName("total_finish")
	val totalFinish: Int? = null,

	@field:SerializedName("total_verify")
	val totalVerify: Int? = null,

	@field:SerializedName("total_reject")
	val totalReject: Int? = null
)