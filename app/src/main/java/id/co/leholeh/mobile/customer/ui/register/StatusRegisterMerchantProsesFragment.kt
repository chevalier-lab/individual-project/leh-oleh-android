package id.co.leholeh.mobile.customer.ui.register

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import id.co.leholeh.databinding.FragmentStatusRegisterMerchantProsesBinding


class StatusRegisterMerchantProsesFragment : Fragment() {

    private lateinit var binding : FragmentStatusRegisterMerchantProsesBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStatusRegisterMerchantProsesBinding.inflate(inflater,container,false )

        return binding.root
    }

}