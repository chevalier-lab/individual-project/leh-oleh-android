package id.co.leholeh.mobile.merchant.model.setting

import com.google.gson.annotations.SerializedName

data class UpdateToko(

	@field:SerializedName("id_user_photo")
	val idUserPhoto: String? = null,

	@field:SerializedName("market_phone_number")
	val marketPhoneNumber: String? = null,

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_user")
	val idUUser: String? = null,

	@field:SerializedName("no_identity")
	val noIdentity: String? = null,

	@field:SerializedName("id_market_photo")
	val idMarketPhoto: String? = null,

	@field:SerializedName("market_address")
	val marketAddress: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id_identity_photo")
	val idIdentityPhoto: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null
)