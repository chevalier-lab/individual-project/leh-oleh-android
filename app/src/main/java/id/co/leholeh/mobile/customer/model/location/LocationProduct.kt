package id.co.leholeh.mobile.customer.model.location

import com.google.gson.annotations.SerializedName

data class LocationProduct(

    @field:SerializedName("lon")
    val lon: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("lat")
    val lat: String? = null,

    @field:SerializedName("province_name")
    val provinceName: String? = null
)