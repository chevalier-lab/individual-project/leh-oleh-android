package id.co.leholeh.mobile.merchant.ui.barang

import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantAturFotoBinding
import id.co.leholeh.databinding.ComponentDialogDeletePhotoBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.PhotosItem
import id.co.leholeh.mobile.merchant.model.barang.ResponseAddPhotosProduct
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_grid_photo.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class MerchantAturFotoActivity : AppCompatActivity() {
    private val viewModelKelolaBarang: KelolaBarangViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(KelolaBarangViewModel::class.java)
    }
    private lateinit var binding: ActivityMerchantAturFotoBinding
    private var id: Int? = null
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private lateinit var adapterPhoto: AdapterUtil<Uri>
    private var arrayPhotoDelete = arrayListOf<Int>()
    private var arrayTempPhoto = arrayListOf<PhotosItem>()
    private var arrayUri = arrayListOf<Uri>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantAturFotoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        //Id Prpduct
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            id = intent.getIntExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, 0)
            setupToolbar()

            binding.imAddGambar.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
            }
            //Show Photo
            setDataPhoto()

            //Delete Photo button
            binding.btSimpan.setOnClickListener {
                viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
                finish()
            }

        }
    }

    //Set Data Photo
    private fun setDataPhoto() {

        viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
        viewModelKelolaBarang.detailProduct.observe(this, Observer { photo ->
            for (element in photo.photos!!) {
                arrayTempPhoto.add(element)
            }
            for (i in arrayTempPhoto.indices) {
                arrayUri.add(arrayTempPhoto[i].uri!!.toUri())
            }
            if (arrayUri.size > 0) adapterPhoto.data = arrayUri

        })

        adapterPhoto = AdapterUtil(
            R.layout.item_grid_photo,
            arrayListOf(),
            { position, itemView, item ->

                Glide.with(this).load(item)
                    .into(itemView.imageViewProductPhoto)
                itemView.imDeletePhoto.setOnClickListener {
                    showDialogDeletePhoto(arrayTempPhoto[position].idUProductPhotos!!.toInt(),position)
                    adapterPhoto.refresh()
                    Log.v("TampungDelete", arrayPhotoDelete.toString())
                }
            },
            { position, item ->
            })

        binding.rvFoto.layoutManager = GridLayoutManager(this, 3)
        binding.rvFoto.adapter = adapterPhoto

    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Atur Foto Barang"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    //Hapus Location Product
    private fun removePhotoProduct(
        id: Int, onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseAddPhotosProduct> =
            apiService.removeProductPhotos(auth.token!!, id)

        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })
    }

    //Send Request Remove Product
    //Change Cover Product Function
    private fun sendRequestDeletePhoto(id: Int,position:Int) {

        removePhotoProduct(id, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                arrayTempPhoto.removeAt(position)
                arrayUri.removeAt(position)
//                finish()
                arrayUri.clear()
                arrayTempPhoto.clear()
                viewModelKelolaBarang.getDataDetailProduct(auth.token!!, this.id!!)
//                locationArray.removeAt(position)
                Log.d("Remove_photo", "Berhasil" + it.message!!)

            } else {
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("Remove_photo", "Else Atas" + it.message!!)

            }
        }, {
            Toast.makeText(
                this,
                it,
                Toast.LENGTH_SHORT
            ).show()

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image: Image = ImagePicker.getFirstImageOrNull(data)
            if (!image.equals(null)) {
                try {
                    val parcelFileDescriptor: ParcelFileDescriptor =
                        this.contentResolver.openFileDescriptor(image.uri, "r")!!
                    val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                    val imageBitmap: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                    val photoFile = createTempFile(imageBitmap)
                    val photoProfile = MultipartBody.Part.createFormData(
                        "photo",
                        photoFile!!.name,
                        photoFile.asRequestBody("image/*".toMediaTypeOrNull())
                    )
                    val partIdProduct =
                        MultipartBody.Part.createFormData("id_u_product", id.toString())

                    binding.progressBar.visibility = View.VISIBLE
                    Toast.makeText(
                        this,
                        "Memproses data..",
                        Toast.LENGTH_SHORT
                    ).show()
                    addPhotosProduct(photoProfile, partIdProduct, {
                        if (it.code == 200) {
                            arrayUri.add(image.uri)
                            adapterPhoto.refresh()
                            Toast.makeText(
                                this,
                                "Foto ditambahkan",
                                Toast.LENGTH_SHORT
                            ).show()
                            //UPDATE UI
                            binding.progressBar.visibility = View.GONE
                            arrayUri.clear()
                            arrayTempPhoto.clear()

                            viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
                        } else {
                            Toast.makeText(
                                this,
                                "Gagal " + it.message,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }, {
                        Toast.makeText(
                            this,
                            it,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.v("Cover_Upload", it)

                    })

                    parcelFileDescriptor.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun addPhotosProduct(
        photos: MultipartBody.Part,
        idProduct: MultipartBody.Part,
        onSuccess: (ResponseAddPhotosProduct) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call: Call<ResponseAddPhotosProduct> =
            apiService.addPhotosProductMerchant(auth.token!!, photos, idProduct)
        call.enqueue(object : Callback<ResponseAddPhotosProduct> {
            override fun onResponse(
                call: Call<ResponseAddPhotosProduct>,
                response: Response<ResponseAddPhotosProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseAddPhotosProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    //3 parameters, param 3 should be dinamyc array list
    private fun showDialogDeletePhoto(id:Int,position: Int) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.component_dialog_delete_photo)
        val layoutInflater = LayoutInflater.from(this)
        val binding = ComponentDialogDeletePhotoBinding.inflate(layoutInflater)
        binding.buttonSubmit.setOnClickListener {
            sendRequestDeletePhoto(id,position)
            dialog.hide()
        }
        binding.buttonHapus.setOnClickListener {
            dialog.hide()
        }

        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        dialog.show()
    }
}