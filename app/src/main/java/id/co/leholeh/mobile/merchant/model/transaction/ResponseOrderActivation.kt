package id.co.leholeh.mobile.merchant.model.transaction

import com.google.gson.annotations.SerializedName

data class ResponseOrderActivation(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataOrderActivation>? = null,

	@field:SerializedName("error")
	val error: List<String>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataOrderActivation(

	@field:SerializedName("shipper_agent_id")
	val shipperAgentId: Any? = null,

	@field:SerializedName("shipper_agent_name")
	val shipperAgentName: Any? = null,

	@field:SerializedName("shipper_order_id")
	val shipperOrderId: String? = null,

	@field:SerializedName("id_u_user_transaction_products")
	val idUUserTransactionProducts: Int? = null,

	@field:SerializedName("shipper_is_request_pickup")
	val shipperIsRequestPickup: Int? = null
)
