package id.co.leholeh.mobile.merchant.network

import id.co.leholeh.mobile.merchant.model.feedback.ResponseListTransactionComplain
import id.co.leholeh.mobile.merchant.model.feedback.ResponseListTransactionReview
import retrofit2.http.GET
import retrofit2.http.Header

interface FeedbackService {
    @GET
    suspend fun getListTransactionReview(@Header("Authorization") auth : String) : ResponseListTransactionReview

    @GET
    suspend fun getListTransactionComplain(@Header("Authorization") auth: String) : ResponseListTransactionComplain
}