package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName

data class ResponsePreviewMerchant(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: DetailMerchant? = null,

    @field:SerializedName("error")
    val error: List<Any?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)