package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.content.Context
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class PagerAdapterTambahBarang(
    private val context: Context,
    fragmentManager: FragmentManager,
    private val pages : List<Fragment>
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 5

    private var mCurrentFragment: Fragment? = null

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        if (mCurrentFragment != `object`)
            mCurrentFragment = `object` as Fragment
        super.setPrimaryItem(container, position, `object`)
    }

    override fun getItem(position: Int): Fragment = pages[position]
}