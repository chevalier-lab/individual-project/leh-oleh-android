package id.co.leholeh.mobile.merchant.ui.profile

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityUpdateKTPBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.constant.INTENT_SEARCH
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdatePhotoProfileMerchant
import id.co.leholeh.mobile.merchant.network.ProfileTokoService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class UpdateKTP : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateKTPBinding
    private lateinit var auth: Login
    private lateinit var cacheUtil: CacheUtil
    private val viewModel: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityUpdateKTPBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        //Cache Util
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        auth = getAuth(cacheUtil)

        //Toolbar
        setupToolbar()
        //CheckLocationNull
        locationNullCheck()

        //update Background photo
        binding.btUpdateKtp.setOnClickListener {
            ImagePicker.create(this)
                .single()
                .start()
        }
        viewModel.toko.observe(this, Observer { profileToko ->
            Glide.with(this).load(profileToko.identityUri).into(binding.imageViewFotoKtp)
        })
    }


    //check location null
    private fun locationNullCheck() {
        viewModel.getDataProfileToko(auth.token!!)
        viewModel.toko.observe(this, Observer { profileToko ->
            if (profileToko.location == null) {
                binding.tvAlert.visibility = View.VISIBLE
                binding.tvAlert.setOnClickListener {
                    startActivity(
                        Intent(
                            this,
                            MerchantLocationActivity::class.java
                        ).putExtra(INTENT_DETAIL, profileToko.id)
                            .putExtra(INTENT_SEARCH, auth.token)

                    )
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val imagePick: Image = ImagePicker.getFirstImageOrNull(data)
            try {
                val parcelFileDescriptor: ParcelFileDescriptor =
                    this.contentResolver.openFileDescriptor(imagePick.uri, "r")!!
                val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                val photoFile = createFileTemp(image)
                val photoProfile = MultipartBody.Part.createFormData(
                    "photo",
                    photoFile!!.name,
                    photoFile.asRequestBody("image/*".toMediaTypeOrNull())
                )

                setUpdatePhotoKtp(auth.token!!, photoProfile, {
                    if (it.code == 200) {
                        Toast.makeText(
                            this,
                            getString(R.string.berhasil_mengubah_foto),
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("Upload_photo", "Berhasil" + it.message!!)
                        startActivity(
                            Intent(
                                this,
                                MerchantTokoSayaActivity::class.java
                            )
                        )

                    } else {
                        Toast.makeText(
                            this,
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("Upload_photo", "Else Atas" + it.message!!)

                    }
                }, {
                    Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                    Log.d("Upload_photo", "Gagal $it")
                })
                parcelFileDescriptor.close()

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setUpdatePhotoKtp(
        authorization: String,
        photoProfile: MultipartBody.Part,
        onSuccess: (ResponseUpdatePhotoProfileMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseUpdatePhotoProfileMerchant> =
            ApiService().createService(ProfileTokoService::class.java)
                .updatePhotoIdentity(authorization, photoProfile)
        call.enqueue(object : Callback<ResponseUpdatePhotoProfileMerchant> {
            override fun onFailure(call: Call<ResponseUpdatePhotoProfileMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseUpdatePhotoProfileMerchant>,
                response: Response<ResponseUpdatePhotoProfileMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun createFileTemp(bitmap: Bitmap): File? {
        val file = File(
            this.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    override fun onResume() {
        super.onResume()
        viewModel.getDataProfileToko(
            auth.token!!
        )
    }

    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Ubah Foto KTP"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}