package id.co.leholeh.mobile.merchant.ui.barang

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.merchantcustomer.MerchantPreviewDataSource
import id.co.leholeh.mobile.merchant.model.barang.ProductItemMerchant
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class KelolaBarangDataSource (coroutineContext : CoroutineContext, private val auth:String) : PageKeyedDataSource<Int, ProductItemMerchant>() {
    private val apiService: ManageProductService by lazy {
        ApiService().createService(ManageProductService::class.java)
    }
    val apiState = MutableLiveData<ApiStatus>()
    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)


    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ProductItemMerchant>
    ) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiService.getListProductsMerchant(auth,
                    FIRST_PAGE
                )
                val responseItem = response.body()!!.data
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, null, FIRST_PAGE + 1)
                        }
                    }
                    else ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                    }
                }

            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d(MerchantPreviewDataSource.TAG, e.localizedMessage!!)
            }
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ProductItemMerchant>
    ) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiService.getListProductsMerchant(auth, params.key)
                val responseItem = response.body()!!.data
                val key = if(params.key > 0 ) params.key - 1 else -1
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                }

            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d(MerchantPreviewDataSource.TAG, e.localizedMessage!!)
            }
        }
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ProductItemMerchant>
    ) {
        scope.launch {
            try {
                apiState.postValue(ApiStatus.LOADING)
                val response = apiService.getListProductsMerchant(auth, params.key)
                val responseItem = response.body()!!.data
                val key = params.key + 1
                when{
                    response.isSuccessful ->{
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if(responseItem.isEmpty()){
                                apiState.postValue(ApiStatus.EMPTY_AFTER)
                            }else{
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                }

            }catch (e : Exception){
                apiState.postValue(ApiStatus.FAILED)
                Log.d(MerchantPreviewDataSource.TAG, e.localizedMessage!!)
            }
        }
    }

    companion object {
        const val FIRST_PAGE = 0
    }

}
