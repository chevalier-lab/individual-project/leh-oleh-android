package id.co.leholeh.mobile.merchant.constant

object ConstantMerchant {
    const val STATUS_PESANAN_PROCESS = "2"
    const val STATUS_PESANAN_PAYMENT_VERIFIED = "1"
    const val STATUS_PESANAN_NOT_PAYMENT = "0"
    const val STATUS_PESANAN_FINISH = "3"
    const val STATUS_PESANAN_REFUND = "4"
    const val STATUS_PESANAN = "status_pesanan"
    const val ID_PESANAN = "id_pesanan"
    const val DATA_PRODUK = "data_produk"
    const val VERIFIED_PESANAN = "verified_pesanan"
    const val IS_VERIFIED_PESANAN = true
}