package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName

data class DetailMerchant(

    @field:SerializedName("market_phone_number")
    val marketPhoneNumber: String? = null,

    @field:SerializedName("is_visible")
    val isVisible: String? = null,

    @field:SerializedName("id_u_user")
    val idUUser: String? = null,

    @field:SerializedName("id_market_photo")
    val idMarketPhoto: String? = null,

    @field:SerializedName("market_address")
    val marketAddress: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("market_name")
    val marketName: String? = null
)