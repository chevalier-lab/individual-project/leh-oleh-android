package id.co.leholeh.mobile.customer.model.userlocation

import com.google.gson.annotations.SerializedName

data class DataUserLocation(
    @field:SerializedName("code")
    val code: Int = 0,

    @field:SerializedName("data")
    val data: List<UserLocation> = listOf(),

    @field:SerializedName("error")
    val error: List<Any> = listOf(),

    @field:SerializedName("message")
    val message: String = ""
)