package id.co.leholeh.mobile.merchant.constant

object ConstantTransactionMerchant {
    const val BASE_URL_LIST_TRANSACTION_MERCHANT = "merchant/manage/transaction"
    const val BASE_URL_DETAIL_TRANSACTION_MERCHANT = "merchant/manage/transaction/{id}"
    const val BASE_URL_UPDATE_TRANSACTION_MERCHANT = "merchant/manage/transaction/{id}"
    const val BASE_URL_ORDER_ACTIVATION = "merchant/order-transaction/activation"
    const val BASE_URL_PICKUP_REQUEST = "merchant/order-transaction/pickup"
    const val BASE_URL_CREATE_ORDER = "merchant/order-transaction"
}