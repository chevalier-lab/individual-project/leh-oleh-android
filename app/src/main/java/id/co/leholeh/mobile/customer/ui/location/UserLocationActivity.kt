package id.co.leholeh.mobile.customer.ui.location

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityUserLocationBinding
import id.co.leholeh.databinding.ComponentAddUserLocationDialogBinding
import id.co.leholeh.databinding.ComponentDeleteUserLocationDialogBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.API_KEY_SHIPPER
import id.co.leholeh.mobile.customer.constant.INTENT_DETAIL
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.userlocation.DataUserLocation
import id.co.leholeh.mobile.customer.model.userlocation.UserLocation
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.GetLocationShipperViewModel
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_user_location.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserLocationActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUserLocationBinding
    private lateinit var userLocationAdapter: AdapterUtil<UserLocation>
    private lateinit var cacheUtil: CacheUtil

    private val viewModel: UserLocationViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(UserLocationViewModel::class.java)
    }

    private val viewModelLocation: GetLocationShipperViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(GetLocationShipperViewModel::class.java)
    }

    private var isActive = 0


    private var arrayIdProvinceLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdSuburbsLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdCitiesLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdAreaLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()

    private val itemProvinceName: ArrayList<String> = arrayListOf()
    val itemCitiesName: ArrayList<String> = arrayListOf()
    val itemAreasName: ArrayList<String> = arrayListOf()
    val itemSuburbsName: ArrayList<String> = arrayListOf()

    private lateinit var auth:Login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUserLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Init cacheUtil
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        auth = getAuth(cacheUtil)

        // Setup Toolbar
        setupToolbar()

        // Setup UI
        initUI()
    }

    @SuppressLint("SetTextI18n")
    private fun initUI() {



        loadData(auth.token!!)
        binding.fabAddLocation.setOnClickListener { showDialogCreateLocation(auth.token!!) }
    }

    @SuppressLint("SetTextI18n")
    private fun loadData(auth: String) {

        viewModel.status.observe(this, Observer {status->
            when (status) {
                ApiStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.layout.visibility = View.GONE
                }
                ApiStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    binding.layout.visibility = View.VISIBLE
                }
                ApiStatus.FAILED -> {
                    binding.progressBar.visibility = View.GONE
                    binding.tvNetworkError.visibility = View.VISIBLE
                }
            }
        })
            viewModel.getUserListLocation(auth)
            viewModel.listLocation.observe(this@UserLocationActivity, Observer {
                binding.progressBar.visibility = View.VISIBLE
                Log.v("DATALOKASI",it.data.toString())
                Log.v("DATALOKASI",it.toString())
                if (it.data.isEmpty()) {
                    binding.progressBar.visibility = View.GONE
                    binding.tvEmptyView.text = "Lokasi Kosong!"
                    binding.imEmptyView.setImageResource(R.drawable.ic_location)
                    binding.layoutEmptyView.visibility = View.VISIBLE
                    binding.rvUserLocation.visibility = View.GONE

                } else {
                    binding.progressBar.visibility = View.GONE
                    binding.layoutEmptyView.visibility = View.GONE
                    binding.rvUserLocation.visibility = View.VISIBLE


                    userLocationAdapter = AdapterUtil(
                        R.layout.item_user_location,
                        it.data,
                        { _, itemView, item ->
                            with(itemView) {
                                tv_city.text = item.city
                                tv_address.text = "${item.province}, ${item.postcode}"
                                tv_detail_address.text = item.address

                                if (item.is_active == "1") tv_active.visibility = View.VISIBLE
                                else tv_active.visibility = View.GONE

                                setOnLongClickListener {
                                    showDialogDeleteLocation(auth, item.address, item.id)
                                    return@setOnLongClickListener true
                                }
                            }
                        },
                        { _, item ->
                            startActivity(Intent(this,EditUserLocationActivity::class.java).putExtra(
                                INTENT_DETAIL,item.id))

                        }
                    )

                    with(binding.rvUserLocation) {
                        adapter = userLocationAdapter
                        layoutManager = LinearLayoutManager(this@UserLocationActivity)
                    }

                    userLocationAdapter.data = it.data
                }
            })

    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            with(this) {
                title = "Lokasi"
                setDisplayHomeAsUpEnabled(true)
                setDisplayShowHomeEnabled(true)
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun showDialogDeleteLocation(auth: String, name: String, id: String) {
        val dialog = Dialog(this)
        val layoutInflater = LayoutInflater.from(this)
        val binds = ComponentDeleteUserLocationDialogBinding.inflate(layoutInflater)

        with(dialog) {
            setContentView(binds.root)
            setCancelable(true)
            show()
        }

        binds.tvMessage.text = "Anda yakin ingin menghapus alamat $name?"

        binds.tvDelete.setOnClickListener {
            requestDeleteLocation(
                auth,
                id,
                {
                    if (it.code == 200) {
                        Snackbar.make(
                            binding.rvUserLocation,
                            "Berhasil menghapus lokasi!",
                            Snackbar.LENGTH_SHORT
                        )
                        viewModel.getUserListLocation(auth)

                    } else {
                        Log.i(TAG_DELETE_LOCATION, it.message)
                        Snackbar.make(
                            binding.rvUserLocation,
                            it.message,
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                },
                {
                    Log.i(TAG_DELETE_LOCATION, it)
                    Snackbar.make(binding.rvUserLocation, it, Snackbar.LENGTH_SHORT).show()
                }
            )

            loadData(auth)
            dialog.dismiss()
        }

        binds.tvCancel.setOnClickListener { dialog.dismiss() }
    }

    private fun showDialogCreateLocation(auth: String) {
        val dialog = Dialog(this)
        val layoutInflater = LayoutInflater.from(this)
        val binds = ComponentAddUserLocationDialogBinding.inflate(layoutInflater)

        with(dialog) {
            setContentView(binds.root)
            setCancelable(true)
            show()
        }

        binds.layoutStatus.visibility = View.GONE

        // Set JsonObject
        val jsonObject = JsonObject()

        // Set location from province
        locationShiiper(binds)

        binds.tvAdd.setOnClickListener {

            val provinsi = binds.dropdownProvinsi.text.toString()
            val cities = binds.dropdownKota.text.toString()
            val suburbs = binds.dropdownSuburb.text.toString()
            val area = binds.dropdownArea.text.toString()
            val kodePos = binds.dropdownKodepos.text.toString()
            val alamat = binds.etAlamat.text.toString()

            if (provinsi.isEmpty() || cities.isEmpty() || suburbs.isEmpty() || area.isEmpty() || kodePos.isEmpty() || alamat.isEmpty()) {
                Toast.makeText(this, "Isi data dengan lengkap!", Toast.LENGTH_SHORT).show()
            } else {
                val kodePos = binds.dropdownKodepos.text.toString()
                val alamat = binds.etAlamat.text.toString()

                jsonObject.apply {
                    addProperty("province_id", arrayIdProvinceLocation[0].id)
                    addProperty("province", arrayIdProvinceLocation[0].name)
                    addProperty("city_id", arrayIdCitiesLocation[0].id)
                    addProperty("city", arrayIdCitiesLocation[0].name)
                    addProperty("suburb_id", arrayIdSuburbsLocation[0].id)
                    addProperty("suburb", arrayIdSuburbsLocation[0].name)
                    addProperty("area_id", arrayIdAreaLocation[0].id)
                    addProperty("area", arrayIdAreaLocation[0].name)
                    addProperty("postcode", kodePos)
                    addProperty("address", alamat)
                    addProperty("is_active", isActive)
                }


                requestAddLocation(
                    auth,
                    jsonObject,
                    {
                        if (it.code == 200) {
                            Snackbar.make(
                                binding.fabAddLocation,
                                "Berhasil menambah lokasi!",
                                Snackbar.LENGTH_SHORT
                            ).show()
                            viewModel.getUserListLocation(auth)

                        } else {
                            Log.i(TAG_ADD_LOCATION, it.message)
                            Snackbar.make(
                                binding.fabAddLocation,
                                it.message,
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    },
                    {
                        Log.i(TAG_ADD_LOCATION, it)
                        Snackbar.make(binding.fabAddLocation, it, Snackbar.LENGTH_SHORT).show()
                    }
                )

                loadData(auth)
                dialog.dismiss()
            }
        }
        binds.tvCancel.setOnClickListener { dialog.dismiss() }
    }

    private fun requestAddLocation(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataUserLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        try {
            ApiService().service.postUserLocation(auth, jsonObject).enqueue(
                object : Callback<DataUserLocation> {
                    override fun onResponse(
                        call: Call<DataUserLocation>,
                        response: Response<DataUserLocation>
                    ) {
                        Log.i("User Location", response.body().toString())
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message()).toString()
                    }

                    override fun onFailure(call: Call<DataUserLocation>, t: Throwable) =
                        onFailed(t.message.toString())
                })
        } catch (e: Exception) {
            Log.i("User Location", "${e.message} --- ${e.printStackTrace()}")
        }
    }

    private fun requestDeleteLocation(
        auth: String,
        id: String,
        onSuccess: (DataUserLocation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        try {
            ApiService().service.deleteUserLocation(auth, id).enqueue(
                object : Callback<DataUserLocation> {
                    override fun onResponse(
                        call: Call<DataUserLocation>,
                        response: Response<DataUserLocation>
                    ) {
                        Log.i("User Current Location", response.body().toString())
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message()).toString()
                    }

                    override fun onFailure(call: Call<DataUserLocation>, t: Throwable) =
                        onFailed(t.message.toString())
                })
        } catch (e: Exception) {
            Log.i("User Current Location", "${e.message} --- ${e.printStackTrace()}")
        }
    }

    private fun locationShiiper(binding:ComponentAddUserLocationDialogBinding) {
        var idProvince: String
        var idSuburbs: String
        //province
        viewModel.getListProvince(API_KEY_SHIPPER)
        viewModel.listShipperProvince.observe(this, Observer { itemProvince ->
            itemProvinceName.clear()
            for (item in itemProvince) {
                itemProvinceName.add(item.name!!)
            }
            val adapterProvince =
                ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemProvinceName)
            binding.dropdownProvinsi.setAdapter(adapterProvince)
            binding.dropdownProvinsi.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    //cities
                    for (i in itemProvince.indices) {
                        if (binding.dropdownProvinsi.text.toString() == itemProvince[i].name) {
                            //get id province
                            arrayIdProvinceLocation.clear()
                            arrayIdProvinceLocation.add(
                                MerchantLocationActivity.PenampungLocation(
                                    itemProvince[i].id.toString(),
                                    itemProvince[i].name.toString()
                                )
                            )
                            Log.v("MerchantLocation", "Id Province : $arrayIdProvinceLocation")


                            viewModelLocation.getCities(itemProvince[i].id.toString())
                            idProvince = itemProvince[i].id.toString()
                            Log.d("IDProvincsi", "Dalam $idProvince")
                            viewModelLocation.cities.observe(
                                this@UserLocationActivity,
                                Observer { itemCities ->
                                    itemCitiesName.clear()
                                    //cleartext
                                    binding.dropdownKota.setText("")
                                    binding.dropdownArea.setText("")
                                    binding.dropdownSuburb.setText("")
                                    binding.dropdownKodepos.setText("")

                                    for (itCities in itemCities) {
                                        itemCitiesName.add(itCities.name!!)
                                    }
                                    val adapterCities =
                                        ArrayAdapter(
                                            applicationContext,
                                            R.layout.item_list_dropdown_text,
                                            itemCitiesName
                                        )
                                    binding.dropdownKota.setAdapter(adapterCities)
                                })

                            break
                        }
                    }
                }

            })
        })

        viewModelLocation.cities.observe(
            this@UserLocationActivity,
            Observer { itemCities ->
                binding.dropdownKota.addTextChangedListener(object :
                    TextWatcher {
                    override fun beforeTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun onTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        Log.d("IDProvincsi", "Citites $itemCities")
                        //surburbs
                        for (iSub in itemCities.indices) {
                            if (binding.dropdownKota.text.toString() == itemCities[iSub].name) {
                                arrayIdCitiesLocation.clear()
                                arrayIdCitiesLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemCities[iSub].id.toString(),
                                        itemCities[iSub].name.toString()
                                    )
                                )

                                Log.v("MerchantLocation", "Id Cities : $arrayIdCitiesLocation")



                                idSuburbs = itemCities[iSub].id.toString()
                                Log.d("IDProvincsi", "SUBURUBS ")
                                viewModelLocation.getSuburbs(idSuburbs)
                                viewModelLocation.suburbs.observe(
                                    this@UserLocationActivity,
                                    Observer { itemSuburbs ->
                                        itemSuburbsName.clear()
                                        //cleartext
                                        binding.dropdownArea.setText("")
                                        binding.dropdownSuburb.setText("")
                                        binding.dropdownKodepos.setText("")

                                        for (itSuburbs in itemSuburbs) {
                                            itemSuburbsName.add(itSuburbs.name!!)
                                        }
                                        val adapterSuburbs =
                                            ArrayAdapter(
                                                applicationContext,
                                                R.layout.item_list_dropdown_text,
                                                itemSuburbsName
                                            )
                                        binding.dropdownSuburb.setAdapter(
                                            adapterSuburbs
                                        )
                                    })
                                break
                            }
                        }
                    }
                })
            })


        viewModelLocation.suburbs.observe(
            this@UserLocationActivity,
            Observer { itemSuburbs ->
                binding.dropdownSuburb.addTextChangedListener(
                    object : TextWatcher {
                        override fun beforeTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun onTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun afterTextChanged(p0: Editable?) {
                            //Areas
                            for (iAreas in itemSuburbs.indices) {
                                if (binding.dropdownSuburb.text.toString() == itemSuburbs[iAreas].name) {

                                    arrayIdSuburbsLocation.clear()
                                    arrayIdSuburbsLocation.add(
                                        MerchantLocationActivity.PenampungLocation(
                                            itemSuburbs[iAreas].id.toString(),
                                            itemSuburbs[iAreas].name.toString()
                                        )
                                    )

                                    Log.v(
                                        "MerchantLocation",
                                        "Id Suburbs : $arrayIdSuburbsLocation"
                                    )

                                    viewModelLocation.getAreas(
                                        itemSuburbs[iAreas].id.toString()
                                    )
                                    viewModelLocation.areas.observe(
                                        this@UserLocationActivity,
                                        Observer { itemAreas ->
                                            itemAreasName.clear()
                                            //cleartext
                                            binding.dropdownArea.setText("")
                                            binding.dropdownKodepos.setText("")
                                            for (itAreas in itemAreas) {
                                                itemAreasName.add(
                                                    itAreas.name!!
                                                )
                                            }
                                            val adapterAreas =
                                                ArrayAdapter(
                                                    applicationContext,
                                                    R.layout.item_list_dropdown_text,
                                                    itemAreasName
                                                )
                                            binding.dropdownArea.setAdapter(
                                                adapterAreas
                                            )

                                        })
                                    break
                                }
                            }
                        }
                    })
            })

        viewModelLocation.areas.observe(
            this@UserLocationActivity,
            Observer { itemAreas ->
                binding.dropdownArea.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        for (i in itemAreas.indices) {
                            if (itemAreas[i].name == binding.dropdownArea.text.toString()) {
                                arrayIdAreaLocation.clear()
                                arrayIdAreaLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemAreas[i].id.toString(),
                                        itemAreas[i].name.toString()
                                    )
                                )
                                Log.v("MerchantLocation", "Id Areas : $arrayIdAreaLocation")
                                binding.dropdownKodepos.setText(itemAreas[i].postcode)
                                break
                            }
                        }
                    }

                })
            })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUserListLocation(auth.token!!)

    }

    companion object {
        private const val TAG_ADD_LOCATION = "ADD_USER_LOCATION"
        private const val TAG_DELETE_LOCATION = "DELETE_USER_LOCATION"
    }
}