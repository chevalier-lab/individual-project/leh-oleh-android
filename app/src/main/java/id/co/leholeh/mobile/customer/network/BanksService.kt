package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantBanks
import id.co.leholeh.mobile.customer.model.banks.*
import id.co.leholeh.mobile.customer.model.wallet.ResponseListBankAccount
import retrofit2.Call
import retrofit2.http.*

interface BanksService {
    @GET(ConstantBanks.BASE_URL_GET_BANKS_NAME)
    suspend fun getListBanksName(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): ResponseListBanksName

    @POST(ConstantBanks.BASE_URL_BANKS_ACCOUNT)
    fun requestBankAccount(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject
    ): Call<ResponseRequestBankAccount>

    @GET(ConstantBanks.BASE_URL_BANKS_ACCOUNT)
    suspend fun getListBankAccount(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int
    ): ResponseListBankAccount
    @GET(ConstantBanks.BASE_URL_DETAIL_BANKS)
    suspend fun getDetailBankAccount(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): ResponseDetailBankDompet

    @PUT(ConstantBanks.BASE_URL_UPDATE_BANKS)
    fun updateBank(
        @Header("Authorization") authorization: String,
        @Body jsonObject: JsonObject,
        @Path("id") id: String
    ): Call<ResponseRequestBankAccount>

    @DELETE(ConstantBanks.BASE_URL_DELETE_BANKS)
    fun deleteBank(
        @Header("Authorization") authorization: String,
        @Path("id") id: String
    ): Call<ResponseRequestBankAccount>
}