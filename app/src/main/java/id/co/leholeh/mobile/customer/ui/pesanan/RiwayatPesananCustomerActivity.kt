package id.co.leholeh.mobile.customer.ui.pesanan

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityRiwayatPesananCustomerBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.PESANAN_DATA
import id.co.leholeh.mobile.customer.model.orders.Order
import id.co.leholeh.mobile.customer.ui.cart.KeranjangActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_pesanan.view.*

class RiwayatPesananCustomerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRiwayatPesananCustomerBinding
    private lateinit var cacheUtil: CacheUtil
    private val viewModel: RiwayatPesananViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(RiwayatPesananViewModel::class.java)
    }
    private lateinit var adapterHistoriPesanan: PagedAdapterUtil<Order>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRiwayatPesananCustomerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)


        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Riwayat Pesanan"

        //After Login
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            initUI()
        } else {
            binding.emptyView.tvEmptyView.text = getString(R.string.anda_belum_login)
            binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_red_pesanan)
            binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
            binding.recyclerPesanan.visibility = View.GONE
        }
    }

    //Option Back
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_cart -> startActivity(Intent(this, KeranjangActivity::class.java))

        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    @SuppressLint("SetTextI18n")
    private fun getState(){
        viewModel.apiStatus.observe(this, Observer {
            Log.d("STATUS_PESANAN", it.toString())
            when (it) {

                ApiStatus.SUCCESS -> {

                }
                ApiStatus.LOADING -> {

                }
                ApiStatus.EMPTY -> {
                    binding.emptyView.tvEmptyView.text = "Pesanan Kosong!"
                    binding.emptyView.imEmptyView.setImageResource(R.drawable.ic_red_pesanan)
                    binding.emptyView.layoutEmptyView.visibility = View.VISIBLE
                    binding.recyclerPesanan.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {

                }
                ApiStatus.EMPTY_AFTER -> {

                }
                ApiStatus.LOADED -> {
                    binding.emptyView.layoutEmptyView.visibility = View.GONE
                    binding.recyclerPesanan.visibility = View.VISIBLE
                }
                ApiStatus.FAILED -> {
                    Toast.makeText(this, "Jaringan bermasalah", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    Toast.makeText(this, "Ada sesuau yang salah", Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    private fun observerListHistoryPesanan(){
        viewModel.getDataListHistoryPesanan().observe(this, Observer {
            adapterHistoriPesanan.submitList(it)
        })
        getState()
    }

    //Init UI
    @SuppressLint("SetTextI18n")
    private fun initUI() {
        val auth = getAuth(cacheUtil)
        adapterHistoriPesanan = PagedAdapterUtil(
            R.layout.item_pesanan,
            { _, itemView, item ->
                when (item.status) {
                    "3" -> {
                        itemView.textViewStatusTransaksi.text = getString(R.string.success)
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_selesai)
                    }
                    "4" -> {
                        itemView.textViewStatusTransaksi.text = getString(R.string.dikembalikan)
                        itemView.textViewStatusTransaksi.setBackgroundResource(R.drawable.status_refund)
                    }
                }
                itemView.textViewIdTransaksi.text = item.transactionToken
                itemView.textViewHargaTransaksi.text =
                    changeToRupiah(item.totalPrice!!.toDouble())

                val tanggal = item.updatedAt!!.substring(0, 10)
                val jam = item.updatedAt.substring(10)
                itemView.textViewTglTransaksi.text = tanggal
                itemView.textViewJamTransaksi.text = jam
            },
            { _, item ->
                //intent
                val intent = Intent(
                    this,
                    DetailPesananActivity::class.java
                )
                intent.putExtra(
                    PESANAN_DATA,
                    item.id
                )

                startActivity(intent)
            })
        binding.recyclerPesanan.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        viewModel.setupListHistoryPesanan(auth.token!!)
        observerListHistoryPesanan()
        binding.recyclerPesanan.adapter = adapterHistoriPesanan
    }
}