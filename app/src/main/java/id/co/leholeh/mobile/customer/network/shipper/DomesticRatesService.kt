package id.co.leholeh.mobile.customer.network.shipper

import id.co.leholeh.mobile.customer.constant.shipper.ConstantRates
import id.co.leholeh.mobile.customer.model.shipper.rates.RatesResponse
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface DomesticRatesService {
    @GET(ConstantRates.BASE_URL_DOMESTIC_ORDER)
    @Headers("User-Agent: Shipper/")
    suspend fun getDomesticRates(
        @Query("apiKey") apiKey: String,
        @Query("o") o: Int,
        @Query("d") d: Int,
        @Query("l") l: Float,
        @Query("w") w: Float,
        @Query("h") h: Float,
        @Query("wt") wt: Float,
        @Query("v") v: Int
    ): RatesResponse
}