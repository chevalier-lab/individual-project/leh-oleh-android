package id.co.leholeh.mobile.merchant.ui.feedback

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityMerchantKomplainBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.merchant.model.feedback.DataTransactionComplain
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_feedback_complain.view.*

class MerchantKomplainActivity : AppCompatActivity() {
    lateinit var binding :ActivityMerchantKomplainBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth: Login
    private val viewModel : FeedbackViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(FeedbackViewModel::class.java)
    }
    private lateinit var adapterKomplain : AdapterUtil<DataTransactionComplain>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityMerchantKomplainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)

        if(getAuth(cacheUtil).token!!.isNotEmpty()){
            auth= getAuth(cacheUtil)
            setupToolbar()
            getDataComplain()
        }

    }

    private fun getDataComplain(){
        viewModel.getDataListComplain(auth.token!!)
        viewModel.complain.observe(this, Observer {
            if(it != null){
                binding.emptyView.visibility = View.GONE
                adapterKomplain.data = it
            }else{
                binding.emptyView.visibility = View.VISIBLE
            }
        })

        adapterKomplain = AdapterUtil(R.layout.item_list_feedback_complain,
            arrayListOf(),
            {_, itemView, item->
                itemView.textViewSubjectComplain.text = item.subject
                itemView.textViewComplainStatus.text = checkComplainStatus(item.status!!)
                itemView.textViewComplainDescription.text = item.description
                itemView.textViewComplainSender.text = item.fullName
                itemView.textViewComplainDate.text = item.createdAt
            },{ _, _ ->

            })
        binding.recyclerKomplain.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerKomplain.adapter = adapterKomplain
    }

    private fun checkComplainStatus(statusPesananProduk : String) : String{
        when (statusPesananProduk) {
            ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED -> {
                return getString(R.string.payment_verified)
            }
            ConstantMerchant.STATUS_PESANAN_PROCESS -> {
                return getString(R.string.process)
            }
            ConstantMerchant.STATUS_PESANAN_FINISH -> {
                return getString(R.string.finish)
            }
            ConstantMerchant.STATUS_PESANAN_REFUND -> {
                return getString(R.string.refund)
            }
        }
        return ""
    }
    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Komplain"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}