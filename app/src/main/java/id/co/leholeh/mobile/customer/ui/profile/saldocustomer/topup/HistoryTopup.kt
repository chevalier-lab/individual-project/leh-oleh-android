package id.co.leholeh.mobile.customer.ui.profile.saldocustomer.topup

import com.google.gson.annotations.SerializedName

data class HistoryTopup(

    @field:SerializedName("balance_transfer")
    val balanceTransfer: String? = null,

    @field:SerializedName("balance_request")
    val balanceRequest: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("balance")
    val balance: String? = null,

    @field:SerializedName("id_u_user_wallet")
    val idUUserWallet: String? = null,

    @field:SerializedName("wallet_name")
    val walletName: String? = null,

    @field:SerializedName("proof_id")
    val proofId: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("wallet_description")
    val walletDescription: String? = null
)