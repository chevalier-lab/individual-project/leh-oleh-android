package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ResponseGetLocationProducts(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<DataLocationItem>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DataLocationItem(

	@field:SerializedName("id_m_locations")
	val idMLocations: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("lon")
	val lon: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("location_id")
	val locationId: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
)
