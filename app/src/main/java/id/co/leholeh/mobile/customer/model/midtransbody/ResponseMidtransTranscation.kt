package id.co.leholeh.mobile.customer.model.midtransbody

import com.google.gson.annotations.SerializedName

data class ResponseMidtransTranscation(

	@field:SerializedName("redirect_url")
	val redirectUrl: String? = null,

	@field:SerializedName("token")
	val token: String? = null,

	@field:SerializedName("error_messages")
	val errorMessages: List<String>? = null
)


