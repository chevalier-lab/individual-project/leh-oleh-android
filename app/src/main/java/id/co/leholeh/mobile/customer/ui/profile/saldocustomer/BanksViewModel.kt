package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.banks.Bank
import id.co.leholeh.mobile.customer.model.banks.BankAccount
import id.co.leholeh.mobile.customer.model.banks.DetailBankDompet
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.BanksService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class BanksViewModel : ViewModel() {
    private val _statusGetListBanksName = MutableLiveData<ApiStatus>()
    val statusGetListBanksName : LiveData<ApiStatus>
        get() = _statusGetListBanksName

    private val _listBanksName = MutableLiveData<List<Bank>>()
    val listBanksName
        get() = _listBanksName

    private val _statusGetListBanksAccount = MutableLiveData<ApiStatus>()
    val statusGetListBanksAccount : LiveData<ApiStatus>
        get() = _statusGetListBanksAccount

    private val _listBanksAccount = MutableLiveData<List<BankAccount>>()
    val listBanksAccount
        get() = _listBanksAccount

    private val _detailBanksAccount = MutableLiveData<DetailBankDompet>()
    val detailBanksAccount
        get() = _detailBanksAccount

    private val serviceBanks : BanksService by lazy {
        ApiService().createService(BanksService::class.java)
    }

    //detailbank
    private suspend fun getDetailBank(authorization : String,id:String){
        try {
            _statusGetListBanksName.postValue(ApiStatus.LOADING)
            _detailBanksAccount.postValue(serviceBanks.getDetailBankAccount(authorization,id).data)
            _statusGetListBanksName.postValue(ApiStatus.SUCCESS)
        }catch (e:Exception){
            Log.d("ERR_REQ_GET_BANKS", e.localizedMessage!!)
            _statusGetListBanksName.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataDetailBank(authorization: String,id:String){
        viewModelScope.launch {
            getDetailBank(authorization,id)
        }
    }

    private suspend fun getListBankName(authorization : String){
        try {
            _statusGetListBanksName.postValue(ApiStatus.LOADING)
            _listBanksName.postValue(serviceBanks.getListBanksName(authorization,0).data)
            _statusGetListBanksName.postValue(ApiStatus.SUCCESS)
        }catch (e:Exception){
            Log.d("ERR_REQ_GET_BANKS", e.localizedMessage!!)
            _statusGetListBanksName.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataListBankName(authorization: String){
        viewModelScope.launch {
            getListBankName(authorization)
        }
    }

    private suspend fun getListBankAccount(authorization : String){
        try {
            _statusGetListBanksAccount.postValue(ApiStatus.LOADING)
            _listBanksAccount.postValue(serviceBanks.getListBankAccount(authorization,0).data)
            _statusGetListBanksAccount.postValue(ApiStatus.SUCCESS)
        }catch (e:Exception){
            Log.d("ERR_REQ_GET_BANKS", e.localizedMessage!!)
            _statusGetListBanksAccount.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataListBankAccount(authorization: String){
        viewModelScope.launch {
            getListBankAccount(authorization)
        }
    }
}