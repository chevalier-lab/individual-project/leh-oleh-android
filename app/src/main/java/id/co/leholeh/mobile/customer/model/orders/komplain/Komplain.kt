package id.co.leholeh.mobile.customer.model.orders.komplain

import com.google.gson.annotations.SerializedName

data class Komplain(
    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("subject")
    val subject: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("id_u_user_transaction_products")
    val idUUserTransactionProducts: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)