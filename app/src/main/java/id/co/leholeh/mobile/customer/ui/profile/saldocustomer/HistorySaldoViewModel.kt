package id.co.leholeh.mobile.customer.ui.profile.saldocustomer

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.wallet.LogsItem
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers

class HistorySaldoViewModel : ViewModel() {
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    lateinit var statusListHistorySaldo: LiveData<ApiStatus>
    private lateinit var historySaldoList: LiveData<PagedList<LogsItem>>

    private val _historySaldoDataSource = MutableLiveData<HistorySaldoDataSource>()

    //DetailBank


    // Start of Get History Saldo
    fun setUpListSaldo(authorization: String) {
        statusListHistorySaldo = Transformations.switchMap<HistorySaldoDataSource, ApiStatus>(
            _historySaldoDataSource,
            HistorySaldoDataSource::apiState
        )
        historySaldoList = initializedSaldoPagedListBuilder(authorization, config).build()
    }

    fun getListHistorySaldo(): LiveData<PagedList<LogsItem>> = historySaldoList

    private fun initializedSaldoPagedListBuilder(authorization: String, config: PagedList.Config):
            LivePagedListBuilder<Int, LogsItem> {

        val dataSourceFactory = object : DataSource.Factory<Int, LogsItem>() {
            override fun create(): DataSource<Int, LogsItem> {
                val historySaldoDataSource = HistorySaldoDataSource(Dispatchers.IO, authorization)
                _historySaldoDataSource.postValue(historySaldoDataSource)

                return historySaldoDataSource
            }
        }
        return LivePagedListBuilder<Int, LogsItem>(dataSourceFactory, config)
    }
    // End of Get History Saldo
}