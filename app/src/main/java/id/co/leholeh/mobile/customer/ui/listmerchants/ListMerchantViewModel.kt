package id.co.leholeh.mobile.customer.ui.listmerchants

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers

class ListMerchantViewModel : ViewModel() {

    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    //LIST MERCHANT
    private lateinit var listMerchants: LiveData<PagedList<Merchants>>
    lateinit var statusMerchant: LiveData<ApiStatus>
    private val _listMerchantDataSource = MutableLiveData<ListMerchantDataSource>()

    //LIST MERCHANT
    fun setupListMerchant() {
        statusMerchant = Transformations.switchMap<ListMerchantDataSource, ApiStatus>(
            _listMerchantDataSource,
            ListMerchantDataSource::apiState
        )
        listMerchants = initializedListMerchantBuilder(config).build()
    }

    fun getDataListMerchant(): LiveData<PagedList<Merchants>> = listMerchants

    private fun initializedListMerchantBuilder(config: PagedList.Config): LivePagedListBuilder<Int, Merchants> {
        val dataSourceFactory = object : DataSource.Factory<Int, Merchants>() {
            override fun create(): DataSource<Int, Merchants> {
                val listMerchantDataSource = ListMerchantDataSource(Dispatchers.IO)
                _listMerchantDataSource.postValue(listMerchantDataSource)
                return listMerchantDataSource
            }
        }
        return LivePagedListBuilder<Int,Merchants>(dataSourceFactory,config)
    }
    //END OF LIST MERCHANT
}