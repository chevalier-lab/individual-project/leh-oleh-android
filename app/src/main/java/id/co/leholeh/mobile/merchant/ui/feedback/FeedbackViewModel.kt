package id.co.leholeh.mobile.merchant.ui.feedback

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.feedback.DataTransactionComplain
import id.co.leholeh.mobile.merchant.model.feedback.DataTransactionReview
import id.co.leholeh.mobile.merchant.network.FeedbackService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class FeedbackViewModel : ViewModel() {
    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
        get() = _status

    private val _complain = MutableLiveData<List<DataTransactionComplain>>()
    val complain : LiveData<List<DataTransactionComplain>>
        get() = _complain

 /*   private val _detailKomplain = MutableLiveData<>()
    val detailKomplain : LiveData<>
        get() = _detailKomplain*/

    private val _review = MutableLiveData<List<DataTransactionReview>>()
    val review : LiveData<List<DataTransactionReview>>
        get() = _review

    private val feedbackService : FeedbackService by lazy {
        ApiService().createService(FeedbackService::class.java)
    }

    private suspend fun getListReview(auth : String){
        try{
            _status.postValue(ApiStatus.LOADING)
            _review.postValue(feedbackService.getListTransactionReview(auth).data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("ERR_REQ_REVIEW", e.localizedMessage!!)
        }
    }

    fun getDataListReview(auth: String){
        viewModelScope.launch {
            getListReview(auth)
        }
    }

    private suspend fun getListComplain(auth : String){
        try{
            _status.postValue(ApiStatus.LOADING)
            _complain.postValue(feedbackService.getListTransactionComplain(auth).data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            _status.postValue(ApiStatus.FAILED)
            Log.d("ERR_REQ_REVIEW", e.localizedMessage!!)
        }
    }

    fun getDataListComplain(auth: String){
        viewModelScope.launch {
            getListComplain(auth)
        }
    }
}