package id.co.leholeh.mobile.merchant.ui.barang

import android.util.Log
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.merchant.model.barang.DataDetailProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.DataLocationItem
import id.co.leholeh.mobile.merchant.model.barang.ProductItemMerchant
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class KelolaBarangViewModel : ViewModel() {



    private val _statusDetailProduct = MutableLiveData<ApiStatus>()
    val statusDetailProduct: LiveData<ApiStatus>
        get() = _statusDetailProduct

    private val _detailProduct = MutableLiveData<DataDetailProductMerchant>()
    val detailProduct: LiveData<DataDetailProductMerchant>
        get() = _detailProduct

    private val _locationProduct = MutableLiveData<List<DataLocationItem>>()
    val locationProduct: LiveData<List<DataLocationItem>>
        get() = _locationProduct

    private val _statusLocationProduct = MutableLiveData<ApiStatus>()
    val statusLocationProduct: LiveData<ApiStatus>
        get() = _statusLocationProduct

    private val apiService: ManageProductService by lazy {
        ApiService().createService(ManageProductService::class.java)
    }

    fun getDataLocationsProduct(token: String, id: Int) {
        viewModelScope.launch {
            getLocationProductMerchant(token, id)
        }
    }

    private suspend fun getLocationProductMerchant(token: String, id: Int) {
        try {
            _statusLocationProduct.postValue(ApiStatus.LOADING)
            _locationProduct.postValue(apiService.getLocationProducts(token, id).data)
            _statusLocationProduct.postValue(ApiStatus.SUCCESS)
        } catch (e: java.lang.Exception) {
            Log.d("GET_LOCATION_MERCHANT", e.localizedMessage)
            _statusLocationProduct.postValue(ApiStatus.FAILED)
        }
    }

    //CONFIG
    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .build()


    //LIST MERCHANT PRODUCT
    private lateinit var listMerchantsProduct: LiveData<PagedList<ProductItemMerchant>>
    lateinit var statusMerchantProduct: LiveData<ApiStatus>
    private val _listMerchantDataSource = MutableLiveData<KelolaBarangDataSource>()


    //LIST PRODUCT MERCHANT
    fun setUpListMerchantProduct(auth: String) {
        statusMerchantProduct = Transformations.switchMap<KelolaBarangDataSource, ApiStatus>(
            _listMerchantDataSource,
            KelolaBarangDataSource::apiState
        )
        listMerchantsProduct = initializedMerchantProductPagedListBuilder(config, auth).build()
    }

    fun getDataListMerchantProduct() : LiveData<PagedList<ProductItemMerchant>> = listMerchantsProduct

    private fun initializedMerchantProductPagedListBuilder(config: PagedList.Config,auth: String) : LivePagedListBuilder<Int, ProductItemMerchant> {
        val dataSourceFactory = object : DataSource.Factory<Int, ProductItemMerchant>(){
            override fun create(): DataSource<Int, ProductItemMerchant> {
                val merchantProductDataSource = KelolaBarangDataSource(Dispatchers.IO,auth)
                _listMerchantDataSource.postValue(merchantProductDataSource)
                return merchantProductDataSource
            }
        }
        return LivePagedListBuilder<Int, ProductItemMerchant>(dataSourceFactory,config)
    }
    //END OF LIST PRODUCT MERCHANT

    fun getDataDetailProduct(token: String, id: Int) {
        viewModelScope.launch {
            getDetailProductMerchant(token, id)
        }
    }

    private suspend fun getDetailProductMerchant(token: String, id: Int) {
        try {
            _statusDetailProduct.postValue(ApiStatus.LOADING)
            _detailProduct.postValue(apiService.getDetailProductMerchant(token, id).data)
            _statusDetailProduct.postValue(ApiStatus.SUCCESS)
        } catch (e: Exception) {
            Log.d("ERROR_REQ_DTL_PRDCT", e.localizedMessage!!)
            _statusDetailProduct.postValue(ApiStatus.FAILED)
        }
    }
}