package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class ResponseStepBanks(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: List<StepBanks>? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class StepBanks(

	@field:SerializedName("id_m_banks")
	val idMBanks: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("step")
	val step: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)
