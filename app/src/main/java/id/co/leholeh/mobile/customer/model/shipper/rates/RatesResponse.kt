package id.co.leholeh.mobile.customer.model.shipper.rates

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RatesResponse(
	@field:SerializedName("data")
	val data: DataTrackingItem? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class ExpressItem(
	@field:SerializedName("finalWeight")
	val finalWeight: Int? = null,

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("insuranceRate")
	val insuranceRate: Int? = null,

	@field:SerializedName("logo_url")
	val logoUrl: String? = null,

	@field:SerializedName("show_id")
	val showId: Int? = null,

	@field:SerializedName("compulsory_insurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("item_price")
	val itemPrice1: Int? = null,

	@field:SerializedName("liability")
	val liability: Int? = null,

	@field:SerializedName("stop_origin")
	val stopOrigin: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("discount")
	val discount: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Double? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("pickup_agent")
	val pickupAgent: Int? = null,

	@field:SerializedName("stop_destination")
	val stopDestination: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null
) : Parcelable

@Parcelize
data class TruckingItem(
	@field:SerializedName("finalWeight")
	val finalWeight: Int? = null,

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("insuranceRate")
	val insuranceRate: Int? = null,

	@field:SerializedName("logo_url")
	val logoUrl: String? = null,

	@field:SerializedName("show_id")
	val showId: Int? = null,

	@field:SerializedName("compulsory_insurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("item_price")
	val itemPriceTrucking: Int? = null,

	@field:SerializedName("liability")
	val liability: Int? = null,

	@field:SerializedName("stop_origin")
	val stopOrigin: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Double? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("pickup_agent")
	val pickupAgent: Int? = null,

	@field:SerializedName("stop_destination")
	val stopDestination: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null
) : Parcelable

@Parcelize
data class Logistic(
	@field:SerializedName("same day")
	val sameDay: List<SameDayItem?>? = null,

	@field:SerializedName("trucking")
	val trucking: List<TruckingItem?>? = null,

	@field:SerializedName("express")
	val express: List<ExpressItem?>? = null,

	@field:SerializedName("regular")
	val regular: List<RegularItem?>? = null,

	@field:SerializedName("instant")
	val instant: List<InstantItem?>? = null
) : Parcelable

@Parcelize
data class SameDayItem(
	@field:SerializedName("finalWeight")
	val finalWeight: Int? = null,

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("insuranceRate")
	val insuranceRate: Int? = null,

	@field:SerializedName("logo_url")
	val logoUrl: String? = null,

	@field:SerializedName("show_id")
	val showId: Int? = null,

	@field:SerializedName("compulsory_insurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("item_price")
	val itemPriceSameDay: Int? = null,

	@field:SerializedName("liability")
	val liability: Int? = null,

	@field:SerializedName("stop_origin")
	val stopOrigin: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("discount")
	val discount: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Int? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("pickup_agent")
	val pickupAgent: Int? = null,

	@field:SerializedName("stop_destination")
	val stopDestination: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("logistic_id")
	val logisticId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null
) : Parcelable

@Parcelize
data class RegularItem(
	@field:SerializedName("finalWeight")
	val finalWeight: Int? = null,

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("insuranceRate")
	val insuranceRate: Int? = null,

	@field:SerializedName("logo_url")
	val logoUrl: String? = null,

	@field:SerializedName("show_id")
	val showId: Int? = null,

	@field:SerializedName("compulsory_insurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("item_price")
	val itemPriceRegular: Int? = null,

	@field:SerializedName("liability")
	val liability: Int? = null,

	@field:SerializedName("stop_origin")
	val stopOrigin: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("discount")
	val discount: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Double? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("pickup_agent")
	val pickupAgent: Int? = null,

	@field:SerializedName("stop_destination")
	val stopDestination: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null
) : Parcelable

@Parcelize
data class Rates(
	@field:SerializedName("logistic")
	val logistic: Logistic? = null
) : Parcelable

@Parcelize
data class DataTrackingItem(
	@field:SerializedName("destinationArea")
	val destinationArea: String? = null,

	@field:SerializedName("rates")
	val rates: Rates? = null,

	@field:SerializedName("rule")
	val rule: String? = null,

	@field:SerializedName("originArea")
	val originArea: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
) : Parcelable

@Parcelize
data class InstantItem(
	@field:SerializedName("finalWeight")
	val finalWeight: Int? = null,

	@field:SerializedName("min_day")
	val minDay: Int? = null,

	@field:SerializedName("rate_id")
	val rateId: Int? = null,

	@field:SerializedName("insuranceRate")
	val insuranceRate: Int? = null,

	@field:SerializedName("logo_url")
	val logoUrl: String? = null,

	@field:SerializedName("show_id")
	val showId: Int? = null,

	@field:SerializedName("compulsory_insurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("item_price")
	val itemPriceInstant: Int? = null,

	@field:SerializedName("liability")
	val liability: Int? = null,

	@field:SerializedName("stop_origin")
	val stopOrigin: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("discount")
	val discount: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Int? = null,

	@field:SerializedName("rate_name")
	val rateName: String? = null,

	@field:SerializedName("pickup_agent")
	val pickupAgent: Int? = null,

	@field:SerializedName("stop_destination")
	val stopDestination: Int? = null,

	@field:SerializedName("max_day")
	val maxDay: Int? = null,

	@field:SerializedName("logistic_id")
	val logisticId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("itemPrice")
	val itemPrice: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null
) : Parcelable
