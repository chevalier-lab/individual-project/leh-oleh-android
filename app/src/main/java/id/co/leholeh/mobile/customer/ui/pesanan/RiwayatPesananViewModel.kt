package id.co.leholeh.mobile.customer.ui.pesanan

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import id.co.leholeh.mobile.customer.model.orders.Order
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.Dispatchers


class RiwayatPesananViewModel:ViewModel() {

    private val config = PagedList.Config.Builder()
        .setPageSize(10)
        .setEnablePlaceholders(false)
        .build()

    private val _status = MutableLiveData<ApiStatus>()
    val status: LiveData<ApiStatus>
        get() = _status

    lateinit var apiStatus: LiveData<ApiStatus>
    private lateinit var listHistoryPesanan: LiveData<PagedList<Order>>
    private val _riwayatPesananDataSource = MutableLiveData<RiwayatPesananCustomerDataSource>()

    //LIST PESANAN
    fun setupListHistoryPesanan(authorization: String){
        apiStatus =
            Transformations.switchMap<RiwayatPesananCustomerDataSource, ApiStatus>(_riwayatPesananDataSource, RiwayatPesananCustomerDataSource::apiState)
        listHistoryPesanan  = initializedHistoryPesananPagedListBuilder(authorization, config).build()
    }

    fun getDataListHistoryPesanan() : LiveData<PagedList<Order>> = listHistoryPesanan

    private fun initializedHistoryPesananPagedListBuilder(
        authorization: String,
        config: PagedList.Config
    ):
            LivePagedListBuilder<Int, Order> {

        val dataSourceFactory = object : DataSource.Factory<Int, Order>() {

            override fun create(): DataSource<Int, Order> {
                val riwayatPesananDataSource = RiwayatPesananCustomerDataSource(
                    Dispatchers.IO,
                    authorization
                )
                _riwayatPesananDataSource.postValue(riwayatPesananDataSource)
                return riwayatPesananDataSource
            }
        }
        return LivePagedListBuilder<Int, Order>(dataSourceFactory, config)
    }
    //END OF LIST PESANAN
}