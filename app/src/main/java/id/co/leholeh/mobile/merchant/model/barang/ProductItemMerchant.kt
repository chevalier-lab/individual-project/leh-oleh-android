package id.co.leholeh.mobile.merchant.model.barang

import com.google.gson.annotations.SerializedName

data class ProductItemMerchant(

	@field:SerializedName("is_visible")
	val isVisible: String? = null,

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("id_merchant")
	val idMerchant: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("discount")
	val discount: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("locations")
	val locations: List<Any>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItemMerchant>? = null,

	@field:SerializedName("slug")
	val slug: String? = null
)