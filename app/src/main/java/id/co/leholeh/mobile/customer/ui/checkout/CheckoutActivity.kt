package id.co.leholeh.mobile.customer.ui.checkout

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityCheckoutBinding
import id.co.leholeh.databinding.ComponentDialogCheckoutChoosePaymentBinding
import id.co.leholeh.databinding.ComponentDialogCheckoutConfirmationBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.API_KEY_MIDTRANS
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.cart.*
import id.co.leholeh.mobile.customer.model.midtransbody.ResponseMidtransTranscation
import id.co.leholeh.mobile.customer.model.midtransbody.ResponsePaymentToken
import id.co.leholeh.mobile.customer.model.orders.DataOrderResponse
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.network.midtrans.MidtransService
import id.co.leholeh.mobile.customer.ui.cart.KeranjangViewModel
import id.co.leholeh.mobile.customer.ui.checkout.delivery.ChooseDeliveryActivity
import id.co.leholeh.mobile.customer.ui.checkout.delivery.ShippingData
import id.co.leholeh.mobile.customer.ui.checkout.delivery.ShippingList
import id.co.leholeh.mobile.customer.ui.location.UserLocationActivity
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.BalanceViewModel
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_checkout.view.*
import kotlinx.android.synthetic.main.item_list_checkout_per_merchant.view.*
import okhttp3.Credentials
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// Updated
class CheckoutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCheckoutBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var checkoutAdapter: AdapterUtil<DataItemCart>
    private lateinit var checkoutListAdapter: AdapterUtil<ProductsItemCart>

    private var currentBalance = 0.0
    private var listMarketPhoneNumber = arrayListOf<String>()
    private var listMarketName = arrayListOf<String>()

    private val viewModel: KeranjangViewModel by lazy {
        ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(KeranjangViewModel::class.java)
    }

    private val checkoutViewModel: CheckoutViewModel by lazy {
        ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(CheckoutViewModel::class.java)
    }

    private val balanceViewModel: BalanceViewModel by lazy {
        ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(BalanceViewModel::class.java)
    }

    private var currentUserAreaID = ""
    private var shipperPrice = 0.0
    private var hargaTotal = 0.0
    private var destinationAddress = ""
    private var destinationDirection = ""
    private var destinationCity = ""
    private var destinationPostCode = ""
    private var destinationCountryCode = "IDN"

    private var isLehOlehPay = true
    private var paymentName = ""
    private var counter = 0

    private var id_u_user_merchant = ""
    private var id_u_user_location = ""

    private var listOfDataItemCart = arrayListOf<DataItemCart>()
    private lateinit var listOfShipperPrice: ArrayList<Double>
    private lateinit var listOfShipperName: ArrayList<String>
    private lateinit var listOfShipperRatesID: ArrayList<Int>
    private lateinit var listOfProductName: ArrayList<String>

    private lateinit var listOfLength: ArrayList<Float>
    private lateinit var listOfWidth: ArrayList<Float>
    private lateinit var listOfHeight: ArrayList<Float>
    private lateinit var listOfWeight: ArrayList<Float>

    private lateinit var auth: Login

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Setup Cache
        this.cacheUtil = CacheUtil()
        this.cacheUtil.start(this, ConstantAuth.PREFERENCES)

        binding.imageViewEditAlamat.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    UserLocationActivity::class.java
                )
            )
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        setData()
        setCurrentLocation()
        setCurrentBalance()

        binding.ivChoosePayment.setOnClickListener { onChoosePaymentDialog() }
        binding.btnProses.setOnClickListener {
            if (isLehOlehPay || paymentName == "LehOlehPay") {
                if (hargaTotal > currentBalance) Toast.makeText(
                    this,
                    "Saldo Anda tidak mencukupi",
                    Toast.LENGTH_SHORT
                ).show()
                else onConfirmCheckoutDialog()
            } else onConfirmCheckoutDialog()
        }
    }

    // SET UI
    @SuppressLint("SetTextI18n")
    private fun setData() {
        var qty: Int
        var qtyTotal: Int

        val auth = getAuth(cacheUtil)
        val arrCategories = arrayListOf<String>()

        with(viewModel) {
            getListCart(auth.token!!)
            listCart.observe(this@CheckoutActivity, Observer { list ->
                qtyTotal = 0
                qty = 0

                listOfDataItemCart.clear()
                listOfDataItemCart.addAll(list)

                // WHEN THE CART IS EMPTY
                if (list.isNullOrEmpty()) {
                    with(binding) {
                        ivDataKosong.visibility = View.VISIBLE
                        rvKeranjang.visibility = View.GONE
                        tvQty.text = "Total (0)"
                        tvSubtotal.text = "-"
                    }
                } else {
                    // WHEN THE CART IS NOT EMPTY
                    if (counter == 0) {
                        listOfShipperPrice = ArrayList()
                        listOfShipperName = ArrayList()
                        listOfShipperRatesID = ArrayList()
                        listOfProductName = ArrayList()

                        listOfLength = ArrayList()
                        listOfWidth = ArrayList()
                        listOfHeight = ArrayList()
                        listOfWeight = ArrayList()

                        counter++
                        listMarketPhoneNumber.clear()
                        listMarketName.clear()

                        for (i in list.indices) {
                            listMarketPhoneNumber.add(list[i].marketPhoneNumber!!)
                            listMarketName.add(list[i].marketName!!)
                            listOfShipperPrice.add(0.0)
                            listOfShipperName.add("_")
                            listOfShipperRatesID.add(0)
                            listOfProductName.add("_")
                        }

                        Log.d("CHECK_COUNTER", "$counter --- $shipperPrice")
                    }

                    with(binding) {
                        ivDataKosong.visibility = View.GONE
                        rvKeranjang.visibility = View.VISIBLE
                    }

                    hargaTotal = 0.0

                    if (intent.hasExtra("EXTRA_SET_LIST")) {
                        intent.getParcelableExtra<ShippingList>("EXTRA_SET_LIST")?.let {
                            Log.d("CHECK_EXTRA_SET_LIST", "$it")

                            listOfShipperPrice = it.listOfShipperPrice
                            listOfShipperName = it.listOfShipperName
                            listOfShipperRatesID = it.listOfShipperRatesID

                            for (i in listOfShipperPrice.indices) {
                                shipperPrice += listOfShipperPrice[i]
                            }
                        }
                    }

                    // SET TOTAL PRICE WITH SHIPPER RATES
                    list.forEach { item -> hargaTotal += item.total!! }
                    binding.tvSubtotal.text = changeToRupiah(hargaTotal + shipperPrice)
                    Log.d("CHECK_HARGA_TOTAL", "$hargaTotal")

                    // SET THE FIRST RECYCLERVIEW
                    checkoutAdapter = AdapterUtil(
                        R.layout.item_list_checkout,
                        list,
                        { position, itemView, item ->
                            with(itemView) {
                                tv_merchant_name.text = item.marketName
                                Log.d(
                                    "CHECK_SHIPPER_RATES",
                                    "${listOfShipperPrice.size} --- $listOfShipperPrice"
                                )

                                tv_nama_shipper.text = listOfShipperName[position]
                                tv_harga_shipper.text = changeToRupiah(listOfShipperPrice[position])

                                iv_icon_shipper.setOnClickListener {
                                    Log.d("CHECK_POSITION", position.toString())

                                    val data = ShippingData(
                                        item.location!!.areaId!!.toInt(),
                                        currentUserAreaID.toInt(),
                                        listOfLength[position],
                                        listOfWidth[position],
                                        listOfHeight[position],
                                        listOfWeight[position],
                                        item.total!!.toInt(),
                                        position
                                    )

                                    val lists = ShippingList(
                                        listOfShipperPrice,
                                        listOfShipperName,
                                        listOfShipperRatesID
                                    )

                                    startActivity(
                                        Intent(
                                            this@CheckoutActivity,
                                            ChooseDeliveryActivity::class.java
                                        ).apply {
                                            putExtra("EXTRA_SHIPPER_DATA", data)
                                            putExtra("EXTRA_SHIPPER_LIST", lists)
                                            putExtra("EXTRA_ISLEHOLEHPAY", isLehOlehPay)
                                        }
                                    )

                                    Log.d("CHECK_EXTRA_LEHOLEHPAY", isLehOlehPay.toString())

                                    Log.d(
                                        "SHIPPER",
                                        "${item.location.areaId!!.toInt()}, " +
                                                "$currentUserAreaID, " +
                                                "$listOfLength, " +
                                                "$listOfWidth, " +
                                                "$listOfHeight, " +
                                                "${item.total.toInt()}, " +
                                                "$listOfWeight"
                                    )
                                }
                            }

                            // SET THE SECOND RECYCLERVIEW
                            checkoutListAdapter = AdapterUtil(
                                R.layout.item_list_checkout_per_merchant,
                                item.products!!,
                                { _, v, product ->
                                    with(v) {
                                        tv_namacart_produk_checkout.text = product.productName
                                        listOfProductName.add(product.productName!!)
                                        qty = product.qty!!.toInt()

                                        qtyTotal += qty
                                        tv_qty_checkout.text = qty.toString()
                                        tv_cart_harga_checkout.text = product.priceSelling

                                        // SET LENGTH, WIDTH, HEIGHT AND WEIGHT OF THE PRODUCT
                                        listOfLength.add(product.productInfo!!.length!!.toFloat())
                                        listOfWidth.add(product.productInfo.width!!.toFloat())
                                        listOfHeight.add(product.productInfo.height!!.toFloat())
                                        listOfWeight.add(product.productInfo.weight!!.toFloat())
                                    }

                                    for (j in product.categories!!.indices)
                                        arrCategories.add(product.categories[j].category!!)

                                    arrCategories.forEach { cat ->
                                        v.tv_kategori_cartproduk_checkout.text = "$cat "
                                    }

                                    binding.tvQty.text = StringBuilder().append("Total (")
                                        .append(qtyTotal.toString()).append("pcs )")

                                    val hargaDiskon =
                                        product.priceSelling!!.toDouble() -
                                                (product.priceSelling.toDouble()) *
                                                product.discount!!.toDouble() / 100.0
                                    v.tv_cart_harga_checkout.text = changeToRupiah(hargaDiskon)

                                    Glide.with(this@CheckoutActivity)
                                        .load(product.uri)
                                        .into(v.im_cart_produk_checkout)
                                },
                                { _, _ -> })

                            with(itemView.rv_item_checkouts) {
                                layoutManager = LinearLayoutManager(this@CheckoutActivity)
                                adapter = checkoutListAdapter
                            }
                        },
                        { _, _ -> })

                    with(binding.rvKeranjang) {
                        layoutManager = LinearLayoutManager(this@CheckoutActivity)
                        adapter = checkoutAdapter
                    }

                    checkoutAdapter.data = list
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            setCurrentLocation()
        }
    }

    // SET USER CURRENT LOCATION
    @SuppressLint("SetTextI18n")
    private fun setCurrentLocation() {
        auth = getAuth(cacheUtil)
        with(checkoutViewModel) {
            getCurrentUserLocation(auth.token!!)
            currentLocation.observe(this@CheckoutActivity, Observer {
                binding.textViewNamaAlamat.text =
                    "${it.address}, ${it.area}, ${it.suburb}, ${it.city}, ${it.province}, ${it.postcode}"
                currentUserAreaID = it.areaId

                destinationAddress = "${it.address}, ${it.city}, ${it.province}"
                destinationDirection = "${it.address}, ${it.city}, ${it.province}"
                destinationCity = it.city
                destinationPostCode = it.postcode

                id_u_user_location = it.id
                Log.i("SHIPPER", it.areaId)
            })
        }
    }

    // SET USER CURRENT BALANCE
    @SuppressLint("SetTextI18n")
    private fun setCurrentBalance() {
        if (intent.hasExtra("EXTRA_LEHOLEHPAY_RETURN")) {
            when (intent.getStringExtra("EXTRA_LEHOLEHPAY_RETURN")) {
                "Leh-Oleh" -> {
                    with(balanceViewModel) {
                        auth = getAuth(cacheUtil)

                        getDataBalance(auth.token!!)
                        balance.observe(this@CheckoutActivity, Observer {
                            currentBalance = it.balance!!.toDouble()
                            binding.textViewMetodePembayaran.text =
                                "${it.walletName} | ${changeToRupiah(it.balance.toDouble())}"
                        })
                    }

                    paymentName = "LehOlehPay"
                    isLehOlehPay = true
                }

                "Pembayaran lainnya" -> {
                    paymentName = "Pembayaran lainnya"
                    isLehOlehPay = false

                    binding.textViewMetodePembayaran.text = paymentName
                }
            }
        } else {
            with(balanceViewModel) {
                auth = getAuth(cacheUtil)

                getDataBalance(auth.token!!)
                balance.observe(this@CheckoutActivity, Observer {
                    currentBalance = it.balance!!.toDouble()
                    binding.textViewMetodePembayaran.text =
                        "${it.walletName} | ${changeToRupiah(it.balance.toDouble())}"
                })
            }

            paymentName = "LehOlehPay"
            isLehOlehPay = true
        }

    }

    // DIALOG CHOOSE PAYMENT
    @SuppressLint("SetTextI18n")
    private fun onChoosePaymentDialog() {
        val dialog = Dialog(this)
        val layoutInflater = LayoutInflater.from(applicationContext)
        val binds = ComponentDialogCheckoutChoosePaymentBinding.inflate(layoutInflater)

        with(dialog) {
            setContentView(binds.root)
            setCancelable(true)
            show()
        }

        with(binds) {
            tvConfirm.setOnClickListener {
                when {
                    rbLeholehpay.isChecked -> {
                        paymentName = "Leh Oleh Pay"
                        isLehOlehPay = true

                        with(balanceViewModel) {
                            auth = getAuth(cacheUtil)
                            getDataBalance(auth.token!!)

                            balance.observe(this@CheckoutActivity, Observer {
                                currentBalance = it.balance!!.toDouble()
                                binding.textViewMetodePembayaran.text =
                                    "$paymentName | ${changeToRupiah(it.balance.toDouble())}"
                            })
                        }
                    }
                    rbLainnya.isChecked -> {
                        paymentName = "Pembayaran lainnya"
                        isLehOlehPay = false

                        binding.textViewMetodePembayaran.text = paymentName
                    }
                }

                Log.d("CHECK_PAYMENT", "LEH OLEH PAY: $isLehOlehPay")
                dialog.dismiss()
            }
            tvCancel.setOnClickListener { dialog.dismiss() }
        }
    }

    // SET CONFIRMATION DIALOG WHEN USER IS READY TO CHECKOUT
    private fun onConfirmCheckoutDialog() {
        Log.d("CHECK_LIST", listOfShipperPrice.toString())
        val dialog = Dialog(this)
        val layoutInflater = LayoutInflater.from(applicationContext)
        val binds = ComponentDialogCheckoutConfirmationBinding.inflate(layoutInflater)

        with(dialog) {
            setContentView(binds.root)
            setCancelable(true)
            show()
        }

        with(binds) {
            tvConfirm.setOnClickListener {
                val full_name = edtNamaPenerima.text.toString()
                val phone_number = edtNomorHp.text.toString()
                val email = edtEmail.text.toString()

                if (full_name != "" && phone_number != "" && email != "") {
                    if (email.isValidEmail()) {
                        setCheckout(full_name, phone_number, email)
                        dialog.dismiss()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "Masukan Email Dengan Benar!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        applicationContext,
                        "Isi Form Dengan Lengkap",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            tvCancel.setOnClickListener {
                dialog.dismiss()
            }
        }
    }

    private fun CharSequence?.isValidEmail() =
        !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this!!).matches()

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }
        return true
    }

    // START THE CHECKOUT REQUEST
    private fun setCheckout(fullname: String, phone: String, email: String) {
        auth = getAuth(cacheUtil)
        val mainJsonObject = JsonObject()

        val jsonObjectT = JsonObject()
        val jsonObjectN = JsonObject()
        val arrayJson = JsonArray()

        //Midtarns inital json
        val jsonObjectMidtrans = JsonObject()
        val jsonObjectCreditCard = JsonObject()
        val jsonObjectCustomerDetail = JsonObject()

        // WHEN SHIPPING ADDRESS IS NOT SET
        if (currentUserAreaID.isEmpty() || destinationAddress.isEmpty())
            Snackbar.make(
                binding.root,
                "Silakan pilih alamat pengiriman terlebih dahulu!",
                Snackbar.LENGTH_SHORT
            ).show()
        else {
            var count = 0
            if (listOfShipperPrice.isNotEmpty()) {
                for (i in listOfShipperPrice.indices) {
                    if (listOfShipperPrice[i] == 0.0) {
                        Snackbar.make(
                            binding.root,
                            "Silakan pilih opsi pengiriman terlebih dahulu!",
                            Snackbar.LENGTH_SHORT
                        ).show()
                        break
                    } else {
                        count++
                    }
                }
            }

            if (count == listOfDataItemCart.size) {
                for (n in listOfDataItemCart.indices) {
                    val jsonObjectO = JsonObject()
                    val jsonArrayProduct = JsonArray()
                    val listProductID = arrayListOf<String>()
                    id_u_user_merchant = listOfDataItemCart[n].idUUserMerchant!!

                    for (j in listOfDataItemCart[n].products!!.indices) {
                        jsonArrayProduct.add(listOfDataItemCart[n].products!![j].idMProducts!!.toInt())
                    }


                    Log.v("ProdukList", listProductID.toString())
                    jsonObjectO.apply {
                        add("list_id_product", jsonArrayProduct)
                        addProperty("id_u_user_merchant", id_u_user_merchant)
                        addProperty("additional", listOfShipperPrice[n].toString())
                    }
                    arrayJson.add(jsonObjectO)
                }

                jsonObjectT.apply {
                    addProperty("full_name", fullname)
                    addProperty("phone_number", phone)
                    addProperty("email", email)
                    addProperty("id_u_user_location", id_u_user_location)
                }

                jsonObjectN.apply {
                    addProperty("title_notif", NOTIFICATION_TITLE)
                    addProperty("desc_notif", NOTIFICATION_MESSAGE)
                    addProperty("is_broadcast", "1")
                }

                mainJsonObject.apply {
                    add("order", arrayJson)
                    add("transaction_info", jsonObjectT)
                    add("notifications", jsonObjectN)
                    addProperty("is_leh_oleh_pay", isLehOlehPay)
                }

                Log.d("CHECK_SHIPPER_RATES", listOfShipperPrice.toString())
                Log.d("CHECK_DATA_JSON", mainJsonObject.toString())

                //Request Midtrans Transaction
                requestPostCheckout(
                    auth.token!!,
                    mainJsonObject,
                    { it ->
                        if (it.code == 200) {
                            CHECKOUT_REQUEST_LOG = "Checkout pesanan berhasil dilakukan!"
                            Log.d("CHECKOUT_ORDER", "$CHECKOUT_REQUEST_LOG --- ${it.code}")

                            for (i in listOfDataItemCart.indices) {
                                val jsonObject = JsonObject()
                                //item product midtrans
                                val jsonObjectItemDetails = JsonArray()
                                val jsonObjectTransaction = JsonObject()

                                if (!isLehOlehPay) {
                                    for (j in listOfDataItemCart[i].products!!.indices) {


//                                        Payment Midtrans
                                        //midtrans item detail
                                        val jsonObjectItem = JsonObject()

                                        jsonObjectItem.addProperty(
                                            "id",
                                            listOfDataItemCart[i].products!![j].idMProducts!!
                                        )
                                        val hargaDiskon =
                                            listOfDataItemCart[i].products!![j].priceSelling!!.toDouble() -
                                                    (listOfDataItemCart[i].products!![j].priceSelling!!.toDouble()) *
                                                    listOfDataItemCart[i].products!![j].discount!!.toDouble() / 100.0
                                        jsonObjectItem.addProperty(
                                            "price",
                                            hargaDiskon
                                        )
                                        jsonObjectItem.addProperty(
                                            "quantity",
                                            listOfDataItemCart[i].products!![j].qty!!
                                        )
                                        jsonObjectItem.addProperty(
                                            "name",
                                            listOfDataItemCart[i].products!![j].productName!!
                                        )
                                        jsonObjectItemDetails.add(jsonObjectItem)
                                    }
                                    Log.v(
                                        "PAYMENT",
                                        "diluar for     Putaran ke " + i + " " + listOfDataItemCart[i]
                                    )

                                    Log.v(
                                        "PAYMENT",
                                        "Json Object Details Putaran ke $i $jsonObjectItemDetails"
                                    )

                                    //ongkir
                                    val jsonObjectOngkir = JsonObject()
                                    //midtrans item detail
                                    jsonObjectOngkir.addProperty(
                                        "id",
                                        "ongkir"
                                    )
                                    jsonObjectOngkir.addProperty(
                                        "price",
                                        it.data!![i]!!.transactionMerchants!!.additional!!.toDouble()
                                    )
                                    jsonObjectOngkir.addProperty(
                                        "quantity",
                                        1
                                    )
                                    jsonObjectOngkir.addProperty(
                                        "name",
                                        "Ongkos Kirim Barang"
                                    )
                                    jsonObjectItemDetails.add(jsonObjectOngkir)


                                    //TransactionDetails
                                    jsonObjectTransaction.addProperty(
                                        "order_id",
                                        it.data[i]!!.transaction!!.transactionToken
                                    )
                                    jsonObjectTransaction.addProperty(
                                        "gross_amount",
                                        it.data[i]!!.transaction!!.totalPrice!!.toInt() + it.data[i]!!.transactionMerchants!!.additional!!.toDouble()
                                    )
                                    //CreditCard
                                    jsonObjectCreditCard.addProperty("secure", true)

                                    //Customer Details
                                    //billing address
                                    val jsonObjectBillingAddres = JsonObject()
                                    jsonObjectBillingAddres.apply {
                                        addProperty(
                                            "first_name",
                                            it.data[i]!!.transactionInfo!!.fullName
                                        )
                                        addProperty(
                                            "last_name",
                                            it.data[i]!!.transactionInfo!!.fullName
                                        )
                                        addProperty("email", it.data[i]!!.transactionInfo!!.email)
                                        addProperty(
                                            "phone",
                                            it.data[i]!!.transactionInfo!!.phoneNumber
                                        )
                                        //address billing
                                        addProperty("address", destinationDirection)
                                        addProperty("city", destinationCity)
                                        addProperty("postal_code", destinationPostCode)
                                        addProperty("country_code", destinationCountryCode)
                                    }

                                    //billing address
                                    val jsonObjectShippingAddres = JsonObject()
                                    jsonObjectShippingAddres.apply {
                                        addProperty(
                                            "first_name",
                                            it.data[i]!!.transactionInfo!!.fullName
                                        )
                                        addProperty(
                                            "last_name",
                                            it.data[i]!!.transactionInfo!!.fullName
                                        )
                                        addProperty("email", it.data[i]!!.transactionInfo!!.email)
                                        addProperty(
                                            "phone",
                                            it.data[i]!!.transactionInfo!!.phoneNumber
                                        )
                                        //address billing
                                        addProperty("address", destinationDirection)
                                        addProperty("city", destinationCity)
                                        addProperty("postal_code", destinationPostCode)
                                        addProperty("country_code", destinationCountryCode)
                                    }

                                    //Customer Detals
                                    jsonObjectCustomerDetail.apply {
                                        addProperty(
                                            "first_name",
                                            it.data[i]!!.transaction!!.fullName
                                        )
                                        addProperty(
                                            "last_name",
                                            it.data[i]!!.transaction!!.fullName
                                        )
                                        addProperty("email", it.data[i]!!.transactionInfo!!.email)
                                        addProperty(
                                            "phone",
                                            it.data[i]!!.transactionInfo!!.phoneNumber
                                        )
                                        add("billing_address", jsonObjectBillingAddres)
                                        add("shipping_address", jsonObjectBillingAddres)
                                    }
                                }

                                jsonObject.apply {
                                    addProperty(
                                        "id_user_transaction",
                                        it.data!![i]?.transactionInfo!!.idUUserTransaction
                                    )
                                    addProperty(
                                        "id_u_user_location",
                                        it.data[i]?.transactionInfo!!.idUUserLocation
                                    )
                                    addProperty(
                                        "id_u_user_is_merchant",
                                        it.data[i]?.transactionMerchants!!.idUUserIsMerchant
                                    )
                                    addProperty(
                                        "consigneeName",
                                        it.data[i]?.transactionInfo!!.fullName
                                    )
                                    addProperty("destinationAddress", destinationAddress)
                                    addProperty("destinationDirection", destinationDirection)
                                    addProperty(
                                        "consigneePhoneNumber",
                                        it.data[i]?.transactionInfo!!.phoneNumber
                                    )
                                    addProperty("consignerPhoneNumber", listMarketPhoneNumber[i])
                                    addProperty("consignerName", listMarketName[i])
                                    addProperty("rateID", listOfShipperRatesID[i])
                                    addProperty("contents", listOfProductName[i])
                                    addProperty("packageType", 1)
                                }

                                Log.d("CHECKOUT_JSONOBJECT", "$jsonObject")

                                //idu uuserTransction
                                val idUUserTransaction: String =
                                    it.data!![i]?.transactionInfo!!.idUUserTransaction!!

                                requestPostCreateOrder(
                                    auth.token!!,
                                    jsonObject,
                                    { dataOrder ->
                                        if (dataOrder.code == 200) {
                                            CHECKOUT_REQUEST_LOG =
                                                "Checkout pesanan berhasil dilakukan!"
                                            Log.d(
                                                "CHECKOUT_ORDER",
                                                "$CHECKOUT_REQUEST_LOG --- $dataOrder"
                                            )
                                            if (!isLehOlehPay) {
                                                //add json midtrans
                                                jsonObjectMidtrans.apply {
                                                    add(
                                                        "transaction_details",
                                                        jsonObjectTransaction
                                                    )
                                                    add("credit_card", jsonObjectCreditCard)
                                                    add("item_details", jsonObjectItemDetails)
                                                    add(
                                                        "customer_details",
                                                        jsonObjectCustomerDetail
                                                    )
                                                }

                                                Log.v("JsonMidtrans", jsonObjectMidtrans.toString())

                                                requestMidtransTransaction(
                                                    jsonObjectMidtrans,
                                                    { responseMidtrans ->
                                                        Log.d(
                                                            "PAYMENTTOKEN",
                                                            "Response Atas $it"
                                                        )

                                                        val jsonObjectUpdatePayment = JsonObject()
                                                        jsonObjectUpdatePayment.addProperty(
                                                            "token_payment",
                                                            responseMidtrans.token
                                                        )

                                                        updatePaymentToken(
                                                            auth.token!!,
                                                            idUUserTransaction,
                                                            jsonObjectUpdatePayment,
                                                            {

                                                                if (i == listOfDataItemCart.size - 1) {
                                                                    Toast.makeText(
                                                                        this,
                                                                        "Pesanan berhasil",
                                                                        Toast.LENGTH_SHORT
                                                                    ).show()
                                                                    startActivity(
                                                                        Intent(
                                                                            this,
                                                                            MainActivity::class.java
                                                                        ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                                                    )
                                                                    finish()
                                                                }
                                                            },
                                                            {
                                                                Log.d(
                                                                    "PAYMENTTOKEN",
                                                                    "Response Atas bawah $it"
                                                                )

                                                                Toast.makeText(
                                                                    this,
                                                                    it,
                                                                    Toast.LENGTH_SHORT
                                                                ).show()
                                                            })
                                                    },
                                                    { message ->
                                                        Log.d(
                                                            "PAYMENTTOKEN",
                                                            "Response bawah $message"
                                                        )

                                                        Toast.makeText(
                                                            this,
                                                            message,
                                                            Toast.LENGTH_SHORT
                                                        ).show()
                                                    })
                                            } else {
                                                startActivity(
                                                    Intent(
                                                        this,
                                                        MainActivity::class.java
                                                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                                                )

                                                Toast.makeText(
                                                    this,
                                                    CHECKOUT_REQUEST_LOG,
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }


                                        } else {
                                            Toast.makeText(
                                                this,
                                                it.message.toString(),
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }
                                    },
                                    { error ->
                                        CHECKOUT_REQUEST_LOG = "Checkout pesanan gagal!"
                                        Log.d("CHECKOUT_ORDER", error)
                                    }
                                )
                            }
                        } else {
                            CHECKOUT_REQUEST_LOG = "Checkout pesanan gagal!"
                            Log.d("CHECKOUT_ORDER", "$CHECKOUT_REQUEST_LOG --- ${it.message}")
                        }
                    },
                    {
                        CHECKOUT_REQUEST_LOG = "Checkout pesanan gagal!"
                        Log.d("CHECKOUT_ORDER", it)
                    }
                )
            }
        }
    }

    private fun requestPostCreateOrder(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (DataOrderResponse) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceTransaction: TransactionService by lazy {
            ApiService().createService(TransactionService::class.java)
        }

        serviceTransaction.createOrder(auth, jsonObject)
            .enqueue(object : Callback<DataOrderResponse> {
                override fun onResponse(
                    call: Call<DataOrderResponse>,
                    response: Response<DataOrderResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) onSuccess(response.body()!!)
                        else onFailed(response.message().toString())
                    } else onFailed(response.message().toString())
                }

                override fun onFailure(call: Call<DataOrderResponse>, t: Throwable) =
                    onFailed(t.message.toString())
            })
    }

    // REQUEST POST CHECKOUT
    private fun requestPostCheckout(
        auth: String,
        jsonObject: JsonObject,
        onSuccess: (OrderCheckout) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call = ApiService().serviceCart.checkoutCart(auth, jsonObject)
        call.enqueue(object : Callback<OrderCheckout> {
            override fun onFailure(call: Call<OrderCheckout>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<OrderCheckout>,
                response: Response<OrderCheckout>
            ) {
                Log.d("CHECKOUT", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }


    // REQUEST Midtrans Transctions
    private fun requestMidtransTransaction(
        jsonObject: JsonObject,
        onSuccess: (ResponseMidtransTranscation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val basicAuth = Credentials.basic(API_KEY_MIDTRANS, "")

        val serviceTransaction: MidtransService by lazy {
            ApiService().createServiceMidtransTransaction(MidtransService::class.java)
        }
        val call = serviceTransaction.requestTransaction(basicAuth, jsonObject)
        call.enqueue(object : Callback<ResponseMidtransTranscation> {
            override fun onFailure(call: Call<ResponseMidtransTranscation>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<ResponseMidtransTranscation>,
                response: Response<ResponseMidtransTranscation>
            ) {
                if (response.code() == 400) {
                    Log.d("PAYMENTTOKEN", "Response Body if 400 : " + response.body().toString())
                    Log.d(
                        "PAYMENTTOKEN",
                        "Response Body if erro 400 : " + response.errorBody().toString()
                    )
                }
                Log.d("PAYMENTTOKEN", "Response Body : " + response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }

    // UPDATE PAYMENT TOKEN
    private fun updatePaymentToken(
        auth: String,
        idUserTransaction: String,
        jsonObject: JsonObject,
        onSuccess: (ResponsePaymentToken) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call = ApiService().serviceOrder.updatePaymentToken(auth, idUserTransaction, jsonObject)
        call.enqueue(object : Callback<ResponsePaymentToken> {
            override fun onFailure(call: Call<ResponsePaymentToken>, t: Throwable) =
                onFailed(t.message.toString())

            override fun onResponse(
                call: Call<ResponsePaymentToken>,
                response: Response<ResponsePaymentToken>
            ) {
                Log.d("PAYMENTTOKEN", response.body().toString())
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message()).toString()
            }
        })
    }


    companion object {
        private const val NOTIFICATION_TITLE = "CHECKOUT ORDER"
        private const val NOTIFICATION_MESSAGE = "CHECKOUT ORDER IS SUCCESS"
        private var CHECKOUT_REQUEST_LOG = ""
    }
}