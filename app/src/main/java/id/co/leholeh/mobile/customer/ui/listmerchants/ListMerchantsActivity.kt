package id.co.leholeh.mobile.customer.ui.listmerchants

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityListMerchantsBinding
import id.co.leholeh.mobile.customer.constant.ConstantGuestMerchants
import id.co.leholeh.mobile.customer.model.merchant.Merchants
import id.co.leholeh.mobile.customer.ui.merchantcustomer.MerchantPreviewActivity
import id.co.leholeh.mobile.customer.util.PagedAdapterUtil
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.android.synthetic.main.item_merchant.view.*

class ListMerchantsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListMerchantsBinding
    private val viewModel: ListMerchantViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ListMerchantViewModel::class.java)
    }
    private lateinit var merchantAdapter: PagedAdapterUtil<Merchants>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListMerchantsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getListMerchant()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    private fun getListMerchant() {
        viewModel.setupListMerchant()
        viewModel.getDataListMerchant().observe(this, Observer {
            merchantAdapter.submitList(it)
        })
       getStateMerchant()

        merchantAdapter =
            PagedAdapterUtil(
                R.layout.item_all_merchants,
                { _, itemView, item ->
                    itemView.textViewNamaMerchantFix.text = item.marketName
                    itemView.textViewAlamatMerchantFix.text = item.marketAddress
                    Glide.with(this).load(item.marketUri)
                        .into(itemView.imageViewMerchantFix)
                },
                { _, item ->
                    val intent = Intent(
                        this,
                        MerchantPreviewActivity::class.java
                    )
                    intent.putExtra(
                        ConstantGuestMerchants.DATA_MERCHANT,
                        item
                    )
                    startActivity(intent)
                })
        binding.rvListMerchant.layoutManager =
            GridLayoutManager(this, 3)
        binding.rvListMerchant.adapter = merchantAdapter
    }

    private fun getStateMerchant(){
        viewModel.statusMerchant.observe(this, Observer {
            Log.d("STATUS_HISTORY", it.toString())
            when (it) {
                ApiStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                ApiStatus.EMPTY -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.EMPTY_BEFORE -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.EMPTY_AFTER -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.LOADED -> {
                    binding.rvListMerchant.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, "Jaringan bermasalah", Toast.LENGTH_SHORT)
                        .show()
                }
                else -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(this, "Ada sesuatu yang salah", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
        }
        return true
    }
}