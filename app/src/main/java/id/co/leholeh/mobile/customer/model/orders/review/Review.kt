package id.co.leholeh.mobile.customer.model.orders.review

import com.google.gson.annotations.SerializedName

data class Review(
    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("review")
    val review: String? = null,

    @field:SerializedName("rating")
    val rating: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("id_u_user_transaction_products")
    val idUUserTransactionProducts: String? = null
)