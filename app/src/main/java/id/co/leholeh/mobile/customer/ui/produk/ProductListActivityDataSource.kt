package id.co.leholeh.mobile.customer.ui.produk

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.model.product.DataFilterProduct
import id.co.leholeh.mobile.customer.model.product.FilterProduct
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ProductListActivityDataSource(
    private val orderDirection: String,
    private val search: String,
    private val jsonObject: JsonObject
) : PageKeyedDataSource<Int, FilterProduct>() {
    private val apiService = ApiService().service
    val apiState = MutableLiveData<ApiStatus>()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, FilterProduct>
    ){
        apiState.postValue(ApiStatus.LOADING)
        val response =
            apiService.getFilterProduct(FIRST_PAGE, "id", orderDirection, search, jsonObject)
        response.enqueue(object : Callback<DataFilterProduct> {
            override fun onResponse(
                call: Call<DataFilterProduct>,
                response: Response<DataFilterProduct>
            ) {
                when {
                    response.isSuccessful -> {
                        val apiResponse = response.body()
                        val responseItem = apiResponse!!.data
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()) {
                                apiState.postValue(ApiStatus.EMPTY)
                            } else {
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, null, FIRST_PAGE + 1)
                        }
                    }
                    else -> {
                        apiState.postValue(ApiStatus.SUCCESS)
                    }
                }
            }

            override fun onFailure(call: Call<DataFilterProduct>, t: Throwable) {
                apiState.postValue(ApiStatus.FAILED)
                Log.d(TAG, t.localizedMessage!!)
            }
        })
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, FilterProduct>
    ) {
        apiState.postValue(ApiStatus.LOADING)
        val response =
            apiService.getFilterProduct(params.key, "id", orderDirection, search, jsonObject)

        response.enqueue(object : Callback<DataFilterProduct> {
            override fun onResponse(
                call: Call<DataFilterProduct>,
                response: Response<DataFilterProduct>
            ) {
                val key = if (params.key > 0) params.key - 1 else -1
                when {
                    response.isSuccessful -> {
                        val apiResponse = response.body()
                        val responseItem = apiResponse!!.data
                        apiState.postValue(ApiStatus.SUCCESS)
                        responseItem?.let {
                            if (responseItem.isEmpty()) {
                                apiState.postValue(ApiStatus.EMPTY_BEFORE)
                            } else {
                                apiState.postValue(ApiStatus.LOADED)
                            }
                            callback.onResult(responseItem, key)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<DataFilterProduct>, t: Throwable) {
                apiState.postValue(ApiStatus.FAILED)
                Log.d(TAG, t.localizedMessage!!)
            }
        })
    }

    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, FilterProduct>
    ) {
            apiState.postValue(ApiStatus.LOADING)
            val response = apiService.getFilterProduct(
                params.key,
                "id",
                orderDirection,
                search,
                jsonObject
            )

            response.enqueue(object : Callback<DataFilterProduct> {
                override fun onResponse(
                    call: Call<DataFilterProduct>,
                    response: Response<DataFilterProduct>
                ) {
                    val responseItem = response.body()!!.data
                    val key = params.key + 1
                    when {
                        response.isSuccessful -> {
                            apiState.postValue(ApiStatus.SUCCESS)
                            responseItem?.let {
                                if (responseItem.isEmpty()) {
                                    apiState.postValue(ApiStatus.EMPTY_AFTER)
                                } else {
                                    apiState.postValue(ApiStatus.LOADED)
                                }
                                callback.onResult(responseItem, key)
                            }
                        }
                    }

                }

                override fun onFailure(call: Call<DataFilterProduct>, t: Throwable) {
                    apiState.postValue(ApiStatus.FAILED)
                    Log.d(TAG, t.localizedMessage!!)
                }
            })

    }

    companion object {
        const val FIRST_PAGE = 0
        const val TAG = "ERR_REQ_MERCHANT"
    }
}