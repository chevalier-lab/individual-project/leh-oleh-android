package id.co.leholeh.mobile.customer.ui.profile

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.ParcelFileDescriptor
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentCustomerProfileBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.constant.ConstantProfile
import id.co.leholeh.mobile.customer.model.ProfileMenuItem
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.profile.ResponseChangePhotoProfile
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.bantuan.BantuanActivity
import id.co.leholeh.mobile.customer.ui.location.UserLocationActivity
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.customer.ui.profile.saldocustomer.CustomerSaldoSayaActivity
import id.co.leholeh.mobile.customer.ui.register.*
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.constant.ConstantProfileMerchant
import id.co.leholeh.mobile.merchant.ui.profile.MerchantTokoSayaActivity
import id.co.leholeh.mobile.utils.ApiStatus
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import kotlinx.android.synthetic.main.item_list_menu_akun.view.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*

class CustomerProfileFragment : Fragment() {

    private lateinit var adapter: AdapterUtil<ProfileMenuItem>
    private lateinit var binding: FragmentCustomerProfileBinding
    private lateinit var cacheUtil: CacheUtil
    private var auth: Login? = null
    private val viewModel: ProfileViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileViewModel::class.java)
    }

    private val viewModelRegisterMerchant: RegisterMerchantViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(RegisterMerchantViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //binding init
        binding = FragmentCustomerProfileBinding.inflate(inflater, container, false)
        // Setup Cache
        cacheUtil = CacheUtil()
        cacheUtil.start(context as Activity, ConstantAuth.PREFERENCES)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup Title
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        binding.toolbar.title = getString(R.string.akun_saya)


        //loading
        /*viewModel.status.observe(requireActivity(), Observer {
            ShimmerUtil.updateProgressRelativeLayout(it,binding.progressBar,binding.tvNetworkError,binding.layoutCustomerProfile)
        })*/

        //intent to edit profile


        //init profil picture


        //registrasi

    }

    //initUI
    fun initUI(){
        //After Login
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            binding.tvNamaAkun.text = ""
            binding.tvEmailAkun.text = ""
            Glide.with(requireContext()).load(R.drawable.ic_baseline_account_circle_24)
                .into(binding.imFotoProfil)
            binding.afterLogin.visibility = View.GONE
            binding.progressBar.visibility = View.GONE
            setupProfileBeforeLogin()
        } else {
            auth = getAuth(cacheUtil)
            binding.beforeLogin.visibility = View.GONE
            checkLoadingStatus()
            getProfileUser()
            setupProfileAfterLogin()
            binding.imFotoProfil.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
            }
            //edit photo profile
        }
        binding.tvRegistrasi.setOnClickListener {
            startActivity(Intent(context, MenuRegisterUserActivity::class.java))
        }
        //login
        binding.tvLogin.setOnClickListener {
            startActivity(Intent(context, LoginActivity::class.java))
        }

    }

    private fun setupProfileBeforeLogin() {
        binding.rvIconmenu.layoutManager = LinearLayoutManager(context)
        adapter =
            AdapterUtil(
                R.layout.item_list_menu_akun,
                listOf(
                    ProfileMenuItem(
                        "Pusat Bantuan",
                        R.drawable.ic_help_outline
                    ),
                    ProfileMenuItem(
                        "Chat dengan Leh-Oleh",
                        R.drawable.ic_chat
                    ),
                    ProfileMenuItem(
                        "Beri Kami Nilai",
                        R.drawable.ic_star_border
                    )
                ), { position, itemView, item ->
                    itemView.tv_menu!!.text = item.label
                    itemView.im_akun_icon!!.setImageResource(item.icon)
                    itemView.im_chevron_right.setImageResource(R.drawable.ic_chevron_right)
                }, { position, _ ->
                    when (position) {

                        0 -> startActivity(
                            Intent(
                                context,
                                BantuanActivity::class.java
                            )
                        )
                        1 -> {

                        }
                        2 -> {

                        }
                    }

                })
        binding.rvIconmenu.adapter = adapter
    }

    private fun setupProfileAfterLogin() {
        //Setup Profil Menu
        binding.rvIconmenu.layoutManager = LinearLayoutManager(context)
        viewModelRegisterMerchant.getDataStatusMerchant(auth!!.token!!)
        adapter =
            AdapterUtil(R.layout.item_list_menu_akun,
                listOf(
                    ProfileMenuItem(
                        "Saldo Saya",
                        R.drawable.ic_monetization
                    ),
                    ProfileMenuItem(
                        "Lokasi",
                        R.drawable.ic_location
                    ),
                    ProfileMenuItem(
                        "Chat dengan Leh-Oleh",
                        R.drawable.ic_chat
                    ),
                    ProfileMenuItem(
                        "Toko Saya",
                        R.drawable.ic_store
                    ),
                    ProfileMenuItem(
                        "Pusat Bantuan",
                        R.drawable.ic_help_outline
                    ),
                    ProfileMenuItem(
                        "Beri Kami Nilai",
                        R.drawable.ic_star_border
                    ),
                    ProfileMenuItem(
                        "Logout",
                        R.drawable.ic_logout
                    )
                ), { position, itemView, item ->
                    itemView.tv_menu!!.text = item.label
                    itemView.im_akun_icon!!.setImageResource(item.icon)
                    itemView.im_chevron_right.setImageResource(R.drawable.ic_chevron_right)
                }, { position, item ->
                    when (position) {
                        0 -> startActivity(
                            Intent(
                                context,
                                CustomerSaldoSayaActivity::class.java
                            )
                        )
                        1 -> startActivity(
                            Intent(
                                context,
                                UserLocationActivity::class.java
                            )
                        )
                        4 -> startActivity(
                            Intent(
                                context,
                                BantuanActivity::class.java
                            )
                        )
                        3 -> {
                            var visible = ""
                            viewModelRegisterMerchant.statusMerchant.observe(
                                viewLifecycleOwner,
                                Observer {
                                    visible = it.isVisible!!
                                })

                            when (visible) {
                                "1" -> startActivity(
                                    Intent(
                                        requireContext(),
                                        MerchantTokoSayaActivity::class.java
                                    )
                                )
                                "0" -> {
                                    //ditolak
                                    val intent = Intent(
                                        requireContext(),
                                        StatusRegisterMerchantActivity::class.java
                                    )
                                    intent.putExtra(
                                        ConstantProfileMerchant.INTENT_MERCHANT_STATUS,
                                        ConstantProfileMerchant.MERCHANT_REJECTED
                                    )
                                    startActivity(intent)
                                }
                                "2" -> {
                                    //proses
                                    val intent = Intent(
                                        requireContext(),
                                        StatusRegisterMerchantActivity::class.java
                                    )
                                    intent.putExtra(
                                        ConstantProfileMerchant.INTENT_MERCHANT_STATUS,
                                        ConstantProfileMerchant.MERCHANT_PROSES
                                    )
                                    startActivity(intent)
                                }
                                else ->
                                    startActivity(
                                        Intent(
                                            requireContext(),
                                            RegisterMerchantActivity::class.java
                                        )
                                    )
                            }
                        }
                        5->{
                            val packageName = "id.co.leholeh"
                            val uri: Uri = Uri.parse("market://details?id=$packageName")
                            val goToMarket = Intent(Intent.ACTION_VIEW, uri)
                            // To count with Play market backstack, After pressing back button,
                            // to taken back to our application, we need to add following flags to intent.
                            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY or
                                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT or
                                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                            try {
                                startActivity(goToMarket)
                            } catch (e: ActivityNotFoundException) {
                                startActivity(Intent(Intent.ACTION_VIEW,
                                    Uri.parse("http://play.google.com/store/apps/details?id=$packageName")))
                            }

                        }
                        6 -> {
                            //logout clicked
                            this.cacheUtil.clear()
                            startActivity(Intent(requireContext(), MainActivity::class.java))
                            requireActivity().finish()
                        }
                    }
//                    2 -> startActivity(Intent(context, ProductListActivity::class.java))
//                    3 -> startActivity(Intent(context, ProductListActivity::class.java))
                })
        binding.rvIconmenu.adapter = adapter
    }

    private fun setPhotoProfile(
        authorization: String,
        photoProfile: MultipartBody.Part,
        onSuccess: (ResponseChangePhotoProfile) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val call: Call<ResponseChangePhotoProfile> =
            ApiService().serviceProfileUser.changePhotoProfile(authorization, photoProfile)
        call.enqueue(object : Callback<ResponseChangePhotoProfile> {
            override fun onFailure(call: Call<ResponseChangePhotoProfile>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseChangePhotoProfile>,
                response: Response<ResponseChangePhotoProfile>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })
    }

    private fun getProfileUser() {
        viewModel.getDataProfileUser(auth!!.token!!)
        viewModel.profile.observe(viewLifecycleOwner, Observer { profile ->
            binding.afterLogin.visibility = View.VISIBLE
            binding.tvNamaAkun.text = profile.fullName
            binding.tvEmailAkun.text = profile.email
            Glide.with(requireContext()).load(profile.uri).circleCrop().into(binding.imFotoProfil)
            binding.ivUbahAkun.setOnClickListener {
                val intent = Intent(requireContext(), RegisterUserActivity::class.java)
                intent.putExtra(ConstantProfile.INTENT_DATA_PROFILE, profile)
                intent.putExtra(
                    ConstantProfile.INTENT_DATA_EDIT,
                    ConstantProfile.INTENT_EDIT_CONDITION
                )
                startActivity(intent)
            }
        })
    }


    private fun checkLoadingStatus() {
        viewModel.status.observe(viewLifecycleOwner, Observer {
            when (it!!) {
                ApiStatus.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                ApiStatus.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                }
                ApiStatus.FAILED -> {
                    binding.progressBar.visibility = View.GONE
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.koneksi_bermasalah),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        })
    }


    override fun onResume() {
        if (!TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))) {
            auth = getAuth(cacheUtil)
            viewModel.getDataProfileUser(auth!!.token!!)
        }
        initUI()

        super.onResume()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val pickedImage: Image = ImagePicker.getFirstImageOrNull(data)
            try {
                val parcelFileDescriptor: ParcelFileDescriptor =
                    requireActivity().contentResolver.openFileDescriptor(pickedImage.uri, "r")!!
                val fileDescriptor: FileDescriptor = parcelFileDescriptor.fileDescriptor
                val image: Bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
                val filePhoto = createTempFile(image)
                val photoProfile = MultipartBody.Part.createFormData(
                    "photo",
                    filePhoto!!.name,
                    filePhoto.asRequestBody("image/*".toMediaTypeOrNull())
                )
                binding.progressBar.visibility= View.VISIBLE
                binding.progressBarPhoto.visibility= View.VISIBLE
                setPhotoProfile(auth!!.token!!, photoProfile, {
                    if (it.code == 200) {
                        Toast.makeText(
                            context,
                            getString(R.string.berhasil_mengubah_foto),
                            Toast.LENGTH_SHORT
                        ).show()
                        viewModel.getDataProfileUser(auth!!.token!!)
                        binding.progressBar.visibility= View.GONE
                        binding.progressBarPhoto.visibility= View.GONE
                    }else{
                        Toast.makeText(
                            context,
                            getString(R.string.berhasil_mengubah_foto),
                            Toast.LENGTH_SHORT
                        ).show()
                        binding.progressBar.visibility= View.GONE
                        binding.progressBarPhoto.visibility= View.GONE
                    }
                }, {
                    Toast.makeText(context, it, Toast.LENGTH_LONG).show()
                    binding.progressBar.visibility= View.GONE
                    binding.progressBarPhoto.visibility= View.GONE
                })

                parcelFileDescriptor.close()
            } catch (e: IOException) {
                e.printStackTrace()

            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }


}