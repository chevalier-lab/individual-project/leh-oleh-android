package id.co.leholeh.mobile.customer.model.orders.detail

import com.google.gson.annotations.SerializedName

data class ResponseDetailOrder(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val dataDetailOrder: DataDetailOrder? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class Location(

	@field:SerializedName("id_u_user_is_merchant")
	val idUUserIsMerchant: String? = null,

	@field:SerializedName("suburb_id")
	val suburbId: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("province")
	val province: String? = null,

	@field:SerializedName("province_id")
	val provinceId: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("suburb")
	val suburb: String? = null,

	@field:SerializedName("area_id")
	val areaId: String? = null,

	@field:SerializedName("city_id")
	val cityId: String? = null,

	@field:SerializedName("is_active")
	val isActive: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("id")
	val id: String? = null
)

data class Additional(

	@field:SerializedName("id_u_user_is_merchant")
	val idUUserIsMerchant: String? = null,

	@field:SerializedName("additional")
	val additional: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("list_id_m_product")
	val listIdMProduct: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null
)

data class Info(

	@field:SerializedName("shipper_agent_id")
	val shipperAgentId: Any? = null,

	@field:SerializedName("shipper_agent_name")
	val shipperAgentName: Any? = null,

	@field:SerializedName("shipper_order_id")
	val shipperOrderId: String? = null,

	@field:SerializedName("id_u_user_transaction_products")
	val idUUserTransactionProducts: String? = null,

	@field:SerializedName("shipper_is_request_pickup")
	val shipperIsRequestPickup: String? = null,

	@field:SerializedName("shipper_order_active")
	val shipperOrderActive: String? = null
)

data class ProductsOrder(

	@field:SerializedName("sub_total_price")
	val subTotalPrice: String? = null,

	@field:SerializedName("token_products")
	val tokenProducts: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("price_selling")
	val priceSelling: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("id_m_products")
	val idMProducts: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("price_default")
	val priceDefault: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_u_product")
	val idUProduct: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItem?>? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("info")
	val info: Info? = null
)

data class InfoTransaction(

	@field:SerializedName("id_u_user_location")
	val idUUserLocation: String? = null,

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("token_payment")
	val tokenPayment: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class CategoriesItem(

	@field:SerializedName("category")
	val category: String? = null
)

data class DataDetailOrder(

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("additional")
	val additional: Additional? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("info")
	val infoTransaction: InfoTransaction? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("merchant_info")
	val merchantInfo: MerchantInfo? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("products")
	val products: List<ProductsOrder>? = null
)

data class MerchantInfo(

	@field:SerializedName("market_phone_number")
	val marketPhoneNumber: String? = null,

	@field:SerializedName("id_u_user")
	val idUUser: String? = null,

	@field:SerializedName("market_address")
	val marketAddress: String? = null,

	@field:SerializedName("location")
	val location: Location? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("market_name")
	val marketName: String? = null
)
