package id.co.leholeh.mobile.customer.ui.register

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.os.Environment
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.esafirm.imagepicker.features.ImagePicker
import com.esafirm.imagepicker.model.Image
import com.google.gson.JsonObject
import id.co.leholeh.R
import id.co.leholeh.databinding.ActivityRegisterMerchantBinding
import id.co.leholeh.mobile.MainActivity
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.model.merchant.ResponseRegisterMerchant
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.merchant.constant.ConstantProfileMerchant
import id.co.leholeh.mobile.merchant.model.setting.ProfileToko
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdateLocationMerchant
import id.co.leholeh.mobile.merchant.model.setting.ResponseUpdateProfileToko
import id.co.leholeh.mobile.merchant.network.ProfileTokoService
import id.co.leholeh.mobile.merchant.ui.profile.MerchantLocationActivity
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.GetLocationShipperViewModel
import id.co.leholeh.mobile.utils.getAuth
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class RegisterMerchantActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterMerchantBinding

    private var arrayIdProvinceLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdSuburbsLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdCitiesLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()
    private var arrayIdAreaLocation = arrayListOf<MerchantLocationActivity.PenampungLocation>()

    private val itemProvinceName: ArrayList<String> = arrayListOf()
    val itemCitiesName: ArrayList<String> = arrayListOf()
    val itemAreasName: ArrayList<String> = arrayListOf()
    val itemSuburbsName: ArrayList<String> = arrayListOf()


    private val viewModelLocation: GetLocationShipperViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(GetLocationShipperViewModel::class.java)
    }


    private lateinit var cacheUtil: CacheUtil
    private lateinit var clicked: String
    private lateinit var auth: Login
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterMerchantBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //SetupToolbar
        setupToolbar()

        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        val checkAuth = cacheUtil.get(ConstantAuth.AUTH)
        if (TextUtils.isEmpty(checkAuth)) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        } else {
            auth = getAuth(cacheUtil)
            binding.inputFotoKtp.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
                clicked = "KTP"
            }
            binding.inputFotoPemilik.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
                clicked = "pemilik"
            }
            binding.inputFotoToko.setOnClickListener {
                ImagePicker.create(this)
                    .single()
                    .start()
                clicked = "toko"
            }
            getDataEditProfileFromIntet()
            binding.buttonAjukan.setOnClickListener {
                actionEditOrRegister()
                binding.progressBar.visibility = View.VISIBLE
            }

            //Location Handler
            locationShiiper()
        }
    }

    private fun actionEditOrRegister() {
        if (intent.getSerializableExtra(ConstantProfileMerchant.INTENT_DATA_PROFILE_TOKO) != null) {
            sendRequestEditProfileMerchant()
        } else {
            sendRequestRegisterMerchant()
        }
    }

    private fun getDataEditProfileFromIntet() {
        if (intent.getSerializableExtra(ConstantProfileMerchant.INTENT_DATA_PROFILE_TOKO) != null) {
            binding.editTextNomorKtp.visibility = View.VISIBLE //not sure should be gone or not
            binding.textViewUnregisteredMerchant.visibility = View.GONE
            val profileToko =
                intent.getSerializableExtra(ConstantProfileMerchant.INTENT_DATA_PROFILE_TOKO) as ProfileToko
            Log.d("DATA PROFILE TOKO", profileToko.toString())
            binding.editTextNamaToko.setText(profileToko.marketName)
            binding.editTextNomorKtp.setText(profileToko.noIdentity)
            binding.editTextAlamatToko.setText(profileToko.marketAddress)
            binding.editTextNomorHp.setText(profileToko.marketPhoneNumber)
            binding.buttonAjukan.text = getString(R.string.ubah)
            Glide.with(this).load(profileToko.identityUri)
                .into(binding.imageViewFotoKtp)
            Glide.with(this).load(profileToko.authorUri)
                .into(binding.imageViewPemilik)
            Glide.with(this).load(profileToko.marketUri)
                .into(binding.imageViewToko)
            binding.imageViewToko.visibility = View.VISIBLE
            binding.imageViewPemilik.visibility = View.VISIBLE
            binding.imageViewFotoKtp.visibility = View.VISIBLE
            binding.layoutLocation.visibility= View.GONE
            binding.tvFotoPemilik.visibility=View.VISIBLE
            binding.tvFotoToko.visibility=View.VISIBLE
            binding.tvFotopKTP.visibility=View.VISIBLE
            //GONE INPUT FOTO
            binding.inputFotoKtp.visibility=View.GONE
            binding.inputFotoPemilik.visibility=View.GONE
            binding.inputFotoToko.visibility=View.GONE

            supportActionBar?.apply {
                this.title = "Ubah Merchant"
            }




        }
    }

    private fun sendRequestEditProfileMerchant() {

        val jsonObject = JsonObject()
        val address = binding.editTextAlamatToko.text.toString()
        val phoneNumber = binding.editTextNomorHp.text.toString()
        val marketName = binding.editTextNamaToko.text.toString()
        jsonObject.apply {
            addProperty("market_address", address)
            addProperty("market_name", marketName)
            addProperty("market_phone_number", phoneNumber)
        }
        requestUpdateToko(auth.token!!, jsonObject, {
            if (it.code == 200) {
                Toast.makeText(
                    this,
                    "Berhasil mengubah data!",
                    Toast.LENGTH_SHORT
                ).show()
                binding.progressBar.visibility = View.GONE
                finish()
            }else{
                Toast.makeText(
                    this,
                    it.message,
                    Toast.LENGTH_SHORT
                ).show()
                binding.progressBar.visibility = View.GONE
            }
        }, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            binding.progressBar.visibility = View.GONE
        })
    }

    private fun sendRequestRegisterMerchant() {
        val drawableFotoKtp = binding.imageViewFotoKtp.drawable as BitmapDrawable
        val bitmapFotoKtp = drawableFotoKtp.bitmap
        val fileKtp = createTempFile(bitmapFotoKtp)
        val imageKtp = MultipartBody.Part.createFormData(
            "foto_ktp",
            fileKtp!!.name,
            fileKtp.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val drawableFotoPemilik = binding.imageViewPemilik.drawable as BitmapDrawable
        val bitmapFotoPemilik = drawableFotoPemilik.bitmap
        val filePemilik = createTempFile(bitmapFotoPemilik)
        val imagePemilik = MultipartBody.Part.createFormData(
            "foto_pengguna",
            filePemilik!!.name,
            filePemilik.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val drawableFotoToko = binding.imageViewToko.drawable as BitmapDrawable
        val bitmapFotoToko = drawableFotoToko.bitmap
        val fileToko = createTempFile(bitmapFotoToko)
        val imageToko = MultipartBody.Part.createFormData(
            "foto_toko",
            fileToko!!.name,
            fileToko.asRequestBody("image/*".toMediaTypeOrNull())
        )

        val noKtp =
            MultipartBody.Part.createFormData("no_ktp", binding.editTextNomorKtp.text.toString())
        val namaToko =
            MultipartBody.Part.createFormData("nama_toko", binding.editTextNamaToko.text.toString())
        val alamatToko =
            MultipartBody.Part.createFormData("alamat", binding.editTextAlamatToko.text.toString())
        val noHp =
            MultipartBody.Part.createFormData("no_hp", binding.editTextNomorHp.text.toString())

        setRegisterMerchant(
            auth.token!!,
            imageKtp,
            imagePemilik,
            imageToko,
            namaToko,
            noHp,
            alamatToko,
            noKtp,
            {
                if (it.code == 200) {

                sendRequestCreateLocation(it.data!!.idMerchant.toString())
                    binding.progressBar.visibility = View.GONE

                }
            },
            {
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
                binding.progressBar.visibility = View.GONE

            })
    }

    private fun setRegisterMerchant(
        authorization: String,
        fotoKtp: MultipartBody.Part,
        fotoPemilik: MultipartBody.Part,
        fotoToko: MultipartBody.Part,
        namaToko: MultipartBody.Part,
        noHP: MultipartBody.Part,
        alamat: MultipartBody.Part,
        noKtp: MultipartBody.Part,
        onSuccess: (ResponseRegisterMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {

        val call: Call<ResponseRegisterMerchant> =
            ApiService().serviceGuestMerchants.registerMerchant(
                authorization, fotoKtp,
                fotoPemilik, fotoToko, namaToko, noHP, alamat, noKtp
            )
        call.enqueue(object : Callback<ResponseRegisterMerchant> {
            override fun onFailure(call: Call<ResponseRegisterMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }

            override fun onResponse(
                call: Call<ResponseRegisterMerchant>,
                response: Response<ResponseRegisterMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

        })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            val image: Image = ImagePicker.getFirstImageOrNull(data)
            when (clicked) {
                "KTP" -> {
                    Glide.with(binding.imageViewFotoKtp).load(image.uri)
                        .into(binding.imageViewFotoKtp)
                    binding.imageViewFotoKtp.visibility = View.VISIBLE
                }
                "pemilik" -> {
                    Glide.with(binding.imageViewPemilik).load(image.uri)
                        .into(binding.imageViewPemilik)
                    binding.imageViewPemilik.visibility = View.VISIBLE
                }
                "toko" -> {
                    Glide.with(binding.imageViewToko).load(image.uri).into(binding.imageViewToko)
                    binding.imageViewToko.visibility = View.VISIBLE
                }
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun createTempFile(bitmap: Bitmap): File? {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            System.currentTimeMillis().toString() + "_image.png"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

    private fun requestUpdateToko(
        authorization: String,
        jsonObject: JsonObject,
        onSuccess: (ResponseUpdateProfileToko) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val profileTokoService: ProfileTokoService by lazy {
            ApiService().createService(ProfileTokoService::class.java)
        }
        val call: Call<ResponseUpdateProfileToko> =
            profileTokoService.updateProfileToko(authorization, jsonObject)
        call.enqueue(object : Callback<ResponseUpdateProfileToko> {
            override fun onResponse(
                call: Call<ResponseUpdateProfileToko>,
                response: Response<ResponseUpdateProfileToko>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateProfileToko>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }


    private fun locationShiiper() {
        var idProvince: String
        var idSuburbs: String
        //province
        viewModelLocation.getProvince()
        viewModelLocation.province.observe(this, Observer { itemProvince ->
            itemProvinceName.clear()
            itemCitiesName.clear()
            itemAreasName.clear()
            itemSuburbsName.clear()
            for (item in itemProvince) {
                itemProvinceName.add(item.name!!)
            }
            val adapterProvince =
                ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, itemProvinceName)
            binding.dropdownProvinsi.setAdapter(adapterProvince)
            binding.dropdownProvinsi.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
                override fun afterTextChanged(p0: Editable?) {
                    //cities
                    for (i in itemProvince.indices) {
                        if (binding.dropdownProvinsi.text.toString() == itemProvince[i].name) {
                            //get id province
                            arrayIdProvinceLocation.clear()
                            arrayIdProvinceLocation.add(
                                MerchantLocationActivity.PenampungLocation(
                                    itemProvince[i].id.toString(),
                                    itemProvince[i].name.toString()
                                )
                            )
                            Log.v("MerchantLocation", "Id Province : $arrayIdProvinceLocation")


                            viewModelLocation.getCities(itemProvince[i].id.toString())
                            idProvince = itemProvince[i].id.toString()
                            Log.d("IDProvincsi", "Dalam $idProvince")
                            viewModelLocation.cities.observe(
                                this@RegisterMerchantActivity,
                                Observer { itemCities ->
                                    itemAreasName.clear()
                                    itemSuburbsName.clear()
                                    itemCitiesName.clear()
                                    //cleartext
                                    binding.dropdownKota.setText("")
                                    binding.dropdownArea.setText("")
                                    binding.dropdownSuburb.setText("")
                                    binding.dropdownKodepos.setText("")

                                    for (itCities in itemCities) {
                                        itemCitiesName.add(itCities.name!!)
                                    }
                                    val adapterCities =
                                        ArrayAdapter(
                                            applicationContext,
                                            R.layout.item_list_dropdown_text,
                                            itemCitiesName
                                        )
                                    binding.dropdownKota.setAdapter(adapterCities)
                                })

                            break
                        }
                    }
                }

            })
        })

        viewModelLocation.cities.observe(
            this@RegisterMerchantActivity,
            Observer { itemCities ->
                binding.dropdownKota.addTextChangedListener(object :
                    TextWatcher {
                    override fun beforeTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun onTextChanged(
                        p0: CharSequence?,
                        p1: Int,
                        p2: Int,
                        p3: Int
                    ) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        Log.d("IDProvincsi", "Citites $itemCities")
                        //surburbs
                        for (iSub in itemCities.indices) {
                            if (binding.dropdownKota.text.toString() == itemCities[iSub].name) {
                                arrayIdCitiesLocation.clear()
                                arrayIdCitiesLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemCities[iSub].id.toString(),
                                        itemCities[iSub].name.toString()
                                    )
                                )

                                Log.v("MerchantLocation", "Id Cities : $arrayIdCitiesLocation")



                                idSuburbs = itemCities[iSub].id.toString()
                                Log.d("IDProvincsi", "SUBURUBS ")
                                viewModelLocation.getSuburbs(idSuburbs)
                                viewModelLocation.suburbs.observe(
                                    this@RegisterMerchantActivity,
                                    Observer { itemSuburbs ->
                                        itemAreasName.clear()
                                        itemSuburbsName.clear()
                                        //cleartext
                                        binding.dropdownArea.setText("")
                                        binding.dropdownSuburb.setText("")
                                        binding.dropdownKodepos.setText("")

                                        for (itSuburbs in itemSuburbs) {
                                            itemSuburbsName.add(itSuburbs.name!!)
                                        }
                                        val adapterSuburbs =
                                            ArrayAdapter(
                                                applicationContext,
                                                R.layout.item_list_dropdown_text,
                                                itemSuburbsName
                                            )
                                        binding.dropdownSuburb.setAdapter(
                                            adapterSuburbs
                                        )
                                    })
                                break
                            }
                        }
                    }
                })
            })


        viewModelLocation.suburbs.observe(
            this@RegisterMerchantActivity,
            Observer { itemSuburbs ->
                binding.dropdownSuburb.addTextChangedListener(
                    object : TextWatcher {
                        override fun beforeTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun onTextChanged(
                            p0: CharSequence?,
                            p1: Int,
                            p2: Int,
                            p3: Int
                        ) {
                        }

                        override fun afterTextChanged(p0: Editable?) {
                            //Areas
                            for (iAreas in itemSuburbs.indices) {
                                if (binding.dropdownSuburb.text.toString() == itemSuburbs[iAreas].name) {

                                    arrayIdSuburbsLocation.clear()
                                    arrayIdSuburbsLocation.add(
                                        MerchantLocationActivity.PenampungLocation(
                                            itemSuburbs[iAreas].id.toString(),
                                            itemSuburbs[iAreas].name.toString()
                                        )
                                    )

                                    Log.v(
                                        "MerchantLocation",
                                        "Id Suburbs : $arrayIdSuburbsLocation"
                                    )

                                    viewModelLocation.getAreas(
                                        itemSuburbs[iAreas].id.toString()
                                    )
                                    viewModelLocation.areas.observe(
                                        this@RegisterMerchantActivity,
                                        Observer { itemAreas ->
                                            itemAreasName.clear()
                                            //cleartext
                                            binding.dropdownArea.setText("")
                                            binding.dropdownKodepos.setText("")
                                            for (itAreas in itemAreas) {
                                                itemAreasName.add(
                                                    itAreas.name!!
                                                )
                                            }
                                            val adapterAreas =
                                                ArrayAdapter(
                                                    applicationContext,
                                                    R.layout.item_list_dropdown_text,
                                                    itemAreasName
                                                )
                                            binding.dropdownArea.setAdapter(
                                                adapterAreas
                                            )

                                        })
                                    break
                                }
                            }
                        }
                    })
            })

        viewModelLocation.areas.observe(
            this@RegisterMerchantActivity,
            Observer { itemAreas ->
                binding.dropdownArea.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun afterTextChanged(p0: Editable?) {
                        for (i in itemAreas.indices) {
                            if (itemAreas[i].name == binding.dropdownArea.text.toString()) {
                                arrayIdAreaLocation.clear()
                                arrayIdAreaLocation.add(
                                    MerchantLocationActivity.PenampungLocation(
                                        itemAreas[i].id.toString(),
                                        itemAreas[i].name.toString()
                                    )
                                )
                                Log.v("MerchantLocation", "Id Areas : $arrayIdAreaLocation")
                                binding.dropdownKodepos.setText(itemAreas[i].postcode)
                                break
                            }
                        }
                    }

                })
            })
    }


    //send create Location
    private fun sendRequestCreateLocation(idMerchant: String) {
        val provinsi = binding.dropdownProvinsi.text.toString()
        val cities = binding.dropdownKota.text.toString()
        val suburbs = binding.dropdownSuburb.text.toString()
        val area = binding.dropdownArea.text.toString()
        val kodePos = binding.dropdownKodepos.text.toString()
        val alamat = binding.editTextAlamatToko.text.toString()

        if (provinsi.isEmpty() || cities.isEmpty() || suburbs.isEmpty() || area.isEmpty() || kodePos.isEmpty() || alamat.isEmpty()) {
            Toast.makeText(this, "Isi data dengan lengkap!", Toast.LENGTH_SHORT).show()
        } else {
            val jsonObject = JsonObject()
            jsonObject.apply {
                addProperty("id_u_user_is_merchant", idMerchant)
                addProperty("province_id", arrayIdProvinceLocation[0].id)
                addProperty("province", arrayIdProvinceLocation[0].name)
                addProperty("city_id", arrayIdCitiesLocation[0].id)
                addProperty("city", arrayIdCitiesLocation[0].name)
                addProperty("suburb_id", arrayIdSuburbsLocation[0].id)
                addProperty("suburb", arrayIdSuburbsLocation[0].name)
                addProperty("area_id", arrayIdAreaLocation[0].id)
                addProperty("area", arrayIdAreaLocation[0].name)
                addProperty("postcode", kodePos)
                addProperty("address", alamat)

                Log.v("MerchantLocation", "Json IdMerchant $idMerchant")
                Log.v("MerchantLocation", "Json province_id " + arrayIdProvinceLocation[0])
                Log.v("MerchantLocation", "Json province $provinsi")
                Log.v("MerchantLocation", "Json city_id " + arrayIdCitiesLocation[0])
                Log.v("MerchantLocation", "Json city $cities")
                Log.v("MerchantLocation", "Json suburb_id " + arrayIdSuburbsLocation[0])
                Log.v("MerchantLocation", "Json suburb $suburbs")
                Log.v("MerchantLocation", "Json area_id " + arrayIdAreaLocation[0])
                Log.v("MerchantLocation", "Json area $area")
                Log.v("MerchantLocation", "Json postcode $kodePos")
                Log.v("MerchantLocation", "Json address $alamat")

                //eksekusi nya
                createLocationMerchant(jsonObject, {
                    if (it.code == 200) {
                        Toast.makeText(
                            this@RegisterMerchantActivity,
                            "Berhasil Mendaftar!",
                            Toast.LENGTH_SHORT
                        ).show()
                        startActivity(
                            Intent(
                                this@RegisterMerchantActivity,
                                StatusRegisterMerchantActivity::class.java
                            )
                        )
                        finish()
                    } else {
                        Toast.makeText(
                            this@RegisterMerchantActivity,
                            it.message,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }, {
                    Toast.makeText(this@RegisterMerchantActivity, it, Toast.LENGTH_LONG).show()
                })
            }
        }
    }

    private fun createLocationMerchant(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateLocationMerchant) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val apiService: ProfileTokoService by lazy {
            ApiService().createService(ProfileTokoService::class.java)
        }
        val call: Call<ResponseUpdateLocationMerchant> =
            apiService.createLocationMerchant(jsonObject)
        call.enqueue(object : Callback<ResponseUpdateLocationMerchant> {
            override fun onResponse(
                call: Call<ResponseUpdateLocationMerchant>,
                response: Response<ResponseUpdateLocationMerchant>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateLocationMerchant>, t: Throwable) {
                onFailed(t.message.toString())
            }
        })
    }


    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Register Merchant"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }

    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
}