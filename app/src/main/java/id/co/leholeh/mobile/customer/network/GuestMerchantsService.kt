package id.co.leholeh.mobile.customer.network

import com.google.gson.JsonObject
import id.co.leholeh.mobile.customer.constant.ConstantGuestMerchants
import id.co.leholeh.mobile.customer.model.merchant.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface GuestMerchantsService {
    @GET(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT)
    suspend fun getListMerchant(
        @Query("page") page: Int?,
        @Query("order-direction") orderDirection: String
    ): Response<ResponseListMerchant>

    @GET(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_RANDOM)
    suspend fun getListRandomMerchant(): ResponseListMerchant

    @GET(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_DETAIL)
    suspend fun getPreviewMerchant(@Path("id") id: Int): ResponsePreviewMerchant

    @GET(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_PRODUCT_LIST)
    suspend fun getListProductByMerchant(
        @Path("id") id: Int,
        @Query("page") page: Int?
    ): Response<ResponseProductByMerchant>

    @GET(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_STATUS)
    suspend fun getStatusMerchant(@Header("Authorization") authorization: String): ResponseGetStatusMerchant

    @Multipart
    @POST(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_REQUEST_MERCHANT)
    fun registerMerchant(
        @Header("Authorization") authorization: String,
        @Part fotoKtp: MultipartBody.Part,
        @Part fotoPemilik: MultipartBody.Part,
        @Part fotoToko: MultipartBody.Part,
        @Part namaToko: MultipartBody.Part,
        @Part noHp: MultipartBody.Part,
        @Part alamat: MultipartBody.Part,
        @Part noKtp: MultipartBody.Part
    ): Call<ResponseRegisterMerchant>

    @DELETE(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT)
    fun deleteRequestMerchant(@Header("Authorization") authorization: String): Call<ResponseDeleteReqMerchant>

    @POST(ConstantGuestMerchants.BASE_URL_GUEST_MERCHANT_PRODUCT_LIST)
    fun getFilterProductMerchant(
        @Path("id") id: Int,
        @Query("page") page: Int,
        @Query("search") search: String,
        @Query("order-direction") orderDirection: String,
        @Body jsonObject: JsonObject
    ):Call<ResponseProductByMerchant>
}