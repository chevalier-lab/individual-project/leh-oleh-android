package id.co.leholeh.mobile.customer.constant.shipper

object ConstantPickup {
    const val BASE_URL_PICKUP_REQUEST = "public/v1/pickup"
    const val BASE_URL_PICKUP_GET_AGENT = "public/v1/agents"
    const val BASE_URL_PICKUP_CANCEL = "public/v1/pickup/cancel"
}