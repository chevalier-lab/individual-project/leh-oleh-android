package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class CartListtem(
    @field:SerializedName("id_m_users")
    val idMUsers: String? = null,

    @field:SerializedName("price_selling")
    val priceSelling: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("discount")
    val discount: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("subTotal")
    val subTotal: Long? = null,

    @field:SerializedName("id_m_products")
    val idMProducts: String? = null,

    @field:SerializedName("product_name")
    val productName: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null ,

    @field:SerializedName("category_product")
    val categoryProduct: String? = null
)