package id.co.leholeh.mobile.customer.model.shipper.order

import com.google.gson.annotations.SerializedName

data class ResponseUpdateOrderWeight(

	@field:SerializedName("data")
	val data: DataUpdateOrderWeight? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class CorrectedFields(

	@field:SerializedName("insurance")
	val insurance: Int? = null,

	@field:SerializedName("length")
	val length: Int? = null,

	@field:SerializedName("width")
	val width: Int? = null,

	@field:SerializedName("weight")
	val weight: Int? = null,

	@field:SerializedName("volumeWeight")
	val volumeWeight: Int? = null,

	@field:SerializedName("finalRate")
	val finalRate: Int? = null,

	@field:SerializedName("compulsoryInsurance")
	val compulsoryInsurance: Int? = null,

	@field:SerializedName("height")
	val height: Int? = null
)

data class DataUpdateOrderWeight(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: String? = null,

	@field:SerializedName("correctedFields")
	val correctedFields: CorrectedFields? = null,

	@field:SerializedName("statusCode")
	val statusCode: Int? = null
)
