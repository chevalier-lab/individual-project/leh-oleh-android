package id.co.leholeh.mobile.merchant.ui.barang

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.gson.JsonObject
import id.co.leholeh.databinding.ActivityMerchantUbahBarangBinding
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.ui.profile.login.LoginActivity
import id.co.leholeh.mobile.merchant.constant.ConstantManageProductMerchant
import id.co.leholeh.mobile.merchant.model.barang.ResponseUpdateProduct
import id.co.leholeh.mobile.merchant.model.barang.ResponseUpdateProductDimension
import id.co.leholeh.mobile.merchant.network.ManageProductService
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.getAuth
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MerchantUbahBarangActivity : AppCompatActivity() {
    lateinit var binding : ActivityMerchantUbahBarangBinding
    private lateinit var cacheUtil: CacheUtil
    private lateinit var auth : Login
    private val viewModelKelolaBarang : KelolaBarangViewModel by lazy {
        ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(KelolaBarangViewModel::class.java)
    }
    private var id : Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMerchantUbahBarangBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cacheUtil = CacheUtil()
        cacheUtil.start(this, ConstantAuth.PREFERENCES)
        if (TextUtils.isEmpty(this.cacheUtil.get(ConstantAuth.AUTH))){
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }else {
            auth = getAuth(cacheUtil)
            id = intent.getIntExtra(ConstantManageProductMerchant.ID_PRODUCT_MERCHANT, 0)
            setupToolbar()
            getDataProduct()
            binding.btProses.setOnClickListener {
                sendRequestUpdateProductMerchant()
            }
        }

    }
    //setup Toolbar
    private fun setupToolbar() {
        supportActionBar?.apply {
            this.title = "Ubah Barang"
            this.setDisplayHomeAsUpEnabled(true)
            this.setDisplayShowHomeEnabled(true)
        }
    }
    //Handler Toolbar Menu
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun getDataProduct(){
        viewModelKelolaBarang.getDataDetailProduct(auth.token!!, id!!)
        viewModelKelolaBarang.detailProduct.observe(this,  Observer{
            it?.apply {
                binding.etNamabarang.setText(productName)
                binding.etQty.setText(qty)
                binding.etHargaJual.setText(priceSelling)
                binding.etHargaModal.setText(priceDefault)
                binding.etDeskripsi.setText(description)
                binding.etDiskon.setText(discount)
                info?.apply {
                    binding.etLength.setText(length)
                    binding.etWidth.setText(width)
                    binding.etHeight.setText(height)
                    binding.etWeight.setText(weight)
                }

            }

        })
    }

    private fun sendRequestUpdateProductMerchant(){
        val name = binding.etNamabarang.text.toString()
        val slug = name.replace(" ", "-").toLowerCase(Locale.getDefault())
        val jumlah = binding.etQty.text.toString()
        val hargaJual = binding.etHargaJual.text.toString()
        val hargaBeli = binding.etHargaModal.text.toString()
        val description = binding.etDeskripsi.text.toString()
        val diskon = binding.etDiskon.text.toString()
        val length = binding.etLength.text.toString()
        val width = binding.etWidth.text.toString()
        val height = binding.etHeight.text.toString()
        val weight = binding.etWeight.text.toString()

        if (name.isEmpty() || jumlah.isEmpty() || hargaJual.isEmpty() ||
            hargaBeli.isEmpty() || description.isEmpty() || diskon.isEmpty()
        ) {
            Toast.makeText(this, "Harap isi semua form!", Toast.LENGTH_SHORT).show()
        }else{
            val jsonObject = JsonObject()
            jsonObject.apply {
                addProperty("product_name", name)
                addProperty("slug", slug)
                addProperty("description", description)
                addProperty("harga_beli", hargaBeli)
                addProperty("harga_jual", hargaJual)
                addProperty("jumlah", jumlah)
                addProperty("diskon", diskon)
            }
            editDataProductMerchant(jsonObject,{
                if(it.code == 200){
                    sendUpdateDimension(length, width, height, weight)
                }
            },{
                Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            })
        }
    }

    private fun editDataProductMerchant(
        jsonObject: JsonObject, onSuccess: (ResponseUpdateProduct) -> Unit,
        onFailed: (String) -> Unit
    ){
        val apiService : ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }
        val call : Call<ResponseUpdateProduct> = apiService.updateProductMerchant(auth.token!!, jsonObject, id!!)
        call.enqueue(object : Callback<ResponseUpdateProduct> {
            override fun onResponse(
                call: Call<ResponseUpdateProduct>,
                response: Response<ResponseUpdateProduct>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateProduct>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun editDataDimensionProduct(
        jsonObject: JsonObject,
        onSuccess: (ResponseUpdateProductDimension) -> Unit,
        onFailed: (String) -> Unit
    ){
        val apiService : ManageProductService by lazy {
            ApiService().createService(ManageProductService::class.java)
        }

        val call : Call<ResponseUpdateProductDimension> = apiService.updateProductDimension(auth.token!!, id!!, jsonObject)
        call.enqueue(object : Callback<ResponseUpdateProductDimension>{
            override fun onResponse(
                call: Call<ResponseUpdateProductDimension>,
                response: Response<ResponseUpdateProductDimension>
            ) {
                if(response.isSuccessful){
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                }
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseUpdateProductDimension>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendUpdateDimension(length : String, width : String, height : String, weight : String){
        val jsonObject = JsonObject()
        jsonObject.apply {
            addProperty("length", length)
            addProperty("width", width)
            addProperty("height", height)
            addProperty("weight", weight)
        }
        editDataDimensionProduct(jsonObject,{
            if(it.code == 200){
                Toast.makeText(this, "Berhasil Update Data", Toast.LENGTH_SHORT).show()
                finish()
            }
        }, {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }
}