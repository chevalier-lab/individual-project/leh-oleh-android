package id.co.leholeh.mobile.customer.ui.profile

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.co.leholeh.mobile.customer.model.profile.Profile
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.utils.ApiStatus
import kotlinx.coroutines.launch

class ProfileViewModel : ViewModel(){

    private val _profile = MutableLiveData<Profile>()
    val profile : LiveData<Profile>
            get() = _profile

    private val _status = MutableLiveData<ApiStatus>()
    val status : LiveData<ApiStatus>
        get() = _status

    private suspend fun getProfileUser(token : String){
        try {
            _status.postValue(ApiStatus.LOADING)
            val apiService = ApiService().serviceProfileUser
            _profile.postValue(apiService.getProfile(token).data)
            _status.postValue(ApiStatus.SUCCESS)
        }catch (e : Exception){
            Log.d("REQ_PROF_USR_FAIL", e.localizedMessage!!)
            _status.postValue(ApiStatus.FAILED)
        }
    }

    fun getDataProfileUser(token : String){
        viewModelScope.launch {
            getProfileUser(token)
        }
    }

}