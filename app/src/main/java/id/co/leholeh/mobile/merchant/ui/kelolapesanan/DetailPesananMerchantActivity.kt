package id.co.leholeh.mobile.merchant.ui.kelolapesanan

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import id.co.leholeh.R
import id.co.leholeh.mobile.customer.ConstantAuth
import id.co.leholeh.mobile.customer.model.auth.Login
import id.co.leholeh.mobile.merchant.constant.ConstantMerchant
import id.co.leholeh.mobile.utils.CacheUtil
import id.co.leholeh.mobile.utils.changeToRupiah
import id.co.leholeh.mobile.utils.getAuth
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.gson.JsonObject
import id.co.leholeh.databinding.ActivityDetailTransaksiMerchantBinding
import id.co.leholeh.databinding.CustomDialogShipperAgentBinding
import id.co.leholeh.mobile.customer.constant.API_KEY_SHIPPER
import id.co.leholeh.mobile.customer.model.shipper.pickuprequest.DataItemGetAgent
import id.co.leholeh.mobile.customer.network.ApiService
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.transaction.DetailProductsItem
import id.co.leholeh.mobile.merchant.model.transaction.ResponseOrderActivation
import id.co.leholeh.mobile.merchant.model.transaction.ResponsePickupRequest
import id.co.leholeh.mobile.merchant.network.TransactionService
import id.co.leholeh.mobile.merchant.ui.profile.ProfileTokoViewModel
import kotlinx.android.synthetic.main.activity_detail_transaksi_merchant.*
import kotlinx.android.synthetic.main.item_produk_detail_pesanan.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

class DetailPesananMerchantActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailTransaksiMerchantBinding
    private lateinit var id: String
    private lateinit var adapterTransaction : AdapterUtil<DetailProductsItem>
    private lateinit var orderId: String
    private lateinit var subUrbId : String
    private lateinit var agentId : String
    private lateinit var agentName : String
    //private lateinit var listLocation:ProductLocation
    private val viewModel: TransactionViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(TransactionViewModel::class.java)
    }

    private val viewModelProfileToko: ProfileTokoViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProfileTokoViewModel::class.java)
    }
    private lateinit var cacheUtil:CacheUtil
    private lateinit var auth:Login


    @SuppressLint("SetTextI18n")
    @ExperimentalStdlibApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTransaksiMerchantBinding.inflate(layoutInflater)
        //Cache Util
        cacheUtil = CacheUtil()
        cacheUtil.start(this,ConstantAuth.PREFERENCES)
        //Auth
        auth= getAuth(cacheUtil)

        //Id
        id = intent.getSerializableExtra(ConstantMerchant.ID_PESANAN)!! as String
        setContentView(binding.root)
        getDataProfileToko()
        loadDataDetailTransaction()
        setUpToolbar()
    }

    @SuppressLint("SetTextI18n", "SimpleDateFormat")
    private fun loadDataDetailTransaction() {
        viewModel.getDetailTransaction(auth.token!!,id)
        viewModel.detailTransaction.observe(this, Observer {
            if(it != null){
                binding.textViewNamaPemesan.text = it.fullName
                val orderDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale("ID")).parse(it.createdAt!!)
                val formatedOrderDate = SimpleDateFormat("dd MMMM yyyy HH:mm:ss", Locale("ID")).format(orderDate!!)
                binding.textViewTglPesanan.text = formatedOrderDate

                it.products?.let {products ->
                    if(it.products.isNotEmpty()){
                        orderId = products[0].shipperOrderId!!
                        checkShipperStatus(orderId, products[0].shipperOrderActive!!, products[0].shipperIsRequestPickup!!)
                        binding.textViewJumlahPesan.text = products.size.toString()
                        adapterTransaction.data = it.products
                    }
                }
                binding.textViewTotalHarga.text = changeToRupiah(it.totalPrice!!.toDouble())
                checkStatusPesanan(it.status!!)
            }
        })

        adapterTransaction = AdapterUtil(R.layout.item_produk_detail_pesanan, arrayListOf(),
            { _, itemView, item ->
                itemView.textViewNamaProdukDetailPesanan.text = item.productName
                itemView.textViewHargaProdDetailPesanan.text = changeToRupiah(item.subTotalPrice!!.toDouble())
                itemView.textViewTotalProdDetailPesanan.text = "x${item.qty}"
                itemView.textViewStatusDetailPesanan.text = checkStatusProduk(item.status!!)
                Glide.with(this).load(item.uri).into(itemView.imageViewProdukDetailPesanan)
            },{ _,item ->
                val intent = Intent(this, DetailProdukPesananMerchantActivity::class.java)
                intent.putExtra(ConstantMerchant.DATA_PRODUK, item)
                startActivity(intent)
            })
        binding.recyclerViewListPesanan.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewListPesanan.adapter = adapterTransaction
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return true
    }

    @SuppressLint("SetTextI18n")
    private fun checkShipperStatus(orderId: String, shipperOrderStatus: String, shipperPickupStatus : String){
        val statusPesanan = intent.getStringExtra(ConstantMerchant.STATUS_PESANAN)
        if(shipperOrderStatus == "0"){
            buttonAction.text = "Aktifkan ongkir"
            buttonAction.setOnClickListener {
                sendRequestOrderActivation(orderId)
            }
            if (statusPesanan == ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED ||
                statusPesanan == ConstantMerchant.STATUS_PESANAN_FINISH ||
                statusPesanan == ConstantMerchant.STATUS_PESANAN_REFUND) {
                binding.buttonAction.visibility = View.GONE
            }else{
                binding.buttonAction.visibility = View.VISIBLE
            }
            binding.textViewStatusOngkir.text = "Belum aktif"
            binding.textViewStatusPengantaran.text = "Belum diproses"
        }else if(shipperOrderStatus == "1" && shipperPickupStatus == "0"){
            buttonAction.text = "Proses pengantaran"
            buttonAction.setOnClickListener {
                showDialogListAgent()
            }
            binding.textViewStatusOngkir.text = "Aktif"
            binding.textViewStatusPengantaran.text = "Belum aktif"
        }else if(shipperOrderStatus == "1" && shipperPickupStatus == "1"){
            binding.textViewStatusPengantaran.text = "Aktif"
            binding.textViewStatusOngkir.text = "Aktif"
            binding.buttonAction.visibility = View.GONE
        }
    }

    private fun checkStatusProduk(statusPesananProduk : String) : String{
        when (statusPesananProduk) {
            ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED -> {
                return getString(R.string.payment_verified)
            }
            ConstantMerchant.STATUS_PESANAN_PROCESS -> {
                return getString(R.string.process)
            }
            ConstantMerchant.STATUS_PESANAN_FINISH -> {
                return getString(R.string.finish)
            }
            ConstantMerchant.STATUS_PESANAN_REFUND -> {
                return getString(R.string.refund)
            }
        }
        return ""
    }

    @SuppressLint("SetTextI18n")
    private fun checkStatusPesanan(status : String): String {
        when (status) {
            ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED -> {
                binding.textViewStatusPesanan.text = getString(R.string.payment_verified)
                binding.constraintStatusOngkirPengantaran.visibility = View.GONE
                return ConstantMerchant.STATUS_PESANAN_PAYMENT_VERIFIED
            }
            ConstantMerchant.STATUS_PESANAN_PROCESS -> {
                binding.textViewStatusPesanan.text = getString(R.string.process)
                if(intent.getBooleanExtra(ConstantMerchant.VERIFIED_PESANAN, false)){
                    finish()
                }
                return ConstantMerchant.STATUS_PESANAN_PROCESS
            }
            ConstantMerchant.STATUS_PESANAN_FINISH -> {
                binding.textViewStatusPesanan.text = getString(R.string.finish)
                binding.constraintStatusOngkirPengantaran.visibility = View.GONE
                return ConstantMerchant.STATUS_PESANAN_FINISH
            }
            ConstantMerchant.STATUS_PESANAN_REFUND -> {
                binding.textViewStatusPesanan.text = getString(R.string.refund)
                binding.constraintStatusOngkirPengantaran.visibility = View.GONE
                return ConstantMerchant.STATUS_PESANAN_REFUND
            }
        }
        return ""
    }

    private fun getDataProfileToko() {
        viewModelProfileToko.getDataProfileToko(auth.token!!)
        viewModelProfileToko.toko.observe(this, Observer { profileToko ->
            profileToko.location?.apply {
                subUrbId = profileToko.location.suburbId!!
            }
        })
    }


    private fun hideShowDataAgent(isVisible : Boolean, binding : CustomDialogShipperAgentBinding){
        if(isVisible){
            binding.textViewNamaAgen.visibility = View.VISIBLE
            binding.textViewKotaAgen.visibility = View.VISIBLE
            binding.textViewNamaPJ.visibility =  View.VISIBLE
            binding.textViewNoPJ.visibility =  View.VISIBLE
            binding.tvLabelKota.visibility =  View.VISIBLE
            binding.tvLabelNamaAgen.visibility =  View.VISIBLE
            binding.tvLabelNamaPJ.visibility =  View.VISIBLE
            binding.tvLabelNoPJ.visibility =  View.VISIBLE
        }else{
            binding.textViewNamaAgen.visibility = View.GONE
            binding.textViewKotaAgen.visibility = View.GONE
            binding.textViewNamaPJ.visibility =  View.GONE
            binding.textViewNoPJ.visibility =  View.GONE
            binding.tvLabelKota.visibility =  View.GONE
            binding.tvLabelNamaAgen.visibility =  View.GONE
            binding.tvLabelNamaPJ.visibility =  View.GONE
            binding.tvLabelNoPJ.visibility =  View.GONE
        }
    }

    private fun showDialogListAgent(){
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val layoutInflater = LayoutInflater.from(this)
        val binding = CustomDialogShipperAgentBinding.inflate(layoutInflater)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)

        var isFormFilled = false
        val listShipperAgent = arrayListOf<DataItemGetAgent>()
        val listNameShipperAgent = arrayListOf<String>()

        viewModel.getDataListShipperAgent(API_KEY_SHIPPER, subUrbId)
        viewModel.listDataAgent.observe(this, Observer {
            if(it != null){
                for (i in it.indices){
                    listShipperAgent.add(it[i])
                    listNameShipperAgent.add(it[i].agent?.name!!)
                }
                Log.d("LIST_AGEN", "$API_KEY_SHIPPER, $subUrbId, $it")
            }
        })

        val adapterShipperAgent = ArrayAdapter(applicationContext, R.layout.item_list_dropdown_text, listNameShipperAgent)
        binding.dropdownShipperAgent.setAdapter(adapterShipperAgent)
        binding.dropdownShipperAgent.addTextChangedListener(object : TextWatcher{
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                for (i in listShipperAgent.indices){
                    if(binding.dropdownShipperAgent.text.toString() == listShipperAgent[i].agent?.name){
                        agentName = listShipperAgent[i].agent?.name!!
                        agentId = listShipperAgent[i].agent?.id.toString()
                        binding.textViewNamaAgen.text = agentName
                        binding.textViewKotaAgen.text = listShipperAgent[i].city?.name
                        binding.textViewNamaPJ.text = listShipperAgent[i].contact?.name
                        binding.textViewNoPJ.text = listShipperAgent[i].contact?.phone
                        isFormFilled = true
                        hideShowDataAgent(true, binding)
                        break
                    }
                }
            }

        })
        binding.tvPositiveAction.setOnClickListener {
            if(isFormFilled){
                sendPickupRequest(dialog)
            }else{
                Toast.makeText(this, "Pilih agen dengan benar", Toast.LENGTH_SHORT).show()
            }

        }
        binding.tvNegativeAction.setOnClickListener {
            dialog.dismiss()
        }
        hideShowDataAgent(false, binding)
        dialog.show()
    }


    private fun requestOrderActivation(
        jsonObject: JsonObject,
        onSuccess: (ResponseOrderActivation) -> Unit,
        onFailed: (String) -> Unit
    ) {
        val serviceTransaction : TransactionService by lazy {
            ApiService().createService(TransactionService::class.java)
        }
        val call = serviceTransaction.sendOrderActivation(auth.token!!, jsonObject)
        call.enqueue(object : Callback<ResponseOrderActivation> {
            override fun onResponse(
                call: Call<ResponseOrderActivation>,
                response: Response<ResponseOrderActivation>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponseOrderActivation>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendRequestOrderActivation(orderId : String){
        val jsonObject = JsonObject()
        jsonObject.addProperty("id_order", orderId)
        requestOrderActivation(jsonObject,{
            if(it.code == 200){
                Toast.makeText(this, "Status ongkir diaktifkan", Toast.LENGTH_SHORT).show()
                viewModel.getDetailTransaction(auth.token!!, id)
            }
        },{
            Toast.makeText(this, it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun pickupRequest(
        jsonObject: JsonObject,
        onSuccess: (ResponsePickupRequest) -> Unit,
        onFailed: (String) -> Unit
    ){
        val serviceTransaction : TransactionService by lazy {
            ApiService().createService(TransactionService::class.java)
        }
        val call = serviceTransaction.sendPickupRequest(auth.token!!,jsonObject)
        call.enqueue(object : Callback<ResponsePickupRequest>{
            override fun onResponse(
                call: Call<ResponsePickupRequest>,
                response: Response<ResponsePickupRequest>
            ) {
                if (response.isSuccessful)
                    if (response.body() != null) onSuccess(response.body()!!)
                    else onFailed(response.message().toString())
                else onFailed(response.message().toString())
            }

            override fun onFailure(call: Call<ResponsePickupRequest>, t: Throwable) {
                onFailed(t.message.toString())
            }

        })
    }

    private fun sendPickupRequest(dialog: Dialog){
        val jsonObject = JsonObject()
        jsonObject.apply {
            addProperty("idOrders", orderId)
            addProperty("agent_id", agentId)
            addProperty("agent_name", agentName)
        }
        pickupRequest(jsonObject,{
            if(it.code == 200){
                Toast.makeText(this, "Pengantaran diproses", Toast.LENGTH_SHORT).show()
                viewModel.getDetailTransaction(auth.token!!, id)
                dialog.dismiss()
            }
        },{

        })
    }

    private fun setUpToolbar(){
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    override fun onResume() {
        if(!auth.equals(null)){
            viewModel.getDetailTransaction(auth.token!!, id)
        }
        super.onResume()
    }
}
