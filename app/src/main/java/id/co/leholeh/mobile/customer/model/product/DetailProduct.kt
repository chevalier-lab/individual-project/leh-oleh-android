package id.co.leholeh.mobile.customer.model.product

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DetailProduct(

    @field:SerializedName("is_visible")
    val isVisible: String? = null,

    @field:SerializedName("price_selling")
    val priceSelling: String? = null,

    @field:SerializedName("id_merchant")
    val idMerchant: String? = null,

    @field:SerializedName("rating")
    val rating: Any? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("discount")
    val discount: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("product_name")
    val productName: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null,

    @field:SerializedName("photos")
    val photos: List<PhotosItem>? = null,

    @field:SerializedName("price_default")
    val priceDefault: String? = null,

    @field:SerializedName("market_name")
    val marketName: String? = null,

    @field:SerializedName("id_u_product")
    val idUProduct: String? = null,

    @field:SerializedName("qty")
    val qty: String? = null,

    @field:SerializedName("location")
    val location: List<DetailLocation>? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("categories")
    val categories: List<DetailCategories>? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("info")
    val info: Info? = null
) : Serializable

data class Info(

    @field:SerializedName("id_u_product")
    val idUProduct: String? = null,

    @field:SerializedName("length")
    val length: String? = null,

    @field:SerializedName("width")
    val width: String? = null,

    @field:SerializedName("weight")
    val weight: String? = null,

    @field:SerializedName("height")
    val height: String? = null
)

data class PhotosItem(

    @field:SerializedName("id_m_medias")
    val idMMedias: String? = null,

    @field:SerializedName("id_u_product_photos")
    val idUProductPhotos: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null
)

