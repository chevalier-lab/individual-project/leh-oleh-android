package id.co.leholeh.mobile.customer.model.shipper.location

import com.google.gson.annotations.SerializedName

data class ResponseAreas(

	@field:SerializedName("data")
	val data: DataAreas? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class DataAreas(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("rows")
	val rows: List<RowsItemAreas>? = null,

	@field:SerializedName("content")
	val content: String? = null
)

data class RowsItemAreas(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("postcode")
	val postcode: String? = null,

	@field:SerializedName("alias")
	val alias: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
