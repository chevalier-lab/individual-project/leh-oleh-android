package id.co.leholeh.mobile.customer.network

import id.co.leholeh.mobile.customer.constant.BASE_URL_NOTIFICATIONS
import id.co.leholeh.mobile.customer.model.notification.ResponseNotifications
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface NotificationsService {
    @GET(BASE_URL_NOTIFICATIONS)
    suspend fun getListNotifications(
        @Header("Authorization") authorization: String,
        @Query("page") page: Int?,
        @Query("order-by") orderBy: String?,
        @Query("order-direction") orderDirection: String?,
        @Query("is-read") isRead: String?
    ): Response<ResponseNotifications>
}