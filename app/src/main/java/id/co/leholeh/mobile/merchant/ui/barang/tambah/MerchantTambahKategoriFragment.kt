package id.co.leholeh.mobile.merchant.ui.barang.tambah

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.leholeh.R
import id.co.leholeh.databinding.FragmentTambahKategoriBinding
import id.co.leholeh.mobile.customer.model.category.Category
import id.co.leholeh.mobile.customer.ui.produk.ProductListViewModel
import id.co.leholeh.mobile.customer.util.AdapterUtil
import id.co.leholeh.mobile.merchant.model.barang.BarangTemporary
import kotlinx.android.synthetic.main.item_list_category.view.*


class MerchantTambahKategoriFragment : Fragment() {

    private lateinit var binding : FragmentTambahKategoriBinding
    private val viewModelProductList: ProductListViewModel by lazy {
        ViewModelProvider(
            this,
            ViewModelProvider.NewInstanceFactory()
        ).get(ProductListViewModel::class.java)
    }

    private var categoriesId = arrayListOf<String>()
    private var categoriesString: ArrayList<String> = arrayListOf()
    private lateinit var adapterTextCategory: AdapterUtil<String>
    private val items: ArrayList<Category> = arrayListOf()
    private val itemsCategory: ArrayList<String> = arrayListOf()

    private var barangTemporary = BarangTemporary()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentTambahKategoriBinding.inflate(inflater, container, false)
        return binding.root
    }

    fun parsingInformasiBarang(barangTemp : BarangTemporary) {
        barangTemporary = barangTemp
    }

    override fun onResume() {
        super.onResume()
        initDataCategories()
    }

    fun getDataBarang() : BarangTemporary {
        barangTemporary.kategoriId = categoriesId
        barangTemporary.kategoriString = categoriesString
        if(barangTemporary.kategoriString != null){
            categoriesString = barangTemporary.kategoriString!!
        }
        return barangTemporary
    }

    fun isCategoryNotNull() : Boolean {
        return if(categoriesId.size > 0){
            true
        }else{
            Toast.makeText(requireContext(), "Kategori minimal 1", Toast.LENGTH_SHORT).show()
            false
        }
    }

    private fun initDataCategories(){
        viewModelProductList.getCategories()
        viewModelProductList.categories.observe(viewLifecycleOwner, Observer {
            items.clear()
            itemsCategory.clear()
            for (item in it.data?.indices!!) {
                items.add(it.data[item])
                itemsCategory.add(it.data[item].category!!)
            }
        })
        Log.d("itemcategories", itemsCategory.toString())
        adapterTextCategory = AdapterUtil(R.layout.item_list_category, arrayListOf(),
            { _, itemView, item ->
                itemView.imageViewDeleteCategories.setOnClickListener {

                }
                itemView.tv_makanan_filter.text = item
            }, { position, item ->
                categoriesString.remove(item)
                categoriesId.removeAt(position)
                adapterTextCategory.refresh()
            })

        val adapterCategory =
            ArrayAdapter(requireContext(), R.layout.item_list_dropdown_text, itemsCategory)
        binding.dropdownMakanan.setAdapter(adapterCategory)
        binding.dropdownMakanan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                for (i in items.indices) {
                    if (binding.dropdownMakanan.text.toString() == items[i].category) {
                        var kondisi = true
                        if (categoriesString.size > 0) {
                            for (j in categoriesString.indices) {
                                if (binding.dropdownMakanan.text.toString() == categoriesString[j]) {
                                    kondisi = false
                                }
                            }
                            if (kondisi) {
                                categoriesString.add(items[i].category.toString())
                                categoriesId.add(items[i].id!!)
                            }
                        } else {
                            categoriesString.add(items[i].category!!)
                            categoriesId.add(items[i].id!!)
                        }
                        Log.d("CATEGORIESSTRING", categoriesString.toString())
                        if (categoriesString.size > 0) adapterTextCategory.data = categoriesString
                        break
                    }
                }
                Log.d("KATEGORI", "KATEGORI value $categoriesString categories $categoriesId")
            }
        })
        binding.rvMakananFilter.layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.HORIZONTAL,
            false
        )
        binding.rvMakananFilter.adapter = adapterTextCategory
        if (categoriesString.size > 0) adapterTextCategory.data = categoriesString
    }
}