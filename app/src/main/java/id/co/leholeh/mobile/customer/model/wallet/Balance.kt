package id.co.leholeh.mobile.customer.model.wallet

import com.google.gson.annotations.SerializedName

data class Balance(

    @field:SerializedName("id_m_wallets")
    val idMWallets: String? = null,

    @field:SerializedName("is_visible")
    val isVisible: String? = null,

    @field:SerializedName("full_name")
    val fullName: String? = null,

    @field:SerializedName("balance")
    val balance: String? = null,

    @field:SerializedName("updated_at")
    val updatedAt: String? = null,

    @field:SerializedName("id_u_user")
    val idUUser: String? = null,

    @field:SerializedName("id_m_users")
    val idMUsers: String? = null,

    @field:SerializedName("wallet_name")
    val walletName: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("created_at")
    val createdAt: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("label")
    val label: String? = null,

    @field:SerializedName("uri")
    val uri: String? = null
)