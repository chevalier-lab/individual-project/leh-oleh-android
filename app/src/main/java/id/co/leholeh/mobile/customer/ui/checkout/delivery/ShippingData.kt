package id.co.leholeh.mobile.customer.ui.checkout.delivery

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ShippingData(
    var EXTRA_ORIGIN: Int,
    var EXTRA_DEST: Int,
    var EXTRA_LENGTH: Float,
    var EXTRA_WIDTH: Float,
    var EXTRA_HEIGHT: Float,
    var EXTRA_WEIGHT: Float,
    var EXTRA_TOTAL: Int,
    var position: Int
) : Parcelable

@Parcelize
data class ShippingList(
    var listOfShipperPrice: ArrayList<Double>,
    var listOfShipperName: ArrayList<String>,
    var listOfShipperRatesID: ArrayList<Int>
) : Parcelable