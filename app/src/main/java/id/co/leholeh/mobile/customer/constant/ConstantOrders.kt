package id.co.leholeh.mobile.customer.constant

object ConstantOrders {
    const val BASE_URL_ORDER_ACTIVE = "guest/order"
    const val BASE_URL_ORDER_HISTORY = "guest/order/history"
    const val BASE_URL_DETAIL_ORDER = "guest/order/{id}"
    const val BASE_URL_REVIEW = "guest/order/review"
    const val BASE_URL_KOMPLAIN = "guest/order/complain"
    const val BASE_URL_GET_STATUS_MIDTRANS = "v2/{token}/status"
    const val BASE_URL_UPDATE_PAYMENT_TOKEN = "guest/order/{id}/payment-token"
}