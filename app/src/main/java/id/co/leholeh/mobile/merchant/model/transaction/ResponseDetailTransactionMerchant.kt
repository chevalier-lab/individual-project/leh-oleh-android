package id.co.leholeh.mobile.merchant.model.transaction

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseDetailTransactionMerchant(

	@field:SerializedName("code")
	val code: Int? = null,

	@field:SerializedName("data")
	val data: DetailTransactionMerchant? = null,

	@field:SerializedName("error")
	val error: List<Any>? = null,

	@field:SerializedName("message")
	val message: String? = null
)

data class DetailProductsItem(

	@field:SerializedName("shipper_agent_id")
	val shipperAgentId: Any? = null,

	@field:SerializedName("sub_total_price")
	val subTotalPrice: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("id_m_products")
	val idMProducts: String? = null,

	@field:SerializedName("product_name")
	val productName: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("shipper_agent_name")
	val shipperAgentName: Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("qty")
	val qty: String? = null,

	@field:SerializedName("shipper_order_id")
	val shipperOrderId: String? = null,

	@field:SerializedName("location")
	val location: List<LocationItemTrans>? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("categories")
	val categories: List<CategoriesItemDetailTrans>? = null,

	@field:SerializedName("id_cover")
	val idCover: String? = null,

	@field:SerializedName("shipper_is_request_pickup")
	val shipperIsRequestPickup: String? = null,

	@field:SerializedName("id_u_user_transaction")
	val idUUserTransaction: String? = null,

	@field:SerializedName("slug")
	val slug: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("shipper_order_active")
	val shipperOrderActive: String? = null
): Serializable

data class DetailTransactionMerchant(

	@field:SerializedName("full_name")
	val fullName: String? = null,

	@field:SerializedName("total_price")
	val totalPrice: String? = null,

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("id_m_users")
	val idMUsers: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("transaction_token")
	val transactionToken: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("products")
	val products: List<DetailProductsItem>? = null
):Serializable

data class CategoriesItemDetailTrans(

	@field:SerializedName("category")
	val category: String? = null
):Serializable

data class LocationItemTrans(

	@field:SerializedName("label")
	val label: String? = null,

	@field:SerializedName("uri")
	val uri: String? = null,

	@field:SerializedName("province_name")
	val provinceName: String? = null
):Serializable
