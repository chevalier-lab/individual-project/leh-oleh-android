package id.co.leholeh.mobile.customer.constant.shipper

object ConstantOrder {
    const val BASE_URL_DOMESTIC_ORDER = "public/v1/orders/domestics"
    const val BASE_URL_ORDER_ACTIVATIONS = "public/v1/activations/{id}"
    const val BASE_URL_ORDER_DETAIL = "public/v1/orders/{id}"
    const val BASE_URL_TRACKING_ID = "public/v1/orders"
    const val BASE_URL_LIST_TRACK_STAT = "public/v1/logistics/status"
    const val BASE_URL_CANCEL_ORDER = "public/v1/orders/{id}/cancel"
    const val BASE_URL_UPDATE_ORDER_WEIGHT = "public/v1/orders/{id}"
}