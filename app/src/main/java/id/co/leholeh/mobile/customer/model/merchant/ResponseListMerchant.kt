package id.co.leholeh.mobile.customer.model.merchant

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ResponseListMerchant(

    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: List<Merchants>? = null,

    @field:SerializedName("error")
    val error: List<Any?>? = null,

    @field:SerializedName("message")
    val message: String? = null
) : Serializable