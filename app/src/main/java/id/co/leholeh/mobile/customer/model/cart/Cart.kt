package id.co.leholeh.mobile.customer.model.cart

import com.google.gson.annotations.SerializedName

data class Cart(

    @field:SerializedName("total")
    val total: Int? = null,

    @field:SerializedName("carts")
    val carts: List<CartListtem>? = null
)