package id.co.leholeh.mobile.merchant.adapter

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.co.leholeh.R
import id.co.leholeh.mobile.merchant.ui.dashboard.HistoriTransaksiMerchantFragment
import id.co.leholeh.mobile.merchant.ui.dashboard.TotalTransaksiMerchantFragment

class SectionsPagerAdapterDashboard(private val context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    @StringRes
    private val tabTitles = intArrayOf(R.string.total_transaksi, R.string.histori_transaksi)

    override fun getItem(position: Int): Fragment {
        var fragment : Fragment? = null
        when(position){
            0-> fragment =
                TotalTransaksiMerchantFragment()
            1-> fragment =
                HistoriTransaksiMerchantFragment()
        }
        return fragment as Fragment
    }

    override fun getCount(): Int {
        return tabTitles.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(tabTitles[position])
    }

}