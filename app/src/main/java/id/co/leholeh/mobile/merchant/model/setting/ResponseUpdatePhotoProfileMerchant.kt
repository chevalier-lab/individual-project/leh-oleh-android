package id.co.leholeh.mobile.merchant.model.setting

import com.google.gson.annotations.SerializedName

data class ResponseUpdatePhotoProfileMerchant(
    @field:SerializedName("code")
    val code: Int? = null,

    @field:SerializedName("data")
    val data: UpdatePhotoProfilMerchant? = null,

    @field:SerializedName("error")
    val error: List<Any?>? = null,

    @field:SerializedName("message")
    val message: String? = null
)